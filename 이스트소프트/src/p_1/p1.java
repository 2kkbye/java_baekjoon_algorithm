package p_1;

import java.util.Scanner;
public class p1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		for(int i=0;i<3;i++) {
			int N = sc.nextInt();
			int arr[]= new int[N];
			for(int a=0;a<arr.length;a++) {
				arr[a] = sc.nextInt();
			}
			int result = solution(arr);
			System.out.println("#"+(i+1)+" "+result);
		}
	}
	public static int solution(int[] ranks) {
		int cnt=0;
		for(int a=0;a<ranks.length;a++) {
			for(int b=0;b<ranks.length;b++) {
				if((ranks[a]+1)==ranks[b]) {
					cnt++;
					break;
				}
			}
		}
		
		return cnt;
	}
}
