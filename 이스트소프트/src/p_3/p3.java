package p_3;

import java.util.Scanner;
public class p3 {

	static int f_x;
	static int f_y;
	static String ar[][];
	static int cnt;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		for(int i=0;i<1;i++) {
			int N = sc.nextInt();
			String arr[] = new String[N];
			for(int a=0;a<arr.length;a++) {
				arr[a] = sc.next();
			}
			
			int result = solution(arr);
			System.out.println(result);
		}
	}
	public static int solution(String[] B) {
		find_first(B);
		ar = new String[B.length][1];
		change(B);
		cnt=0;
		search(f_x,f_y);
		
		return cnt;
	}
	public static void search(int x,int y) {
		if(((x-2)>=0)&&((y-2)>=0)&&ar[x-1][0].charAt(y-1)=='X'&&(ar[x-2][0].charAt(y-2)!='X')) {
			cnt++;
			search(x-2,y-2);
		}
		if(((x-2)>=0)&&((y+2)<=ar.length-1)&&ar[x-1][0].charAt(y+1)=='X'&&(ar[x-2][0].charAt(y+2)!='X')) {
			cnt++;
			search(x-2,y+2);
		}
	}
	public static void change(String[] B) {
		for(int a=0;a<B.length;a++) {
			ar[a][0]=B[a];
		}
	}
	public static void find_first(String arr[]) {
		int sw=0;
		for(int a=0;a<arr.length;a++) {
			for(int b=0;b<arr[a].length();b++) {
				if(arr[a].charAt(b)=='O') {
					f_x=a;
					f_y=b;
					sw=1;
					break;
				}
			}
			if(sw==1) break;
		}
	}

}
