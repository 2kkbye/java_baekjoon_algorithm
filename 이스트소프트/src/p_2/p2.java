package p_2;

import java.util.Scanner;
public class p2 {
	static boolean bol[];
	static int cnt;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		for(int i=0;i<1;i++) {
			int N = sc.nextInt();
			int arr[]=new int[N];
			for(int a=0;a<arr.length;a++) {
				arr[a]= sc.nextInt();
			}
			
			int result = solution(arr);
			System.out.println("#"+result);
		}
	}
	public static int solution(int [] A) {
		bol= new boolean[A.length];
		for(int a=0;a<bol.length;a++) {
			bol[a]=false;
		}
		int result=-1;
		for(int a=0;a<A.length;a++) {
			if(bol[a]==false){
				cnt=0;
				cnt++;
				dfs(A,a);
			}
			//System.out.println(cnt);
			if(cnt>result) {
				result=cnt;
			}
		}
		return result;
	}
	public static void dfs(int[] A,int a) {
		bol[a]=true;
		if(bol[A[a]]==false) {
			cnt++;
			dfs(A,A[a]);
		}
		bol[a]=false;
	}
}
