package 실습_19년도_6주차;

import java.util.Scanner;


interface Stack {
	boolean isEmpty();
	void push(char item);
	char pop();
	void delete();
	char peek();
}

class ArrayStack implements Stack {
	private int top;
	private int stackSize;
	private char itemArray[];
	
	public ArrayStack(int stackSize) {
		top=-1;
		this.stackSize=stackSize;
		itemArray=new char[this.stackSize];
	}
	
	public boolean isEmpty() {
		return (top==-1);
	}
	public boolean isFull() {
		return (top==this.stackSize-1);
	}
	public void push(char item) {
		if(isFull()) {
			System.out.println("inserting fail! Array is full");
		}
		else {
			itemArray[++top] =item;
			System.out.println("inserted item : " +item);
		}
	}
	public char pop() {
		if(isEmpty()) {
			System.out.println("Deleting fail! Array is empty!");
			return 0;
		}
		else {
			return itemArray[top--];
		}
	}
	public void delete() {
		if(isEmpty()) {
			System.out.println("deleting fail! Array Stack is empty");
		}
		else {
			top--;
		}
	}
	public char peek() {
		if(isEmpty()) {
			System.out.println("peeking fail! Array is empty");
			return 0;
		}
		else
			return itemArray[top];
	}
}

public class main {

	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		String str;
		
		while(true) {
			str=sc.next();
			if(str.charAt(0)=='q'&&str.length()<2) {
				break;
			}
			else {
				int stackSize=str.length();
				hwamun(stackSize,str);

			}
		}
	}
	public static void hwamun(int stackSize,String str) {
		ArrayStack S= new ArrayStack(stackSize);
		int tmp=0;//참조변수이다.
		if(stackSize%2==0){ //짝수인경우
			for(int i=0;i<stackSize/2;i++) {
				S.push(str.charAt(i));
				if(str.charAt(i)==str.charAt(str.length()-1-i)) {
					tmp=tmp+1;
				}
				
			}

		}             
		else { //홀수인경우
			for(int i=0;i<stackSize/2;i++) {
				S.push(str.charAt(i));
				if(str.charAt(i)==str.charAt(str.length()-1-i)) {
					tmp=tmp+1;
				}
			}
		}
		if(tmp==str.length()/2) {
			System.out.println("화문입니다");
		}
		else {
			System.out.println("화문이아닙니다");
		}
	}
}
