package 다항식의덧셈;

public class OperatePoly {
	private int degree_A=0, degree_B=0, degree_C=0,index_A=0,index_B=0,index_C=0;
	private int expo_A, expo_B;
	private float coef_A, coef_B, coef_C;

	public Polynomial addPoly(Polynomial A, Polynomial B) { //생성자 이 객체를 메인문에서 만들면 바로 실행이 된다. 
		degree_A = A.getDegree(); //다항식 A의 차수를 알아내려함
		degree_B = B.getDegree(); //다항식 B의 차수를 알아내려함
		expo_A=degree_A; //각각의 차수를 비교위해
		expo_B=degree_B; //각각의 차수를 비교위해 expo에다가 각각 저장

		if(degree_A>=degree_B) { //차수를 비교하여 더 큰값을 새로운 차수 C에 저장, 같은경우도 A를 이용
			degree_C = degree_A; //더큰 차수를 기준점으로 새로운 다항식 C에 넣는다.
		}
		else {
			degree_C = degree_B;
		}
		Polynomial C = new Polynomial(degree_C); //c를 위한 coef배열을 초기화.
		while(index_A<= degree_A&& index_B<= degree_B) {
			if(expo_A > expo_B) { //A의 최고차수가 B의 최고차수보다 크면
				C.setCoef(index_C++, A.getCoef(index_A++)); //C 다항식의 0번쨰 배열에 A다항식 배열의 0번째 계수를 넣는다.
				expo_A--; //그리고 차수를 하나를 감소 A의 차수를
			}
			else if(expo_A == expo_B) { //차수가 같으면 두개를 더한값을 C에다가 넣는다.
				C.setCoef(index_C++, A.getCoef(index_A++)+B.getCoef(index_B++));
				expo_A--; //그리고 각각 감소
				expo_B--; 
			}
			else { //위에 꺼와 똑같음.
				C.setCoef(index_C++, B.getCoef(index_B++));
				expo_B--;
			}
		}
		return C;
	}
}
