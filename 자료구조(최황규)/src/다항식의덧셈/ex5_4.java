package 다항식의덧셈;

public class ex5_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float a[] = new float [] {4,3,5,0}; //다항식 선언
		float b[] = new float [] {3,1,0,2,1}; //다항식 선언
		Polynomial A = new Polynomial(3,a); //차수와(x^3승), 배열 a 보냄, 각각 객체를 생성
		Polynomial B = new Polynomial(4,b); //차수와(x^4승), 배열 b 보냄, 각각 객체를 생성
		OperatePoly optpoly = new OperatePoly(); //객체 생성.
		Polynomial C = optpoly.addPoly(A,B); //생성한 객체에다가 A,B를 보내서 계산하게 한다. 
		System.out.printf("A(X)="); A.printPoly();
		System.out.printf("B(X)="); B.printPoly();
		System.out.printf("C(X)="); C.printPoly();
		
	}

}
