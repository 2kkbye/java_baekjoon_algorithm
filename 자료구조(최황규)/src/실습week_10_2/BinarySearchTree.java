package 실습week_10_2;

public class BinarySearchTree {
	private TreeNode1 root;
	public TreeNode1 insertKey(TreeNode1 root,TreeNode1 input) {

		TreeNode1 p = root;
		TreeNode1 newNode = new TreeNode1();

		newNode.id=input.id;
		newNode.name=input.name;
		newNode.addr=input.addr;
		newNode.left=null;
		newNode.right=null;

		if(p==null) {
			return newNode;
		}
		else if(p.id>newNode.id) {
			p.left=insertKey(p.left,input);
			return p;
		}
		else if(p.id<newNode.id) {
			p.right=insertKey(p.right, input);
			return p;
		}
		else return p;
	}
	public void insertBst(TreeNode1 input) {
		root=insertKey(root,input);
	}
	public TreeNode1 searchBST(int x) {
		TreeNode1 p = root;

		while(p!=null) { //찾아보고 없으면 리턴값이 null로 리턴이됨.
			if(x<p.id) {p=p.left; }
			else if(x>p.id) {p=p.right; }
			else return p;
		}

		return p;
	}
	public void inorder(TreeNode1 root) {
		if(root!= null) {
			inorder(root.left);

			System.out.printf("%d %s %s \n", root.id,root.name,root.addr);

			inorder(root.right);

		}
	}
	public void printBst() {
		inorder(root);
		System.out.println();
	}
}
