package 실습week_10_2;

import java.io.BufferedReader;
import java.io.File;
import java.nio.file.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class main {

	public static void main(String[] args) {
		String temp="";
		BinarySearchTree bsT = new BinarySearchTree();
		File file = new File("st.txt");
		try {
			
			FileInputStream fis = new FileInputStream(file); //파일로부터 바이트로 입력받아, 바이트 단위로 출력할 수있는 클래스이다.
			InputStreamReader isr = new InputStreamReader(fis); //바이트스트림에서 문자 스트림으로 변환.
			BufferedReader br = new BufferedReader(isr); //문자 입력스트림으로부터 문자를 읽어들이거나 문차 출력 스트림으로 문자 내보낼때

			while((temp=br.readLine())!= null) {
				String[] str = temp.split(" ");
				TreeNode1 record = new TreeNode1();
				record.id = Integer.parseInt(str[0]);
				record.name=str[1];
				record.addr=str[2];

				bsT.insertBst(record);

			}
			bsT.printBst();
		}
		catch(FileNotFoundException e){
			e.printStackTrace();

		}
		catch (IOException e){
			e.printStackTrace();
		}

	}

}
