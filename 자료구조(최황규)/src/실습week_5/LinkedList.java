package 실습week_5;

public class LinkedList {
	private ListNode head;
	public LinkedList() {
		head = null;
	}
	public void insertFirstNode(String data) {
		ListNode newNode = new ListNode(data);
		newNode.link=head;
		head=newNode;
	}
	public void insertMiddleNode(ListNode pre, String data) {
		ListNode newNode = new ListNode(data);
		newNode.link=pre.link;
		pre.link=newNode;
	}
	public void insertLastNode(String data) {
		ListNode newNode = new ListNode(data);
		if(head ==null) {
			this.head=newNode;
		}
		else {
			ListNode temp = head;
			while(temp.link !=null) temp = temp.link;
			temp.link=newNode;
		}
	}
	public void init() {
		head=null;
	}
	public void deleteLastNode() {
		ListNode pre, temp;
		if(head==null) return;
		if(head.link==null) {
			head=null;
		}
		else {
			pre=head;
			temp=head.link;
			while(temp.link !=null) {
				pre=temp;
				temp=temp.link;
			}
			pre.link=null;
		}
	}
	public void deleteSelectNode(String data) {
		ListNode pre,temp,old;
		pre=this.head;
		temp=this.head.link;
		if(head==null) return;
		if(data.equals(head.getData())) {
			 head=head.link;
		}
		else {
			while(temp.link !=null) {
				if(data.equals(pre.link.getData())) {
					pre.link=temp.link;
					old=temp.link;
					old=null;
				}
				pre=temp;
				temp=temp.link;
			}
		}
		
		
	}
	public ListNode searchNode(String data) {
		ListNode temp = this.head;
		while(temp!=null) {
			if(data.equals(temp.getData()))
				return temp;
			else
				temp=temp.link;
		}
		return temp;
	}
	public int searchNode2(char dat) {
		String data =Character.toString(dat);
		ListNode temp = this.head;
		while(temp!=null) { //1이면 중복
			if(data.equals(temp.getData())) {
				System.out.println("aa");
				System.out.println(data);
				return 1;
			}
			else
				temp=temp.link;
		}
		return 2;
	}
	public void reverseList() {
		ListNode next = head;
		ListNode current = null;
		ListNode pre = null;
		while(next !=null) {
			pre=current;
			current =next;
			next=next.link;
			current.link=pre;
		}
		head = current;
	}
	public void printList() {
		ListNode temp= this.head;
		System.out.printf("L = (");
		while(temp!=null) {
			System.out.printf(temp.getData());
			temp=temp.link;
			if(temp!=null) {
				System.out.printf(", ");
			}
		}
		System.out.println(")");
	}
}
