package 실습week_9_1;

public class week9 {
	public static void main(String[] args) {
		
		ArrayTree tree = new ArrayTree(16);
		tree.makeBT("A");
		tree.makeBT("B");
		tree.makeBT("C");
		tree.makeBT("D");
		tree.makeBT("E");
		tree.makeBT("F");
		tree.makeBT("G");
		tree.makeBT("H");
		tree.makeBT(null);
		tree.makeBT("I");
		tree.makeBT("J");
		tree.makeBT(null);
		tree.makeBT(null);
		tree.makeBT(null);
		tree.makeBT("K");
		//나머지 데이터 추가
		
		
		System.out.print("\n Preorder : ");
		tree.preorder(1); //1번째 부터 전위순회
		
		System.out.print("\n Inorder : ");
		tree.inorder(1);
		
		System.out.print("\n Postorder : ");
		tree.postorder(1);
	}
}
