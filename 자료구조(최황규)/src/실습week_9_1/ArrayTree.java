package 실습week_9_1;

public class ArrayTree {
	private Object[] aTree;
	int index;

	ArrayTree(int TreeSize) {
		aTree = new Object[TreeSize]; //트리 사이즈 32이다.
		index=1; //처음 들어가는 값은 배열에서 1의 자리에 들어가야한다.
	}
	public void makeBT(Object data) {
		if(this.index>aTree.length) {
			System.out.println("TreeSize over");
		}
		else {
			aTree[index]=data;
			index++;
		}
	}
	public void preorder(int index) {
		if(index < aTree.length) {
			if(aTree[index]!=null) 
				System.out.print(aTree[index]);
			preorder(index*2);
			preorder(index*2+1);

		}


	}
	public void inorder(int index) {
		if(index < aTree.length) {
			inorder(index*2);
			if(aTree[index]!=null) 
				System.out.print(aTree[index]);
			inorder(index*2+1);

		}

	}
	public void postorder(int index) {
		if(index < aTree.length) {
			postorder(index*2);

			postorder(index*2+1);
			if(aTree[index]!=null) 
				System.out.print(aTree[index]);

		}
	}
}
