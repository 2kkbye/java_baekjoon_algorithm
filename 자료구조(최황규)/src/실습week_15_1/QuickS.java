package �ǽ�week_15_1;

public class QuickS {

	int i=0; 
	static int count=0;
	public int partition(int a[], int begin, int end) {
		int pivot, temp, L, R, t;
		L=begin;
		R=end;
		pivot =(begin + end)/2;
		while(L<R) {
			
			while((a[L]<a[pivot]) && (L<R)) {L++; count++;}	
			while((a[R]>=a[pivot])&&(L<R)) {R--; count++;}	
			if(L<R) {
				temp=a[L];
				a[L]=a[R];
				a[R]=temp;
				count++;
			}
		}
		count++;
		temp=a[pivot];
		a[pivot]=a[R];
		a[R]= temp;
		return L;
	}
	public void quickSort(int a[],int begin, int end) {

		if(begin<end) {
			int p;
			p=partition(a,begin,end);
			quickSort(a,begin,p-1);
			quickSort(a,p+1,end);
	
		}
	}
}
