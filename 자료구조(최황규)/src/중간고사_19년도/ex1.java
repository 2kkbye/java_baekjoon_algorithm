package 중간고사_19년도;

interface Stack{
	boolean isEmpty();
	void push(char item);
	char pop();
	void delete();
	char peek();
}
class StackNode{
	char data;
	StackNode link;
}
class LinkedStack implements Stack{

	private StackNode top;
	
	@Override
	public boolean isEmpty() {
		return (top == null);
	}

	@Override
	public void push(char item) {
		StackNode newNode = new StackNode();
		newNode.data = item;
		newNode.link = top;
		top = newNode;
	}

	@Override
	public char pop() {
		if(isEmpty()){
			System.out.println("Deleting fail! Linked Stack is empty!!");
			return 0;
		}else{
			char item = top.data;
			top = top.link;
			return item;
		}
	}

	@Override
	public void delete() {
		if(isEmpty()){
			System.out.println("Deleting fail!! Linked Stack is Empty!!");
		}else{
			top = top.link;
		}
	}

	@Override
	public char peek() {
		if(isEmpty()){
			System.out.println("Peeking fail !  Linked Stack is empty!!");
			return 0;
		}else{
			return top.data;
		}
	}
	
	public void printStack(){
		if(isEmpty())
			System.out.println("Linked Stack is empty!!");
		else{
			StackNode temp = top;
			System.out.print("Linked Stack>> ");
			while(temp != null){
				System.out.printf("\t %c \n", temp.data);
				temp = temp.link;
			}
			System.out.println();
		}
	}
}
class OptExp{
	private String exp;
	private int expSize;
	private char testCh, openPair;
	
	public char[] toPostfix(String infix){
		exp = infix;
		expSize = 10;
		int j=0;
		char postfix[] = new char[expSize];
		LinkedStack S = new LinkedStack();
		
		for(int i=0; i<= expSize; i++){
			testCh = this.exp.charAt(i);
			switch(testCh){
			case '0':case'1':case '2':case'3':case '4':case'5':case '6':case'7':case '8':case'9':
				postfix[j++] = testCh;break;
				
			case '+':case '-':case '*':case '/':
				S.push(testCh);break;
				
			case ')':
				postfix[j++] = S.pop();break;
			
			default:
			}
		}
		postfix[j] = S.pop();
		return postfix;
		
	}
	
	public char[] stackReverse(String infix){
		exp = infix;
		LinkedStack S = new LinkedStack();
		char reverseFix[] = new char[exp.length()];
		int j=0;
		char testCh;
		
		for(int i=0; i<exp.length(); i++){
			testCh = exp.charAt(i);
			if(testCh == '(')
				testCh = ')';
			else if(testCh == ')')
				testCh = '(';
			S.push(testCh);
		}
		while(!S.isEmpty()){
			reverseFix[j++] = S.pop();
		}
		
		return reverseFix;
	}
}

public class ex1 {
	
	public static void main(String[] args) {
		
		OptExp opt = new OptExp();
		String exp = "(3*5)-(6/2)";
		char preFix[];
		
		preFix = opt.stackReverse(exp);
		exp = new String(preFix);
		System.out.println(exp);
		
		preFix = opt.toPostfix(exp);
		exp = new String(preFix);
		System.out.println(exp);
		
		preFix = opt.stackReverse(exp);
		exp = new String(preFix);
		exp = exp.trim();
		System.out.println(exp);		
	}
}