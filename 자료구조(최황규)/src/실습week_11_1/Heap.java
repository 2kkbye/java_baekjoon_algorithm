package 실습week_11_1;

public class Heap {
	private int heapSize;
	private Stud itemHeap[]; //Stud형인 itemHeap 배열 선언을 했음

	public Heap(){
		heapSize = 0;
		itemHeap = new Stud[50];
	}

	public void insertHeap(Stud item){
		int i = ++heapSize;
		while((i != 1) && (item.id<itemHeap[i/2].id)){ 
			itemHeap[i] = itemHeap[i/2];
			i/=2;
		}

		itemHeap[i] = item;
	}

	public int getHeapSize(){
		return this.heapSize;
	}

	public Stud deleteHeap(){
		int parent, child;
		Stud item, temp;
		item = itemHeap[1];
		temp = itemHeap[heapSize--];
		parent = 1; child =2;
		while(child<=heapSize){
			if((child < heapSize) && (itemHeap[child].id>=itemHeap[child+1].id)) //수정
				child++;
			if(temp.id<itemHeap[child].id)
				break;

			itemHeap[parent] = itemHeap[child];
			parent=child;
			child *= 2;
		}
		itemHeap[parent]=temp;
		return item;
	}

	public void printHeap(){
		System.out.printf("\nHeap >>> ");
		for(int i=1; i<=heapSize; i++)
			System.out.print("["+itemHeap[i].id+" "+itemHeap[i].name+" "+itemHeap[i].addr+"] ");
		System.out.println();
		System.out.println();
		
	}
}
