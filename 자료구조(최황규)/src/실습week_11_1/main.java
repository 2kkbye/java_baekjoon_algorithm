package 실습week_11_1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
public class main {
	public static void main(String args[]) {

		int n;
		Stud item;
		Heap h = new Heap(); // Stud 배열 50 생성되고, heapsize=0으로 초기화가 됨

		String arr[] = new String[10];
		int j=0;

		File file = new File("Student.txt");
		try {
			FileInputStream fis = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);
			String temp = "";
			while((temp = br.readLine())!=null){
				arr[j++]=temp;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for(int i=0; i<arr.length; i++){

			System.out.println("inserted Item : ["+arr[i]+"]");
			String[] input = arr[i].split(" ");
			Stud newStud = new Stud(Integer.parseInt(input[0]), input[1], input[2]);
			//이 위에는 그냥 newStud 선언한다음 각각 입력시켜준다음에 넘겨 줘도된다.
			h.insertHeap(newStud);
		}

		h.printHeap();

		n = h.getHeapSize();
		for(int i=1; i<=n; i++){
			item = h.deleteHeap();
			System.out.println("deleted Item : ["+item.id+" "+item.name+" "+item.addr+"] ");
			//h.printHeap();
		}
	}
}
