package 실습_18년도_4주차;

public class ListNode {
	private String data;
	public ListNode link;
	public ListNode() {
		// TODO Auto-generated constructor stub
		this.data=null;
		this.link=null;
	}
	public ListNode(String data) {
		this.data=data;
		this.link=null;
	}
	public ListNode(String data, ListNode link) {
		this.data=data;
		this.link=link;
	}
	public String getData() {
		return this.data;
	}
}