package 실습_18년도_4주차;

import 실습week_5.ListNode;

public class LinkedList {
	private ListNode head;
	public LinkedList() {
		head = null;
	}
	public void insertFirstNode(String data) {
		ListNode newNode = new ListNode(data);
		newNode.link=head;
		head=newNode;
	}
	public void insertLastNode(String data) {
		ListNode newNode = new ListNode(data);
		if(head ==null) {
			this.head=newNode;
		}
		else {
			ListNode temp = head;
			while(temp.link !=null) temp = temp.link;
			temp.link=newNode;
		}
	}
	public void init() {
		head=null;
	}
	public void deleteSelectNode(String data) {
		ListNode pre,temp,old;
		pre=this.head;
		temp=this.head.link;
		if(head==null) return;
		if(data.equals(head.getData())) {
			 head=head.link;
		}
		else {
			while(temp.link !=null) {
				if(data.equals(pre.link.getData())) {
					//System.out.println(pre.link.getData()+"---"+temp.link.getData());
					pre.link=temp.link;
					System.out.println(temp.getData());
					//old=temp;
					//old=null;
				}
				pre=temp;
				temp=temp.link;
			}
		}
	}
	public boolean searchNode(String data) {
		ListNode temp = this.head;
		while(temp!=null) {
			if(data.equals(temp.getData())) {
				return false;
			}
			else
				temp=temp.link;
		}
		return true;
	}
	public void printList() {
		ListNode temp= this.head;
		System.out.printf("L = (");
		while(temp!=null) {
			System.out.printf(temp.getData());
			temp=temp.link;
			if(temp!=null) {
				System.out.printf(", ");
			}
		}
		System.out.println(")");
	}
}

