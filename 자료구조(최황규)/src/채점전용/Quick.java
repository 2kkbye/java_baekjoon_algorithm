package ä������;

public class Quick {
	int i=0;
	public int partition(int a[], int begin, int end){
		int pivot,temp,L,R,t;
		L=begin;
		R=end;
		pivot=(begin+end)/2;
		
		while(L<R){
			while((a[L]>=a[pivot]) && (L<=R)) L++;
			while((a[R]<a[pivot]) && (L<=R)) R--;
			if(L<R){
				temp=a[L];
				a[L]=a[R];
				a[R]=temp;
				
				if(L==pivot){
					i++;
					return R;
				}
			}
		}
		temp=a[pivot];
		a[pivot]=a[R];
		a[R]=temp;
		i++;
		return R;
	}
	
	public void quickSort(int a[],int begin, int end){
		if(begin<end && i==0){
			int p;
			p=partition(a,begin,end);
			quickSort(a,begin,p-1);
			quickSort(a,p+1,end);
		}
	}
	
}
