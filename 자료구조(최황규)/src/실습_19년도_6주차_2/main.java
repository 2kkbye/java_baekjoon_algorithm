package 실습_19년도_6주차_2;

import java.util.Scanner;
public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		OptExp opt = new OptExp();
		String exp = sc.next();
		System.out.println(exp);
		if(opt.testPair(exp))
		System.out.println("괄호 맞음!");
		else
		System.out.println("괄호 틀림!!!");
	}
}
interface Stack{
	boolean isEmpty();
	void push(char item);
	char pop();
	void delete();
	char peek();
}
class ArrayStack implements Stack {
	private int top;
	private int stackSize;
	private char itemArray[];
	
	public ArrayStack(int stackSize) {
		top=-1;
		this.stackSize=stackSize;
		itemArray=new char[this.stackSize];
	}
	
	public boolean isEmpty() {
		return (top==-1);
	}
	public boolean isFull() {
		return (top==this.stackSize-1);
	}
	public void push(char item) {
		if(isFull()) {
			System.out.println("inserting fail! Array is full");
		}
		else {
			itemArray[++top] =item;
			System.out.println("inserted item : " +item);
		}
	}
	public char pop() {
		if(isEmpty()) {
			System.out.println("Deleting fail! Array is empty!");
			return 0;
		}
		else {
			return itemArray[top--];
		}
	}
	public void delete() {
		if(isEmpty()) {
			System.out.println("deleting fail! Array Stack is empty");
		}
		else {
			top--;
		}
	}
	public char peek() {
		if(isEmpty()) {
			System.out.println("peeking fail! Array is empty");
			return 0;
		}
		else
			return itemArray[top];
	}
	public void printStack() {
		if(isEmpty())
			System.out.printf("Array Stack is empty! %n %n");
		else {
			System.out.println("현재 가장 앞에 있는것은"+itemArray[0]);
			System.out.println("Array Stack>> ");
			for(int i=0;i<=top;i++) {
				System.out.printf("%c",itemArray[i]);
			}
			System.out.println();
			System.out.println();
		}
	}
}
class OptExp{
	private String exp;
	private int expSize;
	private char testCh, openPair;
	public boolean testPair(String exp) {
		this.exp = exp;
		expSize = exp.length();
		ArrayStack S = new ArrayStack(expSize);
		for(int i=0;i<expSize;i++) {
			testCh = this.exp.charAt(i);
			switch(testCh) {
			case '(' :
			case '{' :
			case '[' :
				S.push(testCh);break;
			case ')' :
			case '}' :
			case ']' :
				if(S.isEmpty()) return false;
				else {
					openPair = S.pop();
					if((openPair == '(' && testCh !=')') || (openPair == '{' && testCh !='}') || (openPair == '[' && testCh !=']')) {
						return false;
					}
					else break;
				}
			}
		}
		if(S.isEmpty()) return true;
		else return false;
	}
}
