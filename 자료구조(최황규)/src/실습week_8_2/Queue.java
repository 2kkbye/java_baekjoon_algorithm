package �ǽ�week_8_2;

interface Queue {
	boolean isEmpty();
	void enQueue(QNode item);
	QNode deQueue();
	void delete();
	QNode peek();
}
