package 실습week_8_2;

import java.util.Scanner;
public class Ex8_2 {
	public static void main(String[] args) {
		LinkedQueue men = new LinkedQueue();
		LinkedQueue girl = new LinkedQueue();
		Scanner sc = new Scanner(System.in);
		while(true) {
			QNode Q = new QNode();
			System.out.print("성별 과 이름을 입력하세요.<종료는 q> <ex 남 홍길동> : ");
			String str = sc.nextLine();
			if(str.length()==1&&str.charAt(0)=='q') {
				break;
			}
			if(str.charAt(0)=='남') {
				Q.sex="남";
				Q.name=bun(str);
				men.enQueue(Q);
			}
			else {
				Q.sex="여";
				Q.name=bun(str);
				girl.enQueue(Q);
			}
			if(men.rear!=null&&girl.rear!=null) { //둘다 들어있다는 뜻
				QNode men1 = new QNode();
				QNode girl1= new QNode();
				System.out.print("남자 큐 : ");
				men.printQueue();
				System.out.print("여자 큐 : ");
				girl.printQueue();
				men1=men.deQueue();
				girl1=girl.deQueue();
				System.out.println("==========================================================");
				System.out.printf("****커플이 탄생하였습니다 :%s과(와) %s****%n",men1.name,girl1.name);
				System.out.println("==========================================================");
				System.out.print("남자 큐 : ");
				men.printQueue();
				System.out.print("여자 큐 : ");
				girl.printQueue();
				
			}
			else {
				System.out.println("아직 대상자가 없습니다. 기다려주세요");
				System.out.print("남자 큐 : ");
				men.printQueue();
				System.out.print("여자 큐 : ");
				girl.printQueue();
			}
		}
	}
	public static String bun(String str) {
		String a="";
		for(int i=2;i<=str.length()-1;i++) {
			a=a+str.charAt(i);
		}
		return a;
	}
}
