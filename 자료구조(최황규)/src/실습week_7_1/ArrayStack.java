package 실습week_7_1;

public class ArrayStack implements Stack {
	private int top;
	private int stackSize;
	private char itemArray[];
	
	public ArrayStack(int stackSize) {
		top=-1;
		this.stackSize=stackSize;
		itemArray=new char[this.stackSize];
	}
	
	public boolean isEmpty() {
		return (top==-1);
	}
	public boolean isFull() {
		return (top==this.stackSize-1);
	}
	public void push(char item) {
		if(isFull()) {
			System.out.println("inserting fail! Array is full");
		}
		else {
			itemArray[++top] =item;
			System.out.println("inserted item : " +item);
		}
	}
	public char pop() {
		if(isEmpty()) {
			System.out.println("Deleting fail! Array is empty!");
			return 0;
		}
		else {
			return itemArray[top--];
		}
	}
	public void delete() {
		if(isEmpty()) {
			System.out.println("deleting fail! Array Stack is empty");
		}
		else {
			top--;
		}
	}
	public char peek() {
		if(isEmpty()) {
			System.out.println("peeking fail! Array is empty");
			return 0;
		}
		else
			return itemArray[top];
	}
	public void printStack() {
		if(isEmpty())
			System.out.printf("Array Stack is empty! %n %n");
		else {
			System.out.println("현재 가장 앞에 있는것은"+itemArray[0]);
			System.out.println("Array Stack>> ");
			for(int i=0;i<=top;i++) {
				System.out.printf("%c",itemArray[i]);
			}
			System.out.println();
			System.out.println();
		}
	}
}
