package 실습week_10_1;

public class BinarySearchTree {
	private TreeNode root = new TreeNode();
	public void insertBST(int x) {
		TreeNode p = root;
		TreeNode q = new TreeNode();
		
		while(p!=null){
			if(x == p.data){
				return; 
			}
			q = p;
			if(x < p.data){
				p=p.left;
			}
			else {
				p=p.right;
			}
		}
		
		TreeNode newNode = new TreeNode();
		
		newNode.data = x;
		newNode.left = null;
		newNode.right = null;
		
		if(root == null){
			root = newNode;
		}
		else if (x<q.data){
			q.left = newNode;
		}
		else{
			q.right = newNode;
		}
		return;
	}

	public TreeNode searchBST(int x) {
		TreeNode p = root;
		
		while(p!=null) { //찾아보고 없으면 리턴값이 null로 리턴이됨.
			if(x<p.data) {p=p.left; }
			else if(x>p.data) {p=p.right; }
			else return p;
		}
		
		return p;
	}
	public void inorder(TreeNode root) {
		if(root != null) {
			inorder(root.left);
			System.out.printf("%d ", root.data);
			inorder(root.right);
		}
	}
	public void printBst() {
		inorder(root);
		System.out.println();
	}
}
