package �ǽ�week_9_2;

public class Ex9_2 {
	public static void main(String[] args) {
		LinkedTree T = new LinkedTree();
		
		TreeNode n5=T.makeBT(null,'E',null);
		TreeNode n4=T.makeBT(null,'D',null);
		TreeNode n3=T.makeBT(n4,'C',n5);
		TreeNode n2=T.makeBT(null,'B',null);
		TreeNode n1=T.makeBT(n2,'A',n3);
		
		System.out.printf("%n Preorder : ");
		T.preorder(n1);
		
		System.out.printf("%n Inorder : ");
		T.inorder(n1);
		
		System.out.printf("%n Postorder : ");
		T.postorder(n1);
		System.out.println();
		
		T.reverse(n1);
		T.reverse(n3);
		//T.print(n1);
		
		T.preorder(n1);
		System.out.println();
		T.inorder(n1);
		System.out.println();
		T.postorder(n1);
	}
}