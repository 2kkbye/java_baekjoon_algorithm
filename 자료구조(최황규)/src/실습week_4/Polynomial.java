package 실습week_4;

public class Polynomial {
	
	private int degree;
	private float coef[] = new float[20];
	
	public Polynomial(int degree, float coef[]) { //생성자 선언과 동시에 바로시작한다.
		this.degree = degree; //차수를 넣어주고
		this.coef=coef; //배열을 넣어준다.
	}
	Polynomial (int degree){
		this.degree = degree;
		for(int i=0;i<=degree;i++) {
			this.coef[i]=0; //coef 배열을 인수로오는 degree차수만큼 0으로 초기화한다. 
		}
	}
	public int getDegree() {
		return this.degree;
	}
	public float getCoef(int i) {
		return this.coef[i];
	}
	public float setCoef(int i, float coef) {
		return this.coef[i] = coef;
	}
	public void printPoly() {
		int temp = this.degree;
		for(int i=0;i<=this.degree;i++) {
			System.out.printf("%3.0fx^%d",this.coef[i],temp--);
		}
		System.out.println();
	}
}