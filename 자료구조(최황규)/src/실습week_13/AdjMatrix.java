package 실습week_13;

public class AdjMatrix {
	private int matrix[][]=new int[10][10];
	private int totalV=0;
	public void insertVertex(int v) {
		totalV++;
	}

	public void insertEdge(int v1, int v2) {
		if(v1>=totalV || v2>=totalV)
			System.out.println("그래프에 없는 정점입니다!");
		else {
			matrix[v1][v2]=1;
		}
	}
	public void printMatrix() {
		for(int i=0;i<totalV; i++) {
			System.out.printf("\n\t\t");
			for(int j=0;j<totalV;j++)
				System.out.printf("%2d",matrix[i][j]);
		}
	}
	public void DFS(int v){
		System.out.print("깊이우선 :");
		boolean visited[] = new boolean[10];
		System.out.printf(" %c",v+65);
		for(int i=0; i<visited.length; i++)
			visited[i]=false;
		
		visited[v] = true; 
		
		for(int i=0; i<7; i++)
			if(matrix[v][i]==1){
				if(visited[i]==false){
					visited[i]=true;
					System.out.printf(" %c", i+65);
					v=i; i=-1;
					
				}
				
			}
	}
	public void BFS(int v){
		LinkedQueue queue = new LinkedQueue();

		System.out.print("너비우선 :");
		boolean visited[] = new boolean[10];
		System.out.printf(" %c", v+65);

		for(int i=0; i<visited.length; i++)
			visited[i]=false;

		visited[v]=true;

		for(int i=0; i<7; i++){
			if(matrix[v][i]==1){
				if(visited[i]==false){
					visited[i]=true;
					queue.enQueue(i);
					System.out.printf(" %c", i+65);
				}
			}		

			if(i==6){
				boolean b=true;
				for(int j=0; j<7; j++)
					if(visited[j]==false){
						b=false;
						break;
					}
				if(b)
					break;
				v = queue.deQueue();
				i=-1;
			}
		}
	}

}
