package 실습_19년도_7주차;

import java.util.Scanner;

interface Stack {
	boolean isEmpty();
	void push(char item);
	void push2(int num);
	char pop();
	void delete();
	char peek();
}

class ArrayStack implements Stack {
	private int top;
	private int stackSize;
	private char itemArray[];
	private int numArray[];
	
	public ArrayStack(int stackSize) {
		top=-1;
		this.stackSize=stackSize;
		itemArray=new char[this.stackSize];
	}
	
	public boolean isEmpty() {
		return (top==-1);
	}
	public boolean isFull() {
		return (top==this.stackSize-1);
	}
	public void push(char item) {
		if(isFull()) {
			System.out.println("inserting fail! Array is full");
		}
		else {
			itemArray[++top] =item;
			//System.out.println("inserted item : " +item);
		}
	}
	public void push2(int num) {
		
	}
	public char pop() {
		if(isEmpty()) {
			System.out.println("Deleting fail! Array is empty!");
			return 0;
		}
		else {
			return itemArray[top--];
		}
	}
	public void delete() {
		if(isEmpty()) {
			System.out.println("deleting fail! Array Stack is empty");
		}
		else {
			top--;
		}
	}
	public char peek() {
		if(isEmpty()) {
			System.out.println("peeking fail! Array is empty");
			return 0;
		}
		else
			return itemArray[top];
	}
}

public class main {

	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		String str;
		OptExp opt = new OptExp();
		String exp = "(3*5)-(6/2)";
		char postfix[];
		int value;
		System.out.println(exp);
		if(opt.testPair(exp))
			System.out.println("괄호맞음");
		else {
			System.out.println("괄호틀림");
		}
		System.out.print("\n후위표기식 : ");
		postfix = opt.toPostfix(exp);
		System.out.println(postfix);
	}
}
class OptExp{
	private String exp;
	private int expSize;
	private char testCh, openPair;
	public boolean testPair(String exp) {
		this.exp = exp;
		expSize = exp.length();
		ArrayStack S = new ArrayStack(expSize);
		for(int i=0;i<expSize;i++) {
			testCh = this.exp.charAt(i);
			switch(testCh) {
			case '(' :
			case '{' :
			case '[' :
				S.push(testCh);break;
			case ')' :
			case '}' :
			case ']' :
				if(S.isEmpty()) return false;
				else {
					openPair = S.pop();
					if((openPair == '(' && testCh !=')') || (openPair == '{' && testCh !='}') || (openPair == '[' && testCh !=']')) {
						return false;
					}
					else break;
				}
			}
		}
		if(S.isEmpty()) return true;
		else return false;
	}
	public char[] toPostfix(String infix) {
		char testCh;
		exp = infix;
		int expSize = 10;
		int j = 0;
		char postfix[] = new char[expSize];
		ArrayStack S = new ArrayStack(expSize);
		
		for(int i=0;i<=expSize; i++) {
			testCh = this.exp.charAt(i);
			switch(testCh) {
			case'0':
			case'1':
			case'2':
			case'3':
			case'4':
			case'5':
			case'6':
			case'7':
			case'8':
			case'9':
				postfix[j++] = testCh;break;
			case'+':
			case'-':
			case'*':
			case'/':
			S.push(testCh);break;
			case')' : postfix[j++] = S.pop();break;
			
			default:
			}
		}
		postfix[j] = S.pop();
		return postfix;
	}
}
class OptExp2{
	private String exp;
	
//	public int evalPostfix(String postfix) {
//		ArrayStack S = new ArrayStack(postfix.length());
//		exp = postfix;
//		int opr1, opr2, value;
//		char testCh;
//		for(int i=0;i<7;i++) {
//			testCh = exp.charAt(i);
//			if(testCh != '+' && testCh !='-' && testCh !='*' && testCh !='/') {
//				value = testCh - '0';
//			}
//		}
//	}
}