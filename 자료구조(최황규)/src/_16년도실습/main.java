package _16년도실습;

import java.util.Scanner;

public class main{
	public static void main(String args[]){
		LinkedList L = new LinkedList();
		Scanner sc = new Scanner(System.in);
		String inputstr = sc.nextLine();
		System.out.println("(1) 공백 리스트에 문자열 삽입하기");
		for(int i = 0 ; i < inputstr.length() ; i++){
			if(L.searchNode(inputstr.charAt(i) + ""))
				L.insertLastNode(inputstr.charAt(i) + "");
		}
		L.printList();
		System.out.println("(2) 공백제거");
		L.deleteNode(" ");
		L.printList();
		System.out.println("(3) List의 가장 앞에 문자 추가");
		L.insertFirstNode("\"");
		L.printList();
		System.out.println("(4) 리스트 초기화");
		L.init();
		L.printList();
		sc.close();
	}
}

class LinkedList{
	private ListNode head;
	public LinkedList(){
		head = null;
	}
	/**
	 * 리스트 초기화 함수 구현, List에 아무것도 추가되지 않은 상태 (1점)
	 */
	public void init(){
		//작성
		head = null;
	}
	public void insertLastNode(String str){
		ListNode newNode = new ListNode(str);
		if(head == null){
			head = newNode;
		}else{
			ListNode current = head;
			while(current.link!=null){
				current = current.link;
			}
			current.link = newNode;
		}
	}
	/**
	 * 리스트의 가장 앞에 노드 추가 (2점)
	 * @param data
	 */
	public void insertFirstNode(String data){
		ListNode newNode = new ListNode(data);
		if(head == null){
			head = newNode;
		}else{
			ListNode current = head;
			newNode.link = current;
			head = newNode;
		}
	}
	/**
	 * 특정 data를 가진 노드 삭제 (3점)
	 * 데이터가 중복 될 경우 앞쪽에 있는 데이터 삭제
	 * 주의 : 리스트의 가장 앞에있는 데이터 삭제 시 처리
	 * @param data
	 */
	public void deleteNode(String data){
		if(head==null){
			return;
		}else{
			ListNode pre = head;
			ListNode current = head.link;
			while(current.link!=null){
				if(data.equals(current.getData())) {
					pre.link = current.link;
					return;
				}
				pre = current;
				current = current.link;
			}
			pre.link = null;
		}
	}
	public boolean searchNode(String data) {
		ListNode current = this.head;
		while(current != null) {
			if(data.equals(current.getData()))
				return false;
			else
				current = current.link;
		}
		return true;
	}

	public void printList() {
		ListNode temp = this.head;
		System.out.print("L = (");
		while(temp != null) {
			System.out.print(temp.getData());
			temp = temp.link;
			if(temp != null) {
				System.out.print(", ");
			}
		}
		System.out.println(") ");
	}
}



class ListNode{
	private String data;
	public ListNode link;
	public ListNode() {
		this.data = null;
		this.link = null;
	}
	public ListNode(String data) {
		this.data = data;
		this.link = null;
	}

	public ListNode(String data, ListNode link) {
		this.data = data;
		this.link = link;
	}
	public String getData() {
		return this.data;
	}
}
