package 실습_19년도_5주차;

import 실습_19년도_5주차.List;
import 실습_19년도_5주차.Node;

public class ex1 {

	public static void main(String[] ar){
		List A = new List();
		List B = new List();
		List C = new List();
		A.appendTerm(4,5);
		A.appendTerm(2,3);
		A.appendTerm(10,2);
		A.appendTerm(1,0);

		B.appendTerm(3, 4);
		B.appendTerm(-2, 3);
		B.appendTerm(2, 2);
		B.appendTerm(-4, 1);
		C=addPoly(A,B);
		Node p = C.getPL();
		while(p!=null) {
			System.out.print(p.coef+"x^"+p.expo+" ");
			p=p.link;
		}
	}
	public static List addPoly(List A, List B){
		List C = new List();
		Node p = A.getPL();
		Node q = B.getPL();
		float sum=0;
		while(p!=null&&q!=null) {
			if(p.getExpo()==q.getExpo()) {
				sum=p.getCoef()+q.getCoef();
				if(sum!=0) {
					C.appendTerm(sum, p.getExpo());
				}
				p=p.link;
				q=q.link;
			}
			else if(p.getExpo()<q.getExpo()) {
				C.appendTerm(q.getCoef(), q.getExpo());
				q=q.link;
			}
			else {
				C.appendTerm(p.getCoef(), p.getExpo());
				p=p.link;
			}
		}
		while(p!=null) {
			C.appendTerm(p.getCoef(),p.getExpo());
			p=p.link;
		}
		while(q!=null) {
			C.appendTerm(q.getCoef(), q.getExpo());
			q=q.link;
		}
		return C;
	}

}
