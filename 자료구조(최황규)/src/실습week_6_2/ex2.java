package 실습week_6_2;

public class ex2 {

	public static void main(String[] args) {
		CLinkedList CL = new CLinkedList();
		System.out.println("(1) 공백 리스트에 노드 3개 삽입하기");
		CL.insertFirstNode("일");
		CL.printList();
		CL.insertFirstNode("수");
		CL.printList();
		CL.insertFirstNode("월");
		CL.printList();

		System.out.println("(2) 수 노드뒤에 화 노드 삽입하기");
		CListNode pre =CL.searchNode("수");
		CL.insertMiddleNode(pre, "화");
		CL.printList();

	}

}

