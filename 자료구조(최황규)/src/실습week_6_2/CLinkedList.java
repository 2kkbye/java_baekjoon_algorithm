package �ǽ�week_6_2;

import �ǽ�week_5.ListNode;

public class CLinkedList {
	private CListNode CL;

	public CLinkedList() {
		CL=null;
	}
	public void insertFirstNode(String data) {
		CListNode nw = new CListNode(data);
		CListNode tmp;
		if(CL==null) {
			CL=nw;
			nw.link=nw;
		}
		else {
			tmp=CL;
			while(tmp.link!=CL) {
				//System.out.println("-----"+tmp.getData());
				tmp=tmp.link;
			}
			//System.out.println("======"+tmp.getData());
			nw.link=tmp.link;
			tmp.link=nw;
			CL=nw;
		}
	}
	public void insertMiddleNode(CListNode pre, String data) {
		CListNode nw = new CListNode(data);
		if(CL==null) {
			CL=nw;
			nw.link=nw;
		}
		else {
			System.out.println(pre.getData());
			nw.link=pre.link;
			pre.link=nw;
		}
	}
	public CListNode searchNode(String data) {
		CListNode tmp = this.CL;
		while(tmp!=null) {
			if(data==tmp.getData())
				return tmp;
			else tmp=tmp.link;
		}
		return tmp;
	}
	public void printList() {
		CListNode temp=this.CL;
		System.out.printf("L =(");
		while(temp!=null) {
			System.out.printf(temp.getData());
			temp=temp.link;
			if(temp==this.CL) {
				System.out.println(")");
				break;
			}
			System.out.printf(",");
		}
	}
}
