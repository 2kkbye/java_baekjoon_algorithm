package 실습week_2;

import java.util.Random;
public class week1_time {

	public static void main(String[] args) {
		long start, finish;
		double duration=0.0;
		int list_size = 30000;
		int[] list = new int[list_size];

		//입력데이터 생성 : list에 랜덤데이터 입력
		RandomList(list);
		
		start = System.currentTimeMillis();

		//수행 할 알고리즘 (선택정렬 메서드 호출)
		SelectionSortTime(list);
		
		finish = System.currentTimeMillis();
		duration = (finish-start)/1000.0;

		//시간출력.
		System.out.println(duration);

	}
	public static void SelectionSortTime(int [] array) {
		int min;
		int minIndex;
		int i,j;
		
		for(i=0;i<array.length;i++) {
			min = array[i];
			minIndex = i;
			for(j=i+1;j<array.length;j++) {
				if(min > array[j]) {
					min = array[j];
					minIndex = j;
				}
			}
			array[minIndex] = array[i];
			array[i] = min;
		}
	}
	public static int[] RandomList(int [] list) {
		int tmp = 0;
		Random rnd = new Random();
		
		for(int i = 0; i< list.length;i++) {
			list[i] = i;
		}
		for(int i=0;i< list.length;i++) {
			int dst = rnd.nextInt(list.length);
			tmp = list[i];
			list[i] = list[dst];
			list[dst] = tmp;
		}
		return list;
	}
}
