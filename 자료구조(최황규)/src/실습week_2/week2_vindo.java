package 실습week_2;

import java.util.Random;
public class week2_vindo {

	public static void main(String[] args) {
		int list_size = 30000;
		int count =0;
		int[] list = new int[list_size];
		
		RandomList(list);
		count= SelectionSortCount(list);
		System.out.println(count); 
	}
	public static int[] RandomList(int [] list) {
		int tmp=0;
		Random rnd= new Random();
		
		for(int i=0;i<list.length;i++) //list를 사이즈만큼 초기화시긴다. 각 인덱스로 초기화. 
			list[i]	=i;
		 
		for(int i=0;i<list.length;i++) { 
			int dst= rnd.nextInt(list.length); //dst에 랜덤수를 리스트길이중 하나를 생성해서 저장
			tmp=list[i]; //임시변수 tmp에 list[i]를 저장해놓고
			list[i]=list[dst]; //list[i]에는 그 dst를 인덱스로 가지는 list를 넣음
			list[dst]=tmp; //list[dst]는 tmp를 넣어준다. 
		}
		return list;
	}
	public static int SelectionSortCount(int[] array) {
		int min;
		int minIndex;
		int i,j;
		int comparisonCount=0;
		
		for(i=0;i<array.length;i++) {
			min=array[i];
			minIndex=i;
			for(j=i+1;j<array.length;j++) {
				if(min>array[j]) {
					comparisonCount++;
					min=array[j];
					minIndex=j;
				}
			}
			array[minIndex]=array[i];
			array[i]=min;
		}
		return comparisonCount;
	}

}
