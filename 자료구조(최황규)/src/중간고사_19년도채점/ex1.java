package 중간고사_19년도채점;


public class ex1 {

	public static void main(String[] args) {

		OptExp opt = new OptExp();
		String exp = "(3*5)-(6/2)";

		System.out.println("테스트 중위 수식:" + exp);

		exp = expReverse(exp); // 힌트 1) 수식 역순
		System.out.println(exp);
		char postfix[];

		postfix = opt.toPostfix(exp);	// 힌트 2) toPostfix(변경 없이) 불러 결과 받음
		System.out.println(postfix);
		exp = reverse(postfix);	// 힌트 3) 수식 역순

		System.out.println("\n전위표기식:" + exp);	// 힌트 4) 결과 프린트

	}

	public static String expReverse(String exp) {	// 힌트 1) 수식 역순
		LinkedStack x = new LinkedStack();

		for (int i = 0; i < exp.length(); i++) {
			if (exp.charAt(i) == ')') {
				x.push('(');
			} else if (exp.charAt(i) == '(') {
				x.push(')');
			} else {
				x.push(exp.charAt(i));
			}

		}

		String result = "";

		for (int i = 0; i < exp.length(); i++) {
			result += x.pop();
		}

		return result;
	}

	public static String reverse(char post[]) {	// 힌트 3) 수식 역순
		LinkedStack y = new LinkedStack();

		for (int i = 0; i < post.length; i++) {
			y.push(post[i]);
		}

		String result = "";

		for (int i = 0; i < post.length; i++) {
			result += y.pop();
		}

		return result.trim();
	}

}

