package 중간고사_19년도채점;

class LinkedStack implements Stack {
	private StackNode top;

	public boolean isEmpty() {
		return (top == null);
	}

	public void push(char item) {
		StackNode newNode = new StackNode();
		newNode.data = item;
		newNode.link = top;
		top = newNode;
	}

	public char pop() {
		if (isEmpty()) {
			System.out.println("Pop fail");
			return 0;
		} else {
			char item = top.data;
			top = top.link;
			return item;
		}
	}

	public void delete() {
		if (isEmpty()) {
			System.out.println("Delete fail");
		} else {
			top = top.link;
		}
	}

	public char peek() {
		if (isEmpty()) {
			System.out.println("Peek fail");
			return 0;
		} else {
			return top.data;
		}
	}

}
