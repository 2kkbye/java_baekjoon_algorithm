package 중간고사_19년도채점;

public class OptExp {

	private String exp;
	private int expSize;
	private char testCh, openPair;

	public char[] toPostfix(String infix) {	// 제한사항 2) 교재와 아무런 변경사항 없음
		char testCh;
		exp = infix;
		int expSize = infix.length();
		int j = 0;
		char postfix[] = new char[expSize];
		LinkedStack S = new LinkedStack();

		for (int i = 0; i < expSize; i++) {	// 제한사항 1) 왼쪽에서 오른쪽으로 한번만 읽음
			testCh = this.exp.charAt(i);
			switch (testCh) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				postfix[j++] = testCh;
				break;

			case '+':
			case '-':
			case '*':
			case '/':
				S.push(testCh);
				break;

			case ')':
				postfix[j++] = S.pop();
				break;

			default:
			}
		}

		postfix[j] = S.pop();
		return postfix;
	}
}
