package 단지번호붙이기_2667;

import java.io.*;
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.Arrays;
public class Main {
	static int map[][];
	static boolean visit[][];
	static ArrayList<Integer> a_list;
	static int N,count;
	static int dx[] = {-1,0,1,0};
	static int dy[] = {0,1,0,-1};
	static int temp_ans;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		String in;
		map = new int[N][N];
		visit = new boolean[N][N];
		a_list = new ArrayList<Integer>();
		for(int a = 0;a<N;a++) {
			in = br.readLine();
			for(int b = 0;b<N;b++) {
				map[a][b] = Integer.parseInt(in.charAt(b)+"");
			}
		}
		count = 0;
		temp_ans = 0;
		for(int a = 0;a<N;a++) {
			for(int b = 0;b<N;b++) {
				if(map[a][b] ==0) continue;
				if(visit[a][b]) continue;
				temp_ans = 1;
				count++;
				dfs(a,b);
				a_list.add(temp_ans);
			}
		}
		System.out.println(count);
		int arr[] = new int[a_list.size()];
		for(int a = 0;a<a_list.size();a++) {
			arr[a] = a_list.get(a);
		}
		Arrays.sort(arr);
		for(int a = 0;a<arr.length;a++) {
			System.out.println(arr[a]);
		}
	}
	public static void dfs(int x, int y) {
		visit[x][y] = true;
		for(int a = 0;a<4;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx <0 || ry <0 || rx >=N || ry >=N) continue;
			if(map[rx][ry] == 0) continue;
			if(visit[rx][ry]) continue;
			temp_ans++;
			dfs(rx,ry);
		}
	}

}