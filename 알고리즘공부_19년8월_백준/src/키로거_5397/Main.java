package Ű�ΰ�_5397;

import java.util.LinkedList;
import java.util.ListIterator;
import java.io.*;
import java.util.StringTokenizer;
public class Main {

	static int index;
	static LinkedList<Character> a_list = new LinkedList<Character>();
	static ListIterator<Character> iter = a_list.listIterator();
	static String result[];
	public static void main(String[] args)  throws IOException{
		
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		result = new String[N];
		
		for(int a =0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			a_list = new LinkedList<Character>();
			iter = a_list.listIterator();
			String temp = st.nextToken();
			solve(temp,a);
		}
	}
	public static void solve(String temp,int ind) {
		index = 0;
		for(int a= 0;a<temp.length();a++) {
			insert(temp.charAt(a));
		}
		result(ind);
	}
	public static void insert(char t) {
		int index = iter.nextIndex();
		if(t == '<' || t=='>' || t=='-') {
			if(a_list.isEmpty()) return;
			if(t == '<' || t=='>') {
				int max = a_list.size();
				
				if(t == '<' && index-1 >=0) {
					iter.previous();
				}
				else if(t == '>' && index+1 <=max) {
					iter.next();
				}
			}
			else {
				if(index-1>=0) {
					iter.previous();
					iter.remove();
				}
			}
		}
		else {
			iter.add(t);
		}
	}
	public static void result(int ind){
		StringBuilder answer = new StringBuilder();
		iter = a_list.listIterator(0);
		while (iter.hasNext()) {
			answer.append(iter.next());
		}
		System.out.println(answer);
	}
}