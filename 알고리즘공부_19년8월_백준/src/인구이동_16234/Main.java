package �α��̵�_16234;

import java.io.*;
import java.util.StringTokenizer;
import java.util.Queue;
import java.util.LinkedList;
class Node{
	int x, y;
	public Node(int x, int y) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
	}
}
public class Main {

	static int dx[]= {-1,0,1,0};
	static int dy[]= {0,1,0,-1};

	static int map[][];
	static int copy[][];
	static boolean discovered[][];
	static int N,L,R;
	static Queue<Node> q_pop = new LinkedList<Node>();
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		L = Integer.parseInt(st.nextToken());
		R = Integer.parseInt(st.nextToken());
		map = new int[N][N];
		copy = new int[N][N];
		discovered = new boolean[N][N];
		
		for(int a= 0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			for(int b= 0;b<N;b++) {
				map[a][b] = Integer.parseInt(st.nextToken());
			}
		}
		solve();
	}
	public static void solve() {
		int index = 0;
		boolean flag = true;
		while(flag) {
			flag = false;
			init();
			for(int a = 0;a<N;a++) {
				for(int b= 0;b<N;b++) {
					if(discovered[a][b]) continue;
					if(!check(a,b)) continue;
					flag = true;
					discovered[a][b] = true;
					q_pop.add(new Node(a, b));
					bfs();
				}
			}
			index++;
		}
		System.out.println(index-1);
	}
	public static void bfs() {
		int sum = 0,cnt = 0;
		while(!q_pop.isEmpty()) {
			Node n = q_pop.remove();
			int px = n.x;
			int py = n.y;
			cnt++;
			sum +=copy[px][py];
			for(int a =0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx<0 || ry<0 || rx>=N || ry>=N) continue;
				if(discovered[rx][ry]) continue;
				if(Math.abs(copy[rx][ry]-copy[px][py])<L || Math.abs(copy[rx][ry]-copy[px][py])>R) continue;
				q_pop.add(new Node(rx, ry));
				discovered[rx][ry] = true;
			}
		}
		int ins_num = sum/cnt;
		for(int a=0;a<N;a++) {
			for(int b =0;b<N;b++) {
				if(!discovered[a][b]) continue;
				if(discovered[a][b] && copy[a][b] ==-1) continue;
				map[a][b] = ins_num;
				copy[a][b] = -1;
			}
		}
	}
	public static boolean check(int x, int y) {
		for(int a =0;a<4;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx<0 || ry<0 || rx>=N || ry>=N) continue;
			if(discovered[rx][ry]) continue;;
			if(Math.abs(copy[rx][ry]-copy[x][y])<L || Math.abs(copy[rx][ry]-copy[x][y])>R) continue;
			return true;
		}
		return  false;
	}
	public static void init() {
		for(int a= 0;a<N;a++) {
			for(int b= 0;b<N;b++) {
				copy[a][b] = map[a][b];
				discovered[a][b] = false;
			}
		}
	}
}