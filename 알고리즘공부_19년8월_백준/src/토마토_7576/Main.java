package �丶��_7576;

import java.io.*;
import java.util.StringTokenizer;
import java.util.Queue;
import java.util.LinkedList;

class Pair{
	int x, y,z;
	public Pair(int x, int y,int z) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
		this.z = z;
	}
}
public class Main {
	static int map[][][];
	static boolean discovered[][][];
	static int dx[] = {0,0,0,0,1,-1};
	static int dh[] = {-1,+1,0,0,0,0};
	static int dy[] = {0,0,1,-1,0,0};
	
	static int N,M,H;
	static int ans;

	static Queue<Pair> q_tom;
	static Queue<Pair> q_n_tom;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		M = Integer.parseInt(st.nextToken());
		N = Integer.parseInt(st.nextToken());
		H = Integer.parseInt(st.nextToken());
		
		map = new int[N][M][H];
		discovered = new boolean[N][M][H];

		q_tom = new LinkedList<Pair>();
		q_n_tom = new LinkedList<Pair>();

		for(int k = 0;k<H;k++) {
			for(int a = 0;a<N;a++) {
				st = new StringTokenizer(br.readLine());
				for(int b = 0;b<M;b++) {
					int temp = Integer.parseInt(st.nextToken());
					if(temp == 1) {
						q_tom.add(new Pair(a, b, k));
						discovered[a][b][k] = true;
					}
					map[a][b][k] = temp;
				}
			}
		}

		ans = -1;
		for(int a = 0;a<10000000;a++) {
			if(check_tomato()) {
				if(a==0) {
					ans = 0;break;
				}
				ans = a;break;
			}
			spread_tomato();
			copy_tomato();
			if(q_tom.size()==0) break;
		}
		System.out.println(ans);
	}
	public static void copy_tomato() {
		int size = q_n_tom.size();
		for(int a =0;a<size;a++) {
			Pair p = q_n_tom.remove();
			q_tom.add(new Pair(p.x, p.y,p.z));
		}
	}
	public static void spread_tomato() {
		int size = q_tom.size();
		for(int i = 0;i<size;i++) {
			Pair p = q_tom.remove();
			int px = p.x;
			int py = p.y;
			int ph = p.z;
			map[px][py][ph] = 1;
			
			for(int a = 0;a<6;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				int rh = ph+dh[a];
				if(rx<0||ry<0 || rh<0 ||rx>=N|| ry>=M || rh >=H) continue;
				if(discovered[rx][ry][rh]) continue;
				if(map[rx][ry][rh] == -1|| map[rx][ry][rh]==1) continue;
				discovered[rx][ry][rh] = true;
				map[rx][ry][rh] = 1;
				q_n_tom.add(new Pair(rx, ry, rh));
			}
		}
	}
	public static boolean check_tomato() {
		for(int k = 0;k<H;k++) {
			for(int a = 0;a<N;a++) {
				for(int b = 0;b<M;b++) {
					if(map[a][b][k]==1) continue;
					if(map[a][b][k]==-1) continue;
					return false;
				}
			}
		}
		return true;
	}
}

