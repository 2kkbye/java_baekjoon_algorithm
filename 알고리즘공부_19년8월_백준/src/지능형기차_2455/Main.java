package 지능형기차_2455;

import java.io.*;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		//int arr[] = new int[4];
		int bus = 0;
		int result = -1;
		for(int a = 0;a<4;a++) {
			st = new StringTokenizer(br.readLine());
			int take_off = Integer.parseInt(st.nextToken());
			int ride = Integer.parseInt(st.nextToken());
			bus -= take_off;
			bus += ride;
			result = Math.max(result, bus);
		}
		System.out.println(result);
	}

}
