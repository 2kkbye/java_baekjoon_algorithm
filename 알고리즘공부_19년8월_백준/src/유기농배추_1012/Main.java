package ��������_1012;

import java.io.*;
import java.util.StringTokenizer;

public class Main {

	static int map[][];
	static boolean visit[][];
	static int dx[]= {-1,0,1,0};
	static int dy[]= {0,1,0,-1};
	static int count,N,M;
	static int result[];

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int T = Integer.parseInt(st.nextToken());
		result  = new int[T];
		for(int i = 0;i<T;i++) {
			count = 0;
			st = new StringTokenizer(br.readLine());
			M = Integer.parseInt(st.nextToken());
			N = Integer.parseInt(st.nextToken());
			map = new int[N][M];
			visit = new boolean[N][M];
			int size = Integer.parseInt(st.nextToken());
			//init();
			for(int a = 0;a<size;a++) {
				st = new StringTokenizer(br.readLine());
				int y = Integer.parseInt(st.nextToken());
				int x = Integer.parseInt(st.nextToken());
				map[x][y] = 1;
			}
			for(int a = 0;a<N;a++) {
				for(int b=0;b<M;b++) {
					if(map[a][b] ==0) continue;
					if(visit[a][b]) continue;
					count++;
					dfs(a,b);
				}
			}
			result[i] = count;
		}
		for(int a = 0;a<result.length;a++){
			System.out.println(result[a]);
		}
	}
	public static void dfs(int x, int y) {
		visit[x][y] = true;
		for(int a =0;a<4;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx<0 || ry<0 || rx>=N || ry>=M) continue;
			if(map[rx][ry]==0) continue;
			if(visit[rx][ry]) continue;
			dfs(rx,ry);
		}
	}
//	public static void init() {
//		for(int a =0;a<N;a++) {
//			for(int b = 0;b<M;b++) {
//				map[a][b] = 0;
//				visit[a][b] = false;
//			}
//		}
//	}
}