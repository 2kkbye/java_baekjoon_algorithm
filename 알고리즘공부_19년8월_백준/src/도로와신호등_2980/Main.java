package 도로와신호등_2980;

import java.io.*;
import java.util.StringTokenizer;
import java.util.LinkedList;

class Node{
	int loc, R,G,temp_R, temp_G, status;
	public Node(int loc, int R, int G, int temp_R, int temp_G, int status) {
		this.loc = loc;
		this.R = R;
		this.G = G;
		this.temp_R = temp_R;
		this.temp_G = temp_G;
		this.status = status;
	}
}

public class Main {

	static LinkedList<Node> l_color = new LinkedList<Node>();

	static int N, L;
	static int d = 1,index;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());

		N = Integer.parseInt(st.nextToken());
		L = Integer.parseInt(st.nextToken());

		int D,R,G;
		for(int a = 0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			D = Integer.parseInt(st.nextToken());
			R = Integer.parseInt(st.nextToken());
			G = Integer.parseInt(st.nextToken());
			l_color.add(new Node(D, R, G, R, G, 0));
		}
		int ans = 0;
		for(int a = 1;a<1000000;a++) {
			
			if(!check_move()) {
				d++;
				minus_time();
				if(d==L) {
					ans = a;
					break;
				}
				continue;
			}
			else {
				if(!move()) {
					minus_time();
				}
				else {
					d++;
					minus_time();
				}
			}
			if(d==L) {
				ans = a;
				break;
			}
		}
		System.out.println(ans);
	}
	public static boolean move() {
		if(l_color.get(index).loc == d) {
			if(l_color.get(index).status == 0) return false;
			else return true;
		}

		return false;
	}
	public static boolean check_move() {
		for(int a = 0;a<l_color.size();a++) {
			if(l_color.get(a).loc > d) return false;
			if(l_color.get(a).loc == d) {
				index = a;
				return true;
			}
		}
		return false;
	}
	public static void minus_time() {
		for(int a = 0;a<l_color.size();a++) {
			if(l_color.get(a).status == 0) { //빨강
				l_color.get(a).temp_R--;
				if(l_color.get(a).temp_R == 0) {
					l_color.get(a).status = 1;
					l_color.get(a).temp_R = l_color.get(a).R;
				}
			}
			else { //초록
				l_color.get(a).temp_G--;
				if(l_color.get(a).temp_G == 0) {
					l_color.get(a).status = 0;
					l_color.get(a).temp_G = l_color.get(a).G;
				}
			}
		}
	}
}