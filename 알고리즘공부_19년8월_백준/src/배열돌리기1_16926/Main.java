package 배열돌리기1_16926;

import java.io.*;
import java.util.StringTokenizer;
import java.util.ArrayList;
public class Main {

	static int N,M;
	static int map[][];
	static int copy[][];
	static int R;
	static int dx[]= {0,1,0,-1};
	static int dy[]= {-1,0,1,0};
	static boolean flag;
	static int e_x,e_y,s_x,s_y,num_count;
	static ArrayList<Integer> a_list = new ArrayList<Integer>();
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		R = Integer.parseInt(st.nextToken());

		map = new int[N][M];
		copy = new int[N][M];
		
		for(int a =0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			for(int b =0;b<M;b++) {
				map[a][b] = Integer.parseInt(st.nextToken());
			}
		}
		int min = Math.min(M, N);
		for(int a = 0;a<min/2;a++) {
			flag = false;
			s_x = a;s_y = a;
			e_x = a;e_y = a;
			a_list.add(map[a][a]);
			dfs(a+1,a,1,1);
			move(a);
		}
		for(int a =0;a<N;a++) {
			for(int b = 0;b<M;b++) {
				System.out.print(copy[a][b]+" ");
			}
			System.out.println();
		}
	}
	public static void move(int a) {
		int check = R%num_count;
		if(check == 0) check = R;
		f = false;
		dfs2(a+1,a,1,check,1,false);
	}
	static boolean f = false;
	public static void dfs2(int x,int y,int level,int check,int dir,boolean temp) {
		if(temp && x == e_x && y==e_y) {
			f = true;
			return;
		}
		if(level == check && !temp) {
			e_x = x;
			e_y = y;
			temp = true;
			//dfs2(x,y,level,check,dir,temp);
		}
		if(temp) {
			copy[x][y] = a_list.remove(0);
		}
		int a = dir;
		while(!f) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx < 0+s_x || ry < 0+s_y || rx >=N-s_x || ry>=M-s_y) {
				a++;
				if(a==4) a=0;
				continue;
			}
			dfs2(rx,ry,level+1,check,a,temp);
		}
	}
	public static void dfs(int x, int y,int level,int dir) {
		if(flag) return;
		if(x == s_x && y== s_y) {
			flag = true;
			num_count = level;
			return;
		}
		a_list.add(map[x][y]);
		int a = dir;
		while(!flag) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx < 0+s_x || ry < 0+s_y || rx >=N-s_x || ry>=M-s_y) {
				a++;
				if(a==4) a=0;
				continue;
			}
			dfs(rx,ry,level+1,a);
		}
	}
}