package ġŲ���_15686;

import java.io.*;
import java.util.StringTokenizer;
import java.util.LinkedList;
import java.util.ArrayList;
class Node{
	int x, y;
	public Node(int x, int y) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
	}
}
public class Main {
	
	static int min_dist[];
	static boolean visit[];
	static int N,M,ans = Integer.MAX_VALUE;
	static ArrayList<Node> a_home = new ArrayList<Node>();
	static LinkedList<Node> l_chicken = new LinkedList<Node>();
	
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		
		for(int a = 0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			for(int b = 0;b<N;b++) {
				int temp = Integer.parseInt(st.nextToken());
				if(temp == 1) a_home.add(new Node(a, b));
				else if(temp ==0 ) continue;
				else l_chicken.add(new Node(a, b));
			}
		}
		min_dist = new int[a_home.size()];
		visit = new boolean[l_chicken.size()];
		for(int a = 0;a<l_chicken.size();a++) {
			visit[a] = true;
			dfs(a+1,1);
			visit[a] = false;
		}
		System.out.println(ans);
	}
	public static void dfs(int x,int level) {
		if(level == M) {
			solve();
			return;
		}
		for(int a = x;a<l_chicken.size();a++) {
			visit[a] = true;
			dfs(a+1,level+1);
			visit[a] = false;
		}
	}
	public static void solve() {
		init();
		for(int a =0;a<min_dist.length;a++) {
			for(int b = 0;b<l_chicken.size();b++) {
				if(!visit[b]) continue;
				min_dist[a] =  Math.min(min_dist[a], Math.abs(l_chicken.get(b).x - a_home.get(a).x) +Math.abs(l_chicken.get(b).y - a_home.get(a).y)); 
			}
		}
		int sum = 0;
		for(int a =0;a<min_dist.length;a++) {
			sum += min_dist[a];
		}
		ans = Math.min(sum, ans);
	}
	public static void init() {
		for(int a = 0;a<min_dist.length;a++) {
			min_dist[a] = Integer.MAX_VALUE;
		}
	}
}