package ��_1547;

import java.io.*;
import java.util.StringTokenizer;

public class Main {

	static int cup[] = {0,1,2,3};
	static int location[] = {0,1,2,3};
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		for(int a =0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			int x = Integer.parseInt(st.nextToken());
			int y = Integer.parseInt(st.nextToken());
			solve(x,y);
		}
		System.out.println(location[1]);
	}
	public static void solve(int x, int y) {
		int x_loc = cup[x];
		int y_loc = cup[y];
		location[x_loc] = y;
		location[y_loc] = x;
		cup[x] = y_loc;
		cup[y] = x_loc;
	}

}
