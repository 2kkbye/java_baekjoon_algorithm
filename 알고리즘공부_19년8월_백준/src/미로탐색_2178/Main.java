package �̷�Ž��_2178;

import java.io.*;
import java.util.StringTokenizer;
import java.util.Queue;
import java.util.LinkedList;
class Pair{
	int x, y;
	public Pair(int x, int y) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
	}
}
public class Main {
	
	static int dx[] = {-1,0,1,0};
	static int dy[] = {0,1,0,-1};
	static int map[][];
	static int copy[][];
	static int N,M;
	static boolean discovered[][];
	
	static Queue<Pair> q_miro;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N =Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		map = new int[N][M];
		copy = new int[N][M];
		discovered = new boolean[N][M];
		q_miro = new LinkedList<Pair>();
		
		for(int a = 0;a<N;a++) {
			String in = br.readLine();
			for(int b = 0;b<M;b++) {
				map[a][b] = Integer.parseInt(in.charAt(b)+"");
			}
		}
		copy[0][0] = 1;
		discovered[0][0] = true;
		q_miro.add(new Pair(0, 0));
		bfs();
		System.out.println(copy[N-1][M-1]);
	}
	public static void bfs() {
		while(!q_miro.isEmpty()) {
			Pair p = q_miro.remove();
			int px = p.x;
			int py = p.y;
			
			if(px == N-1 && py == M-1) break;
			
			for(int a = 0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx <0 || ry <0 || rx >=N || ry>=M) continue;
				if(map[rx][ry] == 0 ) continue;
				if(discovered[rx][ry]) continue;
				copy[rx][ry] = copy[px][py]+1; 
				q_miro.add(new Pair(rx, ry));
				discovered[rx][ry] = true;
			}
		}
	}
}
