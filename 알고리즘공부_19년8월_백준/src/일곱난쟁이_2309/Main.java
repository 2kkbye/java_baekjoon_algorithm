package �ϰ�������_2309;

import java.io.*;
import java.util.StringTokenizer;

public class Main {
	static int number[] = new int[9];
	static boolean visit[] = new boolean[9];
	static int sort_number[] = new int[7];
	static boolean flag;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		for(int a = 0;a<number.length;a++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			number[a] = Integer.parseInt(st.nextToken());
		}
		flag = false;
		dfs(0,0);

	}
	public static void dfs(int x, int level) {
		if(flag) return;
		if(level == 7) {
			if(!check_sum()) return;
			copy_num();
			sort(sort_number,0, sort_number.length-1);
			print_num();
			flag = true;
			return;
		}
		for(int a = x;a<number.length;a++) {
			if(visit[a]) continue;
			visit[a] = true;
			dfs(a+1,level+1);
			visit[a] = false;
		}

	}
	public static void sort(int[] data, int l, int r) {
		int left = l;
		int right = r;
		int pivot = data[(l + r) / 2]; // pivot ��� ���� (�־��� ��� ����)

		do {
			while (data[left] < pivot)
				left++;
			while (data[right] > pivot)
				right--;

			if (left <= right) {
				//System.out.println("change");
				int temp = data[left];
				data[left] = data[right];
				data[right] = temp;
				left++;
				right--;
			}
		} while (left <= right);
		if (l < right) {
			sort(data, l, right);
		}
		if (r > left) {
			sort(data, left, r);
		}
	}
	public static void copy_num() {
		int i = 0;
		int index = 0;
		while(i<9) {
			if(!visit[i]) {
				i++; continue;
			}
			sort_number[index++] = number[i++];
		}
	}
	public static void print_num() {
		for(int a = 0;a<sort_number.length;a++) {
			System.out.println(sort_number[a]);
		}
	}
	public static boolean check_sum() {
		int sum = 0;
		for(int a = 0;a<visit.length;a++) {
			if(!visit[a])continue;
			sum +=number[a];
		}
		if(sum == 100) return true;
		else return false;
	}
}