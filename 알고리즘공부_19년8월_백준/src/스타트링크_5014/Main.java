package 스타트링크_5014;

import java.io.*;
import java.util.Queue;
import java.util.LinkedList;

public class Main {
	static boolean discovered[];
	static int map[];
	static Queue<Integer> q_person;
	static int F,S,G,U,D;
	
	static int ans = Integer.MAX_VALUE;
	static int up_down[] = new int[2];
	
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		String arr[] = in.split(" ");
		for(int a = 0;a<arr.length;a++) {
			if(a == 0) {F = Integer.parseInt(arr[a]); continue;}
			else if(a == 1) {S = Integer.parseInt(arr[a]); continue;}
			else if(a == 2) {G = Integer.parseInt(arr[a]); continue;}
			else if(a == 3) {U = Integer.parseInt(arr[a]);up_down[0] = U; continue;}
			else if(a == 4) {D = Integer.parseInt(arr[a]);up_down[1] = -1*D; continue;}
		}
		q_person = new LinkedList<Integer>();
		
		map = new int[F+1];
		discovered = new boolean[F+1];
		q_person.add(S);
		discovered[S] = true;
		bfs();
		if(ans == Integer.MAX_VALUE) {
			System.out.println("use the stairs");
		}
		else {
			System.out.println(ans);
		}
	}
	public static void bfs() {
		while(!q_person.isEmpty()) {
			int stair = q_person.remove();
			
			if(stair == G) {
				ans = Math.min(ans, map[stair]);
				continue;
			}
			for(int a= 0;a<2;a++) {
				int n_stair = stair + up_down[a];
				if(n_stair <1 || n_stair >F) continue;
				if(discovered[n_stair]) continue;
				q_person.add(n_stair);
				discovered[n_stair] = true;
				map[n_stair] = map[stair]+1;
			}
		}
	}
}