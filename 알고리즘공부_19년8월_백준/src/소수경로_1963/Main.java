package �Ҽ����_1963;

import java.io.*;
import java.util.StringTokenizer;
import java.util.Queue;
import java.util.LinkedList;

class Pair{
	int num, change_cnt;
	public Pair(int num, int change_cnt) {
		// TODO Auto-generated constructor stub
		this.num = num;
		this.change_cnt = change_cnt;
	}
}
public class Main {
	
	static boolean discovered[] = new boolean[10000];
	static boolean prime_num[] = new boolean[10000];
	static int number[][];
	static int result[];
	static Queue<Pair> q_num;
	
	public static void main(String[] args) throws IOException{
		
		for(int a = 2;a<prime_num.length;a++) {
			if(prime_num[a]) continue;
			//System.out.println(a);
			prime_num_check(a);
		}
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int T = Integer.parseInt(st.nextToken());
		number = new int[T][2];
		result = new int[T];
		init(T);
		
		for(int i=0;i<T;i++) {
			q_num = new LinkedList<Pair>();
			st = new StringTokenizer(br.readLine());
			number[i][0] =Integer.parseInt(st.nextToken());
			number[i][1] =Integer.parseInt(st.nextToken());
			init();
			q_num.add(new Pair(number[i][0], 0));
			discovered[number[i][0]] = true;
			bfs(number[i][1],i);
		}
		for(int a= 0;a<result.length;a++) {
			if(result[a]==Integer.MAX_VALUE) System.out.println("Impossible");
			else System.out.println(result[a]);
			
		}
		
	}
	public static void bfs(int result_num, int T) {
		while(!q_num.isEmpty()) {
			Pair p = q_num.remove();
			int check_num = p.num;
			int check_cnt = p.change_cnt;
			if(check_num == result_num) {
				result[T] = Math.min(result[T], check_cnt);
				break;
			}
			change_num(check_num, check_cnt);
		}
	}
	public static void change_num(int num, int cnt) {
		String n = ""+num;
		int temp[] = new int[4];
		for(int a = 0;a<4;a++) {
			temp[a] = Integer.parseInt(n.charAt(a)+"");
		}
		for(int a = 0;a<4;a++) {
			int now_num = temp[a];
			for(int b = 0;b<=9;b++) {
				if(a == 0 && b==0) continue;
				if(b == now_num) continue;
				temp[a] = b;
				String t_num = "";
				int t_num2;
				for(int c = 0;c<4;c++) {
					t_num = t_num+temp[c];
				}
				t_num2 = Integer.parseInt(t_num);
				//System.out.println(t_num2);
				if(!discovered[t_num2] && !prime_num[t_num2]) {
					q_num.add(new Pair(t_num2, cnt+1));
					discovered[t_num2] = true;
				}
			}
			temp[a] = now_num;
		}
	}
	public static void init(int T) {
		for(int a = 0;a<T;a++) {
			result[a] = Integer.MAX_VALUE;
		}
	}
	public static void init() {
		for(int a= 2;a<discovered.length;a++) {
			discovered[a] = false;
		}
	}
	public static void prime_num_check(int num) {
		for(int a = num*2;a<prime_num.length;a +=num) {
			prime_num[a] = true;
		}
	}
}