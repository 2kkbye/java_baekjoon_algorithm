package ������_2798;

import java.io.*;
import java.util.StringTokenizer;
public class Main {

	static int N,M;
	static int num[];
	static int ans = 0,sum;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		num = new int[N];
		int temp;
		st = new StringTokenizer(br.readLine());
		for(int a = 0;a<N;a++) {
			temp = Integer.parseInt(st.nextToken());
			num[a] = temp;
		}
		for(int a = 0;a<N-2;a++) {
			sum +=num[a];
			dfs(a+1,1);
			sum -= num[a];
		}
		System.out.println(ans);
	}
	public static void dfs(int index, int level) {
		if(level == 3) {
			if(sum <= M) {
				ans = Math.max(ans, sum);
			}
			return;
		}
		for(int a = index;a<N;a++) {
			sum += num[a];
			dfs(a+1,level+1);
			sum -= num[a];
		}
	}
}