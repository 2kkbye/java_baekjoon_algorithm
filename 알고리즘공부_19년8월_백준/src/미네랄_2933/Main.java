package �̳׶�_2933;

import java.io.*;
import java.util.StringTokenizer;
import java.util.Queue;
import java.util.LinkedList;
class Node{
	int x,y;
	public Node(int x, int y) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
	}
}
public class Main {
	static int map[][];
	static int copy[][];
	static boolean discovered[][];
	static int shot[];
	static int R,C,N;
	static int dx[]= {-1,0,1,0};
	static int dy[]= {0,1,0,-1};

	static Queue<Node> q_mineral = new LinkedList<Node>();
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		R = Integer.parseInt(st.nextToken());
		C = Integer.parseInt(st.nextToken());
		map = new int[R][C];
		copy = new int[R][C];
		discovered = new boolean[R][C];

		for(int a =0;a<R;a++) {
			st = new StringTokenizer(br.readLine());
			String str = st.nextToken();
			for(int b = 0;b<C;b++) {
				int temp = 0;
				if(str.charAt(b)=='x') temp = 1; //�̳׶�
				map[a][b] = temp;
			}
		}
		st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		st = new StringTokenizer(br.readLine());
		shot = new int[N];
		for(int a =0;a<N;a++) {
			shot[a] = Integer.parseInt(st.nextToken())-1;
		}
		solve();
		print();
	}
	public static void solve() {
		for(int a = 0;a<shot.length;a++) {
			//print();
			if(a%2== 0) shot((R-1)-shot[a],0);
			else shot((R-1)-shot[a],1);
			if(check()) {init();continue;}
			down_block();
			init();
		}
	}
	public static void init() {
		for(int a= 0;a<R;a++) {
			for(int b =0;b<C;b++) {
				discovered[a][b] = false;
				copy[a][b] = 0;
			}
		}
	}
	public static void down_block() {
		int min = Integer.MAX_VALUE;
		for(int a = R-1;a>=0;a--) {
			for(int b =0;b<C;b++) {
				if(copy[a][b] == 0) continue;
				int rx = a,ry = b;
				for(int c = 1;c<=R;c++) {
					rx = rx+dx[2];
					ry = ry+dy[2];
					if(rx>=R ||discovered[rx][ry] ) {
						min = Math.min(min, c-1);break;
					}
				}
			}
		}
		for(int a = R-1;a>=0;a--) {
			for(int b =0;b<C;b++) {
				if(copy[a][b] == 0) continue;
				map[a][b] = 0;
				map[a+min][b] = copy[a][b];
			}
		}
	}
	public static boolean check() {
		boolean flag = true;
		for(int b = 0;b<C;b++) {
			if(map[R-1][b] == 0) continue;
			if(discovered[R-1][b]) continue;
			discovered[R-1][b] =true;
			q_mineral.add(new Node(R-1, b));
			bfs();
		}

		for(int a =0;a<R;a++) {
			for(int b =0;b<C;b++) {
				if(map[a][b] == 0 )continue;
				if(discovered[a][b]) continue;
				flag = false;
				copy[a][b] = map[a][b];
			}
		}
		return flag;
	}
	public static void bfs() {
		Node n;
		while(!q_mineral.isEmpty()) {
			n = q_mineral.remove();
			int px = n.x;
			int py = n.y;

			for(int a = 0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx< 0 || ry <0 || rx>=R || ry>=C) continue;
				if(discovered[rx][ry] || map[rx][ry]!=1) continue;
				discovered[rx][ry] = true;
				q_mineral.add(new Node(rx, ry));
			}

		}
	}
	public static void shot(int index,int sw) {
		if(sw == 0) { //���ʺ���
			for(int a = 0;a<C;a++) {
				if(map[index][a]==0) continue;
				map[index][a] = 0;
				break;
			}
		}
		else { //�����ʺ���
			for(int a = C-1;a>=0;a--) {
				if(map[index][a]==0) continue;
				map[index][a] = 0;
				break;
			}
		}
	}
	public static void print() {
		for(int a =0;a<R;a++) {
			for(int b= 0;b<C;b++) {
				if(map[a][b] == 0) {
					System.out.print('.');
				}
				else {
					System.out.print('x');
				}
			}
			System.out.println();
		}
	}
}
