package ���ÿ�_17143;

import java.io.*;
import java.util.StringTokenizer;
import java.util.ArrayList;
class Shark{
	int x,y,s,d,z;
	public Shark(int x, int y, int s, int d, int z) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
		this.s = s;
		this.d = d;
		this.z = z;
	}
}
public class Main {

	static int R,C,M,ans = 0;
	static int map[][];
	static ArrayList<Shark> l_shark = new ArrayList<Shark>();
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,1,-1};
	
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		R = Integer.parseInt(st.nextToken());
		C = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		map = new int[R][C];
		for(int a = 0;a<M;a++) {
			st = new StringTokenizer(br.readLine());
			int x = Integer.parseInt(st.nextToken())-1;
			int y = Integer.parseInt(st.nextToken())-1;
			int s = Integer.parseInt(st.nextToken()); //�ӷ�
			int d = Integer.parseInt(st.nextToken())-1; //����
			int z = Integer.parseInt(st.nextToken()); //ũ��
			map[x][y] = 1;
			l_shark.add(new Shark(x, y, s, d, z));
		}
		for(int a = 0;a<C;a++) {
			kill_shark(a);
			move();
			check_map();
		}
		System.out.println(ans);
	}

	public static void check_map() {
		
		for(int a = 0;a<R;a++) {
			for(int b = 0;b<C;b++) {
				if(map[a][b]==0 || map[a][b]==1) continue;
				int size = l_shark.size();
				int max_z = -1,max_d =-1,max_s=-1;
				for(int c = size-1;c>=0;c--) {
					if(l_shark.get(c).x != a || l_shark.get(c).y != b) continue;
					Shark s = l_shark.remove(c);
					if(s.z<max_z) continue;
					max_z = s.z;max_d = s.d;max_s = s.s;
				}
				l_shark.add(new Shark(a, b, max_s, max_d, max_z));
				map[a][b] = 1;
			}
		}
	}
	public static void move() {
		
		for(int a = 0;a<l_shark.size();a++) {
			int x = l_shark.get(a).x;
			int y = l_shark.get(a).y;
			int s = l_shark.get(a).s;
			int d = l_shark.get(a).d;
			map[x][y] -=1;
			int size = 0;
			if(d == 0 || d==1) {
				size = (R*2)-2;
			}
			else {
				size = (C*2)-2;
			}
			
			int rx = x,ry = y,rs=s%size;
			while(rs!=0) {
				rx = rx+dx[d];
				ry = ry+dy[d];
				if(rx <0 || ry<0 || rx>=R || ry>=C) {
					rx = rx-dx[d];
					ry = ry-dy[d];
					if(d %2 ==0) d +=1;
					else d -=1;
					continue;
				}
				rs--;
			}
			l_shark.get(a).x = rx;
			l_shark.get(a).y = ry;
			l_shark.get(a).d = d;
			map[rx][ry] +=1;
		}
	}
	public static void kill_shark(int index) {
		for(int a = 0;a<R;a++) {
			if(map[a][index]==0) continue;
			for(int b = 0;b<l_shark.size();b++) {
				if(l_shark.get(b).x == a && l_shark.get(b).y == index) {
					ans += l_shark.get(b).z;
					l_shark.remove(b);
					map[a][index] = 0;
					return;
				}
			}
		}
	}
}