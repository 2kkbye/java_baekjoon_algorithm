package 사다리조작_15684;

import java.io.*;
import java.util.StringTokenizer;


public class Main {
	
	static int dy[]= {1,-1};
	static int map[][];
	static int N,M,H,ans = Integer.MAX_VALUE;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = (Integer.parseInt(st.nextToken())*2)-1;
		M = Integer.parseInt(st.nextToken());
		H = Integer.parseInt(st.nextToken());
		map = new int[H+1][N];
		int x,y;
		for(int a =0;a<M;a++) {
			st = new StringTokenizer(br.readLine());
			x = Integer.parseInt(st.nextToken())-1;
			y = (Integer.parseInt(st.nextToken())*2)-1;
			map[x][y] = 1;
		}
		for(int a = 0;a<H;a++) {
			if(check(0)) break;
			for(int b = 1;b<N;b+=2) {
				if(map[a][b] == 1) continue;
				map[a][b] = 1;
				dfs(a,1);
				map[a][b] = 0;
			}
		}
		if(ans == Integer.MAX_VALUE) {
			System.out.println(-1);
		}
		else {
			System.out.println(ans);
		}
	}
	public static void dfs(int x, int level) {
		if(level>=4) return;
		if(ans<level) return;
		if(check(level)) {
			return;
		}
		for(int a = x;a<H;a++) {
			for(int b = 1;b<N;b+=2) {
				if(map[a][b] ==1 ) continue;
				map[a][b] = 1;
				dfs(a,level+1);
				map[a][b] = 0;
			}
		}
	}
	public static boolean check(int level) {
		for(int a = 0;a<N;a+=2) {
			int x = 0,y = a;
			while(true) {
				if(y+dy[0]<N && map[x][dy[0]+y] ==1) {
					y = y+2;
					x= x+1;
					continue;
				}
				else if(y+dy[1]>=0 && map[x][dy[1]+y] ==1) {
					y = y-2;
					x = x+1;
					continue;
				}
				if(x == H) {
					if(y != a) return false; 
					break;
				}
				x = x+1;
			}
		}
		ans = Math.min(level, ans);
		return true;
	}
}
