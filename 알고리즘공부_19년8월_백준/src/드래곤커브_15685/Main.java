package �巡��Ŀ��_15685;

import java.io.*;
import java.util.StringTokenizer;
import java.util.ArrayList;

public class Main {

	static int dx[]= {0,-1,0,1};
	static int dy[]= {1,0,-1,0};
	
	static int x,y,d,g,ans = 0;
	static ArrayList<Integer> l_dir = new ArrayList<Integer>();
	static boolean visit[][] = new boolean[101][101];
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		for(int a= 0; a<N; a++) {
			st = new StringTokenizer(br.readLine());
			y = Integer.parseInt(st.nextToken());
			x = Integer.parseInt(st.nextToken());
			d = Integer.parseInt(st.nextToken());
			g = Integer.parseInt(st.nextToken());
			visit[x][y] = true;
			solve(x,y,g,d);
		}
		for(int a = 0;a<100;a++) {
			for(int b =0 ;b<100;b++) {
				if(visit[a][b] && visit[a+1][b] && visit[a][b+1] && visit[a+1][b+1]) ans++;
			}
		}
		System.out.println(ans);
	}
	public static void solve(int x, int y,int g, int d) {
		l_dir.add(d);
		int size,dir;
		for(int a = 0;a<g;a++) {
			size = l_dir.size()-1;
			for(int b = size;b>=0;b--) {
				dir = l_dir.get(b)+1;
				if(dir == 4) dir = 0;
				l_dir.add(dir);
			}
		}
		for(int a = 0;a<l_dir.size();a++) {
			int index = l_dir.get(a);
			x = x+dx[index];
			y = y+dy[index];
			if(x<0 || y<0 || x>101 || y>101) continue;
			visit[x][y] = true;
		}
		l_dir.removeAll(l_dir);
	}
}
