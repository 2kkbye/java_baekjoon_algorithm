package 회전하는큐_1021;

import java.io.*;
import java.util.StringTokenizer;
import java.util.LinkedList;

class Node{
	int data,left,right;
	public Node(int data) {
		// TODO Auto-generated constructor stub
		this.data = data;
		left = 0;
		right = 0;
	}
}
public class Main {

	static LinkedList<Node> a_list = new LinkedList<Node>();
	static int findNum[];
	static int findNum_ind = 0;
	static int index_num = 0;
	static int ans = 0;
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		findNum = new int[M];
		st = new StringTokenizer(br.readLine());
		for(int a = 0;a<M;a++) {
			findNum[a] = Integer.parseInt(st.nextToken());
		}
		
		for(int a = 1;a<=N;a++) {
			a_list.add(new Node(a));
		}
		while(findNum_ind<findNum.length) {
			index_init();
			if(findNum(findNum[findNum_ind])==1) { //왼쪽
				left(index_num);
			}
			else {
				right(index_num);
			}
		}
		System.out.println(ans);
		
	}
	public static int findNum(int num) {
		for(int a = 0;a<a_list.size();a++) {
			if(a_list.get(a).data !=num) continue;
			if(a_list.get(a).left<=a_list.get(a).right) {
				index_num = a_list.get(a).left;
				ans += index_num;
				return 1;
			}
			else {
				index_num = a_list.get(a).right;
				ans += index_num;
				return 2;
			}
		}
		return 0;
	}
	public static void left(int index) {
		int num = 0;
		for(int a = 0;a<index;a++) {
			num = a_list.remove(0).data;
			a_list.add(new Node(num));
		}
		a_list.remove(0);
		findNum_ind++;
	}
	public static void right(int index) {
		int num = 0;
		for(int a = 0;a<index;a++) {
			num = a_list.remove(a_list.size()-1).data;
			a_list.add(0,new Node(num));
		}
		a_list.remove(0);
		findNum_ind++;
	}
	public static void index_init() {
		int size = a_list.size();
		int left = 0;
		for(int a = 0;a<a_list.size();a++) {
			a_list.get(a).left = left;
			a_list.get(a).right = size-left;
			left++;
		}
	}
}