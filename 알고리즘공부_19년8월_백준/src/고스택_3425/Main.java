package ������_3425;

import java.io.*;
import java.util.StringTokenizer;
import java.util.ArrayList;

public class Main {

	static String ins[] = new String[100000+1];
	static long num[] = new long[10000+1];

	static ArrayList<Long> a_list = new ArrayList<Long>();
	static int N;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		while(true) {
			int input_ind = 0;
			while(true) {
				st = new StringTokenizer(br.readLine());
				String input = st.nextToken();
				if(input.equals("END")) break;
				if(input.equals("QUIT")) System.exit(0);
				ins[input_ind++] = input;
				if(input.equals("NUM")) ins[input_ind-1] = ins[input_ind-1]+Integer.parseInt(st.nextToken());  
			}
			st = new StringTokenizer(br.readLine());
			N = Integer.parseInt(st.nextToken());
			for(int a = 0;a<N;a++) {
				st = new StringTokenizer(br.readLine());
				num[a] = Long.parseLong(st.nextToken());
			}
			for(int a = 0;a<N;a++) {
				a_list.add(num[a]);
				if(!solve()) {
					System.out.println("ERROR");
				}
				int size = a_list.size();
				for(int b = 0;b<size;b++) {
					a_list.remove(0);
				}
			}
			System.out.println();
			br.readLine();
			init(input_ind);
		}
	}
	public static boolean solve() {
		boolean flag = true;
		for(int a = 0;a<ins.length;a++) {
			if(ins[a] == null) break;
			if(ins[a].length()>3) { //NUM
				String c = "";
				for(int b = 3;b<ins[a].length();b++) {
					c = c+ins[a].charAt(b);
				}
				a_list.add(0, Long.parseLong(c));
				continue;
			}
			if(ins[a].equals("POP")) {
				if(a_list.size()==0) {flag = false;break;}
				a_list.remove(0);
			}
			else if(ins[a].equals("INV")) {
				if(a_list.size()==0) {flag = false;break;}
				long temp =a_list.remove(0);
				a_list.add(0,temp*-1); continue;
			}
			else if(ins[a].equals("DUP")) {
				if(a_list.size()==0) {flag = false;break;}
				long temp = a_list.get(0);
				a_list.add(0,temp);continue;
			}
			else if(ins[a].equals("SWP") || ins[a].equals("ADD") || ins[a].equals("SUB") || ins[a].equals("MUL") ||ins[a].equals("DIV") ||ins[a].equals("MOD")) {
				if(a_list.size()<2) {flag = false;break;}
				long temp;
				long first = a_list.remove(0);
				long second = a_list.remove(0);
				if(ins[a].equals("SWP")) {
					a_list.add(0,first);a_list.add(0,second);continue;
				}
				else if(ins[a].equals("ADD")) {
					temp = first + second;
					a_list.add(0,temp);continue;
				}
				else if(ins[a].equals("SUB")) {
					a_list.add(0,second-first);continue;
				}
				else if(ins[a].equals("MUL")) {
					temp = first * second;
					a_list.add(0,first*second);continue;
				}
				else if(ins[a].equals("DIV") || ins[a].equals("MOD")) {
					int f = 0;
					if(first == 0) {flag = false;break;}
					if(first<0) f++;
					if(second<0) f++;
					
					if(ins[a].equals("DIV")) {
						temp = Math.abs(second)/Math.abs(first);
						if(f ==1) temp *=-1;
						a_list.add(0,temp);continue;
					}
					else if(ins[a].equals("MOD")) {
						temp = Math.abs(second)%Math.abs(first);
						if(second<0) temp *=-1;
						a_list.add(0,temp);continue;
					}
				}

			}
		}
		if(a_list.size()!=1 || !flag) return false;
		long ans = a_list.get(0);
		if(Math.abs(ans)>1000000000) return false;
		System.out.println(ans);
		return true;
	}
	public static void init(int index) {
		for(int a = 0;a<N;a++) {
			num[a] = 0;
		}
		for(int a =0;a<index;a++) {
			if(a>=ins.length) break;
			ins[a] = null;
		}
	}
}
