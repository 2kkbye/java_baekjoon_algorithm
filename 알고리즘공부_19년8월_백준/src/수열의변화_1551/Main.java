package 수열의변화_1551;

import java.io.*;
import java.util.StringTokenizer;

public class Main {

	static int N,K;
	static String arr[];
	
	static int num[];
	static int num_temp[];
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		K = Integer.parseInt(st.nextToken());
		num = new int[N];
		num_temp = new int[N];
		
		st = new StringTokenizer(br.readLine());
		String str = st.nextToken();
		arr = str.split(",");
		changetoNum();
		for(int a = 0;a<K;a++) {
			new_array();
			copy();
		}
		result();
	}
	public static void new_array() {
		for(int a = 1;a<N;a++) {
			num_temp[a-1] = num[a]-num[a-1];
		}
		N--;
	}
	public static void copy() {
		for(int a = 0;a<N;a++) {
			num[a] = num_temp[a];
		}
	}
	public static void result() {
		for(int a = 0;a<N;a++) {
			if(a==N-1)System.out.print(num[a]);
			else System.out.print(num[a]+",");
		}
	}
	public static void changetoNum() {
		for(int a = 0;a<arr.length;a++) {
			num[a] = Integer.parseInt(arr[a]);
		}
	}
}