package �κ��ùķ��̼�_2174;

import java.io.*;
import java.util.StringTokenizer;
import java.util.ArrayList;

class Node{
	int x, y, dir;
	public Node(int x, int y, int dir) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
		this.dir = dir;
	}
}

public class Main {
	static int dx[]= {-1,0,1,0}; //NESW
	static int dy[]= {0,1,0,-1};

	static int map[][];

	static int A,B; //A : ��,  B : ��
	static int N,M; //N : �κ����� M : ���ɾ� ����
	static ArrayList<Node> a_robot = new ArrayList<Node>();
	static String res="";
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		A = Integer.parseInt(st.nextToken());
		B = Integer.parseInt(st.nextToken());
		map = new int[B+1][A+1];

		st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());

		int x, y;
		String z;
		for(int a = 1 ;a<=N;a++) {
			st = new StringTokenizer(br.readLine());
			y = Integer.parseInt(st.nextToken());
			x = Integer.parseInt(st.nextToken());
			z = st.nextToken();
			map[B-x+1][y] = a;
			a_robot.add(new Node(B-x+1, y, check_dir(z)));
		}
		int robot_num,num;
		String ins;
		boolean flag = true;
		for(int a = 0;a<M;a++) {
			st = new StringTokenizer(br.readLine());
			robot_num = Integer.parseInt(st.nextToken());
			ins = st.nextToken();
			num = Integer.parseInt(st.nextToken());
			if(!LR(robot_num, ins, num)) {
				flag = false;
				break;
			}
		}
		if(flag) System.out.println("OK");
		else System.out.println(res);
	}
	public static boolean robotMove(int robot_num, int num) {
		Node n = a_robot.get(robot_num-1);
		int px = n.x;
		int py = n.y;
		int dir = n.dir;

		map[px][py] = 0;
		for(int a = 0;a<num;a++) {
			px = px+dx[dir];
			py = py+dy[dir];
			if(px<1 || py<1 || px>B || py>A) {
				res = "Robot "+robot_num+" crashes into the wall";
				return false;
			}
			if(map[px][py]!=0) {
				res = "Robot "+robot_num+" crashes into robot "+map[px][py];
				return false;
			}
		}
		a_robot.get(robot_num-1).x = px;
		a_robot.get(robot_num-1).y = py;
		map[px][py] = robot_num;
		return true;
	}
	public static boolean LR(int robot_num, String str, int num) {
		int dir=0;
		if(str.equals("L")) {
			dir = a_robot.get(robot_num-1).dir;
			for(int a = 0;a<num;a++) {
				dir -=1;
				if(dir == -1) dir = 3;
			}
		}
		else if(str.equals("F")) {
			if(!robotMove(robot_num,num)) {
				return false;
			}
			return true;
		}
		else {
			dir = a_robot.get(robot_num-1).dir;
			for(int a =0;a<num;a++) {
				dir += 1;
				if(dir == 4) dir = 0;
			}
		}
		a_robot.get(robot_num-1).dir = dir;
		return true;
	}
	public static int check_dir(String str) {
		if(str.equals("N")) { //��
			return 0;
		}
		else if(str.equals("E")) { //��
			return 1;
		}
		else if(str.equals("S")) { //��
			return 2;
		}
		else  { //��
			return 3;
		}
	}

}
