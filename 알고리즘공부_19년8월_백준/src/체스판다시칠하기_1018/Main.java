package ü���Ǵٽ�ĥ�ϱ�_1018;

import java.io.*;
import java.util.StringTokenizer;
public class Main {

	static int N,M;
	static int map[][];
	static int copy[][] = new int[8][8];

	static int dx[]= {0,1}; //������, ��
	static int dy[]= {1,0};
	static int ans;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());

		map = new int[N][M];
		for(int a = 0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			String str = st.nextToken();
			for(int b = 0;b<M;b++) {
				if(str.charAt(b)=='W') continue;
				map[a][b] = 1;
			}
		}
		ans = Integer.MAX_VALUE;
		for(int a = 0;a<=(N-8);a++) {
			for(int b = 0;b<=(M-8);b++) {
				copy(a,b);
				solve(0);
				copy(a,b);
				solve(1);
			}
		}
		System.out.println(ans);
	}
	public static void solve(int temp) {
		//print();
		int cnt = 0;
		if(copy[0][0] != temp) cnt = 1;
		copy[0][0] = temp;
		for(int a = 0;a<8;a++) {
			for(int b = 0;b<8;b++) {
				cnt = cnt+check(a,b);
			}
		}
		//print();
		ans = Math.min(ans, cnt);
	}
	public static int check(int x, int y) {
		int cnt = 0;
		for(int a = 0;a<2;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx <0 || ry<0 || rx>=8 || ry>=8) continue;
			if(copy[x][y] == copy[rx][ry] ) {
				cnt++;
				if(copy[x][y]==0) copy[rx][ry] = 1;
				else copy[rx][ry] = 0;
			}
		}
		return cnt;
	}
	public static void copy(int x, int y) {
		for(int a = 0;a<8;a++) {
			for(int b = 0;b<8;b++) {
				copy[a][b] = map[x+a][y+b];
			}
		}
	}
	public static void print() {
		System.out.println("==============================");
		for(int a = 0;a<8;a++) {
			for(int b = 0;b<8;b++) {
				System.out.print(copy[a][b]+" ");
			}
			System.out.println();
		}
		System.out.println("==============================");
	}
}