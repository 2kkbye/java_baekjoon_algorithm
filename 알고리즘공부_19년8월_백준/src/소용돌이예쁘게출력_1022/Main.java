package 소용돌이예쁘게출력_1022;

import java.io.*;
import java.util.StringTokenizer;

public class Main {
	static int dx[] = {-1,0,1,0};
	static int dy[] = {0,-1,0,1};
	static int map[][] = new int[10001][10001];
	static int num,x,y,r1,r2,c1,c2;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		r1 = Integer.parseInt(st.nextToken());
		c1 = Integer.parseInt(st.nextToken());
		r2 = Integer.parseInt(st.nextToken());
		c2 = Integer.parseInt(st.nextToken());
		solve();
		String max_length = ""+length_check(r1,r2,c1,c2);
		for(int a = r1;a<=r2;a++) {
			for(int b = c1;b<=c2;b++) {
				String temp = map[a+5000][b+5000]+"";
				for(int c = 0;c<max_length.length()-temp.length();c++) {
					System.out.print(" ");
				}
				System.out.print(map[a+5000][b+5000]+" ");
			}
			System.out.println();
		}
	}
	public static int length_check(int r1, int r2, int c1, int c2) {
		int max = -1;
		for(int a = r1;a<=r2;a++) {
			for(int b = c1;b<=c2;b++) {
				Math.max(max, map[a+5000][b+5000]);
			}
		}
		return max;
	}
	public static void solve() {
		x = 5000; y = 5001;
		num = 3;
		int index = 0;
		int draw_size = 1;
		map[5000][5000] = 1;
		map[x][y] = 2;
		while(draw_size<49) {
			if(index == 1 || index == 3) {
				draw_size++;
			}
			draw(index,draw_size);
			index++;
			if(index == 4) index = 0;
		}
	}
	public static void draw(int index, int draw_size) {
		int rx = x,ry = y;
		for(int a= 0;a<draw_size;a++) {
			rx = rx+dx[index];
			ry = ry+dy[index];
			if(ry > r2+5005 || rx > r1+5000+49) {
				num++; continue;
			}
			map[rx][ry] = num++;
		}
		x = rx; y = ry;
	}
}