package 게리맨더링_17471;

import java.io.*;
import java.util.StringTokenizer;
import java.util.ArrayList;
public class Main {

	static boolean visit_r[];
	static boolean visit_b[];
	
	static int map[][];
	static int pop[];
	static int N,ans;
	static int r_sum, b_sum,total_sum;
	static ArrayList<Integer> a_list = new ArrayList<Integer>();
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		pop = new int[N+1];
		map = new int[N+1][N+1];

		visit_r = new boolean[N+1];
		visit_b = new boolean[N+1];
		st = new StringTokenizer(br.readLine());
		for(int a = 1;a<=N;a++) {
			int temp =  Integer.parseInt(st.nextToken());
			total_sum+= temp;
			pop[a] = temp;
		}
		for(int a =1;a<=N;a++) {
			st = new StringTokenizer(br.readLine());
			int temp = Integer.parseInt(st.nextToken());
			for(int b =0;b<temp;b++) {
				int index = Integer.parseInt(st.nextToken());
				map[a][index] = 1;map[index][a] = 1;
			}
		}
		ans = Integer.MAX_VALUE;
		for(int a = 1;a<=N;a++) {
			a_list.add(a);
			dfs(a,1);
			a_list.remove(a_list.size()-1);
		}
		if(ans == Integer.MAX_VALUE) {
			System.out.println(-1);
		}
		else {
			System.out.println(ans);
		}
	}
	public static void dfs(int x,int level) {
		if(level>=N) return;
		solve();
		for(int a = x+1;a<=N;a++) {
			a_list.add(a);
			dfs(a,level+1);
			a_list.remove(a_list.size()-1);
		}
	}
	public static void solve() {
		//System.out.println(a_list);
		init();
		int x = a_list.get(0);
		visit_r[x] = true;
		for(int a = 1;a<a_list.size();a++) {
			int temp = a_list.get(a);
			if(map[x][temp]==0) continue;
			visit_r[temp] = true;
			dfs2(temp,0);
			break;
		}
		for(int a = 0;a<a_list.size();a++) {
			if(!visit_r[a_list.get(a)]) return;
		}
		for(int a = 1;a<visit_r.length;a++) {
			if(visit_r[a]) continue;
			visit_r[a] = true;
			dfs2(a,1);
			break;
		}
		for(int a = 1;a<visit_r.length;a++) {
			if(!visit_r[a]) return;
		}
		for(int a =0;a<a_list.size();a++) {
			r_sum += pop[a_list.get(a)];
		}
		b_sum = total_sum-r_sum;
		ans = Math.min(ans, Math.abs(r_sum-b_sum));
	}
	public static void dfs2(int x,int sw) {
		if(sw == 0) {
			for(int a = 1;a<a_list.size();a++) {
				int temp = a_list.get(a);
				if(map[x][temp]==0) continue;
				if(visit_r[temp]) continue;
				visit_r[temp] = true;
				dfs2(temp,0);
			}
		}
		else {
			for(int a = 1;a<=N;a++) {
				if(visit_r[a]) continue;
				if(map[x][a]==0) continue;
				visit_r[a] = true;
				dfs2(a,1);
			}
		}
	}
	public static void init() {
		r_sum = 0;b_sum = 0;
		for(int a= 1;a<=N;a++) {
			visit_r[a] = false;
			visit_b[a] = false;
		}
	}
}