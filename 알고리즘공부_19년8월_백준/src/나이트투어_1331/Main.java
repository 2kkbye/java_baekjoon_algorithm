package 나이트투어_1331;

import java.io.*;
import java.util.StringTokenizer;

public class Main {
	
	static boolean visit[][] = new boolean[7][7];
	static int check_x = 0,check_y = 0;
	static int start_x, start_y;
	static int x, y;
	static int dx[]= {-1,-2,-2,-1,1,2,2,1};
	static int dy[]= {-2,-1,1,2,2,1,-1,-2};
	public static void main(String[] args) throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		
		String str;
		boolean flag = true;
		for(int a = 0;a<36;a++) {
			st = new StringTokenizer(br.readLine());
			str = st.nextToken();
			if(a == 0) {
				change(str.charAt(1), str.charAt(0), 0);
				continue;
			}
			change(str.charAt(1), str.charAt(0), 1);
			if(!solve()) {
				flag = false; break;
			}
		}
		if(flag && last_check()) System.out.println("Valid");
		else System.out.println("Invalid");
	}
	public static boolean last_check() {
		for(int a = 0;a<dx.length;a++) {
			int rx = check_x +dx[a];
			int ry = check_y +dy[a];
			if(rx <1 || ry<1 || rx>6 || ry > 6) continue;
			if(rx == start_x && ry == start_y) {
				return true;
			}
		}
		return false;
	}
	public static boolean solve() {
		
		for(int a = 0;a<dx.length;a++) {
			int rx = check_x +dx[a];
			int ry = check_y +dy[a];
			if(rx <1 || ry<1 || rx>6 || ry > 6) continue;
			if(visit[rx][ry]) continue;
			if(rx == x && ry == y) {
				visit[rx][ry] = true;
				check_x = rx;
				check_y = ry;
				return true;
			}
		}
		return false;
	}
	public static void change(char temp_x,char temp_y, int sw) {
		if(sw == 0) {
			check_y = (int)temp_y - 64;
			check_x = 6 - ((int)temp_x - 48)+1;
			visit[check_x][check_y] = true;
			start_x = check_x;
			start_y = check_y;
		}
		else {
			
			x = 6 - ((int)temp_x - 48)+1;
			y = (int)temp_y - 64;
		}
		
	}
}
