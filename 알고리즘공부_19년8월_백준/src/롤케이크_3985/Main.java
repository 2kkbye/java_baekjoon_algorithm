package ������ũ_3985;

import java.io.*;
import java.util.StringTokenizer;

public class Main {

	static int L,N;
	static int cake[];
	static int people[];
	
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		L = Integer.parseInt(st.nextToken());
		st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		
		cake = new int[L+1];
		people = new int[N+1];
		
		int first_ans = -1;
		int ans = 0;
		
		for(int a = 1;a<=N;a++) {
			st = new StringTokenizer(br.readLine());
			int p = Integer.parseInt(st.nextToken());
			int k = Integer.parseInt(st.nextToken());
			insert_number(p, k, a);
			
			if(first_ans<(k-p)) {
				first_ans = k-p;
				ans = a;
			}
		}
		System.out.println(ans);
		System.out.println(check_people());
	}
	public static void insert_number(int x, int y, int person) {
		for(int a = x;a<=y;a++) {
			if(cake[a] !=0) continue;
			cake[a] = person;
		}
	}
	public static int check_people() {
		for(int a = 1;a<=L;a++) {
			if(cake[a]==0) continue;
			people[cake[a]]++;
		}
		int max = 0;
		int max_index = 0;
		for(int a =N;a>=1;a--) {
			if(people[a]>=max) {
				max_index = a;
				max = people[a];
			}
		}
		return max_index;
	}
}
