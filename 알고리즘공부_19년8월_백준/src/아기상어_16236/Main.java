package �Ʊ���_16236;

import java.io.*;
import java.util.StringTokenizer;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;
class shark{
	int x,y,age,eat;
	public shark(int x, int y, int age, int eat) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
		this.age = age;
		this.eat = eat;
	}
}
class Node{
	int x,y;
	public Node(int x, int y) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
	}
}
public class Main {

	static int dx[]= {-1,0,1,0};
	static int dy[]= {0,1,0,-1};

	static int map[][];
	static int copy[][];
	static boolean discovered[][];
	static int N,ans;
	static Queue<Node> q_shark = new LinkedList<Node>();

	static LinkedList<shark> l_shark = new LinkedList<shark>();
	static LinkedList<shark> l_eat = new LinkedList<shark>();
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		map = new int[N][N];
		copy = new int[N][N];
		discovered = new boolean[N][N];
		for(int a = 0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			for(int b = 0;b<N;b++) {
				int temp  = Integer.parseInt(st.nextToken());
				if(temp == 9) {
					l_shark.add(new shark(a, b, 2, 2));
					continue;
				}
				map[a][b] = temp;
			}
		}
		for(int a = 0;a<Integer.MAX_VALUE;a++) {
			bfs();
			if(!eat()) break;
			init();
		}
		System.out.println(ans);

	}
	public static boolean eat() {
		if(l_eat.size() == 0) {
			return false;
		}
		sort();
		shark s = l_eat.get(0);
		ans +=s.eat;
		l_shark.get(0).x = s.x;
		l_shark.get(0).y = s.y;
		l_shark.get(0).eat -=1;
		map[s.x][s.y] = 0;
		if(l_shark.get(0).eat == 0 ) {
			l_shark.get(0).age++;
			l_shark.get(0).eat = l_shark.get(0).age;
		}
		return true;
	}
	public static void sort() {
		l_eat.sort(new Comparator<shark>() {
			@Override
			public int compare(shark arg0, shark arg1) {
				// TODO Auto-generated method stub
				int f_x = arg0.x;
				int f_y = arg0.y;
				int f_size = arg0.eat;
				int s_x = arg1.x;
				int s_y = arg1.y;
				int s_size = arg1.eat;
				if(f_size<s_size) return -1;
				else if(f_size>s_size) return 1;
				else {
					if(f_x < s_x) return -1;
					else if(f_x > s_x) return 1;
					else {
						if(f_y < s_y) return -1;
						else return 1;
					}
				}
			}
		});
	}
	public static void bfs() {
		q_shark.add(new Node(l_shark.get(0).x, l_shark.get(0).y));
		discovered[l_shark.get(0).x][l_shark.get(0).y] = true;
		
		int max_age = l_shark.get(0).age;
		while(!q_shark.isEmpty()) {
			Node n = q_shark.remove();
			int px = n.x;
			int py = n.y;

			for(int a = 0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx<0 || ry<0 || rx>=N || ry>=N) continue;
				if(discovered[rx][ry] || map[rx][ry] >max_age) continue;
				copy[rx][ry] = copy[px][py]+1;
				discovered[rx][ry] = true;
				q_shark.add(new Node(rx, ry));
				if(map[rx][ry]<max_age && map[rx][ry]!=0) {
					l_eat.add(new shark(rx, ry, map[rx][ry], copy[rx][ry]));
				}

			}
		}
	}
	public static void init() {
		for(int a =0;a<N;a++) {
			for(int b = 0;b<N;b++) {
				discovered[a][b] = false;
				copy[a][b] = 0;
			}
		}
		l_eat.removeAll(l_eat);
	}
}