package 삼성기출복습;

import java.io.*;
import java.util.StringTokenizer;

public class Main부분수열의합 {

	static int num[];
	static boolean visit[];
	static int sum = 0,N;
	static boolean check[] = new boolean[10000000+1];
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		num = new int[N];
		st = new StringTokenizer(br.readLine());
		
		for(int a = 0;a<N;a++) {
			num[a] = Integer.parseInt(st.nextToken());
		}
		for(int a = 0;a<N;a++) {
			sum = num[a];
			check[sum] = true;
			dfs(a+1);
			sum -= num[a];
		}
		System.out.println(check());
	}
	public static int check() {
		int num = 1;
		for(int a =1;a<check.length;a++) {
			if(check[a]) continue;
			num = a;
			break;
		}
		return num;
	}
	public static void dfs(int x) {
		for(int a = x;a<N;a++) {
			sum = sum + num[a];
			check[sum] = true;
			dfs(a+1);
			sum -= num[a];
		}
	}
}