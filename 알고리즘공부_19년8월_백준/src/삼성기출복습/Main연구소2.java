package 삼성기출복습;

import java.io.*;
import java.util.StringTokenizer;
import java.util.LinkedList;
import java.util.Queue;

public class Main연구소2 {

	static int N,M;
	static int dx[]= {-1,0,1,0};
	static int dy[]= {0,1,0,-1};
	static int map[][];
	static int copy[][];
	static boolean visit[];
	static boolean discovered[][];
	static int ans = Integer.MAX_VALUE;
	static LinkedList<Node> l_virus = new LinkedList<Node>();
	static Queue<Node> q_virus = new LinkedList<Node>();
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());

		map = new int[N][N];
		copy = new int[N][N];
		discovered = new boolean[N][N];

		for(int a = 0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			for(int b = 0;b<N;b++) {
				int temp  =Integer.parseInt(st.nextToken());
				if(temp == 0) continue;
				if(temp == 2) {
					l_virus.add(new Node(a, b));
					continue;
				}
				map[a][b] = temp;
			}
		}
		check();
		visit = new boolean[l_virus.size()];
		for(int a = 0;a<l_virus.size();a++) {
			visit[a] = true;
			dfs(a+1,1);
			visit[a] = false;
		}
		if(ans == Integer.MAX_VALUE) {
			System.out.println(-1);
		}
		else System.out.println(ans-1);
	}
	public static void dfs(int x, int level) {
		if(level == M) {
			init();
			bfs();
			//print();
			check();
			
			return;
		}
		for(int a = x;a<l_virus.size();a++) {
			visit[a] = true;
			dfs(a+1,level+1);
			visit[a] = false;
		}
	}
	public static void bfs() {
		while(!q_virus.isEmpty()) {
			Node n = q_virus.remove();
			int px = n.x;
			int py = n.y;

			for(int a = 0;a<4;a++) {
				int rx = px + dx[a];
				int ry = py + dy[a];
				if(rx <0 || ry<0 || rx>=N || ry>=N) continue;
				if(map[rx][ry]==1 || discovered[rx][ry]) continue;
				discovered[rx][ry] = true;
				q_virus.add(new Node(rx, ry));
				copy[rx][ry] = copy[px][py]+1;
			}
		}
	}
	public static void check() {
		int max = 0;
		for(int a = 0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(map[a][b] == 1) continue;
				if(copy[a][b]!=0) {
					max = Math.max(max, copy[a][b]);
					continue;
				}
				return;
			}
		}
		ans = Math.min(max, ans);
	}
	public static void init() {
		for(int a = 0;a<N;a++) {
			for(int b= 0;b<N;b++) {
				copy[a][b] = 0;
				discovered[a][b] = false;
			}
		}
		for(int a = 0;a<visit.length;a++) {
			if(!visit[a]) continue;
			int x = l_virus.get(a).x;
			int y = l_virus.get(a).y;
			copy[x][y] = 1;
			q_virus.add(new Node(x,y));
			discovered[x][y] = true;
		}
	}
	public static void print() {
		System.out.println("======================");
		for(int a = 0;a<N;a++) {
			for(int b = 0;b<N;b++) {
				System.out.print(copy[a][b]+" ");
			}
			System.out.println();
		}
		System.out.println("======================");

	}
}