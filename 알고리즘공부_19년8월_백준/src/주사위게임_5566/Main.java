package 주사위게임_5566;

import java.io.*;
import java.util.StringTokenizer;

public class Main {
	static int N,M;
	
	static int num[];
	static int now_number = 1;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		
		num = new int[N+1];
		for(int a = 1; a<num.length; a++) {
			st = new StringTokenizer(br.readLine());
			num[a] = Integer.parseInt(st.nextToken());
		}
		int n;
		int ans = 0;
		for(int a = 1;a<=M;a++) {
			st = new StringTokenizer(br.readLine());
			n = Integer.parseInt(st.nextToken());
			num_throw(n);
			if(now_number>=N) {
				ans = a;
				break;
			}
		}
		System.out.println(ans);
	}
	public static void num_throw(int n) {
		now_number +=n;
		if(now_number>=N) return;
		now_number +=num[now_number];
	}
}