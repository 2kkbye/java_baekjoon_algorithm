package 이차원배열과연산_17140;

import java.io.*;
import java.util.StringTokenizer;
import java.util.Comparator;
import java.util.LinkedList;
class Node{
	int num,cnt;
	public Node(int num, int cnt) {
		// TODO Auto-generated constructor stub
		this.num = num;
		this.cnt = cnt;
	}
}
public class Main{

	static LinkedList<LinkedList<Node>> a_list = new LinkedList<LinkedList<Node>>();
	static int map[][] = new int[100][100];
	static boolean visit[] = new boolean[100];

	static int max_x=3,max_y=3,ans_x,ans_y,ans;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		ans_x = Integer.parseInt(st.nextToken())-1;
		ans_y = Integer.parseInt(st.nextToken())-1;
		ans = Integer.parseInt(st.nextToken());

		for(int a = 0;a<3;a++) {
			st = new StringTokenizer(br.readLine());
			for(int b =0;b<3;b++) {
				map[a][b] = Integer.parseInt(st.nextToken());
			}
		}
		boolean flag = false;
		for(int a = 0;a<=101;a++) {
			if(result_check()) {
				System.out.println(a);
				flag = true;
				break;
			}
			a_list.removeAll(a_list);
			if(max_x>=max_y) { //R연산
				solve(0);
			}
			else {
				solve(1);
			}
		}
		if(!flag) {
			System.out.println(-1);
		}
	}
	public static void solve(int sw) {
		if(sw==0) {
			R();
		}
		else {
			C();
		}
		write(sw);
	}
	public static void write(int sw) {
		sort();
		int temp_ind =0;
		int list_ind;
		if(sw == 0) {
			for(int a = 0;a<max_x;a++) {
				list_ind = 0;
				for(int b= 0 ;b<max_y;b++) {
					if(list_ind>=a_list.get(a).size()) {
						temp_ind = b;
						break;
					}
					if(b %2== 0) {
						map[a][b] = a_list.get(a).get(list_ind).num;
						continue;
					}
					temp_ind = b+1;
					map[a][b] = a_list.get(a).get(list_ind++).cnt;
				}
				for(int b = temp_ind;b<max_y;b++) {
					map[a][b] = 0;
				}
			}
		}
		else {
			for(int a = 0;a<max_y;a++) {
				list_ind = 0;
				for(int b= 0 ;b<max_x;b++) {
					if(list_ind>=a_list.get(a).size()) {
						temp_ind = b;
						break;
					}
					if( b %2== 0 ) {
						map[b][a] = a_list.get(a).get(list_ind).num;
						continue;
					}
					temp_ind = b+1;
					map[b][a] = a_list.get(a).get(list_ind++).cnt;
				}
				for(int b = temp_ind;b<max_x;b++) {
					map[b][a] = 0;
				}
			}
		}
	}
	public static void R() {
		int max_size = 0;
		for(int a =0;a<max_x;a++) {
			init();
			a_list.add(new LinkedList());
			int t_num=0,t_cnt=0;
			for(int b = 0;b<max_y;b++) {
				if(visit[b] || map[a][b] == 0) continue;
				visit[b] = true;
				t_num = map[a][b];
				t_cnt = 1;
				for(int c = b+1;c<max_y;c++) {
					if(visit[c] || map[a][c]==0) continue;
					if(t_num != map[a][c]) continue;
					t_cnt++;
					visit[c] = true;
				}
				a_list.get(a).add(new Node(t_num, t_cnt));
			}
			max_size = Math.max(max_size, a_list.get(a).size()*2);
		}
		max_y = Math.max(max_size, max_y);
	}
	public static void C() {
		int max_size = 0;
		for(int a =0;a<max_y;a++) {
			init();
			a_list.add(new LinkedList());
			int t_num=0,t_cnt=0;
			for(int b = 0;b<max_x;b++) {
				if(visit[b] || map[b][a] == 0) continue;
				visit[b] = true;
				t_num = map[b][a];
				t_cnt = 1;
				for(int c = b+1;c<max_x;c++) {
					if(visit[c] || map[c][a]==0) continue;
					if(t_num != map[c][a]) continue;
					t_cnt++;
					visit[c] = true;
				}
				a_list.get(a).add(new Node(t_num, t_cnt));
			}
			max_size = Math.max(max_size, a_list.get(a).size()*2);
		}
		max_x = Math.max(max_size, max_x);
	}
	public static void sort() {
		for(int a = 0;a<a_list.size();a++) {
			a_list.get(a).sort(new Comparator<Node>() {
				@Override
				public int compare(Node arg0, Node arg1) {
					// TODO Auto-generated method stub
					int fir_cnt = arg0.cnt;
					int fir_num= arg0.num;
					int sec_cnt=arg1.cnt;
					int sec_num=arg1.num;
					if ( fir_cnt== sec_cnt) {
						if(sec_num>fir_num) {
							return -1;
						}
						else {
							return 1;
						}
					}
					else if(fir_cnt>sec_cnt) return 1;
					else
						return -1;
				}
			});
		}
	}
	public static void init() {

		//a_list = new LinkedList<LinkedList<Node>>();
		int max = Math.max(max_x, max_y);
		for(int a = 0;a<max;a++) {
			visit[a] = false;
		}
	}
	public static boolean result_check() {
		if(map[ans_x][ans_y]==ans) {
			return true;
		}
		return false;
	}
}
