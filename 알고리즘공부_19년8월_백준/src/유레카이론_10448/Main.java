package 유레카이론_10448;

import java.io.*;
import java.util.StringTokenizer;

public class Main {

	static int T[] = new int[500+1];
	static int result[];
	static int num[];
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());

		result = new int[N];
		num = new int[N];

		insert_T();

		for(int a = 0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			int num = Integer.parseInt(st.nextToken());
			if(solve(num,a)) {
				result[a] = 1;
			}
			else {
				result[a] = 0;
			}
		}
		for(int a = 0;a<result.length;a++) {
			System.out.println(result[a]);
		}
	}
	public static boolean solve(int num, int index) {
		int sum = 0;
		for(int a = 1;a<T.length;a++) {
			if(T[a]>= num) break;
			sum = T[a];
			for(int b = a;b<T.length;b++) {
				sum +=T[b];
				if(sum >=num) {
					sum -=T[b];
					break;
				}
				for(int c = b; c<T.length;c++) {

					sum +=T[c];
					if(sum > num) { 
						sum -= T[c];
						break;
					}
					if(sum == num) {
						return true;
					}
					else {
						sum -=T[c];
						continue;
					}
				}
				sum -=T[b];
			}
			sum -=T[a];
		}
		return false;
	}
	public static void insert_T() {
		T[1] = 1;
		for(int a = 2;a<T.length;a++) {
			T[a] = T[a-1] + a;
		}
	}

}
