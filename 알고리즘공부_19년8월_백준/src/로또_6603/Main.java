package �ζ�_6603;

import java.io.*;
import java.util.StringTokenizer;
import java.util.ArrayList;
public class Main {
	static int N;
	static int number[];
	static boolean visit[];
	static int result[] = new int[6];
	static ArrayList<Integer> a_number;
	public static void main(String[] args) throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		boolean flag = true;
		while(true) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			N = Integer.parseInt(st.nextToken());
			if(N == 0 ) break;
			
			number = new int[N];
			visit = new boolean[N];
			a_number = new ArrayList<Integer>();
			for(int a = 0;a<N;a++) {
				number[a] = Integer.parseInt(st.nextToken());
			}
			dfs(0,0);
			System.out.println();
		}
	}
	public static void dfs(int x, int level) {
		if(level == 6) {
			print();
			return;
		}
		for(int a = x;a<number.length;a++) {
			if(visit[a]) continue;
			visit[a] = true;
			a_number.add(number[a]);
			dfs(a+1,level+1);
			a_number.remove(a_number.size()-1);
			visit[a] = false;
		}
	}
	public static void print() {
		for(int a = 0;a<a_number.size();a++) {
			System.out.print(a_number.get(a)+" ");
		}
		System.out.println();
	}
}
