package AC_5430;

import java.io.*;
import java.util.StringTokenizer;
public class Main {

	static int S,E;
	static int status;
	static int arr[] = new int[100000 +1];
	static String num[];
	static char ins[];
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int T = Integer.parseInt(st.nextToken());
		
		for(int i=0;i<T;i++) {
			String in = br.readLine();
			ins = in.toCharArray();
			st = new StringTokenizer(br.readLine());
			E = Integer.parseInt(st.nextToken())-1;
			S = 0;
			
			in = br.readLine();
			in = in.replace("[", "");
			in = in.replace("]", "");
			if(in.equals("") && check()) {System.out.println("error");continue;}
			else if(in.equals("")) {System.out.println("[]");continue;}
			num = in.split(",");
			init();
			if(solve()) {
				print();
			}
			else {
				System.out.println("error");
			}
		}
	}
	public static boolean check() {
		for(int a = 0;a<ins.length;a++) {
			if(ins[a]=='D') return true;
		}
		return false;
	}
	public static void print() {
		System.out.print("[");
		if(status == 1) {
			for(int a= S;a<=E;a++) {
				if(a!=E) {
					System.out.print(arr[a]+",");
					continue;
				}
				System.out.print(arr[a]);
			}
		}
		else {
			for(int a= E;a>=S;a--) {
				if(a!=S) {
					System.out.print(arr[a]+",");
					continue;
				}
				System.out.print(arr[a]);
			}
		}
		System.out.println("]");
	}
	public static boolean solve() {
		for(int a = 0;a<ins.length;a++) {
			if(ins[a]=='R') {
				status *=-1;
			}
			else {
				if((S == E && arr[S]==0) ||(S>E) ) return false;
				delete(status);
			}
		}
		return true;
	}
	public static void delete(int status) {
		if(status == 1) {
			arr[S]=0;
			S++;
		}
		else {
			arr[E]=0;
			E--;
		}
	}
	public static void init() {
		status = 1;
		for(int a = 0; a<num.length; a++) {
			arr[a] = Integer.parseInt(num[a]);
		}
	}
}
