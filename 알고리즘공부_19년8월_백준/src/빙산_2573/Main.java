package ����_2573;

import java.io.*;
import java.util.Queue;
import java.util.StringTokenizer;
import java.util.LinkedList;
class Pair{
	int x, y, ice;
	public Pair(int x, int y, int ice) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
		this.ice = ice;
	}
}
public class Main {
	static int dx[]= {-1,0,1,0};
	static int dy[]= {0,1,0,-1};

	static int N,M;
	static boolean visited[][];
	static int map[][];
	static Queue<Pair> q_ice;
	//static boolean discovered[][];
	static int island;
	
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		map = new int[N][M];
		q_ice = new LinkedList<Pair>();
		visited = new boolean[N][M];
		for(int a = 0;a<N;a++) {
			String in = br.readLine();
			for(int b= 0;b<M;b++) {
				int temp = Integer.parseInt(in.split(" ")[b]);
				map[a][b] = temp;
				if(temp !=0) {
					q_ice.add(new Pair(a, b, temp));
				}
			}
		}
		boolean flag = false;
		int ans = 0;
		while(flag!=true) {
			ans++;
			solve();
			if(q_ice.size()==0) {
				flag = true;continue;
			}
			if(check_island()) break;
		}
		if(flag) System.out.println(0);
		else System.out.println(ans);

	}
	public static boolean check_island() {
		island = 0;
		init();
		for(int a =0;a<N;a++) {
			for(int b= 0;b<M;b++) {
				if(map[a][b] ==0 )continue;
				if(visited[a][b]) continue;
				island++;
				if(island==2) return true; 
				dfs(a,b);
			}
		}
		return false;
	}
	public static void init() {
		for(int a= 0;a<N;a++) {
			for(int b = 0;b<M;b++) {
				visited[a][b] = false;
			}
		}
	}
	public static void dfs(int x, int y) {
		visited[x][y] = true;
		
		for(int a = 0;a<4;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx<0 || ry<0 || rx>=N || ry >=M) continue;
			if(map[rx][ry]==0) continue;
			if(visited[rx][ry]) continue;
			dfs(rx,ry);
		}
	}
	public static void solve() {
		int size = q_ice.size();
		int cnt;
		for(int a=0;a<size;a++) {
			Pair p = q_ice.remove();
			cnt = check_sea(p.x, p.y);
			q_ice.add(new Pair(p.x, p.y, p.ice - cnt));
		}
		for(int a=0;a<size;a++) {
			Pair p = q_ice.remove();
			if(p.ice <=0) {map[p.x][p.y] = 0;continue;}
			map[p.x][p.y] = p.ice;
			q_ice.add(new Pair(p.x, p.y, p.ice));
		}
	}
	public static int check_sea(int x, int y) {
		int cnt = 0;
		for(int a = 0;a<4;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];

			if(rx<0 || ry<0 || rx>=N || ry >=M) continue;
			if(map[rx][ry]!=0) continue;
			cnt++;
		}
		return cnt;

	}
}
