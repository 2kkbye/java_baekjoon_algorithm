package 벽부수고이동하기_2206;


import java.util.Queue;
import java.util.LinkedList;
import java.io.*;

class Pair{
	int x, y,dis;
	boolean hit;
	public Pair(int x, int y, boolean hit, int dis) {
		this.x = x;
		this.y = y;
		this.hit = hit;
		this.dis = dis;
	}
}
public class Main {

	static Queue<Pair> q_land;

	static int dx[]= {-1,0,1,0};
	static int dy[]= {0,1,0,-1};
	static boolean discovered[][][];
	static int map[][];
	static int ans;
	static int N,M;
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		N = Integer.parseInt(in.split(" ")[0]);
		M = Integer.parseInt(in.split(" ")[1]);
		map = new int[N][M];

		q_land = new LinkedList<Pair>();
		ans = Integer.MAX_VALUE;
		int temp;
		for(int a = 0;a<N;a++) {
			in = br.readLine();
			for(int b = 0;b<M;b++) {
				temp =  Integer.parseInt(in.split("")[b]);
				map[a][b] = temp;
				System.out.print(temp);
			}
			System.out.println();
		}
//		for (int i = 0; i < N; i++) {
//            String str = br.readLine();
//            for (int j = 0; j < M; j++) {
//                map[i][j] = Integer.parseInt(str.charAt(j) + "");
//            }
//        }
		discovered = new boolean[N][M][2];
		discovered[0][0][0] = true;
		discovered[0][0][1]	= true;
		
		q_land.add(new Pair(0,0,false,1));
		
		while(!q_land.isEmpty()) {
			Pair p = q_land.remove();
			
			int px = p.x;
			int py = p.y;
			boolean hit = p.hit;
			int dis = p.dis;
			
			if(px == N-1 && py == M-1) {
				ans = Math.min(dis, ans);
				continue;
			}
			
			for(int a=0;a<4;a++) {
				
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx < 0 || ry <0 || rx>=N || ry>=M) continue;
				
				if(!hit) { //벽은 안부순경우
					if(map[rx][ry]==0 && !discovered[rx][ry][0]) {
						discovered[rx][ry][0] = true;
						q_land.add(new Pair(rx, ry, hit, dis+1));
					}
					else if(map[rx][ry]==1 && !discovered[rx][ry][1]) {
						discovered[rx][ry][1] = true;
						q_land.add(new Pair(rx, ry, true, dis+1));
					}
				}
				else { //벽을 부순경우
					if(map[rx][ry]==0 && !discovered[rx][ry][1]) {
						discovered[rx][ry][1] = true;
						q_land.add(new Pair(rx, ry, hit, dis+1));
					}
				}
			}
		}
		if(ans == Integer.MAX_VALUE) {
			System.out.println(-1);
		}
		else {
			System.out.println(ans);
		}
	}

}