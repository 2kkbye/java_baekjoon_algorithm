package 영식이의손가락_1614;

import java.io.*;
import java.util.StringTokenizer;

public class Main {

	static int num[] = new int[6];
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());

		int hurt = Integer.parseInt(st.nextToken());

		st = new StringTokenizer(br.readLine());
		int hurt_num = Integer.parseInt(st.nextToken());
		if(hurt == 1 || hurt == 5) {
			plus8(hurt, hurt_num+1);
		}
		else if(hurt == 3) {
			plus3(hurt, hurt_num+1);
		}
		else {
			if(hurt == 2) {
				plus2(hurt, hurt_num+1);
			}
			else {
				plus4(hurt, hurt_num+1);
			}

		}
	}
	public static void plus2(int num, int cnt) {
		long number = num;
		for(int a = 1;a<cnt;a++) {
			if(a%2 == 1) number += 6;
			else number += 2;
		}
		System.out.println(number-1);
	}
	public static void plus4(int num, int cnt) {
		long number = num;
		for(int a = 1;a<cnt;a++) {
			if(a%2 == 1) number += 2;
			else number += 6;
		}
		System.out.println(number-1);
	}
	public static void plus3(int num, int cnt) {
		long number = num;
		for(int a = 1;a<cnt;a++) {
			number += 4;
		}
		System.out.println(number-1);
	}
	public static void plus8(int num, int cnt) {
		long number = num;
		for(int a = 1;a<cnt;a++) {
			number += 8;
		}
		System.out.println(number-1);
	}
}