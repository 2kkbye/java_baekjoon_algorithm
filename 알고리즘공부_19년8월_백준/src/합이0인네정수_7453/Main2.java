package 합이0인네정수_7453;

import java.io.*;
import java.util.StringTokenizer;

public class Main2 {
	static long ab[];
	static long sum[];
	
	static long a_ar[];
	static long b_ar[];
	static long c_ar[];
	static long d_ar[];
	
	static int N;
	static int ans;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		ab = new long[N*N];
		sum = new long[N*N];
		a_ar = new long[N];
		b_ar = new long[N];
		c_ar = new long[N];
		d_ar = new long[N];
		
		for(int i =0;i<N;i++) {
			st = new StringTokenizer(br.readLine());
			a_ar[i] = Long.parseLong(st.nextToken());
			b_ar[i] = Long.parseLong(st.nextToken());
			c_ar[i] = Long.parseLong(st.nextToken());
			d_ar[i] = Long.parseLong(st.nextToken());
		}
		
		int idx = 0;
	    for (int i = 0; i < N; i++) {
	        for (int j = 0; j < N; j++) {
	        	ab[idx] = a_ar[i] + b_ar[j];
	            sum[idx++] = c_ar[i] + d_ar[j];
	        }
	    }
		ans = 0;
		sort(0, sum.length-1);
		for(int a = 0;a<ab.length;a++) {
			if(ab[a]==0 && binary_search(0, sum.length-1, 0) !=-1) {
				ans++;
			}
			else if(ab[a]!=0 && binary_search(0, sum.length-1, ab[a]*-1) !=-1) {
				ans++;
			}
		}
		System.out.println(ans);
	}
	public static int binary_search(int start, int end,long find_num) {
		int s = start;
		int e = end;
		int mid;
		while(s<=e) {
			mid = (s+e)/2;
			if(sum[mid] == find_num) {
				return mid;
			}
			else if(sum[mid]>find_num){
				e = mid-1;
				continue;
			}
			else {
				s = mid+1;continue;
			}
		}
		return -1;
	}
	public static void sort(int s, int e) {
		int start = s;
		int end = e;
		long pivot = sum[(s+e)/2];
		do {
			while(sum[start]<pivot) {
				start++;
			}
			while(sum[end]>pivot) {
				end--;
			}
			if(start<=end) {
				long temp = sum[start];
				sum[start] = sum[end];
				sum[end] = temp;
				start++;
				end--;
			}
		}while(start<=end);
		if(s < end) {
			sort(s,end);
		}
		if(e>start) {
			sort(start,e);
		}
	}
	public static void print() {
		System.out.println("+===============================");
		for(int b = 0;b<sum.length;b++) {
			System.out.print(sum[b]+" ");
		}
		System.out.println();
		System.out.println("+===============================");
	}

}
