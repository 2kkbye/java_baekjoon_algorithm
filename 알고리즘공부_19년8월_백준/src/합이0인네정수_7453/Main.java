package 합이0인네정수_7453;

import java.io.*;
import java.util.StringTokenizer;

public class Main {
	static int number[][];
	static boolean visit[][];
	static int sum_num[] = new int[4];
	static int N;
	static int ans;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		number = new int[4][N];
		visit = new boolean[4][N];
		for(int b =0;b<N;b++) {
			st = new StringTokenizer(br.readLine());
			for(int a = 0;a<4;a++) {
				number[a][b] = Integer.parseInt(st.nextToken());
			}
		}
		ans = 0;
		for(int a = 0;a<N;a++) {
			sum_num[0] = number[0][a];
			dfs(1);
		}
		//System.out.println(ans);
	}
	public static void dfs(int level) {
		if(level == 4) {
			if(check_sum()) ans++;
			return;
		}
		for(int a = 0;a<N;a++) {
			sum_num[level] = number[level][a];
			dfs(level+1);
		}
	}
	public static boolean check_sum() {
		int sum = 0;
		for(int a = 0;a<4;a++) {
			System.out.print(sum_num[a]+" ");
		}
		System.out.println();
		for(int a = 0;a<4;a++) {
			sum +=sum_num[a];
		}
		if(sum == 0) return true;
		else return false;
	}
}