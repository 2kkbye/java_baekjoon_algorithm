package 아이러브크로아티아_9517;

import java.io.*;
import java.util.StringTokenizer;

public class Main {
	
	static int person_index =0;
	static int max_time = 210;
	static int time[];
	static char grade[];
	
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		person_index = Integer.parseInt(st.nextToken());
		st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		
		time = new int[N];
		grade = new char[N];
		
		for(int a = 0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			time[a] = Integer.parseInt(st.nextToken());
			grade[a] = st.nextToken().charAt(0);
		}
		while(max_time>0) {
			for(int a = 0;a<time.length;a++) {
				if(grade[a]=='T') {
					max_time -= time[a];
					if(max_time<=0) break;
					person_index++;
					if(person_index == 9) person_index = 1;
				}
				else {
					max_time -= time[a];
					if(max_time<=0) break;
				}
			}
		}
		System.out.println(person_index);
	}
}
