package ��������_2468;

import java.io.*;
import java.util.StringTokenizer;

public class Main {
	static int N;
	static int max_num;

	static int map[][];
	static int copy[][];
	static boolean visit[][];
	static int ans;
	static int dx[]= {-1,0,1,0};
	static int dy[]= {0,1,0,-1};
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		max_num = Integer.MIN_VALUE;
		map = new int[N][N];
		copy = new int[N][N];
		visit = new boolean[N][N];
		ans = 1;
		for(int a = 0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			for(int b = 0;b<N;b++) {
				int temp = Integer.parseInt(st.nextToken());
				map[a][b] = temp;
				max_num = Math.max(max_num, temp);
			}
		}
		for(int i = 1;i<=max_num;i++) {
			init(i);
			int cnt = 0;
			for(int a = 0;a<N;a++) {
				for(int b=0; b<N;b++) {
					if(copy[a][b] ==0)continue;
					if(visit[a][b]) continue;
					visit[a][b] = true;
					dfs(a,b);
					cnt++;
				}
			}
			ans = Math.max(ans, cnt);
		}
		System.out.println(ans);
	}
	public static void dfs(int x, int y) {
		for(int a = 0;a<4;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if( rx<0 || ry<0 || rx>=N || ry>=N) continue;
			if(copy[rx][ry]==0) continue;
			if(visit[rx][ry]) continue;
			visit[rx][ry] = true;
			dfs(rx,ry);
		}
	}
	public static void init(int water_land) {
		for(int a = 0;a<N;a++) {
			for(int b = 0;b<N;b++) {
				visit[a][b] = false;
				if(map[a][b]>water_land) copy[a][b] = map[a][b];
				else copy[a][b] = 0;
			}
		}
	}
}