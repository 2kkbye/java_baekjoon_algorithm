package 발레리노_1857;

import java.util.Queue;
import java.awt.Point;
import java.io.*;
import java.util.LinkedList;

public class Main {
	static int n,m;
	static int map[][];
	static int copy[][];

	static int dx[]= {-2,-2,-1,1,2,2,1,-1};
	static int dy[]= {-1,1,2,2,1,-1,-2,-2};
	static int s_x,s_y,d_x,d_y;

	static int count;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		n = Integer.parseInt(in.split(" ")[0]);
		m = Integer.parseInt(in.split(" ")[1]);
		map = new int[n][m];
		copy = new int[n][m];
		Queue<Point> q_ballet = new LinkedList<Point>();

		for(int a= 0;a<n;a++) {
			in = br.readLine();
			for(int b= 0;b<m;b++) {
				int temp = Integer.parseInt(in.split(" ")[b]);
				if(temp == 3) { //출발지
					s_x = a;s_y = b;
					q_ballet.add(new Point(s_x,s_y));
				}
				if(temp == 4) { //목적지
					d_x = a;d_y = b;
					copy[d_x][d_y] = Integer.MAX_VALUE;
				}
				map[a][b] = temp;
			}
		}
		count = 0;
		while(!q_ballet.isEmpty()) {
			//print();
			Point p = q_ballet.remove();
			int px = p.x;
			int py = p.y;
			for(int a=0;a<dx.length;a++) {
				int rx = px + dx[a];
				int ry = py + dy[a];
				if(rx>= 0 && ry>=0 && rx<n && ry<m) {
					if(map[rx][ry]!=2 && map[rx][ry]!=3) {
						if(map[rx][ry]==1) { //방석이 깔려있는곳
							if(copy[rx][ry]==0) {
								copy[rx][ry] = copy[px][py];
								q_ballet.add(new Point(rx,ry));
								continue;
							}
							if(copy[rx][ry]<copy[px][py]) continue;
							copy[rx][ry] = copy[px][py];
							q_ballet.add(new Point(rx,ry));
							continue;

						}
						else if(map[rx][ry] == 0) { //빈칸

							if(copy[rx][ry]==0) {
								copy[rx][ry] = copy[px][py]+1;
								q_ballet.add(new Point(rx,ry));
								continue;
							}
							if(copy[rx][ry]<copy[px][py]+1) continue;
							copy[rx][ry] = copy[px][py]+1;
							q_ballet.add(new Point(rx,ry));
							continue;
						}
						else if(map[rx][ry]==4) { //목적지
							//System.out.println(count);
							if(copy[rx][ry]==(copy[px][py])) {
								count++;
								continue;
							}
							if(copy[rx][ry] > copy[px][py]) {
								copy[rx][ry] = copy[px][py];
								count = 0;
							}
						}
					}
				}
			}
		}
		if(copy[d_x][d_y]==Integer.MAX_VALUE){
			System.out.println(-1);
		}
		else {
			System.out.println(copy[d_x][d_y]);
			System.out.println(count+1);
		}
	}
	public static void print() {
		System.out.println("===========================");
		for(int a= 0;a<n;a++) {
			for(int b = 0;b<m;b++) {
				System.out.print(copy[a][b]+" ");
			}
			System.out.println();
		}
		System.out.println("===========================");
	}
}