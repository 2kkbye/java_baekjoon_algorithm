package 발레리노_1857;

import java.util.Queue;
import java.io.*;
import java.util.LinkedList;

class ballet{
	int x, y, cnt;
	public ballet(int x, int y, int cnt) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
		this.cnt = cnt;
	}
}

public class Main2 {
	static int n,m;
	static int map[][];

	static int dx[]= {-2,-2,-1,1,2,2,1,-1};
	static int dy[]= {-1,1,2,2,1,-1,-2,-2};
	static int s_x,s_y,d_x,d_y;
	
	static int ans = 0;
	static int count;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		n = Integer.parseInt(in.split(" ")[0]);
		m = Integer.parseInt(in.split(" ")[1]);
		map = new int[n][m];
		Queue<ballet> q_ballet = new LinkedList<ballet>();

		for(int a= 0;a<n;a++) {
			in = br.readLine();
			for(int b= 0;b<m;b++) {
				int temp = Integer.parseInt(in.split(" ")[b]);
				if(temp == 3) { //출발지
					s_x = a;s_y = b;
					q_ballet.add(new ballet(s_x,s_y,0));
				}
				if(temp == 4) { //목적지
					d_x = a;d_y = b;
				}
				map[a][b] = temp;
			}
		}
		count = 0;
		ans = Integer.MAX_VALUE;
		while(!q_ballet.isEmpty()) {
			//System.out.println("a");
			//print();
			ballet p = q_ballet.remove();
			int px = p.x;
			int py = p.y;
			int cnt = p.cnt;
			if(cnt > ans) continue;

			for(int a=0;a<dx.length;a++) {
				int rx = px + dx[a];
				int ry = py + dy[a];
				if(rx>= 0 && ry>=0 && rx<n && ry<m) {
					if(map[rx][ry]!=2 && map[rx][ry]!=3) {
						if(map[rx][ry]==1) { //방석이 깔려있는곳
							q_ballet.add(new ballet(rx, ry, cnt));
						}
						else if(map[rx][ry] == 0) { //빈칸
							q_ballet.add(new ballet(rx, ry, cnt+1));
						}
						else if(map[rx][ry]==4) { //목적지
							if(cnt == ans) {
								count++;continue;
							}
							ans = Math.min(ans, cnt);
							count = 0;
						}
					}
				}
			}
		}
		if(ans==Integer.MAX_VALUE){
			System.out.println(-1);
		}
		else {
			System.out.println(ans);
			System.out.println(count+1);
		}
	}

}
