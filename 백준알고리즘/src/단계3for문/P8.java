package 단계3for문;

import java.util.Scanner;
public class P8 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); //재사용이 가능한 객체이다.
		
		int sum = 0;
		int num=sc.nextInt(); 
		sc.nextLine();
		String st =sc.nextLine();
		for(int i=0;i<st.length();i++) {
			sum = sum + st.charAt(i)-48; //charAt는 이 문자열중 그 i번째 문자가 숫자면 아스키코드로 숫자로 인식한다. 0이면 48, 1이면 49
			//그러니깐 결론은 0+1이면 1일것 같지만 48+49와 같은 값이된다. 그래서 0을 빼줘야한다. 
		}
		System.out.println(sum);
	}
} 