package 동적계획법기초;

import java.util.Scanner;
public class 모든쌍최단경로 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int arr[][] = {{0,4,2,5,100},{100,0,1,100,4},{1,3,0,1,2},{-2,100,100,0,2},{100,-3,3,1,0}};
		
		
		for(int a=0;a<arr.length;a++) {
			for(int b=0;b<arr.length;b++) {
				System.out.print(arr[a][b]+" ");
			}
			System.out.println();
		}
		System.out.println("==================");
		for(int k=0;k<arr.length;k++) {
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr.length;b++) {
					if(a!=b && a!=k && b!=k) {
						arr[a][b] = Math.min(arr[a][b], arr[a][k]+arr[k][b]);
					}
				}
			}
		}
		
		for(int a=0;a<arr.length;a++) {
			for(int b=0;b<arr.length;b++) {
				System.out.print(arr[a][b]+" ");
			}
			System.out.println();
		}
	}
}
