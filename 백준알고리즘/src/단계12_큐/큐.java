package 단계12_큐;

import java.util.Scanner;
public class 큐 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt();
		int arr[]=new int[num];
		int front=-1;
		int rear=-1;
		String str;
		for(int i=0;i<num;i++) {
			str=sc.next();
			
			if(str.equals("push")) { //정수 x를 큐에 넣는다.
				int a=sc.nextInt();
				arr[++rear]=a;
			}
			else if(str.equals("pop")) { //가장 앞에 있는 정수를 뺴고, 그 수를 출력, 정수가 없을 경우에는 -1을 출력한다.
				if(front==rear) {
					System.out.println(-1);
				}
				else {
					++front;
					System.out.println(arr[front]);
				}
			}
			else if(str.equals("size")) { //큐에 들어있는 정수의 개수를 출력한다.
				if(front==rear) {
					System.out.println(0);
				}
				else {
					System.out.println(rear-front);
				}
			}
			else if(str.equals("empty")) { //비어있으면 1, 아니면 0
				if(front==rear) {
					System.out.println(1);
				}
				else {
					System.out.println(0);
				}
			}
			else if(str.equals("front")) { //가장 앞에 있는 정수를 출력한다. 없을경우 -1
				if(rear==front) {
					System.out.println("-1");
				}
				else {
					++front;
					System.out.println(arr[front]);
					front--;
				}
			}
			else if(str.equals("back")) { //가장 뒤에 있는 정수를 출력한다. 없는경우 -1
				if(rear==front) {
					System.out.println("-1");
				}
				else {
					System.out.println(arr[rear]);
				}
			}
			
		}

	}

}
