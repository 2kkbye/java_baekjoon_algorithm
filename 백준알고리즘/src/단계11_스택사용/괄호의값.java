package 단계11_스택사용;

import java.util.Scanner;
import java.util.Stack;

public class 괄호의값 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str=sc.next();
		int size=0;
		int p=0,l=0;
		int mul=1;int total=0;
		Stack<Character> s = new Stack<>();
	
		for(int i=0;i<str.length();i++) {
			if(str.charAt(i)=='(') {
	
				++p;
				s.push(str.charAt(i));
				mul=mul*2;
				if((i+1<str.length())&&(str.charAt(i+1)==')')){
					total=mul+total;
				}
				continue;
			}
			else if(str.charAt(i)=='[') {

				++l;
				s.push(str.charAt(i));
				mul=mul*3;
				if((i+1<str.length())&&(str.charAt(i+1)==']')){
					total=mul+total;
				}
				continue;
			}
			else if(str.charAt(i)==')') {
				if(p==0) {
					System.out.println(0);
					System.exit(0);
				}
				--p;
				s.pop();
				mul=mul/2;
				continue;
			}
			else if(str.charAt(i)==']') {
				if(l==0) {
					System.out.println(0);
					System.exit(0);
				}
				--l;
				s.pop();
				mul=mul/3;
				continue;
			}
		}
		if((l!=0)||(p!=0)||(s.size()!=0)) {
			System.out.println(0);
		}
		else {
			System.out.println(total);
		}
		sc.close();

	}
}