package 단계9_정렬;

import java.io.*;
import java.util.StringTokenizer;

public class 수정렬하기3 {

	static int arr[];
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		arr = new int[N];
		int num = 0;
		for(int a = 0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			num = Integer.parseInt(st.nextToken());
			arr[a] = num;
		}
		quickSort(0, arr.length-1);
		print();
	}
	public static void quickSort(int start, int end) {
		int s = start;
		int e = end;
		int pivot = arr[(start+end)/2];
		do {
			while(arr[s]<pivot) s++;
			while(arr[e]>pivot) e--;
			if(s<=e) {
				int temp = arr[s];
				arr[s] = arr[e];
				arr[e] = temp;
				s++;e--;
			}
		}while(s<=e);
		if(s<end)
			quickSort(s, end);
		if(start<e) {
			quickSort(start,e);
		}
	}
	public static void print() {
		for(int a = 0;a<arr.length;a++) {
			System.out.println(arr[a]);
		}
	}
}