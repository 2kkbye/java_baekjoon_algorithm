package 단계9_정렬;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Scanner;
import java.util.StringTokenizer;
public class 통계학 {

	static StringTokenizer st;
	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		st= new StringTokenizer(br.readLine());
		int num = Integer.parseInt(st.nextToken());
		int arr[]=new int[num];
		int arr_2[][]=new int[num][2];
		int sum=0; //산술평균값 구하기위하여.

		for(int i=0;i<arr_2.length;i++) {
			arr_2[i][0]=4001;
			arr_2[i][1]=0;
		}
		for(int i=0;i<arr.length;i++) {
			st= new StringTokenizer(br.readLine());
			arr[i]=Integer.parseInt(st.nextToken());
			sum=arr[i]+sum;
		}
		quickSort(arr);
		for(int i=0;i<arr.length;i++) {
			frequency(arr_2, arr[i]);
		}
		double a=(double)sum/num;
		System.out.format("%.0f",a);
		System.out.println();
		//		bw.write(String.valueOf(Math.round(sum/num)+"\n"));
		if(num%2==1) {
			bw.write(String.valueOf(arr[num/2]+"\n"));
		}
		else {
			bw.write(String.valueOf(arr[num/2+1]+"\n"));
		}
		int result=max_Check(arr_2);
		bw.write(String.valueOf(result+"\n"));
		bw.write(String.valueOf(arr[num-1]-arr[0]+"\n"));

		bw.flush();
	}
	private static int max_Check(int arr[][]) {
		int max_num,max_index,sec_num,sec_index;
		int count=0;
		int temp;
		max_num=arr[0][0];max_index=arr[0][1];
		sec_num=-4444;sec_index=-4444;
		for(int i=1;i<arr.length;i++) {
			if((arr[i][1]>max_index)&&(arr[i][0]!=4001)) {
				max_num=arr[i][0];
				max_index=arr[i][1];
				sec_num=-4444;
				sec_index=-4444;
			}
			else if(arr[i][1]==max_index&&(arr[i][0]!=4001)) {
				if(sec_num!=-4444) {
					continue;
				}
				else if((sec_num==-4444)) {
						sec_num=arr[i][0];
						sec_index=arr[i][1];
				}
			}
		}
		if(sec_index!=-4444) {
			return sec_num;
		}
		else {
			return max_num;
		}
	}
	private static void frequency(int arr[][],int num){
		boolean a=false;
		int tmp;
		for(int i=0;i<arr.length;i++) {
			if(arr[i][0]==num) {
				arr[i][1]=arr[i][1]+1;
				break;
			}
			else if(arr[i][0]==4001){
				arr[i][0]=num;
				arr[i][1]=arr[i][1]+1;
				break;
			}
		}
	}
	private static void quickSort(int arr[]) {
		quickSort(arr , 0 , arr.length-1);
	}
	private static void quickSort(int a[], int begin, int end) {
		int part2=partition(a, begin, end);
		if(begin<part2-1)	{
			quickSort(a, begin, part2-1);
		}
		if(part2 < end) {
			quickSort(a,part2,end);
		}
	}
	private static int partition(int a[], int begin, int end) {
		int pivot = a[(begin + end) / 2];

		while(begin <= end) {
			while(a[begin] < pivot) begin++;
			while(a[end] > pivot) end--;
			if(begin <= end) {
				swap(a, begin, end);
				begin++;
				end--;
			}
		}

		return begin;
	}
	public static void swap (int a[], int begin, int end) {
		int temp=a[begin];
		a[begin]=a[end];
		a[end]=temp;
	}
}
