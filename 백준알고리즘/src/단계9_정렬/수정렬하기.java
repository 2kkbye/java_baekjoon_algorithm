package 단계9_정렬;

import java.util.Scanner;
public class 수정렬하기 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt();
		
		int arr[]=new int[num];
		for(int i=0;i<arr.length;i++) {
			arr[i]=sc.nextInt();
		}
		int min;
		int temp;
		for(int i=0;i<arr.length-1;i++) {
			min=i;
			for(int j=i+1;j<arr.length;j++) {
				if(arr[min]>arr[j]) {
					min=j;
				}
			}
			temp=arr[i];
			arr[i]=arr[min];
			arr[min]=temp;
		}
		for(int i=0;i<arr.length;i++) {
			System.out.println(arr[i]);
		}
	}

}
