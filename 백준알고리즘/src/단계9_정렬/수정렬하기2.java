package 단계9_정렬;

import java.util.Scanner;

public class 수정렬하기2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt();

		int arr[]=new int[num];
		for(int i=0;i<arr.length;i++) {
			arr[i]=sc.nextInt();
		}
		quickSort(arr);


		for(int i=0;i<arr.length;i++) {
			System.out.println(arr[i]);
		}
	}
	private static void quickSort(int arr[]) {
		quickSort(arr , 0 , arr.length-1);
	}
	private static void quickSort(int a[], int begin, int end) {
		int part2=partition(a, begin, end);
		if(begin<part2-1)	{
			quickSort(a, begin, part2-1);
		}
		if(part2 < end) {
			quickSort(a,part2,end);
		}
	}
	private static int partition(int a[], int begin, int end) {
		int pivot = a[(begin + end) / 2];
		
		while(begin <= end) {
			while(a[begin] < pivot) begin++;
			while(a[end] > pivot) end--;
			if(begin <= end) {
				swap(a, begin, end);
				begin++;
				end--;
			}
		}
		
		return begin;
	}
	public static void swap (int a[], int begin, int end) {
		int temp=a[begin];
		a[begin]=a[end];
		a[end]=temp;
	}
}