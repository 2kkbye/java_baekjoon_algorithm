package 단계4if문;

import java.util.Scanner;
public class P4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt(); //시험본 과목의 개수.
		double[] arr = new double[N]; //초기 점수 저장할 배열
		double[] arr1 = new double[N]; //계산한 후 저장할 배열
		int max=0; //최대값 저장할 변수
		double sum=0;
		
		for(int i=0;i<arr.length;i++) {
			arr[i]=sc.nextInt();
		}
		for(int i=0;i<arr.length;i++) {
			if(arr[i]>max) {
				max=(int) arr[i];
			}
		}
		for(int i=0;i<arr.length;i++) {
			arr1[i]= arr[i]/max*100;
		}
		for(int i=0;i<arr1.length;i++) {
			sum=arr1[i]+sum;
		}
		System.out.printf("%.2f",sum/N);
	}
}
