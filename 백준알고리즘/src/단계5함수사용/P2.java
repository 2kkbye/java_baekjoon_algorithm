package 단계5함수사용;

import java.util.Scanner;

public class P2 {
	public static void main(String[] args) {
		HanSoo();
	}
	public static void HanSoo() {
		int cnt=0;
		int dis1,dis2;
		int one=0;//1의자리
		int ten=0;//10의자리
		int hun=0;//100의자리
		int thu=0;//1000의자리
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt();

		for(int i=1;i<=num;i++) {
			if(i>=1&&i<100) {
				cnt++;
			}
			else if(i>=100&&i<1000) {
				hun=i/100;
				ten=(i%100)/10;
				one=(i%100)%10;
				dis1=hun-ten;
				dis2=ten-one;
				if(dis1==dis2) {
					cnt++;
				}
			}
		}
		System.out.println(cnt);
	}
}
