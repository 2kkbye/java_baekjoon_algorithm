package 단계8_규칙찾기;

import java.util.Scanner;
public class 분수찾기 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt();
		int a=0; //분모
		int b=0; //분자
		int cnt=2;//실제 횟수 세는것 num이랑 계속 비교한다.
		int i=2;//주기이다
		while(true) {
			if(num==1) {
				System.out.println("1/1");
				System.exit(0);
			}
			else {
				if(i%2==0) {//주기가 짝수
					a=i;
					b=1;
					if(cnt==num) {
						System.out.println(b+"/"+a);
						System.exit(0);
					}
					for(int x=0;x<i-1;x++) {
						b=b+1;
						a=a-1;
						cnt++;
						if(cnt==num) {
							System.out.println(b+"/"+a);
							System.exit(0);
						}
					}
					cnt++;
					i=i+1;//주기인데 뭔가 애매하다. 
				}
				else { //주기가 홀수
					a=1;
					b=i;
					if(cnt==num) {
						System.out.println(b+"/"+a);
						System.exit(0);
					}
					for(int x=0;x<i-1;x++) {
						cnt++;
						b=b-1;
						a=a+1;
						if(cnt==num) {
							System.out.println(b+"/"+a);
							System.exit(0);
						}
					}
					cnt++;
					i=i+1;
				}
			}
		}
	}
}
