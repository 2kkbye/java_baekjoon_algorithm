package 단계8_규칙찾기;

import java.util.Scanner;
public class FlymetotheAl {
	public static void main(String[] args) {
		

		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		long arr[]=new long[n];
		for(int i=0;i<n;i++) {
			int x=sc.nextInt();
			int y=sc.nextInt();
			int dis=y-x; //y,x의 차이
			arr[i]=func(dis);
		}
		for(int i=0;i<arr.length;i++) {
			System.out.println(arr[i]);
		}
	}
	public static long func(int dis) {
		double root=Math.sqrt(dis);  //제곱근값
		long n=Math.round(root); //인트형 변수로 반올림한 값이 들어간다. 근사값이들어간다.

		long nn=n*n;

		if(dis<=nn) {
			return 2*n-1;
		}
		else
			return 2*n;

	}
}
