package 단계8_규칙찾기;

import java.util.Scanner;
public class ACM호텔 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt(); //테스트케이스 개수
		int arr[]=new int[num];
		for(int i=0;i<num;i++) {
			int H=sc.nextInt();
			int W=sc.nextInt();
			int N=sc.nextInt();
			if(N>H*W) {
				System.out.println("잘못입력");
			}
			else {
				arr[i]=AcmHotel(H,W,N,num);
			}
		}
		for(int i=0;i<arr.length;i++) {
			System.out.println(arr[i]);
		}
	}
	public static int AcmHotel(int H,int W,int N,int num) {
		int arr[]=new int[H*W];
		int h=1;
		int w=1;
		int n=1;
		while(true) {
			if(N==n) {
				return ((h*100)+w);
			}
			else if(h<H) {
				h++;
				n++;
			}
			else if(h==H) {
				h=1;
				w++;
				n++;
			}
		}
	}
}
