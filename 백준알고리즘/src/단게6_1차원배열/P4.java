package 단게6_1차원배열;

import java.util.Arrays;
import java.util.Scanner;
public class P4 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int arr[] = new int[8];
		int asc[] = {1,2,3,4,5,6,7,8};
		int desc[] = {8,7,6,5,4,3,2,1};

		for(int i=0;i<arr.length;i++) {
			arr[i]=sc.nextInt();
		}
		for(int i=0;i<arr.length;i++) {
			if(Arrays.equals(arr, asc)) {
				System.out.println("ascending");
				break;
			}
			else if(Arrays.equals(arr, desc)) {
				System.out.println("descending");
				break;
			}
			else
				System.out.println("mixed");
			break;
		}
	}
}
