package 단계10_소수찾기;

import java.util.Scanner;
public class 소수 {
	static int cnt=0;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num1=sc.nextInt();
		int num2=sc.nextInt();
		int sum=0;
		int tmp=0;
		int min=0;
		for(int i=num1;i<=num2;i++) {
			tmp=Sosu_find(i);
			if(tmp==1) {
				if(sum==0) {
					min=i;
					sum=sum+i;
				}
				else {
					sum=sum+i;
				}
			}
		}
		if(sum!=0) {
			System.out.println(sum);
			System.out.println(min);
		}
		else {
			System.out.println("-1");
		}

	}
	public static int Sosu_find(int num) {
		int tmp=0;
		if(num>1) {
			for(int i=1;i<=num/2;i++) {
				if(num%i==0)
					tmp++;
			}
			if(tmp>=2) {
				return 0;
			}
			else {
				return 1;
			}
		}
		return 0;
	}

}
