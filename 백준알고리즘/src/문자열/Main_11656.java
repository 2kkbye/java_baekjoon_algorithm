package ���ڿ�;

import java.io.*;
import java.util.StringTokenizer;

public class Main_11656 {

	static String arr[];
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		String str = st.nextToken();
		arr = new String[str.length()];
		String temp ="";
		for(int b = str.length()-1;b>=0;b--) {
			temp = str.charAt(b)+temp;
			arr[b] = temp;

		}
		Stringsort(0,arr.length-1);
		print();
	}
	
	public static void print() {
		for(int a =0;a<arr.length;a++) {
			System.out.println(arr[a]);
		}
	}
	
	public static void Stringsort(int start, int end) {
		int s = start;
		int e = end;
		String pivot = arr[(start+end)/2];
		do {
			//System.out.println("c");
			while(pivot.compareTo(arr[s]) > 0) s++;
			while(pivot.compareTo(arr[e]) < 0) e--;
			if(s<=e) {
				String temp = arr[s];
				arr[s] = arr[e];
				arr[e] = temp;
				s++;
				e--;
			}
		}while(s<=e);
		if(s<end) {
			Stringsort(s, end);
		}
		if(start<e) {
			Stringsort(start, e);
		}
	}
}
