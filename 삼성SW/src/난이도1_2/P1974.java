package 난이도1_2;

import java.util.Scanner;
public class P1974 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int arr[][]=new int[9][9];

			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length;b++) {
					arr[a][b]=sc.nextInt();
				}
			}
			int cnt=0;
			int ar[]= {0,0,0,0,0,0,0,0,0,0};
			int sw=1;
			//3*3 사각형 전체적으로 돌면서 확인
			for(int j=0;j<=6;j+=3) {
				for(int a=0;a<arr.length;a++) {
					for(int b=j;b<j+3;b++) {
						if(ar[arr[a][b]]==0) {
							ar[arr[a][b]]=ar[arr[a][b]]+1;
						}
						else {
							sw=0;
						}
					}
					cnt=cnt+1;
					if(cnt == 3) {
						ar=chogi(ar);
						cnt=0;
					}
				}
			}
			//가로
			for (int a=0;a<9;a++) {
				for(int b=0;b<9;b++) {
					if(ar[arr[a][b]]==0) {
						ar[arr[a][b]]=ar[arr[a][b]]+1;
					}
					else {
						sw=0;
					}
				}
				ar=chogi(ar);
			}
			//세로
			for (int a=0;a<9;a++) {
				for(int b=0;b<9;b++) {
					if(ar[arr[b][a]]==0) {
						ar[arr[b][a]]=ar[arr[b][a]]+1;
					}
					else {
						sw=0;
					}
				}
				ar=chogi(ar);
			}
			System.out.println("#"+(i+1)+" "+sw);
		}
	}
	//초기화
	public static int [] chogi(int arr[]) {
		for(int i=0;i<arr.length;i++) {
			arr[i]=0;
		}
		return arr;
	}

}
