package ���̵�1_2;

import java.util.Scanner;
public class P1984 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		int arr[] = new int[10];
		for(int i=0;i<T;i++) {
			int max=-100;
			int max_i=0;
			int min=100;
			int min_i=0;
			for(int a=0;a<arr.length;a++) {
				arr[a]=sc.nextInt();
				if(arr[a]<min) {
					min=arr[a];
					min_i=a;
				}
				if(arr[a]>max) {
					max=arr[a];
					max_i=a;
				}
			}
			float sum=0;
			for(int a=0;a<arr.length;a++) {
				if(a==min_i || a==max_i) {
					continue;
				}
				else {
					sum=sum+arr[a];
				}
			}
			float result=sum/8;
			System.out.print("#"+(i+1)+" ");
			System.out.format("%.0f", result);
			System.out.println();
		}
	}
}
