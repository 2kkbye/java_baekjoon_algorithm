package 난이도1_2;

import java.util.Scanner;
public class P2001 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int N= sc.nextInt(); // 2중 배열 크기
			int M= sc.nextInt(); // 파리채 크기.
			
			int arr[][]= new int[N][N];
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length;b++) {
					arr[a][b]=sc.nextInt();
				}
			}
			int max=0;
			int temp=0;
			for(int a=0;a<=N-M;a++) {
				for(int b=0;b<=N-M;b++) {
					temp=check(M,a,b,arr);
					if(temp>max) {
						max=temp;
					}
				}
			}
			System.out.println("#"+(i+1)+" "+max);
			
		}

	}
	public static int check(int M,int x,int y,int ar[][]) {
		int result=0;
		for(int a=x;a<x+M;a++) {
			for(int b=y;b<y+M;b++) {
				result=result+ar[a][b];
			}
		}
		return result;
	}

}
