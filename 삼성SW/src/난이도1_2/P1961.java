package 난이도1_2;

import java.util.Scanner;
public class P1961 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();		
		for(int i=0;i<T;i++) {
			int N=sc.nextInt();
			int arr[][]=new int[N][N];
			int temp[][] = new int[N][N];
			String ar[][] = new String[N][N];
			//초기화
			for(int a=0;a<ar.length;a++) {
				for(int b=0;b<ar[a].length;b++) {
					ar[a][b]="";
				}
			}
			//입력받기
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length;b++) {
					arr[a][b]=sc.nextInt();
				}
			}
			//90도
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length;b++) {
					temp[b][(arr.length-1)-a]=arr[a][b];
				}
			}
			ar=func(temp,ar,0);
			//180도
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length;b++) {
					temp[(arr.length-1)-a][(arr.length-1)-b]=arr[a][b];
				}
			}
			ar=func(temp,ar,1);
			//270도
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length;b++) {
					temp[(arr.length-1)-b][a]=arr[a][b];
				}
			}
			ar=func(temp,ar,2);
			
			System.out.println("#"+(i+1));
			for(int a=0;a<ar.length;a++) {
				for(int b=0;b<ar[a].length;b++) {
					System.out.print(ar[a][b]+" ");
				}
				System.out.println();
			}
		}
	}
	public static String [][] func(int arr[][],String ar[][],int index){
		for(int i=0;i<arr.length;i++) {
			for(int j=0;j<arr[i].length;j++) {
				ar[i][index]=ar[i][index]+Integer.toString(arr[i][j]);
			}
		}
		
		return ar;
	}

}
