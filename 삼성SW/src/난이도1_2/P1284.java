package 난이도1_2;

import java.util.Scanner;
public class P1284 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int a_size = sc.nextInt();
		int arr[] = new int[a_size+1];
		for(int a=0;a<a_size;a++) {
			
			int P = sc.nextInt(); // A사 1L당 요금
			int Q = sc.nextInt(); // B사 기본요금
			int R = sc.nextInt(); // B사 기본 기준
			int S = sc.nextInt(); // 1L당 요금
			int W = sc.nextInt(); // 사용량
			
			int result_A = 0;
			result_A = P*W;
			//System.out.println(result_A);
			int result_B = 0;
			if(W <= R) {
				result_B = Q;
			}
			else {
				int ex_B = W-R;
				result_B = (ex_B * S) + Q;
			}
			if(result_A > result_B) {
				//System.out.println("!!!!!"+result_A);
				arr[a+1]=result_B;
			}
			else {
				//System.out.println("!!!!!"+result_B);
				arr[a+1]=result_A;
			}
		}
		for(int a=1;a<arr.length;a++) {
			System.out.println("#"+a+" "+arr[a]);
		}
	}
}
