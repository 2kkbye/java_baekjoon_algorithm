package 난이도1_2;

import java.util.Scanner;
public class P1859 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();

		for(int i=0;i<T;i++) {
			int N = sc.nextInt();
			int arr[]= new int[N];			
			int max=-999;
			int max_index=0;
			int sum=0;
			long result=0;
			int cnt=0;
			//데이터 입력, 1차 최대값 찾기.
			int ar[]=new int[2];
			for(int a=0;a<arr.length;a++) {
				arr[a]=sc.nextInt();
				if(max < arr[a]) {
					max=arr[a];
					max_index=a;
				}
			}
			for(int a=0;a<arr.length;a++) {
				if(a == max_index) {
					if(cnt==0) {
						max_index=max_num(arr,max_index+1);
						continue;
					}
					result=(sum+(cnt*arr[max_index]))+result;
					max_index=max_num(arr,max_index+1);
					cnt=0;
					sum=0;
				}
				else {
					cnt=cnt+1;
					sum=sum-arr[a];
				}
			}
			System.out.println("#"+(i+1)+" "+result);
		}

	}
	public static int max_num(int arr[],int start_index) {
		int max=-999;
		int max_index=-999;
		for(int i=start_index;i<arr.length;i++) {
			if(arr[i]>max) {
				max=arr[i];
				max_index=i;
			}
		}
		return max_index;
	}

}
