package 난이도1_2;

import java.util.Scanner;
public class P1928 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		for(int i=0;i<num;i++) {
			String en_qu = sc.next();
			String t_result="";
			String result="";
			for(int a=0;a<en_qu.length();a++) {
				String re = func(en_qu.charAt(a));
				t_result=t_result+re;
				if(((a+1)%4)==0) {//4번째일경우 처리해줘야함
					String temp="";
					for(int b=0;b<t_result.length();b++) {
						temp=temp+t_result.charAt(b);
						if(((b+1)%8)==0) {
							char x= (char) Integer.parseInt(temp,2);
							result=result+x;
							temp="";
						}

					}
					t_result="";
				}
			}
			System.out.println("#"+(i+1)+" "+result);

		}
	}
	public static String func(char c) {
		int result=0;
		if(c>='A' && c<='Z') {
			result=c-65;

		}
		else if(c>='a' && c<='z') {
			result=c-71;
		}
		else if(c>='0' && c<='9') {
			result=c+4;
		}
		else if(c=='+') {
			result=c+19;
		}
		else if(c=='/') {
			result=c+16;
		}
		String a = Integer.toBinaryString(result);
		String temp="";
		temp=a;
		for(int i=0;i<(6-a.length());i++) {
			temp="0"+temp;
		}
		return temp;
	}
}