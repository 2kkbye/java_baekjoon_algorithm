package 난이도4;

import java.util.Scanner;
public class P1249_2 {
	static int arr[][];
	static int dis[][];
	static boolean check[][];
	static int max_size = Integer.MAX_VALUE;
	static int bol_cnt;
	static int N;
	static int result;
	static int sw;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T =sc.nextInt();
		for(int i=0;i<T;i++) {
			N = sc.nextInt();
			arr = new int[N][N];
			dis = new int[N][N];
			check = new boolean[N][N];
			String temp[][] = new String[N][1];
			for(int a=0;a<temp.length;a++) {
				temp[a][0] = sc.next();
			}
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr.length;b++) {
					arr[a][b]=(int)temp[a][0].charAt(b)-48;
					dis[a][b]=max_size;
					check[a][b] = false;
				}
			}
			result = Integer.MAX_VALUE;
			bol_cnt=0;
			dis[0][0]=0;
			sw=0;
			dig(0,0);
		}
	}
	public static void print() {
		for(int a=0;a<arr.length;a++) {
			for(int b=0;b<arr.length;b++) {
				if(dis[a][b]!=max_size) {
					System.out.print(dis[a][b]+" ");
				}
				else {
					System.out.print("-1 ");
				}
			}
			System.out.println();
		}
	}
	//먼저 주변 4개 좌표에 대하여  dis[]에 있는 무한대인 값들을 arr에 있는값으로 넣어주기
	public static int[] check_sum(int x,int y) {
		int ar[]=new int[2];
		int min_value = Integer.MAX_VALUE;
		int index_x=-1;
		int index_y=-1;
		if((y+1)<=(arr.length-1)&&check[x][y+1]==false&&dis[x][y+1]==max_size) {
			dis[x][y+1]=arr[x][y+1]+dis[x][y];
			if(dis[x][y+1]<min_value) {
				index_x=x;
				index_y=y+1;
				min_value=dis[x][y+1];
			}
		}
		if(x+1<=(arr.length-1)&&check[x+1][y]==false&&dis[x+1][y]==max_size) {
			dis[x+1][y]=arr[x+1][y]+dis[x][y];
			if(dis[x+1][y]<min_value) {
				index_x=x+1;
				index_y=y;
				min_value=dis[x+1][y];
			}
		}
		if(x-1>=0&&check[x-1][y]==false&&dis[x-1][y]==max_size) {
			dis[x-1][y]=arr[x-1][y]+dis[x][y];
			if(dis[x-1][y]<min_value) {
				index_x=x-1;
				index_y=y;
				min_value=dis[x-1][y];
			}
		}
		if(y-1>=0&&check[x][y-1]==false&&dis[x][y-1]==max_size) {
			dis[x][y-1]=arr[x][y-1]+dis[x][y];
			if(dis[x][y-1]<min_value) {
				index_x=x;
				index_y=y-1;
				min_value=dis[x][y-1];
			}
		}
		ar[0]=index_x;
		ar[1]=index_y;
		return ar;
	}
	public static void dig(int x,int y) {
		if(x==0&&y==0) {
			sw=0;
		}
		if(sw==1) {
			return;
		}
		if(x==-1&&y==-1) {
			return;
		}
		System.out.println(x+"============="+y);
		print();
		if(x==arr.length-1&&y==arr.length-1) {
			if(dis[x][y]<result) {
				sw=1;
				result=dis[x][y];
				return;
			}
		}
		int ar[] = new int[2];
		check[x][y]=true;
		bol_cnt++;
		if(bol_cnt==((N*N)-1)) {
			return;
		}
		ar=check_sum(x,y);
		dig(ar[0],ar[1]);
		check[x][y]=false;
		bol_cnt--;
	}
}