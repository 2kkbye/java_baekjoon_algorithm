package ���̵�4;

import java.util.Scanner;
public class P1249 {
	static int arr[][];
	static int min;
	static int pre_num;
	static boolean check[][];
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int N = sc.nextInt();
			String temp[][] = new String[N][1];
			arr=new int[N][N];
			check=new boolean[N][N];
			for(int a=0;a<check.length;a++) {
				for(int b=0;b<check.length;b++) {
					check[a][b]=false;
				}
			}
			for(int a=0;a<temp.length;a++) {
				temp[a][0] = sc.next();
			}
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr.length;b++) {
					arr[a][b]=(int)temp[a][0].charAt(b)-48;
				}
			}
			min=Integer.MAX_VALUE;
			pre_num=Integer.MAX_VALUE;
			bfs(0,0);
			System.out.println("#"+(i+1)+" "+min);
		}
	}
	public static void bfs(int x,int y) {
		check[x][y]=true;
		if(x==arr.length-1&&y==arr.length-1) {
			if(arr[x][y]<min) {
				min=arr[x][y];
				return;
			}
			else {
				if(pre_num>arr[x][y]) {
					pre_num=arr[x][y];
				}
				return;
			}
		}	
		if(arr[x][y]>min||pre_num<arr[x][y]) {
			return;
		}
		if((y+1)<=(arr.length-1)&&check[x][y+1]==false) {
			arr[x][y+1]=arr[x][y+1]+arr[x][y];
			bfs(x,y+1);
			arr[x][y+1]=arr[x][y+1]-arr[x][y];
			check[x][y+1]=false;
		}
		if(x+1<=(arr.length-1)&&check[x+1][y]==false) {
			arr[x+1][y]=arr[x+1][y]+arr[x][y];
			bfs(x+1,y);
			arr[x+1][y]=arr[x+1][y]-arr[x][y];
			check[x+1][y]=false;
		}
		if(x-1>=0&&check[x-1][y]==false) {
			arr[x-1][y]=arr[x-1][y]+arr[x][y];
			bfs(x-1,y);
			arr[x-1][y]=arr[x-1][y]-arr[x][y];
			check[x-1][y]=false;
		}
		if(y-1>=0&&check[x][y-1]==false) {
			arr[x][y-1]=arr[x][y-1]+arr[x][y];
			bfs(x,y-1);
			arr[x][y-1]=arr[x][y-1]-arr[x][y];
			check[x][y-1]=false;
		}
		return;
	}
}
