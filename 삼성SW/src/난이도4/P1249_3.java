package ���̵�4;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class P1249_3 {

	static int arr[][];
	static int dis[][];
	static boolean check[][];
	static int max_size = Integer.MAX_VALUE;
	static int bol_cnt;
	static int N;
	static int result;
	static ArrayList<Point> list;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T =sc.nextInt();
		for(int i=0;i<T;i++) {
			list = new ArrayList<Point>();
			N = sc.nextInt();
			arr = new int[N][N];
			dis = new int[N][N];
			check = new boolean[N][N];
			String temp[][] = new String[N][1];
			for(int a=0;a<temp.length;a++) {
				temp[a][0] = sc.next();
			}
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr.length;b++) {
					arr[a][b]=(int)temp[a][0].charAt(b)-48;
					dis[a][b]=max_size;
					check[a][b] = false;
				}
			}
			result = Integer.MAX_VALUE;
			bol_cnt=0;
			dis[0][0]=0;
			list.add(new Point(0,0));
			dig();
			System.out.println("#"+(i+1)+" "+dis[arr.length-1][arr.length-1]);
		}
	}
	public static void print() {
		for(int a=0;a<arr.length;a++) {
			for(int b=0;b<arr.length;b++) {
				if(dis[a][b]!=max_size) {
					System.out.print(dis[a][b]+" ");
				}
				else {
					System.out.print("-1 ");
				}
			}
			System.out.println();
		}
	}
	public static void sort() {
		list.sort(new Comparator<Point>() {
			@Override
			public int compare(Point arg0, Point arg1) {
				// TODO Auto-generated method stub
				int first_x = arg0.x;
				int first_y= arg0.y;
				int second_x=arg1.x;
				int second_y=arg1.y;
				
				if (dis[first_x][first_y] == dis[second_x][second_y])
					return 0;
				else if (dis[first_x][first_y] > dis[second_x][second_y])
					return 1;
				else
					return -1;
			}
		});
	}
	public static void check_sum(int x,int y) {
		if((y+1)<=(arr.length-1)&&check[x][y+1]==false&&dis[x][y+1]==max_size) {
			dis[x][y+1]=arr[x][y+1]+dis[x][y];
			check[x][y+1]=true;
			list.add(new Point(x,y+1));
		}
		if(x+1<=(arr.length-1)&&check[x+1][y]==false&&dis[x+1][y]==max_size) {
			dis[x+1][y]=arr[x+1][y]+dis[x][y];
			check[x+1][y]=true;
			list.add(new Point(x+1,y));
			
		}
		if(x-1>=0&&check[x-1][y]==false&&dis[x-1][y]==max_size) {
			dis[x-1][y]=arr[x-1][y]+dis[x][y];
			check[x-1][y]=true;
			list.add(new Point(x-1,y));
		}
		if(y-1>=0&&check[x][y-1]==false&&dis[x][y-1]==max_size) {
			dis[x][y-1]=arr[x][y-1]+dis[x][y];
			check[x][y-1]=true;
			list.add(new Point(x,y-1));
		}
	}
	public static void dig() {
		
		while(list.size()!=0) {
			sort();
			int a=list.get(0).x;
			int b=list.get(0).y;
			if(a==arr.length-1&&b==arr.length-1) {
				if(dis[a][b]<result) {
					result=dis[a][b];
				}
			}
			check_sum(a,b);
			list.remove(0);
		}
	}
}
