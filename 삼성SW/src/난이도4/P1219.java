package 난이도4;

import java.util.Scanner;
public class P1219 {
	static int arr[][];
	static int re;
	static boolean bol[];
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		for(int i=0;i<10;i++) {
			arr = new int[100][100];
			bol = new boolean[100];
			int t=sc.nextInt();
			int N = sc.nextInt();
			//초기화
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr.length;b++) {
					arr[a][b] = -3;
				}
			}
			for(int a=0;a<bol.length;a++) {
				bol[a]=false;
			}
			//입력받기
			for(int a=0;a<N;a++) {
				int x=sc.nextInt();
				int y=sc.nextInt();
				arr[x][y]=1;
			}
			re=0;
			search(0);
			System.out.println("#"+(i+1)+" "+re);

		}

	}
	public static void search(int v) {
		if(re==1) {
			return;
		}
		bol[v]=true;
		if(v==99) {
			re=1;
			return;
		}
		for(int a=0;a<arr.length;a++) {
			if(arr[v][a]==1&&bol[a]==false) {
				search(a);
			}
		}
		bol[v]=false;
	}
	public static void print() {
		for(int a=0;a<arr.length;a++) {
			for(int b=0;b<arr.length;b++) {
				System.out.print(arr[a][b]+" ");
			}
			System.out.println();
		}
	}

}
