package 난이도4;

import java.util.Scanner;

public class P1211 {
	static int arr[][];
	static int result;
	static int start;
	static int sw;
	static int min;
	static int cnt;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		for(int i=0;i<10;i++) {
			int t = sc.nextInt();
			
			arr	= new int[100][100];
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr.length;b++) {
					arr[a][b] = sc.nextInt();
				}
			}
			min=Integer.MAX_VALUE;
			result = 0;
			for(int a=0;a<arr.length;a++) {
				if(arr[0][a]==1) {
					cnt = 0;
					sw=0;
					start = a;
					dfs(0,a);
				}
			}
			System.out.println("#"+(i+1)+" "+result);
		}
	}
	public static void dfs(int x,int y) {
		//System.out.println("a");
		if(sw==1) return;
		cnt++;
		if(arr[x][y]==1&&x==arr.length-1) {
			sw=1;
			if(cnt<min) {
				min=cnt;
				result=start;
			}
			return;
		}
		if(((y-1)>=0)&&arr[x][y-1]==1) {
			bfs(x,y,1);
		}
		if(((y+1)<=arr.length-1)&&arr[x][y+1]==1) {
			bfs(x,y,2);
		}
		dfs(x+1,y);
	}
	//sw 1은 왼쪽,2는 오른쪽
	public static void bfs(int x,int y,int sw) {
		cnt++;
		if(sw==1&&y-1>=0&&arr[x][y-1]==1) {
			bfs(x,y-1,sw);
		}
		else if(sw==2&&y+1<=arr.length-1&&arr[x][y+1]==1) {
			bfs(x,y+1,sw);
		}
		else {
			dfs(x+1,y);
		}	
	}
}