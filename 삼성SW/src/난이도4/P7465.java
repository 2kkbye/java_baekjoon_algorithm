package 난이도4;

import java.io.*;

public class P7465 {
	static int map[][];
	static boolean number[];
	static int N;
	static int ans;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine().trim());
		for(int i=1;i<=T;i++) {
			String in = br.readLine().trim();
			N = Integer.parseInt(in.split(" ")[0])+1;
			int M = Integer.parseInt(in.split(" ")[1]);
			map = new int[N][N];
			number = new boolean[N];
			for(int a=0;a<M;a++) {
				in = br.readLine().trim();
				int x = Integer.parseInt(in.split(" ")[0]);
				int y = Integer.parseInt(in.split(" ")[1]);
				map[x][y]=1;map[y][x]=1;
			}
			
			ans = 0;
			//2차원 배열과 그 2차원배열의 숫자를 나타내는 1차원 boolean 배열이 있습니다. 방문된거는 true로 만단하며 처음에 새롭게 등장한 숫자에 대해서만 true시켜줬습니다.
			for(int a=1;a<N;a++) {
				//if(number[a]) continue;
				for(int b=1;b<N;b++) {
					if(!number[b] && map[a][b]==1 && !number[a]) {
						number[a]=true;
						number[b]=true;
						dfs(b);
						ans++;
					}
					else if(!number[b] && map[a][b]==1 &&number[a]) {
						number[a]=true;
						number[b]=true;
						dfs(b);
					}
				}
			}
			System.out.println("#"+i+" "+ans);
		}
	}
	public static void dfs(int x) {
		for(int a=x;a<x+1;a++) {
			for(int b=1;b<N;b++) {
				if(!number[b] && map[a][b]==1) {
					number[b]=true;
					dfs(b);
				}
			}
		}
	}
}