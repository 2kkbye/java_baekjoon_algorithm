package ���̵�3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
public class P6781 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			String num1=sc.next();
			String col1=sc.next();
			
			ArrayList<Integer> num = new ArrayList<Integer>();
			ArrayList<Character> col = new ArrayList<Character>();
			for(int a=0;a<9;a++) {
				num.add((int)num1.charAt(a)-48);
				col.add(col1.charAt(a));
			}
			
			ArrayList<Color> list = new ArrayList<Color>();			
			for(int a=0;a<9;a++) {
				list.add(new Color(num.get(a),col.get(a)));
			}			
			list.sort(new Comparator<Color>() {
				@Override
				public int compare(Color arg0, Color arg1) {
					// TODO Auto-generated method stub
					int age0 = arg0.getNum();
					int age1 = arg1.getNum();
					if (age0 == age1)
						return 0;
					else if (age0 > age1)
						return 1;
					else
						return -1;
				}
			});
			int e_cnt=0;
			int p_cnt=0;
			int total=0;
			int temp=0;
			for(int a=0;a<list.size();a++) {
				for(int b=a+1;b<list.size();b++) {
					if((list.get(a).getNum()==list.get(b).getNum())&&(list.get(a).getColor()==list.get(b).getColor())) {
						if(e_cnt==0) {
							e_cnt=e_cnt+1;
							temp=b;
							continue;
						}
						else if(e_cnt==1) {
							total++;
							e_cnt=0;
							list.remove(temp);
							list.remove(b-1);
							break;
						}
					}
					else if((list.get(a).getNum()+1)==list.get(b).getNum()||(list.get(a).getNum()+2)==list.get(b).getNum()) {
						if(p_cnt==0&&((list.get(a).getNum()+1)==list.get(b).getNum()) && (list.get(a).getColor()==list.get(b).getColor())) {
							p_cnt++;
							temp=b;
							continue;
						}
						else if(p_cnt==1&&((list.get(a).getNum()+2)==list.get(b).getNum())&& (list.get(a).getColor()==list.get(b).getColor())) {
							total++;
							p_cnt=0;
							list.remove(temp);
							list.remove(b-1);
							break;
						}
					}
//					p_cnt=0;
//					e_cnt=0;
				}
			}
			if(total>=3) {
				System.out.println("#"+(i+1)+" Win");
			}
			else {
				System.out.println("#"+(i+1)+" Continue");
			}
		}
	}
}
class Color {
	int anum;
	char acolor;
	public Color() {
		
	}
	public Color(int num, char color) {
		anum= num;
		acolor = color;
	}
	public int getNum() {return anum;}
	public char getColor() {return acolor;}
}