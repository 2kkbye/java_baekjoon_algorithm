package 난이도3;

import java.util.Scanner;
public class P1220 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		for(int i=0;i<10;i++) {
			int n= sc.nextInt();
			int arr[][] = new int[n][n];
			
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length;b++) {
					arr[a][b] = sc.nextInt();
				}
			}
			int cnt=0;
			int sw =-1;
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length;b++) {
					if(arr[a][b]==1) {
						sw=func(arr,a,b);
						if(sw == 2) {
							cnt =cnt+1;
						}
					}
				}
			}
			System.out.println("#"+(i+1)+" "+cnt);
		}
	}
	public static int func(int arr[][], int index, int index_b) {
		int sw =0;
		for (int i=index+1;i<arr.length;i++) {
			if(arr[i][index_b] == 0 ) {
				continue;
			}
			else if(arr[i][index_b]==1) { //자기자신이랑  같은거
				sw =1;
				break;
			}
			else if(arr[i][index_b]==2) {
				sw =2;
				break;
			}
		}
		if(sw==2) {
			return 2;
		}
		else {
			return 1;
		}
	}

}
