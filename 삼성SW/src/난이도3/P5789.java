package 난이도3;

import java.util.Scanner;
public class P5789 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		
		for(int i=0;i<T;i++) {
			int N = sc.nextInt();
			int Q = sc.nextInt();
			
			int arr[] = new int[N+1];
			//초기화
			for(int a=0;a<arr.length;a++) {
				arr[a]=0;
			}
			for(int a=1;a<=Q;a++) {
				int L = sc.nextInt();
				int R = sc.nextInt();
				func(arr,a,L,R);
			}
			System.out.print("#"+(i+1)+" ");
			for(int a=1;a<arr.length;a++) {
				System.out.print(arr[a]+" ");
			}
			System.out.println();
		}
	}
	public static void func(int arr[],int a,int L, int R) {
		
		for(int i=L;i<=R;i++) {
			arr[i] = a;
		}
	}
}
