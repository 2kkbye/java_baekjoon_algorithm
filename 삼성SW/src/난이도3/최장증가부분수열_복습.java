package 난이도3;

import java.util.Scanner;
public class 최장증가부분수열_복습 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int N = sc.nextInt();
			int arr[] = new int[N];
			for(int a=0;a<arr.length;a++) {
				arr[a] = sc.nextInt();
			}
			int dp[] = new int[N];
			dp[0]=1;
			int max =0;
			for(int a=1;a<arr.length;a++) {
				dp[a]=1;
				for(int b=0;b<a;b++) {
					if(arr[a]>arr[b] && dp[a]<=dp[b]) {
						dp[a]=dp[b]+1;
					}
				}
				if(dp[a]>max) {
					max = dp[a];
				}
			}
			System.out.println("#"+(i+1)+" "+max);
		}

	}

}
