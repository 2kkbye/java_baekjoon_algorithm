package 난이도3;

import java.util.ArrayList;
import java.util.Scanner;
public class P4371 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T =sc.nextInt();
		for(int i=0;i<T;i++) {
			ArrayList<Integer> list = new ArrayList<Integer>();
			int N = sc.nextInt();
			for(int a=0;a<N;a++) {
				int n = sc.nextInt();
				if(n!=1) {
					int sw=0;
					for(int b=0;b<list.size();b++) {
						if(((n-1)%list.get(b))==0){ //안에 있다는 것
							sw=1;
							break;
						}
					}
					if(sw==0) {
						list.add(n-1);
					}
				}
			}
			System.out.println("#"+(i+1)+" "+list.size());
		}
	}
}
