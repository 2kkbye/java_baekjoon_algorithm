package ���̵�3;

import java.util.Scanner;
import java.util.ArrayList;

public class P5986 {
	static int cnt;
	static int N;
	static int start;
	static ArrayList<Integer> list;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			N = sc.nextInt();
			cnt=0;
			list = new ArrayList<Integer>();
			for(int a=2;a<N;a++) {
				sosu_find(a);
			}
			for(int a=0;a<list.size();a++) {				
				backtracking(list.get(a), 0,1,a);
			}
			System.out.println("#"+(i+1)+" "+cnt);
		}
	}
	public static void backtracking(int a,int sum,int count,int index) {
		sum = sum + a;
		if(sum == N && count ==3) {
			cnt++;
			return;
		}
		if(sum > N || count >3) {
			return;
		}
		
		if(sum < N && count < 3) {
			for(int k=index;k<list.size();k++) {
				backtracking(list.get(k), sum , count+1,k);
			}
		}
	}
	public static void sosu_find(int num) {
		int tmp=0;
		if(num>1) {
			for(int i=1;i<=Math.sqrt(num);i++) {
				if(num%i==0)
					tmp++;
				if(tmp>=2) {
					return;
				}
			}
			if(tmp<2) {
				list.add(num);
			}
		}
	}
}
