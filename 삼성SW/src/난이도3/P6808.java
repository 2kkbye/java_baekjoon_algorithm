package 난이도3;

import java.util.Scanner;
import java.util.ArrayList;
public class P6808 {
	static boolean bol[]=new boolean[9];
	static int kyu_win;
	static int kyu_lose;
	static int kyu_sum;
	static int young_sum;
	static ArrayList<Integer> young;
	static ArrayList<Integer> temp;
	static ArrayList<Integer> kyu;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int arr[]=new int[19];
			//유동값
			young = new ArrayList<Integer>();
			//고정값
			kyu = new ArrayList<Integer>();
			for(int a=0;a<9;a++) {
				int temp =sc.nextInt();
				arr[temp]=1;
				kyu.add(temp);
			}
			for(int a=1;a<arr.length;a++) {
				if(arr[a]!=1) {
					young.add(a);
				}
			}
			for(int a=0;a<bol.length;a++) {
				bol[a] = false;
			}
			kyu_win=0;
			kyu_lose=0;
			temp = new ArrayList<Integer>();
			for(int a=0;a<kyu.size();a++) {
				search(a,1);
			}
			System.out.println("#"+(i+1)+" "+kyu_win+" "+kyu_lose);
		}
	}
	public static void check() {
		for(int a=0;a<9;a++) {
			if(temp.get(a)>kyu.get(a)) {
				young_sum=young_sum+temp.get(a)+kyu.get(a);
			}
			else {
				kyu_sum=kyu_sum+kyu.get(a)+temp.get(a);
			}
		}
		if(kyu_sum>young_sum) {
			kyu_win++;
		}
		else if(kyu_sum<young_sum) {
			kyu_lose++;
		}
	}
	public static void search(int x,int level) {
		bol[x]=true;
		temp.add(young.get(x));
		if(level==9) { //검사 
			check();
			bol[x]=false;
			temp.remove(temp.size()-1);
			kyu_sum=0;
			young_sum=0;
			return;
		}
		for(int a=0;a<9;a++) {
			if(bol[a]==false) {
				search(a,level+1);
			}
		}
		temp.remove(temp.size()-1);
		bol[x]=false;
	}
}