package ���̵�3;

import java.math.BigInteger;
import java.util.Scanner;
public class P5607 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int N = sc.nextInt();
			int R = sc.nextInt();
			long arr[] = new long[N+1];
			arr[0] = 1;
			for(int a=1;a<arr.length;a++) {
				arr[a] = (arr[a-1]*a)%1234567891;
			}
			BigInteger p = BigInteger.valueOf(1234567891);
			BigInteger n = BigInteger.valueOf(arr[N]);
			BigInteger r = BigInteger.valueOf(arr[R]).modInverse(p);
			BigInteger n_r = BigInteger.valueOf(arr[N-R]).modInverse(p);
			BigInteger result = n.multiply(r).multiply(n_r).mod(p);
			
			System.out.println("#"+(i+1)+" "+result);
		}
	}
}