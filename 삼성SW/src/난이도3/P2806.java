package 난이도3;

import java.util.Scanner;

public class P2806 {
	static int col[];	//퀸의 위치를 저장 할 변수
	static int result;// 결과값 저장할 변수
	static int n; //n*n의 크기
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			n=sc.nextInt();
			result = 0;
			for(int a=1;a<=n;a++) { //열을 나타냄 0부터 n까지
				col = new int[n+1]; //결과값 저장할 배열
				col[1]= a; //첫번째에 a를 퀸을 놓고

				DFS(2); //2번째로 넘어감
			}
			System.out.println("#"+(i+1)+" "+result);
		}
	}
	public static void DFS(int level) {
		if(level > n) {
			result++;
		} 
		else {
			for(int a=1;a<=n;a++) {

				col[level] = a;

				if(Check(level)) {
					DFS(level+1);
				}
				else {
					col[level]=0;
				}
			}
		}
	}
	public static boolean Check(int check) {
		for(int a=1;a<check;a++) {
			if((col[check]==col[a]) || Math.abs(check-a)==Math.abs(col[check]-col[a]) ) {
				return false;
			}
		}
		return true;
	}
}
