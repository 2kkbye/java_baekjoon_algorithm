package 난이도3;

import java.util.Scanner;

public class P3142 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int n = sc.nextInt(); //뿔 수
			int m = sc.nextInt(); //마리 수
			
			int x,y;
			int a=m;
			int b=0;
			while(true) {
				if((a+(b*2))==n) {
					break;
				}
				else {
					a--;
					b++;
				}
				if(a<0 || b>m){
					System.out.println("잘못");
					break;
				}
			}
			System.out.println("#"+(i+1)+" "+a+" "+b);
		}

	}

}