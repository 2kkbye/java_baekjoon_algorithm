package ���̵�3;

import java.util.Scanner;
public class P1244 {
	static int cnt;
	static int t_max=0;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			String str = sc.next();
			cnt = sc.nextInt();

			int arr[] = new int[str.length()];
			for(int a=0;a<arr.length;a++) {
				arr[a] = (int)str.charAt(a)-48;
			}
			t_max=0;
			backtracking(0,0,arr);

			System.out.println("#"+(i+1)+" "+t_max);
		}
	}
	public static void swap(int a, int k,int arr[]) {
		int temp;
		temp = arr[k];
		arr[k]=arr[a];
		arr[a] = temp;
	}
	public static void max(int arr[]) {
		String str = "";
		for(int a=0;a<arr.length;a++) {
			str = str+arr[a];
		}
		int temp = Integer.parseInt(str);
		if(temp>t_max) t_max = temp;
	}
	public static void backtracking(int n,int count,int arr[]) {
		if(count >= cnt	) {
			max(arr);
			return;
		}
		for(int a=n;a<arr.length-1;a++) {
			for(int b=a+1;b<arr.length;b++) {
				if(arr[a] <= arr[b]) {
					swap(a,b,arr);
					backtracking(a,count+1,arr);
					swap(b,a,arr);
				}
			}
		}
	}
}
