package 난이도3;

import java.util.Scanner;
public class P1289 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		
		for(int i=0;i<T;i++) {
			String s = sc.next();
			String temp = "";
			char arr[] = s.toCharArray(); //처음 받는것
			char ar[] = new char[arr.length]; //0으로 초기화 되어있는것
			
			//초기화
			for(int a=0;a<ar.length;a++) {
				ar[a]='0';
			}
			
			//int index = -1;
			int cnt=0;
			for (int a=0;a<arr.length;a++) {
				if(arr[a]!=ar[a]) {
					cnt++;
					ar=change(ar,a+1,arr[a]);
				}
			}
			System.out.println("#"+(i+1)+" "+cnt);
		}

	}
	public static char [] change(char arr[], int index, char s) {
		for(int i=index;i<arr.length;i++) {
			arr[i]=s;
		}
		
		return arr;
	}

}
