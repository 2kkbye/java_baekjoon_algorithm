package ���̵�3;

import java.util.Scanner;
public class P6057 {
	static int arr[][];
	static int cnt;
	static int N;
	static int M;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			N = sc.nextInt();
			M = sc.nextInt();
			arr = new int[N+1][N+1];
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr.length;b++) {
					arr[a][b]=0;
				}
			}
			for(int a=0;a<M;a++) {
				int x = sc.nextInt();
				int y = sc.nextInt();
				arr[x][y]=arr[y][x]=1;
			}
			cnt=0;
			for(int a=1;a<arr.length;a++) {
				for(int b=a+1;b<arr.length;b++) {
					if(arr[a][b]==1) {
						backtracking(a,b,1);
					}
				}
			}
			System.out.println("#"+(i+1)+" "+cnt);
		}
	}
	public static void backtracking(int start, int level,int count) {
		//count=count+1;
		if(count ==2 && arr[level][start]==1) {
			cnt++;
			return;
		}
		if(count >= 2) {
			return;
		}
		if(count == 1) {
			for(int a=level;a<arr.length;a++) {
				if(arr[level][a]==1) {
					backtracking(start,a,count+1);
				}
			}
		}
	}
}
