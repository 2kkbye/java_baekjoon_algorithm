package 난이도3;

import java.util.Scanner;
public class P1240 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int x=sc.nextInt(); //행
			int y=sc.nextInt(); //열
			String ar[][]=new String[x][1];
			for(int a=0;a<ar.length;a++) {
				for(int b=0;b<ar[a].length;b++) {
					ar[a][b]=sc.next();
				}
			}
			//1이 속해있는 행의 번호 구하기.
			int temp=0;
			int sw=0;
			for(int a =0;a<ar.length;a++) {
				for(int b=0;b<ar[a].length;b++) {
					for(int c=0;c<ar[a][b].length();c++) {
						if(ar[a][b].charAt(c)=='1') {
							temp = a;
							sw=1;
							break;
						}
					}
					if(sw==1) {
						break;
					}
				}
				if(sw==1) {
					break;
				}
			}
			int cnt=0;
			String spend="";
			int index=7;
			sw=0;
			int result[] = new int[8];
			for(int a=y-1;a>=0;a--) {
				if(ar[temp][0].charAt(a)=='1' && sw==0) {
					sw=1;
				}
				if(sw ==1) {
					spend=ar[temp][0].charAt(a)+spend;
					cnt++;
				}
				if(cnt ==7) {
					result[index]=func(spend);
					index--;
					cnt=0;
					spend="";
				}
				if(index<0) {
					break;
				}
			}
			int sum = (((result[0]+result[2]+result[4]+result[6])*3)+(result[1]+result[3]+result[5]));
			if((sum+result[7])%10==0) {
				sum=0;
				for(int a=0;a<result.length;a++) {
					sum = sum+result[a];
				}
				
			}
			else sum=0;
			System.out.println("#"+(i+1)+" "+sum);
		}
	}
	public static int func(String s) {
		String re[] = {"0001101","0011001","0010011","0111101","0100011","0110001","0101111","0111011","0110111","0001011"};
		int result=0;
		for(int i=0;i<re.length;i++) {
			if(re[i].equals(s)) {
				result = i;
			}
		}
		return result;
	}
}
