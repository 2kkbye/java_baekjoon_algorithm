package 난이도3;

import java.util.Scanner;
public class P3431 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int L = sc.nextInt(); //최소
			int U = sc.nextInt(); //최대
			int X = sc.nextInt(); //현재
			if( X > U) {
				System.out.println("#"+(i+1)+" -1");
			}
			else if(X > L && X < U) {
				System.out.println("#"+(i+1)+" 0");
			}
			else {
				System.out.println("#"+(i+1)+" "+(L-X));
			}
		}

	}

}
