package 난이도3;

import java.util.Scanner;
public class P1215 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		for(int i=0;i<10;i++) {
			int N = sc.nextInt();
			String s="";
			for(int a=0;a<9;a++) {
				s=s+sc.nextLine();
			}
			char ar[]=s.toCharArray();
			char arr[][] = new char[8][8];
			int q=-1;
			for (int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length;b++) {
					q=q+1;
					arr[a][b]=ar[q];
				}
			}
			int cnt=0;
			//가로
			int sw=0;
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<=arr[a].length-N;b++) {
					for(int c=0;c<N/2;c++) {
						if(arr[a][b+c]==arr[a][b+(N-1)-c]) {
							sw=0;
						}
						else {
							sw=1;
							break;
						}
					}
					if(sw==0) {
						cnt=cnt+1;
					}
				}
			}
			//세로
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<=arr[a].length-N;b++) {
					for(int c=0;c<N/2;c++) {
						if(arr[b+c][a]==arr[b+(N-1)-c][a]) {
							sw=0;
						}
						else {
							sw=1;
							break;
						}
					}
					if(sw==0) {
						cnt=cnt+1;
					}
				}
			}
			System.out.println("#"+(i+1)+" "+cnt);
		}

	}
}