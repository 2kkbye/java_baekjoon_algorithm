package 난이도3;

import java.util.LinkedList;
import java.util.Scanner;
public class P4466 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			LinkedList<Integer> list = new LinkedList<Integer>();
			int N = sc.nextInt(); //배열 개수
			int K = sc.nextInt(); //정렬해서 위에서 3개까지 입력.
			int arr[] = new int[N];
			
			for(int a=0;a<arr.length;a++) {
				arr[a] = sc.nextInt();
			}
			//버블정렬
			for(int a = 1;a<arr.length;a++) {
				for(int b=0;b<arr.length-a;b++) {
					if(arr[b]>arr[b+1]) {
						int temp = arr[b];
						arr[b]= arr[b+1];
						arr[b+1]= temp;
					}
				}
			}
			int temp=0;
			int cnt=0;
			for(int a=arr.length-1;a>=0;a--) {
				if(cnt<K) {
					list.add(arr[a]);
					cnt++;
				}
				if(cnt ==K) {
					break;
				}
			}
			int sum=0;
			for(int a=0;a<list.size();a++) {
				sum=sum+list.get(a);
			}
			System.out.println("#"+(i+1)+" "+sum);
		}
	}
}
