package 난이도3;

import java.util.Scanner;
public class P6485 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int bus[] = new int[5001];
			for(int a=0;a<bus.length;a++) {
				bus[a] = 0;
			}

			int N = sc.nextInt();			
			int arr[][] = new int[N][2];
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length;b++) {
					arr[a][b] = sc.nextInt();
				}
			}
			//확인하고자 하는 정류장
			int P = sc.nextInt();
			int p_ar[] = new int[P];
			for(int a=0;a<p_ar.length;a++) {
				p_ar[a] = sc.nextInt();
			}
			for(int a=0;a<arr.length;a++) {
				for(int b=arr[a][0];b<=arr[a][1];b++) {
					bus[b]=bus[b]+1;
				}
			}
			System.out.print("#"+(i+1)+" ");
			for(int a=0;a<p_ar.length;a++) {
				System.out.print(bus[p_ar[a]]+" ");
			}
            System.out.println();
		}
	}
}
