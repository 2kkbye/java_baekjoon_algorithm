package 난이도3;

import java.util.Scanner;
public class P4615 {
	static char arr[][];
	static int count_b;
	static int count_w;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int N = sc.nextInt();
			int M = sc.nextInt();
			arr = new char[N+1][N+1];
			int temp = (N/2)-1;
			chogi();
			arr[temp+1][temp+1]='W';
			arr[temp+1][temp+2]='B';
			arr[temp+2][temp+1]='B';
			arr[temp+2][temp+2]='W';
			count_b=0;
			count_w=0;
			for(int a=0;a<M;a++) {
				int x = sc.nextInt();
				int y = sc.nextInt();
				int w_b = sc.nextInt();
				check(y,x,w_b);
			}
			count();
			System.out.println("#"+(i+1)+" "+count_b+" "+count_w);
		}
	}
	public static void count() {
		for(int a=1;a<arr.length;a++) {
			for(int b=1;b<arr.length;b++) {
				if(arr[a][b] =='W') count_w++;
				else if(arr[a][b]=='B') count_b++;
			}
		}
	}
	public static void chogi() {
		for(int a=0;a<arr.length;a++) {
			for(int b=0;b<arr.length;b++) {
				arr[a][b] = '0';
			}
		}
	}
	public static void check(int x, int y,int w_b) {
		char c;
		char d;
		if(w_b==1) {
			c='B';
			d='W';
		}
		else {
			c='W';
			d='B';
		}
		//새로 놓일 장소 이 좌표를 기준으로 밑,위,왼,오,대각선오위, 대각선왼위, 대각선왼아, 대각선오아
		arr[x][y]=c;
		//아래 검사
		int temp=0;

		for(int a=1;a<=arr.length-2;a++) {
			if(x+a<=arr.length-1) {
				if(arr[x+a][y]=='0') {
					break;
				}
				else if(arr[x+a][y]==c) {
					temp = a-1;
					break;
				}
				else continue;
			}
			else break;
		}
		for(int a=temp;a>=1;a--) {
			if(x+a<=arr.length-1) {
				if(arr[x+a][y]!='0' && arr[x+a][y]!=c) {
					arr[x+a][y] = c;
				}
				else break;
			}
			else break;
		}
		temp=0;
		//위검사
		for(int a=1;a<=arr.length-2;a++) {
			if(x-a>=1) {
				if(arr[x-a][y]=='0') {
					break;
				}
				else if(arr[x-a][y]==c) {
					temp = a-1;
					break;
				}
				else continue;
			}
			else break;
		}
		for(int a=temp;a>=1;a--) {
			if(x-a>=1) {
				if(arr[x-a][y]!='0' && arr[x-a][y]!=c) {
					arr[x-a][y] = c;
				}
				else break;
			}
			else break;
		}
		temp=0;
		//오른쪽 검사
		for(int a=1;a<=arr.length-2;a++) {
			if(y+a<=arr.length-1) {
				if(arr[x][y+a]=='0') {
					break;
				}
				else if(arr[x][y+a]==c) {
					temp = a-1;
					break;
				}
				else continue;
			}
			else break;
		}
		for(int a=temp;a>=1;a--) {
			if(y+a<=arr.length-1) {
				if(arr[x][y+a]!='0' && arr[x][y+a]!=c) {
					arr[x][y+a] = c;
				}
				else break;
			}
			else break;
		}
		temp=0;
		//왼쪽 검사
		for(int a=1;a<=arr.length-2;a++) {
			if(y-a>=1) {
				if(arr[x][y-a]=='0') {
					break;
				}
				else if(arr[x][y-a]==c) {
					temp = a-1;
					break;
				}
				else continue;
			}
			else break;
		}
		for(int a=temp;a>=1;a--) {
			if(y-a>=1) {
				if(arr[x][y-a]!='0' && arr[x][y-a]!=c) {
					arr[x][y-a] = c;
				}
				else break;
			}
			else break;
		}
		temp=0;
		//오른쪽 대각선 위
		for(int a=1;a<=arr.length-2;a++) {
			if(x-a>=1&&y+a<=arr.length-1) {
				if(arr[x-a][y+a]=='0') {
					break;
				}
				else if(arr[x-a][y+a]==c) {
					temp = a-1;
					break;
				}
				else continue;
			}
			else break;
		}
		for(int a=temp;a>=1;a--) {
			if(x-a>=1&&y+a<=arr.length-1) {
				if(arr[x-a][y+a]!='0' && arr[x-a][y+a]!=c) {
					arr[x-a][y+a]=c;
				}
				else break;
			}
			else break;
		}
		temp=0;
		//왼쪽 대각선 위
		for(int a=1;a<=arr.length-2;a++) {
			if(x-a>=1&&y-a>=1) {
				if(arr[x-a][y-a]=='0') {
					break;
				}
				else if(arr[x-a][y-a]==c) {
					temp = a-1;
					break;
				}
				else continue;
			}
			else break;
		}
		for(int a=temp;a>=1;a--) {
			if(x-a>=1&&y-a>=1) {
				if(arr[x-a][y-a]!='0' && arr[x-a][y-a]!=c) {
					arr[x-a][y-a]=c;
				}
				else break;
			}
			else break;
		}
		temp=0;
		//오른쪽 대각선 아래
		for(int a=1;a<=arr.length-2;a++) {
			if(((x+a)<=arr.length-1)&&((y+a)<=arr.length-1)) {
				if(arr[x+a][y+a]=='0') {
					break;
				}
				else if(arr[x+a][y+a]==c) {
					temp = a-1;
					break;
				}
				else continue;
			}
			else break;
		}
		for(int a=temp;a>=1;a--) {
			if(((x+a)<=arr.length-1)&&((y+a)<=arr.length-1)) {
				if(arr[x+a][y+a]!='0' && arr[x+a][y+a]!=c) {
					arr[x+a][y+a]=c;
				}
				else break;
			}
			else break;
		}
		temp=0;
		//왼쪽 대각선 아래
		for(int a=1;a<=arr.length-2;a++) {
			if(x+a<=arr.length-1 && y-a>=1) {
				if(arr[x+a][y-a]=='0') {
					break;
				}
				else if(arr[x+a][y-a]==c) {
					temp = a-1;
					break;
				}
				else continue;
			}
			else break;
		}
		for(int a=temp;a>=1;a--) {
			if(x+a<=arr.length-1 && y-a>=1) {
				if(arr[x+a][y-a]!='0' && arr[x+a][y-a]!=c) {
					arr[x+a][y-a]=c;
				}
				else break;
			}
			else break;
		}
	}
}
