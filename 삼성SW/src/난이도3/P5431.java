package 난이도3;

import java.util.Scanner;
public class P5431 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int x = sc.nextInt();
			int y = sc.nextInt();
			int arr[] = new int[x+1]; // 제출했는지 제출 안했는지 구분할 배열
			for(int a=0;a<arr.length;a++) {
				arr[a] = -1;
			}
			for(int a=0;a<y;a++) {
				int num = sc.nextInt();
				arr[num] = 0;
			}
			System.out.print("#"+(i+1)+" ");
			for(int a=1;a<arr.length;a++) {
				if(arr[a]!=0) {
					System.out.print(a+" ");
				}
			}
			System.out.println();
		}
	}
}