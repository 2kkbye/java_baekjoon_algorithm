package 난이도3;

import java.util.Scanner;
public class P2814 {
	static int arr[][];
	static boolean tr[];
	static int max_len;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int N = sc.nextInt();
			int M = sc.nextInt();
			arr=new int[N+1][N+1];
			tr = new boolean[N+1];
			//초기화
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr.length;b++) {
					arr[a][b]=0;
				}
			}
			for(int a=0;a<tr.length;a++) {
				tr[a]=false;
			}
			for(int a=0;a<M;a++) {
				int x=sc.nextInt();
				int y = sc.nextInt();
				arr[x][y]=arr[y][x]=1;
			}
			max_len=0;
			for(int a=1;a<arr.length;a++) {
				search(a,1);
			}
			System.out.println("#"+(i+1)+" "+max_len);
		}
	}
	public static void search(int x,int cnt) {
		if(cnt>max_len) {
			max_len=cnt;
		}
		tr[x]=true;
		for(int b=1;b<arr.length;b++) {
			if(arr[x][b]==1&&tr[b]==false) {
				search(b,cnt+1);
			}
		}
		tr[x]=false;
	}
}
