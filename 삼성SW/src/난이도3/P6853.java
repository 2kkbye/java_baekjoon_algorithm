package 난이도3;

import java.util.Scanner;
public class P6853 {

	static int X1,Y1,X2,Y2;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int result[][] = new int[T][3];
			X1 = sc.nextInt();
			Y1 = sc.nextInt();
			X2 = sc.nextInt();
			Y2 = sc.nextInt();
			int N = sc.nextInt();
			for(int a=0;a<N;a++) {
				int x = sc.nextInt();
				int y = sc.nextInt();
				int temp = check(x,y);
				if(temp ==1) { //내부에 있는점
					result[i][0]=result[i][0]+1;
				}
				else if(temp ==2) { //네변 중 적어도 하나의 위에 있는점
					result[i][1]=result[i][1]+1;
				}
				else {
					result[i][2]=result[i][2]+1;
				}
			}
			System.out.println("#"+(i+1)+" "+result[i][0]+" "+result[i][1]+" "+result[i][2]);
		}
	}
	public static int check(int x, int y) {
		if((x==X1&&((y>=(Y1))&&(y<=(Y2)))) || (x==X2&&((y>=(Y1))&&(y<=(Y2)))) || (y==Y1&&((x>=(X1))&&(x<=(X2)))) || (y==Y2&&((x>=(X1))&&(x<=(X2))))) {
			return 2;
		}
		
		else if(((x>=(X1+1))&&(x<=(X2-1)))&&((y>=(Y1+1))&&(y<=(Y2-1)))) {
			return 1;
		}
		return 0;
	}

}
