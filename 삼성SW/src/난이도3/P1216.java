package 난이도3;

import java.util.Scanner;
public class P1216 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		for(int i=0;i<10;i++) {
			int N = sc.nextInt();
			String s="";
			for(int a=0;a<101;a++) {
				s=s+sc.nextLine();
			}
			char ar[]=s.toCharArray();
			char arr[][] = new char[100][100];
			int q=-1;
			for (int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length;b++) {
					q=q+1;
					arr[a][b]=ar[q];
				}
			}
			int cnt=0;
			int max_len = -999;
			//가로
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length-2;b++) {
					for(int c=b+2;c<arr.length;c++) {
						if(arr[a][b] == arr[a][c]) {
							for(int d=1;d<=(c-b)/2;d++) {
								if(arr[a][b+d]==arr[a][c-d]) {
									cnt=cnt+1;
								}
								else {
									cnt=-9999;
									break;
								}
							}
							if((cnt)==(c-b)/2 && max_len < (c-b+1)) {
								max_len = c-b+1;
							}
							cnt=0;
						}
						else continue;
					}
				}
			}
			cnt=0;
			//세로
			for(int a=0;a<arr.length;a++) {
				for(int b=0;b<arr[a].length-2;b++) {
					for(int c=b+2;c<arr.length;c++) {
						if(arr[b][a] == arr[c][a]) {
							for(int d=1;d<=(c-b)/2;d++) {
								if(arr[b+d][a]==arr[c-d][a]) {
									cnt=cnt+1;
								}
								else {
									cnt=-9999;
									break;
								}
							}
							if((cnt)==(c-b)/2 && max_len < (c-b+1)) {
								max_len = c-b+1;
							}
							cnt=0;
						}
						else continue;
					}
				}
			}
			System.out.println("#"+(i+1)+" "+max_len);
		}
	}
}
