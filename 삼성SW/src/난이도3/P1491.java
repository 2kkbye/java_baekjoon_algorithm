package ���̵�3;

import java.util.Scanner;
public class P1491 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int N = sc.nextInt();
			int A = sc.nextInt();
			int B = sc.nextInt();
			
			long min = 999999999;
			for(long a=1;a<=N/2;a++) {
				for(long b=a;b*a<=N;b++) {
					long temp = (A*Math.abs(a-b))+(B*(N-(a*b)));
					if(temp<min) {
						min = temp;
					}
				}
			}
			System.out.println("#"+(i+1)+" "+min);
		}
	}
}