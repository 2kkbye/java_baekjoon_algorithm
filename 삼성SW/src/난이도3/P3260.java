package ���̵�3;

import java.math.BigDecimal;
import java.util.Scanner;
public class P3260 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			BigDecimal a = sc.nextBigDecimal();
			BigDecimal b = sc.nextBigDecimal();
			BigDecimal result = a.add(b);
			System.out.println("#"+(i+1)+" "+result);
		}
	}
}