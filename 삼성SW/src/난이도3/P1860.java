package 난이도3;

import java.util.Scanner;
public class P1860 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			int N = sc.nextInt(); //제한된 인원수
			int M = sc.nextInt(); // 0초에서 M초마다 붕어빵 생성
			int K = sc.nextInt(); // 몇개씩 생성하는지
			int arr[] = new int[11112];
			for(int a=0;a<arr.length;a++) { //초기화
				arr[a] =0;
			}
			int sw =0;
			for(int a=0;a<N;a++) {
				int temp =sc.nextInt();
				if(temp < M) {
					sw =1;
				}
				arr[temp]++;
			}
			int cnt =0;
			int bread_sum = 0;
			for(int a=M;a<arr.length;a++) {
				if(a%M ==0) { //빵을 추가해준다.
					bread_sum=bread_sum+K;
				}
				bread_sum = bread_sum-arr[a]; //빵을 사람 수만 큼 빼준다.
				if(bread_sum < 0) { //빵이 0보다 적어지면 불가능
					sw =1;
					break;
				}
			}
			if(sw == 1) {
				System.out.println("#"+(i+1)+" Impossible");
			}
			else {
				System.out.println("#"+(i+1)+" Possible");
			}
		}
	}
}
