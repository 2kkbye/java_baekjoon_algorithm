package com.mobile;

public class OTab extends Mobile{

	private String mobileName;
	private int batterySize;
	private String osType;

	public OTab() {
		// TODO Auto-generated constructor stub
	}
	public OTab(String mobileName, int batterySize, String osType) {
		// TODO Auto-generated constructor stub
		this.mobileName = mobileName;
        this.batterySize = batterySize;
        this.osType = osType;
	}
	
	public String getMobileName() {
		return mobileName;
	}

	public void setMobileName(String mobileName) {
		this.mobileName = mobileName;
	}

	public int getBatterySize() {
		return batterySize;
	}

	public void setBatterySize(int batterySize) {
		this.batterySize = batterySize;
	}

	public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}
	
	//海磐府 荤侩
	@Override
	public void operate(int time) {
		// TODO Auto-generated method stub
		int num = getBatterySize();
		num = num-(time*12);
		setBatterySize(num);
	}
	//海磐府 面傈
	@Override
	public void charge(int time) {
		// TODO Auto-generated method stub
		int num = getBatterySize();
		num = num+(time*8);
		setBatterySize(num);
	}

}
