package com.mobile;

public class MobileTest {

    public static void main(String[] args) {
  
        OTab ot = new OTab("OTab", 1000, "AND-20");
        
        System.out.println("  Mobile\t\tBattery\t\tOS");
        System.out.println("---------------------------------------------");
       
        System.out.println(ot.getMobileName()+ "\t\t" + ot.getBatterySize() +
                "\t\t" + ot.getOsType());
        System.out.println("---------------------------------------------");
        System.out.println();
        System.out.println("10분 충전");
   
        ot.charge(10);
        System.out.println("  Mobile\t\tBattery\t\tOS");
        System.out.println("---------------------------------------------");

        System.out.println(ot.getMobileName()+ "\t\t" + ot.getBatterySize() +
                "\t\t" + ot.getOsType());
        System.out.println("---------------------------------------------");
        System.out.println();
        System.out.println("5분 통화");

		ot.operate(5);
        System.out.println("  Mobile\t\tBattery\t\tOS");
        System.out.println("---------------------------------------------");

        System.out.println(ot.getMobileName()+ "\t\t" + ot.getBatterySize() +
                "\t\t" + ot.getOsType());
        System.out.println("---------------------------------------------");
    }

}
