package 네이버코테_3;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;

class Pair{
	int p_num, time, page_num;
	public Pair(int p_num, int time, int page_num) {
		// TODO Auto-generated constructor stub
		this.p_num = p_num;
		this.time = time;
		this.page_num = page_num;
	}
}
public class Solution {
	
	static ArrayList<Pair> a_list = new ArrayList<Pair>();
	static ArrayList<Pair> a_print = new ArrayList<Pair>();
	static ArrayList<Pair> a_ready = new ArrayList<Pair>();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr[][] = {{1, 0, 5},{2, 2, 2},{3, 3, 1},{4, 4, 1},{5, 10, 2}};
		solution(arr);
	}
	public static int solution(int arr[][]) {
		int answer = 0;
		int time = -1;
		insert(arr);
		while(!a_list.isEmpty()) {
			time++;
			print();
			list_goReady(time);
			solve();
		}
		return answer;
	}
	public static void solve() {
		sort();
		System.out.println(a_list.get(0).p_num);
	}
	public static void sort() {
		a_ready.sort(new Comparator<Pair>() {
			@Override
			public int compare(Pair arg0, Pair arg1) {
				// TODO Auto-generated method stub
				int first_page = arg0.page_num;
				int first_num= arg0.p_num;
				int second_page=arg1.page_num;
				int second_num=arg1.p_num;
				
				if (first_page == second_page) {
					if(first_num> second_num) {
						return 1;
					}
					else {
						return -1;
					}
				}
				else if (first_page>second_page)
					return 1;
				else
					return -1;
			}
		});
	}
	public static void list_goReady(int time) {
		Pair p = a_list.remove(0);
		if(p.time==time) {
			if(a_print.size()==0) {
				a_print.add(new Pair(p.p_num, p.time, p.page_num-1));
			}
			else {
				a_ready.add(new Pair(p.p_num, p.time, p.page_num));
			}
		}
		else {
			a_list.add(0,new Pair(p.p_num, p.time, p.page_num));
		}
	}
	public static void print() {
		if(a_print.size()==0) return;
		Pair p = a_print.remove(0);
		int page_num = p.page_num-1;
		if(page_num != 0) a_print.add(new Pair(p.p_num, p.time, page_num));
	}
	public static void insert(int arr[][]) {
		for(int a = 0;a<arr.length;a++) {
			a_list.add(new Pair(arr[a][0], arr[a][1], arr[a][2]));
		}
	}

}
