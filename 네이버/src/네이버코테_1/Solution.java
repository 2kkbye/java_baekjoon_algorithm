package 네이버코테_1;

public class Solution {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String emails[] = {"d@co@m.com", "a@abc.com", "b@def.com", "c@ghi.net"};
		//String emails[] = {"abc.def@x.com", "abc", "abc@defx", "abc@defx.xyz"};
		String emails[] = {"da@", ".@.com"};
		System.out.println(solution(emails));
	}
	public static int solution(String[] emails) {
		int answer = 0;
		String domain[] = {"com","net","org"};
		for(int a = 0;a<emails.length;a++) {
			String total[] = emails[a].split("\\@");
			if(total.length!=2 || total[0].equals("")) continue;
			String temp2[] = total[1].split("\\.");
			if(temp2.length!=2 || temp2[0].equals("")) continue;
			if(!check_name(total[0]) || !check_domain(temp2[0]) || !check_Tdomain(temp2[1],domain)) continue;
			answer++;
		}
		return answer;
	}
	public static boolean check_name(String temp) {
		boolean flag = true;
		for(int a = 0;a<temp.length();a++) {
			if((temp.charAt(a)>='a'&& temp.charAt(a)<='z') || temp.charAt(a)=='.') continue;
			flag = false;
			break;
		}
		return flag;
	}
	public static boolean check_domain(String temp) {
		boolean flag = true;
		for(int a = 0;a<temp.length();a++) {
			if((temp.charAt(a)>='a'&& temp.charAt(a)<='z')) continue;
			flag = false;
			break;
		}
		return flag;
	}
	public static boolean check_Tdomain(String temp,String domain[]) {
		boolean flag = false;
		for(int a = 0;a<domain.length;a++) {
			if(temp.equals(domain[a])) {
				flag = true;
				break;
			}
		}
		return flag;
	}

}
