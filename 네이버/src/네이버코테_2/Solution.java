package 네이버코테_2;

public class Solution {
	public static void main(String[] args) {
		//String drum[] = {"######",">#*###","####*#","#<#>>#",">#*#*<","######"};
		String drum[] = {"###>#<","#*####",">*>*<#","#>####","#<<*##<",">#####"};
		
		System.out.println(solution(drum));
	}
	static int des;
	static int ans;
	static boolean arrive;
	static boolean meet_star;
	static char map[][];
	public static int solution(String drum[]) {
		int answer = 0;
		map= new char[drum.length+1][drum[0].length()];
		des = drum.length;
		ans = 0;
		copy(map,drum);
		for(int a = 0;a<map[0].length;a++) {
			meet_star = false;
			arrive = false;
			dfs(0,a);	
		}
		return ans;
	}
	public static void dfs(int x, int y) {
		if(arrive) return;
		if(x == des) {
			arrive = true;
			ans++;
			return;
		}
		if(map[x][y]== '#') {
			dfs(x+1,y);
		}
		else if(map[x][y]=='>') {
			dfs(x,y+1);
		}
		else if(map[x][y]=='<') {
			dfs(x,y-1);
		}
		else if(map[x][y]=='*' && !meet_star) {
			meet_star = true;
			dfs(x+1,y);
		}
	}
	public static void copy(char[][]map, String drum[]) {
		for(int a = 0;a<map.length-1;a++) {
			for(int b = 0;b<map[a].length;b++) {
				map[a][b] = drum[a].charAt(b);
			}
		}
	}
}
