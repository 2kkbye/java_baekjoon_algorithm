package ����;

import java.io.*;
import java.util.StringTokenizer;
public class solution_3 {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		
		int arr[] = new int[151];
		for(int a = 0;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			int start = Integer.parseInt(st.nextToken());
			int end = Integer.parseInt(st.nextToken());
			for(int b = start;b<end;b++) {
				arr[b] += 1;
			}
		}
		int max = 0;
		for(int a =0;a<arr.length;a++) {
			if(arr[a]<=max) continue;
			max = arr[a];
		}
		System.out.println(max);

	}
}
