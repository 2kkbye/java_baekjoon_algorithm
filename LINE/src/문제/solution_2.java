package ����;

import java.io.*;
import java.util.ArrayList;
public class solution_2 {
	static long num[];
	static long result[];
	
	static boolean visit[];
	static int index = 0;
	static ArrayList<Long> a_list = new ArrayList<Long>();
	
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine().trim();
		String arr[] = str.split(" ");
		int ans = Integer.parseInt(br.readLine());
		num = new long[arr.length];
		visit = new boolean[arr.length];
		int size = 1;
		
		for(int a =1;a<=num.length;a++) {
			size*=a;
		}
		result = new long[size];
		copy(arr);
		for(int a =0;a<num.length;a++) {
			visit[a] = true;
			a_list.add(num[a]);
			dfs(1);
			visit[a] = false;
			a_list.remove(0);
		}
		sort(0, result.length-1);
		System.out.println(result[ans-1]);
	}
	public static void dfs(int level) {
		if(level == num.length) {
			String temp ="";
			for(int a =0;a<a_list.size();a++) {
				temp = temp+a_list.get(a);
			}
			result[index++] = Long.parseLong(temp);
			return;
		}
		for(int a = 0;a<num.length;a++) {
			if(visit[a]) continue;
			visit[a] = true;
			a_list.add(num[a]);
			dfs(level+1);
			visit[a] = false;
			a_list.remove(a_list.size()-1);
		}
	}
	public static void copy(String str[]) {
		for(int a =0;a<str.length;a++) {
			num[a] = Integer.parseInt(str[a]);
		}
	}
	public static void sort(int s, int e) {
		int start = s;
		int end = e;
		long pivot = result[(start+end)/2];
		do {
			while(result[start]<pivot) {
				start++;
			}
			while(result[end]>pivot) {
				end--;
			}
			if(start<=end) {
				long temp = result[start];
				result[start] = result[end];
				result[end] = temp;
				start++;
				end--;
			}
		}while(start<=end);
		if(s<end) {
			sort(s,end);
		}
		if(e>start) {
			sort(start,e);
		}
		
	}
}
