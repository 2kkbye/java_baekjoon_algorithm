package ����;

import java.io.*;
import java.util.StringTokenizer;
import java.util.Queue;
import java.util.LinkedList;

public class solution_4 {
	static int copy[];
	static int map[];
	static boolean discovered[];
	static int dx[]= {-1,1};
	static int N;
	static Queue<Integer> q_person;
	static int ans = 1;
	
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		N = Integer.parseInt(st.nextToken());
		map = new int[N];
		copy = new int[N];
		discovered = new boolean[N];
		st = new StringTokenizer(br.readLine());
		for(int a =0;a<N;a++) {
			map[a] = Integer.parseInt(st.nextToken());
		}
		q_person = new LinkedList<Integer>();
		
		for(int a =0;a<N;a++) {
			if(map[a]==1) continue;
			init();
			discovered[a] = true;
			q_person.add(a);
			bfs();
		}
		System.out.println(ans);
	}
	public static void bfs() {
		boolean flag = false;
		while(!q_person.isEmpty()) {
			int x = q_person.remove();
			if(flag) continue;
			for(int a= 0;a<2;a++) {
				int rx = x+dx[a];
				if(rx <0 || rx >= N) continue;
				if(discovered[rx]) continue;
				if(map[rx] ==1) {
					ans = Math.max(copy[x]+1, ans);
					flag = true;
					break;
				}
				discovered[rx] = true;
				copy[rx] = copy[x]+1;
				ans = Math.max(copy[rx], ans);
				q_person.add(rx);
			}
		}
	}
	public static void init() {
		for(int a =0;a<N;a++){
			copy[a] = map[a];
			discovered[a] = false;
		}
	}
}
