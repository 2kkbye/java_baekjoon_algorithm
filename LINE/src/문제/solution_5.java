package 문제;

import java.io.*;
import java.util.StringTokenizer;
public class solution_5 {
	static int N,M;
	static int map[][];
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken())+1; //가로
		M = Integer.parseInt(st.nextToken())+1; //세로
		map = new int[M][N];
		
		st = new StringTokenizer(br.readLine());
		int x = Integer.parseInt(st.nextToken()); //가로
		int y = Integer.parseInt(st.nextToken()); //세로
		
		draw();
		if(y<0 || y>=N || x<0 || x>=M) {
			System.out.println("fail");
		}
		else {
			System.out.println(x+y);
			System.out.println(map[y][x]);
		}
	}
	public static void draw() {
		for(int a =0;a<map.length;a++) {
			for(int b =0;b<map[a].length;b++) {
				if(a==0 && b==0) continue;
				if(a==0 || b==0) {map[a][b] = 1;continue;}
				map[a][b] = map[a][b-1]+ map[a-1][b];
			}
		}
	}
}
