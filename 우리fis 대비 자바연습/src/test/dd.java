package test;
public class dd {
	final static boolean LOG_ENABLE = false;
	public static void main(String[] args){
		TestClass t1 = new TestClass();
		TestClass t2 = new TestClass(0);
	}
}
class TestClass {
	static{
		System.out.println("클래스 초기화 블록");
	}
	{
		System.out.println("인스턴스 초기화 블록");
	}
	TestClass(){
		System.out.println("기본 생성자");
	}
	TestClass(int value){
		System.out.println("생성자 오버로딩");
	}
}