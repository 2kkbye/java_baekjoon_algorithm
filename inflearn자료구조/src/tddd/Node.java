package tddd;

public class Node<E> {
	public String key; 
	public String value;
	public Node<E> next;


	public Node(String Id) {
		key = Id;
		next = null;
	}

	public Node(String Id, String Pw) {
		this.key = Id;
		this.value = Pw;
	}

	public Node<E> getNext() {
		return next;
	}
	
	public String getKey() {
		return this.key;
	}
	public String getValue() {
		return this.value;
	}
	
	public void setNext(Node nextNode) {
		this.next = nextNode;
	}
	
	public String toString(){
        return String.valueOf(this.value);
    }
}