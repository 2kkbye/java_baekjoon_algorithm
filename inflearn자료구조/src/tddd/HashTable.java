package tddd;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public class HashTable<K, V> implements KWHashMap<K, V> {
	private LinkedList<Entry<K, V>>[] table;
	private int numKeys;
	private static final int CAPACITY = 101;
	private static final double LOAD_THRESHOLD = 3.0;
	public int count=0;
	
	private static class Entry<K, V> implements Map.Entry<K, V> {
		private K key;
		private V value;

		public Entry(K key, V value) {
			this.key = key;
			this.value = value;
		}

		public K getKey() {
			return key;
		}

		public V getValue() {
			return value;
		}

		public V setValue(V val) {
			V oldVal = value;
			value = val;
			return oldVal;
		}

		public String toString() {
			return key.toString() + "=" + value.toString();
		}
	}

	public HashTable() {
		table = new LinkedList[CAPACITY];
	}
	
	public V get(Object key) {
		int index = key.hashCode() % table.length;
		if (index < 0) {
			index += table.length;
		}
		if (table[index] == null) {
			return null;
		}
		for (Entry<K, V> nextItem : table[index]) {
			if (nextItem.key.equals(key)) {
				return nextItem.value;
			}
		}
		return null;
	}
	
	/*HashTableApp에서 입력받은 부분 HashTable에 넣는 부분입니다.*/
	public V put(K key, V value) {
		//index를 구하는 부분이며 cell의 개수가 2개인 배열을 사용하기위해 index를 0,1로 구분하였습니다.
		int index = key.hashCode() % table.length;
		
		if(index%10 < 5)
			index = 0;
		else
			index = 1;
		
		//table[index]의 데이터가 비어있는 경우 추가하는 부분입니다.
		if (table[index] == null) {
			System.out.println("새로운 로그인 정보가 추가되었습니다.");
			table[index] = new LinkedList<Entry<K, V>>();
			table[index].add(new Entry<K, V>(key, value));
		}
		
		//table[index]가 비어있지 않은 경우입니다.
		else { 
			Iterator<Entry<K, V>> iter = table[index].iterator();
			
			while (iter.hasNext()) {
				Entry<K, V> nextItem = iter.next();
				//입력한 id와 저장되어있는 id가 같은 경우입니다.
				if (nextItem.key.equals(key)) {
					
					//value값이 일치하면 로그인이 되었다는 출력이 나오고 다른경우 비밀번호가 다르다는 출력이 나옵니다.
					
					if(nextItem.getValue().toString().equals(value.toString())) {
						System.out.println("로그인이 되었습니다.");
						break;
					}
					else {
						System.out.println("비밀번호가 다릅니다.");
						break;
					}
				}
				
				// iter의 데이터를 모두 확인했고 id가 일치한 값을 찾지 못한 경우 새롭게 추가해주는 부분입니다.
				else if(iter.hasNext() == false && !nextItem.key.equals(key)) {
					//인덱스는 같고 key값이 다르면
					System.out.println("새로운 로그인 정보가 추가되었습니다.");
					table[index].addFirst(new Entry<K, V>(key, value));
					break;
				}
			}
			
		}
		
		//화면 출력을 호출하는 부분입니다.
		printHashTable();
		System.out.println("=======================");

		numKeys++;
		if (numKeys > (LOAD_THRESHOLD * table.length)) {
			rehash();
		}
		return null;
	}
	
	public int size() {
		return numKeys;
	}

	public boolean isEmpty() {
		return numKeys == 0;
	}

	/*키값 지우는 부분*/
	public V remove(Object key) {
		int index = key.hashCode() % table.length;
		if (index < 0) {
			index += table.length;
		}
		if (table[index] == null) {
			return null;
		}
		Iterator<Entry<K, V>> iter = table[index].iterator();
		while (iter.hasNext()) {
			Entry<K, V> nextItem = iter.next();
			if (nextItem.key.equals(key)) {
				V returnValue = nextItem.value;
				iter.remove();
				return returnValue;
			}
		}
		return null;
	}
	
	/*size증가시 새로 hash작업하는 것*/
	public void rehash() { 
		LinkedList<Entry<K, V>>[] oldTable = table;
		table = new LinkedList[2 * oldTable.length + 1];
		numKeys = 0;
		for (int i = 0; i < oldTable.length; i++) {
			if (oldTable[i] != null) {
				for (Entry<K, V> nextEntry : oldTable[i]) { 
					put(nextEntry.key, nextEntry.value);
				} 
			}
		}
	}
	
	public java.util.Set<Map.Entry<K, V>> entrySet() {
		return new EntrySet();
	}

	private class EntrySet extends java.util.AbstractSet<Map.Entry<K, V>> {
	
		public int size() {
			return numKeys;
		}

		public Iterator<Map.Entry<K, V>> iterator() {
			return new SetIterator();
		}
	}

	private class SetIterator implements Iterator<Map.Entry<K, V>> {
		int index = 0;
		Iterator<Entry<K, V>> localIterator = null;

		public boolean hasNext() {
			if (localIterator != null) {
				if (localIterator.hasNext()) {
					return true;
				} else {
					localIterator = null;
					index++;
				}
			}
			while (index < table.length && table[index] == null) {
				index++;
			}
			if (index == table.length) {
				return false;
			}
			localIterator = table[index].iterator();
			return localIterator.hasNext();
		}

		public Map.Entry<K, V> next() {
			return localIterator.next();
		}

		public void remove() {
			localIterator.remove();
			if (table[index].size() == 0) {
				table[index] = null;
			}
			numKeys--;
		}

	}// End of SetIterator

	//화면 출력 부분입니다.
	public void printHashTable() {
		for (int i = 0; i < table.length; i++) {
			if (table[i] != null) {
				System.out.print("Bucket " + i + ":  ");
				Iterator itr = table[i].iterator();
				if (itr.hasNext()) {
					while (itr.hasNext()) {
						Entry<String, Node> node = (Entry<String, Node>) itr.next();
						System.out.print(node.getKey() + "-" + node.getValue() + " ");
					}
				}
				System.out.println("");

			}
		}
	}
}