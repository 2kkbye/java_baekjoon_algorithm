package inflearn예제;

import java.util.Scanner;
public class P3 { //버블정렬
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt();
		int temp=0;
		int arr[] = new int[num];
		for(int i=0;i<arr.length;i++) {
			arr[i]=sc.nextInt();
		}
		for(int i=arr.length-1;i>0;i--) { //값에의한 호출
			for(int j=0;j<i;j++) { //이부분도 조심해야한다.버블정렬 알고리즘을보면 마지막에있는거는 안해주니깐 j<i로 한다.
				if(arr[j]>arr[j+1]) {
					temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
		for(int i=0;i<arr.length;i++) {
			System.out.print(arr[i]+" ");
		}
		
	}
	
}
