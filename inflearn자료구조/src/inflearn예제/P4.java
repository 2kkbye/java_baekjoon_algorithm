package inflearn예제;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
public class P4 { //파일 읽어서 문자열로 출력하기.
	public static void main(String[] args) {
		
		String [] name = new String[1000]; //파일로만 정해진다. 그래서 어떻게 되는지 모른다.
		String [] number = new String[1000];
		int n=0; //사람숫자.
		
		try { //외부 게시물을 끌어올때는 파일이 존재하지 않을 경우를 어떻게 할것인지를 예외처리문이 있어야한다.
			Scanner fp = new Scanner(new File("input.txt"));
			
			while(fp.hasNext() ) { //다음에 더 읽을것이 남아있느냐라는 뜻이다.
				name[n] = fp.next();
				number[n] = fp.next();
				n++;
			}
			
			fp.close();
		} catch (FileNotFoundException e) {
			System.out.println("no File");
			System.exit(1); //exit(0)은 정상적인 종료, 0이 아닌값은 비정상적인 오류로 종료.
		}

	}
}
