package inflearn예제;

import java.util.Scanner;
public class P1 { //num1의 num2승을 구하는값
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int num1=sc.nextInt();
		int num2=sc.nextInt();
		
		int result= power(num1,num2);
		System.out.println(num1+" 의  "+num2+" 승은 "+result);
		
		sc.close();
	}
	public static int power(int num1, int num2) { //메서드라는것은 함수라는 뜻이다. 
		
		int result=1;
		for(int i=1;i<=num2;i++) {
			result=result*num1;
		}
		return result;
	}
}
