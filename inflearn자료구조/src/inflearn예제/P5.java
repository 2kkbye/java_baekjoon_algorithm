package inflearn예제;

public class P5 {
	static int [][] arr = {{6,6,3,1},{2,8,0,5},{1,6,5,4},{5,0,7,3}};

	public static void main(String[] args) {

		for(int i=0;i<arr.length;i++) {
			for(int j=0;j<arr[i].length;j++) {
				sosu(arr[i][j]); //먼저 각 수에대한 소수판별
				surround(i,j); //주변좌표 검색해서 
			}
		}
	}
	public static void surround(int i,int j) { //주변좌표 검색
		if(i==0&&j==0) {
			makeTen(arr[0][0],arr[0][1]);
			makeTen(arr[0][0],arr[1][0]);
			makeTen(arr[0][0],arr[1][1]);
			makeTen(arr[0][1],arr[0][0]);
			makeTen(arr[1][0],arr[0][0]);
			makeTen(arr[1][1],arr[0][0]);
		}
		else if(i==0&&j==3) {
			makeTen(arr[0][3],arr[0][2]);
			makeTen(arr[0][3],arr[1][2]);
			makeTen(arr[0][3],arr[1][3]);
			makeTen(arr[0][2],arr[0][3]);
			makeTen(arr[1][2],arr[0][3]);
			makeTen(arr[1][3],arr[0][3]);
		}
		else if(i==3&&j==0) {
			makeTen(arr[3][0],arr[2][0]);
			makeTen(arr[3][0],arr[2][1]);
			makeTen(arr[3][0],arr[3][1]);
			makeTen(arr[2][0],arr[3][0]);
			makeTen(arr[2][1],arr[3][0]);
			makeTen(arr[3][1],arr[3][0]);
		}
		else if(i==3&&j==3) {
			makeTen(arr[3][3],arr[2][2]);
			makeTen(arr[3][3],arr[3][2]);
			makeTen(arr[3][3],arr[2][3]);
			makeTen(arr[2][2],arr[3][0]);
			makeTen(arr[3][2],arr[3][0]);
			makeTen(arr[2][3],arr[3][0]);
		}
		else if((i==0&&j==1)||(i==0)&&(j==2)) {
			makeTen(arr[i][j],arr[i][j-1]);
			makeTen(arr[i][j], arr[i+1][j-1]);
			makeTen(arr[i][j], arr[i+1][j]);
			makeTen(arr[i][j], arr[i+1][j+1]);
			makeTen(arr[i][j], arr[i][j+1]);
			makeTen(arr[i][j-1],arr[i][j]);
			makeTen(arr[i+1][j-1],arr[i][j]);
			makeTen(arr[i+1][j],arr[i][j]);
			makeTen(arr[i+1][j+1],arr[i][j]);
			makeTen(arr[i][j+1],arr[i][j]);

		}
		else if((i==3&&j==1)||(i==3)&&(j==2)) {
			makeTen(arr[i][j],arr[i][j-1]);
			makeTen(arr[i][j], arr[i-1][j-1]);
			makeTen(arr[i][j], arr[i-1][j]);
			makeTen(arr[i][j], arr[i-1][j+1]);
			makeTen(arr[i][j], arr[i][j+1]);
			makeTen(arr[i][j-1],arr[i][j]);
			makeTen(arr[i-1][j-1],arr[i][j]);
			makeTen(arr[i-1][j],arr[i][j]);
			makeTen(arr[i-1][j+1],arr[i][j]);
			makeTen(arr[i][j+1],arr[i][j]);
		}
		else if((i==1&&j==0)||(i==2&&j==0)) {
			;
		}
		else {
			
		}
	}
	public static void makeTen(int ten_1,int one_1) {
		int ten=ten_1*10;
		int result=ten+one_1;
		sosu(result);
	}
	public static void makeHun() {

	}

	public static void sosu(int num) { //소수판별해주는 메소드
		int cnt=0;
		for(int i=1;i<=num;i++) {
			if(num%i==0) {
				cnt++;
			}
		}
		if(cnt==2) {
			System.out.println(num);
		}
	}
}
