package chapter2_1;

public class Person1 { //클래스도 하나의 타입이다.
	private String name;
	private String number;

	public Person1(String name, String number) {  //생성자
		this.name = name;
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public String getNumber() {
		return number;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setNumber(String number) {
		this.number = number;
	}
}
