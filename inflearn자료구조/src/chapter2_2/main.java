package chapter2_2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
public class main {	
	static Sagak [] arr = new Sagak [100];
	static int n=0;
	public static void main(String[] args) {		
		try {
			Scanner fp = new Scanner(new File("input2.txt"));
			while(fp.hasNext()) {
				int x=fp.nextInt();
				int y=fp.nextInt();
				int width = fp.nextInt();
				int height= fp.nextInt();				
				arr[n]=new Sagak(); //각각의 배열요소도 참조변수이므로 선언을 해줘야한다.중요중요중요!!!!!
				arr[n].x=x;
				arr[n].y=y;
				arr[n].width=width;
				arr[n].height=height;
				arr[n].result=width*height;							
				n++;
			}
		} catch (FileNotFoundException e) {
			System.out.println("파일 없음/.");
		}
		System.out.println(n);
		System.out.println(arr.length);
		bubblesort();
		for(int i=0;i<n;i++) {
			System.out.println(arr[i].result);
		}
	}
	public static void bubblesort() {
		for(int i=n-1;i>0;i--) {
			for(int j=0;j<i;j++) {
				if((calculate(arr[j]))>(calculate(arr[j+1]))) {
					Sagak tmp = arr[j];
					arr[j]= arr[j+1];
					arr[j+1] = tmp;
				}
			}
		}
	}
	public static int calculate(Sagak r) {
		return r.height*r.width;
	}
}
