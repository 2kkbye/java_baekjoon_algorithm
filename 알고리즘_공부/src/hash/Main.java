package hash;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hash h = new Hash(10);
		h.addData(1);
		h.addData(2);
		h.addData(11);
	}
}
class Hash{
	private class Node{
		private int data;
		public Node next;
		public Node(int data) {
			this.data = data;
			this.next = null;
		}
	}
	int size;
	Node hTable[];
	public Hash(int s) {
		this.size = s;
		hTable = new Node[this.size];
	}
	public int getHashcode(int data) {
		int hashcode = data%size;
		return hashcode;
	}
	public void addData(int data) {
		int hashvalue = getHashcode(data);
		Node newNode = new Node(data);
		if(hTable[hashvalue] ==null) {
			hTable[hashvalue] = newNode;
			return;
		}
		Node cur = hTable[hashvalue];
		Node pre = null;
		while(cur!=null) {
			if(cur.data == newNode.data) {
				return;
			}
			else {
				pre = cur;
				cur = cur.next;
			}
		}
		pre.next = newNode;
	}
}