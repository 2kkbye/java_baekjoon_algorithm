package LinkedList;

public class Main {
	public static void main(String[] args) {
		LinkedList list = new LinkedList();
		list.first_add(1);
		list.first_add(2);
		list.first_add(3);
		list.print_list();
		list.last_add(4);
		list.last_add(5);
		if(list.search_node(11)) {
			list.print_list();
		}
		
	}
}

class Node{
	private int data;
	Node next;
	public Node(int data) {
		this.data = data;
		this.next = null;
	}
	public int getData() {
		return this.data;
	}
}
class LinkedList{
	Node head;
	public LinkedList() {
		head = null;
	}
	public void first_add(int data) {
		Node newNode = new Node(data);
		if(head == null) {
			head = newNode;
		}
		else {
			newNode.next = head;
			head = newNode;
		}	
	}
	public void last_add(int data) {
		Node newNode = new Node(data);
		if(head == null) {
			head = newNode;
		}
		else {
			Node cur = head;
			while(cur.next!=null) {
				cur = cur.next;
			}
			cur.next = newNode;
		}
	}
	public boolean search_node(int data) {
		Node cur = head;
		while(cur!=null) {
			if(cur.getData() == data) {
				return true;
			}
			cur = cur.next;
		}
		return false;
	}
	public void print_list() {
		Node cur = head;
		while(cur!=null) {
			System.out.print(cur.getData());
			cur = cur.next;
		}
		System.out.println();
	}
}