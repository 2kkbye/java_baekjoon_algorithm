package 수열합치기;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class Solution {
	private static Scanner sc;
	private static UserSolution usersolution = new UserSolution();
	private static int[] arr = new int[1000];
	
	private static int run(int Ans)
	{
		usersolution.init();
		
		int N = sc.nextInt();
		for(int i = 0; i < N; i++)
		{
			String op = sc.next();
			if(op.charAt(0) == 'M') {
				int len = sc.nextInt();
				for(int j = 0; j < len; j++)
					arr[j] = sc.nextInt();
				usersolution.mergenums(len, arr);
				
			}else if(op.charAt(0) == 'K') {
				int kth = sc.nextInt();
				int ret = sc.nextInt();
				if(ret != usersolution.findkth(kth))
					Ans = 0;
			}			
		}
		return Ans;
	}
	
	public static void main(String[] args) throws FileNotFoundException	{
	
		//System.setIn(new java.io.FileInputStream("input.txt"));
		sc = new Scanner(System.in);

		int TC = sc.nextInt();		
		for (int testcase = 1; testcase <= TC; ++testcase) {
            System.out.println("#" + testcase + " " + run(100));
		}
		sc.close();
	}
}
