package 수열합치기;

class UserSolution {
	static LinkedList l = new LinkedList();
	void init()	{
		l.init();
		l.insertFirstNode(-1);
	}
	void mergenums(int n, int[] arr) {
		if(l.size == 1) {
			for(int a = 0;a<n;a++) {
				l.insertLastNode(arr[a]);
			}
		}
		else {
			ListNode temp = l.searchNode(arr[0]);
			if(temp== null) {
				for(int a = 0;a<n;a++) {
					l.insertLastNode(arr[a]);
				}
			}
			else {
				l.insertMiddleNode(temp, arr,n);
			}
		}
		//l.printList();
	}

	int findkth(int kth) {
		return l.check(kth,l.size);
	}
}
class ListNode {
	private int data;
	public ListNode link;
	public ListNode() {
		this.data=-1;
		this.link=null;
	}
	public ListNode(int data) {
		this.data=data;
		this.link=null;
	}
	public ListNode(int data, ListNode link) {
		this.data=data;
		this.link=link;
	}
	public int getData() {
		return this.data;
	}
	public void setData(int data) {
		this.data = data;
	}
}
class LinkedList {
	private ListNode head;
	int size = 0;
	public LinkedList() {
		head = null;
	}
	public void insertFirstNode(int data) {
		ListNode newNode = new ListNode(data);
		newNode.link=head;
		head=newNode;
		size++;
	}
	public void insertMiddleNode(ListNode pre, int arr[], int n) {
		for(int a = 0;a<n;a++) {
			ListNode newNode = new ListNode(arr[a]);
			newNode.link=pre.link;
			pre.link=newNode;
			pre = newNode;
			size++;
		}

	}
	public void insertLastNode(int data) {
		size++;
		ListNode newNode = new ListNode(data);
		if(head ==null) {
			this.head=newNode;
		}
		else {
			ListNode temp = head;
			while(temp.link !=null) temp = temp.link;
			temp.link=newNode;
		}
	}
	public void init() {
		head=null;
		size = 0;
	}
	public ListNode searchNode(long data) {
		ListNode temp = this.head;
		ListNode pre = this.head;
		while(temp!=null) {
			if(data < temp.getData())
				return pre;
			else {
				pre = temp;
				temp=temp.link;
			}
		}
		pre = null;
		return pre;
	}
	public void reverseList() {
		ListNode next = head;
		ListNode current = null;
		ListNode pre = null;
		while(next !=null) {
			pre=current;
			current =next;
			next=next.link;
			current.link=pre;
		}
		head = current;
	}
	public int check(int count,int size) {
		ListNode temp= this.head;
		int cnt = 0;
		if(count<0) {
			count = size + count;
		}
		while(temp!=null) {
			if(temp.getData() == -1) {temp=temp.link;continue;}
			if(count<0) {
				cnt--;
			}
			else {
				cnt++;
			}
			if(cnt != count) {temp=temp.link;continue;}
			return temp.getData();
		}
		return 0;
		
	}
	public void printList() {
		ListNode temp= this.head;
		System.out.printf("L = (");
		while(temp!=null) {
			System.out.print(temp.getData());
			temp=temp.link;
			if(temp!=null) {
				System.out.printf(", ");
			}
		}
		System.out.println(")");
	}
}