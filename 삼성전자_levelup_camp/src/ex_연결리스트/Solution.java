package ex_연결리스트;

public class Solution {
	public static void main(String[] args) {
		LinkedList l = new LinkedList();
		l.insertFirst(1);
		l.insertFirst(2);
		l.insertFirst(3);
		l.insertLast(5);
		l.insertLast(7);
		l.insertLast(10);
		l.insertFirst(0);
		l.print();
		System.out.println();
		l.insertMiddle(10, 9);
		l.print();
	}
}
class Node{
	private int data;
	public Node link;
	public Node(int data) {
		// TODO Auto-generated constructor stub
		this.data = data;
		this.link = null;
	}
	public int getData() {
		return this.data;
	}
}
class LinkedList{
	private Node head;
	public LinkedList() {
		head = null;
	}
	public void insertFirst(int data) {
		Node newNode = new Node(data);
		if(head == null) {
			head = newNode;
			return;
		}
		else {
			newNode.link = head;
			head= newNode;
		}
	}
	public void insertLast(int data) {
		Node newNode = new Node(data);
		Node pre = head;
		while(pre.link!=null) {
			pre = pre.link;
		}
		pre.link = newNode;
	}
	public void insertMiddle(int find_data,int data) {
		Node newNode = new Node(data);
		
		Node pre = null;
		Node cur = head;
		while(cur!=null) {
			if(cur.getData() == find_data) {
				break;
			}
			pre = cur;
			cur = cur.link;
		}
		if(pre == null) {
			insertFirst(data);
		}
		else {
			newNode.link = pre.link;
			pre.link = newNode;
		}
	}
	public void print() {
		Node pre = head;
		while(pre!=null) {
			System.out.print(pre.getData()+" ");
			pre = pre.link;
		}
	}
}