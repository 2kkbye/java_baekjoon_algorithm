package 키조사하기_해시;

class UserSolution {

	static Hash h;
	void init(){
		h = new Hash();
	}
	int checkKey(long key){
		int ans = h.add(key);
		return ans;
	}
}
class Hash {

	// 데이터를 저장할 Entry는 값과 다음 Entry를 가진다.
	private class Entry{
		private long value;
		public Entry next;
	}

	private int size;
	private Entry[] hTable;

	// Hash Table은 size와 배열 테이블을 생성한다.
	public Hash() {
		this.size = 65536;
		this.hTable = new Entry[size];
	}
	// key 값으로 HashTable에 저장될 index를 반환한다.
	public int hashMethod(long key){
		int ans = (int) (key%size);
		return ans;
	}

	// 저장할 데이터로 key를 추출한다.
	// 실제 Hash 에는 특별한 알고리즘이 적용되 hashCode를 얻는다.
	public long getKey(long data){
		return data;
	}

	// data를 HashTable에 저장한다.
	public int add(long data){

		// data의 hashCode를 key와 저장할 index를 얻는다.
		long key = getKey(data);
		int hashValue = hashMethod(key);

		// 저장할 Entry 생성
		Entry entry = new Entry();
		entry.value = data;


		// HashTable의 저장할 index가 비어있다면,  저장하고 끝낸다.
		if(hTable[hashValue] == null){

			hTable[hashValue] = entry;
			return 1;
		}


		Entry pre = null;
		Entry cur = hTable[hashValue];

		// 해당 index의 연결리스트는 값의 크기가 작은 값부터 큰 순서로 정렬되어있다.
		while(cur!=null){

			if(cur.value == key){
				return 0;
			}
			else {    // 해당 index의 커서 노드의 값이 key보다 작으면
				// 커서를 하나 뒤로 옮긴다.
				pre = cur;            
				cur = cur.next;
			}
		}
		pre.next = entry;
		return 1;
	}
}
