package ex_�ؽ��Լ�;

public class Solution {
	public static void main(String[] args) {
		Hash h = new Hash();
		h.add(2);
		h.add(10);
		h.add(18);
	}
}
class Node{
	private int data;
	public Node link;
	public Node(int data) {
		// TODO Auto-generated constructor stub
		this.data = data;
		this.link = null;
	}
	public int getData() {
		return this.data;
	}
}
class Hash{
	Node hTable[];
	public Hash() {
		// TODO Auto-generated constructor stub
		hTable = new Node[8];
	}
	public int getIndex(int data) {
		return data % 8;
	}
	public void add(int data) {
		Node newNode = new Node(data);
		int index = getIndex(data);
		if(hTable[index] == null) {
			hTable[index] = newNode;
			return;
		}
		Node pre = hTable[index];
		while(pre.link!=null) {
			pre = pre.link;
		}
		pre.link = newNode;
	}
}
