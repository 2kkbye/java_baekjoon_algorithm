package ex_퀵소트예시;

public class Solution {
	static int i = 0;
	//static int a[]= {37, 7, 8, 9, 12, 13, 14};
	//static int a[]= {29, 14, 12, 11, 1, 3, 4};
	static int a[]= {20, 7, 23, 19, 10, 8, 13};
	public static void main(String[] args) {
		sort(a,0, a.length-1);
	}
	public static void sort(int[] data, int l, int r) {
		int left = l;
		int right = r;
		int pivot = data[(l + r) / 2]; // pivot 가운데 설정 (최악의 경우 방지)

		do {
			while (data[left] < pivot)
				left++;
			while (data[right] > pivot)
				right--;

			if (left <= right) {
				//System.out.println("change");
				int temp = data[left];
				data[left] = data[right];
				data[right] = temp;
				left++;
				right--;
			}
		} while (left <= right);
		if (l < right) {
			sort(data, l, right);
		}
		if (r > left) {
			sort(data, left, r);
		}
	}
	public static void print() {
		System.out.println("+===============================");
		for(int b = 0;b<a.length;b++) {
			System.out.print(a[b]+" ");
		}
		System.out.println();
		System.out.println("+===============================");
	}

}
