package 연결리스트_예시;

public class Solution {
	public static void main(String[] args) {
		Hash h = new Hash();
		int ans = h.add(100);
		System.out.println(ans);
		ans = h.add(100);
		System.out.println(ans);
	}
}
class Hash {

	// 데이터를 저장할 Entry는 값과 다음 Entry를 가진다.
	private class Entry{
		private int value;
		public Entry next;
	}

	private int size;
	private Entry[] hTable;

	// Hash Table은 size와 배열 테이블을 생성한다.
	public Hash() {
		this.size = 65536;
		this.hTable = new Entry[size];
	}
	// key 값으로 HashTable에 저장될 index를 반환한다.
	public int hashMethod(int key){
		System.out.println(key % size);
		return key % size;
	}

	// 저장할 데이터로 key를 추출한다.
	// 실제 Hash 에는 특별한 알고리즘이 적용되 hashCode를 얻는다.
	public int getKey(int data){
		return data;
	}

	// data를 HashTable에 저장한다.
	public int add(int data){

		// data의 hashCode를 key와 저장할 index를 얻는다.
		int key = getKey(data);
		int hashValue = hashMethod(key);

		// 저장할 Entry 생성
		Entry entry = new Entry();
		entry.value = data;


		// HashTable의 저장할 index가 비어있다면,  저장하고 끝낸다.
		if(hTable[hashValue] == null){

			hTable[hashValue] = entry;
			return 1;
		}


		Entry pre = null;
		Entry cur = hTable[hashValue];

		// 해당 index의 연결리스트는 값의 크기가 작은 값부터 큰 순서로 정렬되어있다.
		while(cur!=null){

			if(cur.value == key){
				return 0;
			}
			else {    // 해당 index의 커서 노드의 값이 key보다 작으면
				// 커서를 하나 뒤로 옮긴다.
				pre = cur;            
				cur = cur.next;
			}
		}
		pre.next = entry;
		return 1;
	}
	// data가 있는지 확인
	public int get(int data){
		int key = getKey(data);
		int hashValue = hashMethod(key);
		
		Entry cur = hTable[hashValue];
		while(cur!=null){
			if(cur.value == key){             
				return 0;
			}
			else{
				cur = cur.next;
			}
		}
		return -1;
	}
}
