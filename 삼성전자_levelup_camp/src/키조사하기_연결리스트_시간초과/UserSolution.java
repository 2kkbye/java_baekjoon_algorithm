package 키조사하기_연결리스트_시간초과;

class UserSolution {
	
	static LinkedList l = new LinkedList();
	void init(){
		l.init();
	}
	int checkKey(long key){
		int ans = 0;
		if(!l.searchNode(key)) {
			ans = 1;
			//l.insertLastNode(key);
			l.insertFirstNode(key);
		}
		return ans;
	}
}
class ListNode{
	private long data;
	public ListNode link;
	public ListNode() {
		this.data = -1;
		this.link = null;
	}
	public ListNode(long data) {
		this.data = data;
		this.link = null;
	}
	public long getData() {
		return this.data;
	}
	public void setData(long data) {
		this.data = data;
	}
}
class LinkedList{
	int size = 0;
	private ListNode head;

	public LinkedList() {
		// TODO Auto-generated constructor stub
		head = null;
	}
	public void init() {
		head = null;
		size = 0;
	}
	public void insertFirstNode(long data) {
		size++;
		ListNode newNode = new ListNode(data);
		newNode.link=head;
		head=newNode;
	}
	public boolean searchNode(long data) {
		ListNode temp = head;
		while(temp!=null) {
			if(temp.getData() == data) {
				return true;
			}
			temp = temp.link;
		}
		return false;
	}
}
