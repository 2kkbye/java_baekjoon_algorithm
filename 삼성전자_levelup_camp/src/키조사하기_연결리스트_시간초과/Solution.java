package 키조사하기_연결리스트_시간초과;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

class Solution {
	private static Scanner sc;
	private static UserSolution usersolution = new UserSolution();
	
	private static int run(int Ans)
	{
		usersolution.init();						
		
		int N = sc.nextInt();
		long key = 0;
		int ret = 0;
		for(int i = 0; i < N; i++)
		{
			key = sc.nextLong();
			ret = sc.nextInt();			
			if(ret != usersolution.checkKey(key))
				Ans = 0;
		}
		return Ans;
	}
	
	public static void main(String[] args) throws FileNotFoundException	{
	
		sc = new Scanner(System.in);

		int TC = sc.nextInt();
		int Ans = 100;
		for (int testcase = 1; testcase <= TC; ++testcase) {
            System.out.println("#" + testcase + " " + run(Ans));
		}
		sc.close();
	}
}