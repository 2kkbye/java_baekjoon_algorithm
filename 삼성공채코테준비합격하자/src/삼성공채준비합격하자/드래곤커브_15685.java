package 삼성공채준비합격하자;

import java.util.Scanner;
import java.util.ArrayList;

public class 드래곤커브_15685 {
	static int dx[]= {1,0,-1,0};
	static int dy[]= {0,-1,0,1};
	
	static int visit[][]= new int[101][101];
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		int result=0;
		ArrayList<Integer> direction;
		for(int i=0;i<N;i++) {
			direction =  new ArrayList<Integer>();
			int x = sc.nextInt();
			int y = sc.nextInt();
			int d = sc.nextInt();
			int g = sc.nextInt();
			
			direction.add(d);
			visit[y][x]=1;
			for(int a=0;a<g;a++) {
				int size = direction.size()-1;
				for(int b=size;b>=0;b--) {
					int temp = (direction.get(b)+1)%4;
					direction.add(temp);
				}
			}
			int rx = x;
			int ry = y;
			for(int a=0;a<direction.size();a++) {
				rx = rx+dx[direction.get(a)];
				ry = ry+dy[direction.get(a)];
				visit[ry][rx]=1;
			}
		}
		for(int a=0;a<100;a++) {
			for(int b=0;b<100;b++) {
				if((visit[a][b]==1)&&(visit[a][b+1]==1)&&(visit[a+1][b]==1)&&(visit[a+1][b+1]==1)) {
					result++;
				}
			}
		}
		System.out.println(result);
	}

}
