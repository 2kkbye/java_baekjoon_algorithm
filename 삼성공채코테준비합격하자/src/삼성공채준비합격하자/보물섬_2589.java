package 삼성공채준비합격하자;

import java.util.Scanner;
import java.util.Queue;
import java.awt.Point;
import java.util.LinkedList;

public class 보물섬_2589 {

	static int dx[]= {0,0,-1,1};
	static int dy[]= {1,-1,0,0};
	static boolean visit[][];
	static int land[][] = new int[50][50];

	static Queue<Point> q_first;
	static int max;
	static int R,C;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		R = sc.nextInt();
		C = sc.nextInt();
		String temp[][] = new String[R][1];
		for(int a=0;a<R;a++) {
			temp[a][0] = sc.next();
		}
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(temp[a][0].charAt(b)=='W') {
					land[a][b]=-1;
					continue;
				}
			}
		}
		max =-1;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(land[a][b]==-1) {
					continue;
				}
				else if(land[a][b]==0) {
					visit = new boolean[50][50];
					q_first = new LinkedList<Point>();
					q_first.add(new Point(a,b));
					visit[a][b]= true;
					bfs();
				}
			}
		}
		System.out.println(max);
	}
	public static void print() {
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				System.out.print(land[a][b]+" ");
			}
			System.out.println();
		}
	}
	public static void bfs() {
		while(!q_first.isEmpty()) {
			int px=q_first.peek().x;
			int py=q_first.peek().y;
			q_first.remove();
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx >= 0 && ry >= 0 && rx < R && ry < C) {
					if(visit[rx][ry]==false && land[rx][ry]!=-1) {
						visit[rx][ry]= true;
						q_first.add(new Point(rx,ry));
						land[rx][ry]= land[px][py]+1;
						if(land[rx][ry] > max) {
							max =land[rx][ry];
						}
					}
				}
				if(a==3) {
					land[px][py]=0;
				}
			}
		}
	}
}