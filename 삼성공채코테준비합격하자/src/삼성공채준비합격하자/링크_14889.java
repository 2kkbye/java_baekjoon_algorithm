package 삼성공채준비합격하자;

import java.util.Scanner;
import java.util.ArrayList;
public class 링크_14889 {

	static int team[][];
	static int R;
	
	static boolean visit[]; //방문했는지 안했는지 체크할 배열
	static ArrayList<Integer> s_list = new ArrayList<Integer>(); 
	static ArrayList<Integer> l_list = new ArrayList<Integer>();
	static int diff;
	//level 3이면 검사
	static int s_team;
	static int l_team;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		R = sc.nextInt();
		team = new int[R][R];
		visit  = new boolean[R];
		for(int a=0;a<R;a++) {
			for(int b=0;b<R;b++) {
				team[a][b] = sc.nextInt();
			}
		}
		diff = Integer.MAX_VALUE;
		for(int a=0;a<=R/2;a++) {
			visit[a]=true;
			s_list.add(a);
			s_team =0;l_team=0;
			dfs(a,1);
			visit[a]=false;
			s_list.remove(0);
		}
		System.out.println(diff);
	}
	public static void sum_list(int sw) {
		if(sw ==0) { //steam
			for(int a=0;a<s_list.size()-1;a++) {
				for(int b=a+1;b<s_list.size();b++) {
					s_team = s_team+team[s_list.get(a)][s_list.get(b)];
					s_team = s_team+team[s_list.get(b)][s_list.get(a)];
				}
			}
		}
		else { //lteam
			for(int a=0;a<l_list.size()-1;a++) {
				for(int b=a+1;b<l_list.size();b++) {
					l_team = l_team+team[l_list.get(a)][l_list.get(b)];
					l_team = l_team+team[l_list.get(b)][l_list.get(a)];
				}
			}
		}

	}
	public static void fal_check() {
		l_list = new ArrayList<Integer>();
		for(int a=0;a<R;a++) {
			if(visit[a]==false) {
				l_list.add(a);
			}
		}
	}
	public static void diff_team() {
		int d = Math.abs((s_team-l_team));
		if(d<diff) {
			diff =d;
		}
	}
	public static void dfs(int rx, int level) {
		if(level == (R/2)) {
			sum_list(0);
			fal_check();
			sum_list(1);
			diff_team();
			s_team=0;l_team=0;
			return;
		}
		for(int a=rx+1;a<=(R/2)+level;a++) {
			visit[a]=true;
			s_list.add(a);
			dfs(a,level+1);
			visit[a]=false;
			s_list.remove(level);
		}
	}
}
