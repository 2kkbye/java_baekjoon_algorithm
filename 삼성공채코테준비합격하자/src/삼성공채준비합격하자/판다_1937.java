package 삼성공채준비합격하자;

import java.util.Scanner;
public class 판다_1937 {

	static int dx[]= {0,0,-1,1};
	static int dy[]= {1,-1,0,0};
	static int panda[][];
	static int N;
	static int dp[][];
	static int ret;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();

		panda = new int[N][N];
		dp = new int[N][N];
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				panda[a][b]=sc.nextInt();
			}
		}
		ret =0;
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				ret= Math.max(ret,dfs(a,b));

			}
		}
		System.out.println(ret);
	}
	public static int dfs(int x,int y) {
		if(dp[x][y]!=0) {
			return dp[x][y];
		}
		int day = 1;
		for(int a=0;a<4;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx < 0 || ry < 0 || rx >= N || ry >= N) continue;
			if(panda[x][y]>=panda[rx][ry]) continue;
			day = Math.max(day, dfs(rx,ry)+1);
			dp[x][y] = day;
		}
		return day;
	}
}
