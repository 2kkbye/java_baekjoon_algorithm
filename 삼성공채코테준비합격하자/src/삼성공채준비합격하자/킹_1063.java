package 삼성공채준비합격하자;

import java.util.Scanner;
public class 킹_1063 {

	static int dx[] = {0,0,1,-1,-1,-1,1,1};
	static int dy[] = {1,-1,0,0,1,-1,1,-1};
	static String dir[] = {"R","L","B","T","RT","LT","RB","LB"};
	static int king_map[][] = new int[9][9];
	static int black_map[][] = new int[9][9];
	static int ins_arr[]; 
	static int kX,kY;
	static int bX,bY;
	static int cnt; 
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String st = sc.nextLine();
		String str[] = st.split(" ");
		reverse(str);
		String temp[] = new String[cnt];
		for(int a=0;a<temp.length;a++) {
			temp[a] = sc.next();
		}
		ins_dir(temp);
		king_map[kX][kY]=2;
		black_map[bX][bY]=2;
		for(int a=0;a<ins_arr.length;a++) {
			int rx = kX+dx[ins_arr[a]];
			int ry = kY+dy[ins_arr[a]];
			if(rx > 0 && ry > 0 && rx < 9 && ry < 9) {
				if(check_black(kX, kY, dx[ins_arr[a]], dy[ins_arr[a]])) {
					int px = bX + dx[ins_arr[a]];
					int py = bY + dy[ins_arr[a]];
					if(px > 0 && py > 0 && px < 9 && py < 9) {
						king_map[rx][ry]=2;
						king_map[kX][kY]=0;
						kX=rx;kY=ry;
						
						black_map[px][py]=2;
						black_map[bX][bY]=0;
						bX = px;bY = py;
					}
					else if(black_map[rx][ry]==0) {
						king_map[rx][ry]=2;
						king_map[kX][kY]=0;
						kX=rx;kY=ry;
					}
				}
				else { //검은돌이 없다면
					king_map[rx][ry]=2;
					king_map[kX][kY]=0;
					kX=rx;kY=ry;
				}
			}
			else continue;
		}
		System.out.println("--");
		System.out.println(((char)(kY+64))+""+(9-kX));
		System.out.println(((char)(bY+64))+""+(9-bX));

	}
	public static boolean check_black(int x,int y, int dX, int dY) {
		for(int a=1;a<2;a++) {
			if((x+dX) > 0 && (y+dY) > 0 && (x+dX) < 9 && (y+dY) < 9) {
				if(black_map[x+dX][y+dY]==2) {
					return true;
				}
				else {
					x=x+dX;
					y=y+dY;
				}
			}
			else break;
		}

		return false;
	}
	public static void ins_dir(String arr[]) { //명령어 숫자로 바꿔주기
		ins_arr = new int[arr.length];
		for(int a=0;a<arr.length;a++) {
			for(int b=0;b<dir.length;b++) {
				if(arr[a].equals(dir[b])) {
					ins_arr[a]=b;
					break;
				}
			}
		}
	}
	public static void reverse(String str[]) {
		cnt = Integer.valueOf(str[2]);
		kY =  (int)str[0].charAt(0)-64;
		kX = 9-((int)str[0].charAt(1)-48);
		bY = (int)str[1].charAt(0)-64;
		bX = 9-((int)str[1].charAt(1)-48);
	}
}
