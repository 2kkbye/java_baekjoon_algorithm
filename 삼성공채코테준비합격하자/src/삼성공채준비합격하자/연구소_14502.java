package 삼성공채준비합격하자;

import java.awt.Point;
import java.util.Scanner;
import java.util.Queue;
import java.util.LinkedList;
public class 연구소_14502 {
	static int dx[] = {0,0,-1,1};
	static int dy[] = {1,-1,0,0};
	static int lab[][];
	static int temp[][];
	static boolean visit[][];
	static int R;
	static int C;
	static int min;
	static Queue<Point> q_virus = new LinkedList<Point>(); //바이러스 퍼트릴것
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		R = sc.nextInt();
		C = sc.nextInt();
		lab = new int[R][C];
		visit = new boolean[R][C];
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				lab[a][b]=sc.nextInt();
			}
		}
		min =-1;
		dfs(0,0,0);
		System.out.println(min);
	}
	public static void copy() {
		temp = new int[R][C];
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				temp[a][b]=lab[a][b];
			}
		}
	}
	public static void copy_reverse() {
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				lab[a][b]=temp[a][b];
			}
		}
	}
	public static void dfs(int x,int y,int level) {
		if(level == 3) {
			copy();
			spread_virus();
			check_safe();
			copy_reverse();
			return;
		}
		for(int a=x;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(lab[a][b]==0) {
					lab[a][b]=1;
					dfs(a,b+1,level+1);
					lab[a][b]=0;
				}
			}
		}
	}
	public static void bfs() {
		while(!q_virus.isEmpty()) {
			int px = q_virus.peek().x;
			int py = q_virus.peek().y;
			q_virus.remove();
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx >=0 && ry >= 0 && rx < R && ry < C) {
					if(lab[rx][ry]!=1 && visit[rx][ry]==false) {
						q_virus.add(new Point(rx,ry));
						lab[rx][ry]=2;
						visit[rx][ry]=true;
					}
				}
			}
		}
	}
	public static void spread_virus() {
		visit = new boolean[R][C];
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(lab[a][b]!=2) continue;
				q_virus.add(new Point(a,b));
				visit[a][b]=true;
			}
		}
		bfs();
	}
	public static void check_safe() {
		int cnt=0;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(lab[a][b]==0) {
					cnt++;
				}
			}
		}
		if(cnt>min) {
			min = cnt;
		}
	}
	public static void print() {
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				System.out.print(lab[a][b]+" ");
			}
			System.out.println();
		}
	}
	
}