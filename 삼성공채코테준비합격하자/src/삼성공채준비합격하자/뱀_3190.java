package �Ｚ��ä�غ��հ�����;

import java.util.Scanner;
import java.util.LinkedList;
import java.awt.Point;
public class ��_3190 {

	static int map[][];
	static int apple[][];
	static LinkedList<Point> snake = new LinkedList<Point>();
	static LinkedList<Point> all_dir = new LinkedList<Point>();

	static int second[];
	static String dir[];

	static int N;
	static int dirX,dirY;
	static int index;
	static int result;
	static int sw;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		int app = sc.nextInt();

		map = new int[N][N];
		apple = new int[N][N];

		for(int a=0;a<app;a++) {
			int a_x = sc.nextInt();
			int a_y = sc.nextInt();
			apple[a_x-1][a_y-1]=1;
		}
		app = sc.nextInt();
		second = new int[app];
		dir = new String[app];
		for(int a=0;a<app;a++) {
			second[a] = sc.nextInt();
			dir[a] = sc.next();
		}
		dirX = 0;
		dirY = 1;
		result=0;
		sw=0;
		index =0;
		dfs(0,0,0);
		all_dir.add(new Point(0,0));
		System.out.println(result);
	}
	public static boolean check(int x,int y) {
		for(int a=0;a<snake.size();a++) {
			if((x==snake.get(a).x) && (y==snake.get(a).y)) {
				return false;
			}
		}
		return true;
	}
	public static void dfs(int x,int y,int level) {
		if(sw==1) {
			return;
		}
		if(x <0 || y<0 || x>=N || y >= N) {
			result = level;
			sw=1;
			return;
		}
		if(!check(x,y)) {
			result = level;
			sw=1;
			return;
		}
		if(apple[x][y]==1) {
			snake.add(0, new Point(x,y));
			apple[x][y]=0;
		}
		else {
			if(snake.size()>=1) {
				snake.remove(snake.size()-1);
			}
			snake.add(0, new Point(x,y));
		}
		if(index <second.length && level == second[index]) {
			if(dir[index].equals("L")) {
				check_RL(0);
				index++;
			}
			else if(index <second.length && dir[index].equals("D") ) {
				check_RL(1);
				index++;
			}
		}
		all_dir.add(0, new Point(x+dirX,y+dirY));
		dfs((x+dirX),(y+dirY),level+1);
	}
	public static void check_RL(int temp) {
		int tempX = all_dir.get(1).x;
		int tempY = all_dir.get(1).y;
		if(temp ==0){ //L
			if(tempX == all_dir.get(0).x) {
				if(tempY > all_dir.get(0).y) {
					dirX=1;
					dirY=0;
				}
				else if(tempY < all_dir.get(0).y) {
					dirX=-1;
					dirY=0;
				}
			}
			else if(tempX>all_dir.get(0).x) {
				dirX = 0;
				dirY = -1;
			}
			else {
				dirX = 0;
				dirY = 1;
			}
		}
		else { //D
			if(tempX == all_dir.get(0).x) {
				if(tempY > all_dir.get(0).y) {
					dirX=-1;
					dirY=0;
				}
				else if(tempY < all_dir.get(0).y) {
					dirX=1;
					dirY=0;
				}
			}
			else if(tempX>all_dir.get(0).x) {
				dirX = 0;
				dirY = 1;
			}
			else {
				dirX = 0;
				dirY = -1;
			}
		}
	}
}
