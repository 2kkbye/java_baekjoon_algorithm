package 삼성공채준비합격하자;

import java.util.Scanner;
import java.util.LinkedList;
import java.awt.Point;
import java.util.Queue;

public class 아기상어_16234 {
	static int dx[]= {0,-1,0,1}; //왼쪽, 위쪽, 오른쪽, 아래
	static int dy[]= {-1,0,1,0};

	static int map[][];
	static int fish[][];
	static boolean visit[][];

	static Queue<Point> q_fish = new LinkedList<Point>();
	static LinkedList<Point> q_eating = new LinkedList<Point>();
	static int N;
	static int c_size;
	static int c_eat;
	static int result;
	static int start_x,start_y;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();

		map = new int[N][N];
		fish = new int[N][N];
		visit = new boolean[N][N];

		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				int temp =sc.nextInt();
				if(temp ==0) continue;
				if(temp ==9) {
					q_fish.add(new Point(a,b));
					visit[a][b]=true;
					start_x = a;start_y=b;
					continue;
				}
				fish[a][b]=temp;
			}
		}
		if(q_fish.size()==0) {
			System.out.println(0);
		}
		else {
			c_size=2;
			c_eat=0;
			result =0;
			bfs();
		}
		System.out.println(result);
	}
	public static void print() {
		System.out.println("----------------------------");
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				System.out.print(map[a][b]+" ");
			}
			System.out.println();
		}
	}
	public static void check(int rx, int ry) {
		if(q_eating.isEmpty()) {
			q_eating.add(new Point(rx,ry));
			return;
		}
		else {
			int px = q_eating.get(0).x;
			int py = q_eating.get(0).y;
			if(map[px][py] < map[rx][ry]) { //기존이 더 가까움
				return;
			}
			if(map[px][py] > map[rx][ry]) { //기존이 더 먼 경우
				q_eating.remove();
				q_eating.add(new Point(rx,ry));
			}
			if(map[px][py] == map[rx][ry]) {
				if(px > rx) {
					q_eating.remove();
					q_eating.add(new Point(rx,ry));
				}
				else if(px == rx) {
					if(py > ry) {
						q_eating.remove();
						q_eating.add(new Point(rx,ry));
					}
				}
			}
		}
	}
	public static void bfs() {
		while(!q_fish.isEmpty()) {
			//print();
			int px = q_fish.peek().x;
			int py = q_fish.peek().y;
			q_fish.remove();
			for(int a=0;a<4;a++) {
				int rx = px + dx[a];
				int ry = py + dy[a];
				if(rx >= 0 && ry >= 0 && rx < N && ry < N) {
					if(visit[rx][ry]==false && fish[rx][ry] <= c_size) {
						if(fish[rx][ry]!=0 && fish[rx][ry]!=c_size) { //0이 아닌경우
							q_fish.add(new Point(rx,ry));
							map[rx][ry] = map[px][py]+1;
							visit[rx][ry]=true;
							check(rx,ry);
						}
						else { //0인경우, 같은수
							q_fish.add(new Point(rx,ry));
							visit[rx][ry]=true;
							map[rx][ry] = map[px][py]+1;
						}
					}
				}
			}
			if(q_fish.size()==0) {
				if(q_eating.size()!=0) {
					c_eat++;
					if(c_size == c_eat){
						c_size++;
						c_eat =0;
					}
					int rx = q_eating.get(0).x;
					int ry = q_eating.get(0).y;
					q_eating.remove();
					result = result +map[rx][ry];
					q_fish.add(new Point(rx,ry));
					map = new int[N][N];
					visit = new boolean[N][N];
					visit[rx][ry]=true;
					fish[rx][ry]=0;
				}
			}
		}
	}
}
