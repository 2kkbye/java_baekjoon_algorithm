package �Ｚ��ä�غ��հ�����;

import java.awt.Point;
import java.util.Scanner;
import java.util.LinkedList;
import java.util.Queue;

public class ����_2234 {
	static int dx[]= {1,0,-1,0};
	static int dy[]= {0,1,0,-1};
	static int binary[];

	static boolean visit[][];
	static int wall[][];
	static int max_room[][];
	static int temp_cnt[][];

	static Queue<Point> q_wall = new LinkedList<Point>();
	static int R,C;
	static int room_cnt;
	static int room_max;
	static int room_temp;
	static int double_max;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		C = sc.nextInt();
		R = sc.nextInt();

		visit = new boolean[R][C];
		wall = new int[R][C];
		max_room = new int[R][C];
		temp_cnt = new int[R][C];
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				wall[a][b]= sc.nextInt();	
			}
		}
		room_cnt=0;
		room_max = -1;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(visit[a][b]==false) {
					room_temp=1;
					room_cnt++;
					q_wall.add(new Point(a,b));
					visit[a][b]=true;
					bfs();
				}
			}
		}
		double_max =-1;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				check(a,b);
			}
		}
		System.out.println(room_cnt);
		System.out.println(room_max);
		System.out.println(double_max);
	}
	public static void check(int x,int y) {
		for(int a=0;a<4;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx < 0 || ry < 0 || rx >= R || ry >= C) continue;
			if(temp_cnt[x][y]==temp_cnt[rx][ry]) continue;
			if(double_max < (max_room[x][y]+max_room[rx][ry])) {
				double_max = max_room[x][y]+max_room[rx][ry];
			}
		}
	}
	public static void change_two(int num) {
		binary = new int[4];
		String s= Integer.toBinaryString(num);
		for(int a=3;a>=4-s.length();a--) {
			binary[a]=(int)s.charAt(a-(4-s.length()))-48;
		}
	}
	public static void bfs() {
		while(!q_wall.isEmpty()) {
			int px = q_wall.peek().x;
			int py = q_wall.peek().y;
			q_wall.remove();
			change_two(wall[px][py]);
			for(int a=0;a<4;a++) {
				if(binary[a]!=0) continue;
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx >= 0 && ry >= 0 && rx < R && ry < C) {
					if(visit[rx][ry]==false) {
						q_wall.add(new Point(rx,ry));
						visit[rx][ry]=true;
						room_temp++;
					}
				}
			}
		}
		if(room_temp>room_max) {
			room_max = room_temp;
		}
		max_room();
	}
	public static void max_room() {
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(visit[a][b]==false) continue;
				if(max_room[a][b]!=0) continue;
				max_room[a][b]=room_temp;
				temp_cnt[a][b]=room_cnt;
			}
		}
	}
}
