package 삼성공채준비합격하자;

import java.util.Scanner;

public class Easy_12100 {
	
	static int dx[] = {-1,1,0,0}; //상하좌우
	static int dy[] = {0,0,-1,1};
	static int visit[] = new int[5];
	static int map[][];
	static int copy[][];
	static int N;
	static int ans;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		map = new int[N][N];
		copy = new int[N][N];
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				map[a][b] = sc.nextInt();
			}
		}
		ans = -1;
		dfs(0);
		System.out.println(ans);
	}
	public static void print() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				System.out.print(copy[a][b]+" ");
			}
			System.out.println();
		}
	}
	public static void check_sum(int d) {
		if(d==0) { //위쪽으로 
			for(int a=0;a<N;a++) {
				for(int b=0;b<N-1;b++) {
					if(copy[b][a] == copy[b+1][a]) {
						copy[b][a] = copy[b][a]*2;
						copy[b+1][a]=0;
					}
				}
			}
		}
		else if(d==1) { //아럐쪽으로
			for(int a=0;a<N;a++) {
				for(int b=N-1;b>=1;b--) {
					if(copy[b][a]==copy[b-1][a]) {
						copy[b][a] = copy[b][a]*2;
						copy[b-1][a]=0;
					}
				}
			}
		}
		else if(d==2) { //왼쪽으로
			for(int a=0;a<N;a++) {
				for(int b=0;b<N-1;b++) {
					if(copy[a][b]== copy[a][b+1]) {
						copy[a][b] = copy[a][b]*2;
						copy[a][b+1]=0;
					}
				}
			}
		}
		else if(d==3) {//오른쪽으로
			for(int a=0;a<N;a++) {
				for(int b=N-1;b>=1;b--) {
					if(copy[a][b]== copy[a][b-1]) {
						copy[a][b] = copy[a][b]*2;
						copy[a][b-1]=0;
					}
				}
			}
		}
	}
	public static void move(int d) { //상하좌우
		if(d==0) { //위쪽으로 
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					if(copy[b][a]!=0) {
						int rx = b, ry = a;
						int temp = copy[rx][ry];
						copy[rx][ry]=0;
						while(true) {
							rx = rx+dx[d];
							ry = ry+dy[d];
							if(rx < 0 || ry <0 || rx >= N || ry >= N) {
								rx = rx-dx[d]; ry = ry-dy[d];
								break;
							}
							if(copy[rx][ry]!=0) {
								rx = rx-dx[d]; ry = ry-dy[d];
								break;
							}
						}
						copy[rx][ry]= temp;
					}
				}
			}
		}
		else if(d==1) { //아럐쪽으로
			for(int a=0;a<N;a++) {
				for(int b=N-1;b>=0;b--) {
					if(copy[b][a]!=0) {
						int rx = b, ry = a;
						int temp = copy[rx][ry];
						copy[rx][ry]=0;
						while(true) {
							rx = rx+dx[d];
							ry = ry+dy[d];
							if(rx < 0 || ry <0 || rx >= N || ry >= N) {
								rx = rx-dx[d]; ry = ry-dy[d];
								break;
							}
							if(copy[rx][ry]!=0) {
								rx = rx-dx[d]; ry = ry-dy[d];
								break;
							}
						}
						copy[rx][ry]= temp;
					}
				}
			}
		}
		else if(d==2) { //왼쪽으로
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					if(copy[a][b]!=0) {
						int rx = a, ry = b;
						int temp = copy[rx][ry];
						copy[rx][ry]=0;
						while(true) {
							rx = rx+dx[d];
							ry = ry+dy[d];
							if(rx < 0 || ry <0 || rx >= N || ry >= N) {
								rx = rx-dx[d]; ry = ry-dy[d];
								break;
							}
							if(copy[rx][ry]!=0) {
								rx = rx-dx[d]; ry = ry-dy[d];
								break;
							}
						}
						copy[rx][ry]= temp;
					}
				}
			}
		}
		else if(d==3) {//오른쪽으로
			for(int a=0;a<N;a++) {
				for(int b=N-1;b>=0;b--) {
					if(copy[a][b]!=0) {
						int rx = a, ry = b;
						int temp = copy[rx][ry];
						copy[rx][ry]=0;
						while(true) {
							rx = rx+dx[d];
							ry = ry+dy[d];
							if(rx < 0 || ry <0 || rx >= N || ry >= N) {
								rx = rx-dx[d]; ry = ry-dy[d];
								break;
							}
							if(copy[rx][ry]!=0) {
								rx = rx-dx[d]; ry = ry-dy[d];
								break;
							}
						}
						copy[rx][ry]= temp;
					}
				}
			}
		}
	}
	public static void init() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				copy[a][b] = map[a][b];
			}
		}
	}
	public static int check_max() {
		int t_max=0;
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(copy[a][b]>t_max) {
					t_max = copy[a][b];
				}
			}
		}
		return t_max;
	}
	public static void dfs(int level) {
		if(level ==5) {
			init();
			for(int a=0;a<5;a++) {
				move(visit[a]);
				check_sum(visit[a]);
				move(visit[a]);
			}
			ans = Math.max(ans, check_max());
			return;
		}
		for(int a=0;a<4;a++) {
			visit[level] = a;
			dfs(level+1);
		}
	}
}