package 삼성공채준비합격하자;

import java.util.Scanner;
public class 시험감독_13458 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		int test[] = new int[N];
		for(int a=0;a<test.length;a++) {
			test[a] = sc.nextInt();
		}
		int B = sc.nextInt();
		int C = sc.nextInt();
		long total =0;
		for(int a=0;a<test.length;a++) {
			if((test[a]-B)<=0) {
				total++;
			}
			else {
				total++;
				test[a] = test[a]-B;
				total = total+(test[a]/C);
				if((test[a]%C)>0) {
					total++;
				}
			}
		}
		System.out.println(total);
	}
}
