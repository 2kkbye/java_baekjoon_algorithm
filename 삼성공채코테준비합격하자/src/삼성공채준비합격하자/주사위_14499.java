package 삼성공채준비합격하자;

import java.util.ArrayList;
import java.util.Scanner;
public class 주사위_14499 {

	static int dx[] = {0,0,-1,1};
	static int dy[] = {1,-1,0,0};

	static int map[][];
	static ArrayList<Integer> list = new ArrayList<Integer>();
	static ArrayList<Integer> ins = new ArrayList<Integer>();
	static int R,C;
	static int X,Y;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		R = sc.nextInt();
		C = sc.nextInt();
		X = sc.nextInt();
		Y = sc.nextInt();
		int N = sc.nextInt();
		map = new int[R][C];
		
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				map[a][b] = sc.nextInt();
			}
		}
		for(int a=0;a<6;a++) {
			list.add(0);
		}
		check(X,Y);
		
		for(int a=0;a<N;a++) {
			ins.add(sc.nextInt());
		}
		int rx = X;
		int ry = Y;
		for(int a=0;a<N;a++) {
			rx = rx+dx[(ins.get(a)-1)];
			ry = ry+dy[(ins.get(a)-1)];
			if(rx >=0 && ry >=0 && rx < R && ry < C) {
				move((ins.get(a)-1));
				check(rx,ry);
				System.out.println(list.get(3));
			}
			else {
				rx = rx - dx[ins.get(a)-1];
				ry = ry - dy[ins.get(a)-1];
			}
		}
		sc.close();
	}
	public static void move(int sw) {
		if(sw==0) { //우(동)
			int temp1 = list.get(5);
			int temp2= list.get(4);
			int temp3= list.get(3);
			int temp4 = list.get(1);
			list.remove(5);list.remove(4);list.remove(3);list.remove(1);
			list.add(1, temp1);list.add(temp2);list.add(temp4);list.add(temp3);
			//System.out.println(list);
		}
		else if(sw==1) {//좌(서)
			int temp1 = list.get(5);
			int temp2= list.get(4);
			int temp3= list.get(3);
			int temp4 = list.get(1);
			list.remove(5);list.remove(4);list.remove(3);list.remove(1);
			list.add(1, temp2);list.add(temp1);list.add(temp3);list.add(temp4);
			//System.out.println(list);
		}
		else if(sw==2) { //상(북)
			int temp = list.get(3);
			list.remove(3);
			list.add(0, temp);
			//System.out.println(list);
		}
		else if(sw==3) { //하(남)
			int temp = list.get(0);
			list.remove(0);
			list.add(3, temp);
			//System.out.println(list);
		}
	}
	public static void check(int x, int y) {
		if(map[x][y]==0) {
			int temp = list.get(1);
			map[x][y]= temp;
		}
		else {
			int temp = map[x][y];
			map[x][y]=0;
			list.remove(1);
			list.add(1, temp);
		}
	}
}