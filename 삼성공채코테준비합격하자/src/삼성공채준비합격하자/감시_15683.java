package 삼성공채준비합격하자;

import java.util.Scanner;
import java.awt.Point;
import java.util.ArrayList;
public class 감시_15683 {

	static int R,C;
	static int map[][];
	static int cctv[][];
	static int remove[][];
	static int dir[]= {0,4,2,4,4,1};
	static int result;
	static ArrayList<Point> l_cctv = new ArrayList<Point>();
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		R = sc.nextInt();
		C = sc.nextInt();
		map = new int[R][C];
		cctv = new int[R][C];
		remove = new int[R][C];
		int aa=0;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				int temp = sc.nextInt();
				if(temp ==0) continue;
				if(temp ==6) {
					aa++;
					cctv[a][b]=-1;
					continue;
				}
				cctv[a][b]=temp;
				l_cctv.add(new Point(a,b));
			}
		}
		if(l_cctv.size()==0) {
			System.out.println((R*C)-aa);
		}
		else {
			int rx = l_cctv.get(0).x;
			int ry = l_cctv.get(0).y;
			int temp = dir[cctv[rx][ry]];
			result = Integer.MAX_VALUE;
			for(int a=0;a<temp;a++) {
				write(rx,ry,cctv[rx][ry],a);
				//print();
				dfs(1);
				back(rx,ry,cctv[rx][ry],a);
				//print();
			}
			System.out.println(result);
		}

	}
	public static void check() {
		int cnt =0;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(map[a][b]==7) continue;
				if(cctv[a][b]!=0) continue;
				cnt++;
			}
		}
		if(cnt < result) {
			result = cnt;
		}
		//System.out.println(cnt);
	}
	public static void dfs(int level) {
		if(level == l_cctv.size()) {
			//print();
			check();
			return;
		}
		int rx = l_cctv.get(level).x;
		int ry = l_cctv.get(level).y;
		int temp = dir[cctv[rx][ry]];
		for(int a=0;a<temp;a++) {
			write(rx,ry,cctv[rx][ry],a);
			//print();
			dfs(level+1);
			back(rx,ry,cctv[rx][ry],a);
			//print();
		}
	}
	public static void print() {
		System.out.println("------------------------");
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				System.out.print(map[a][b]+" ");
			}
			System.out.println();
		}
	}
	public static void write(int x,int y,int c_num, int sw) {
		if(c_num == 1) {
			if(sw==0) {
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1 ) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
			}
			else if(sw==1) {
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1 ) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
			}
			else if(sw==2) {
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
			}
			else if(sw==3) {
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1 ) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
			}
		}
		else if(c_num ==2) {
			if(sw==0) {
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1 ) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
			}
			else if(sw==1) {
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1 ) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1 ) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
			}
		}
		else if(c_num ==3) {
			if(sw==0) {
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1 ) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
			}
			else if(sw==1) {
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1 ) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
			}
			else if(sw==2) {
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1 ) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
			}
			else if(sw==3) {
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1 ) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
			}
		}
		else if(c_num ==4) {
			if(sw==0) {
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1 ) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1 ) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
			}
			else if(sw==1) {
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1 ) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
			}
			else if(sw==2) {
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1 ) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}

			}
			else if(sw==3) {
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1) {
						map[x][a]=7;
						remove[x][a]++;
					}
					else break;
				}
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1) {
						map[a][y]=7;
						remove[a][y]++;
					}
					else break;
				}
			}
		}
		else if(c_num ==5) {
			for(int a=x+1;a<R;a++) {
				if(cctv[a][y]!=-1) {
					map[a][y]=7;
					remove[a][y]++;
				}
				else break;
			}
			for(int a=x-1;a>=0;a--) {
				if(cctv[a][y]!=-1) {
					map[a][y]=7;
					remove[a][y]++;
				}
				else break;
			}
			for(int a=y+1;a<C;a++) {
				if(cctv[x][a]!=-1) {
					map[x][a]=7;
					remove[x][a]++;
				}
				else break;
			}
			for(int a=y-1;a>=0;a--) {
				if(cctv[x][a]!=-1) {
					map[x][a]=7;
					remove[x][a]++;
				}
				else break;
			}
		}
	}
	public static void back(int x,int y,int c_num, int sw) {
		if(c_num == 1) {
			if(sw==0) {
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							remove[x][a]=0;
							map[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
			}
			else if(sw==1) {
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
			}
			else if(sw==2) {
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
			}
			else if(sw==3) {
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
			}
		}
		else if(c_num ==2) {
			if(sw==0) {
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
			}
			else if(sw==1) {
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
			}
		}
		else if(c_num ==3) {
			if(sw==0) {
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
			}
			else if(sw==1) {
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
			}
			else if(sw==2) {
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
			}
			else if(sw==3) {
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
			}
		}
		else if(c_num ==4) {
			if(sw==0) {
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
			}
			else if(sw==1) {
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
			}
			else if(sw==2) {
				for(int a=y+1;a<C;a++) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}

			}
			else if(sw==3) {
				for(int a=x+1;a<R;a++) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
				for(int a=y-1;a>=0;a--) {
					if(cctv[x][a]!=-1) {
						if(remove[x][a]>0 && remove[x][a]==1) {
							map[x][a]=0;
							remove[x][a]=0;
						}
						else if(remove[x][a]>1) {
							remove[x][a]--;
						}
					}
					else break;
				}
				for(int a=x-1;a>=0;a--) {
					if(cctv[a][y]!=-1) {
						if(remove[a][y] >0 && remove[a][y]==1) {
							map[a][y]=0;
							remove[a][y]=0;
						}
						else if(remove[a][y]>1) {
							remove[a][y]--;
						}
					}
					else break;
				}
			}
		}
		else if(c_num ==5) {
			for(int a=x+1;a<R;a++) {
				if(cctv[a][y]!=-1) {
					if(remove[a][y] >0 && remove[a][y]==1) {
						map[a][y]=0;
						remove[a][y]=0;
					}
					else if(remove[a][y]>1) {
						remove[a][y]--;
					}
				}
				else break;
			}
			for(int a=x-1;a>=0;a--) {
				if(cctv[a][y]!=-1) {
					if(remove[a][y] >0 && remove[a][y]==1) {
						map[a][y]=0;
						remove[a][y]=0;
					}
					else if(remove[a][y]>1) {
						remove[a][y]--;
					}
				}
				else break;
			}
			for(int a=y+1;a<C;a++) {
				if(cctv[x][a]!=-1) {
					if(remove[x][a]>0 && remove[x][a]==1) {
						map[x][a]=0;
						remove[x][a]=0;
					}
					else if(remove[x][a]>1) {
						remove[x][a]--;
					}
				}
				else break;
			}
			for(int a=y-1;a>=0;a--) {
				if(cctv[x][a]!=-1) {
					if(remove[x][a]>0 && remove[x][a]==1) {
						map[x][a]=0;
						remove[x][a]=0;
					}
					else if(remove[x][a]>1) {
						remove[x][a]--;
					}
				}
				else break;
			}
		}
	}
}