package 삼성공채준비합격하자;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class c_tree {
	int x,y,age;
	public c_tree(int x,int y,int age) {
		this.x = x;
		this.y = y;
		this.age = age;
	}
}
//틀린 문제알고리즘  잘못된 알고리즘////////////////////////////////////////////////////////
public class 나무재테크_16235_틀린알고리즘 {
	static int dx[] = {-1,-1,-1,0,1,1,1,0};
	static int dy[] = {-1,0,1,1,1,0,-1,-1};

	static int resource[][]; //초기 양분
	static int add_resource[][]; //겨울마다 추가 양분

	static ArrayList<c_tree> l_tree = new ArrayList<c_tree>();
	static ArrayList<c_tree> l_temp = new ArrayList<c_tree>();
	static ArrayList<c_tree> l_index = new ArrayList<c_tree>();
	static int N,M,K;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		M = sc.nextInt();
		K = sc.nextInt();
		add_resource = new int[N][N];
		resource = new int[N][N];
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				int temp = sc.nextInt();
				resource[a][b] = 5;
				add_resource[a][b] =temp;
			}
		}
		for(int a=0;a<M;a++) {
			int rx = sc.nextInt();
			int ry = sc.nextInt();
			int rage = sc.nextInt();
			l_tree.add(new c_tree(rx-1, ry-1, rage));
		}
		for(int a=0;a<K;a++) {
			spring();
			check_remove();
			summer();
			fall();
			winter();
		}
		System.out.println(l_tree.size());
	}
	public static void winter() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				resource[a][b] = resource[a][b] + add_resource[a][b];
			}
		}
	}
	public static void fall() {
		int temp = l_tree.size();
		for(int a=0;a<temp;a++) {
			if((l_tree.get(a).age%5)==0) {
				int rx = l_tree.get(a).x;
				int ry = l_tree.get(a).y;
				for(int b=0;b<8;b++) {
					int px = rx+dx[b];
					int py = ry+dy[b];
					if(px >= 0 && py >= 0 && px < N && py < N) {
						l_tree.add(new c_tree(px, py, 1));
					}
				}
			}
		}
	}
	public static void summer() {
		for(int a=0;a<l_index.size();a++) {
			int rx = l_index.get(a).x;
			int ry = l_index.get(a).y;
			int age = l_index.get(a).age;
			resource[rx][ry] = resource[rx][ry]+(age/2);
		}
		l_index = new ArrayList<c_tree>();
	}
	public static void eating_resource(ArrayList<c_tree> list) {
		for(int a=0;a<list.size();a++) {
			int rx = list.get(a).x;
			int ry = list.get(a).y;
			int age = list.get(a).age;
			if(resource[rx][ry]>=age) {
				resource[rx][ry]=resource[rx][ry]-age;
				age++;
				list.get(a).age=age;
			}
			else {
				l_index.add(list.get(a));
			}
		}
	}
	public static void check_remove() {
		ArrayList<Integer> l_death = new ArrayList<Integer>();
		if(l_index.size()==0) {
			return;
		}
		for(int a=0;a<l_index.size();a++) {
			for(int b=0;b<l_tree.size();b++) {
				if((l_index.get(a).x==l_tree.get(b).x)&&(l_index.get(a).y==l_tree.get(b).y)&&(l_index.get(a).age==l_tree.get(b).age)) {
					l_death.add(b);
					break;
				}
			}
		}
		if(l_death.size()!=0) {
			Collections.sort(l_death);
			for(int a=l_death.size()-1;a>=0;a--) {
				int temp = l_death.get(a);
				l_tree.remove(temp);
			}
		}
	}
	public static void sort(ArrayList<c_tree> list) { // 나이 작은게 맨앞으로 온다.
		list.sort(new Comparator<c_tree>() {
			@Override
			public int compare(c_tree arg0, c_tree arg1) {
				// TODO Auto-generated method stub
				int age_1 = arg0.age;
				int age_2= arg1.age;

				if (age_1 == age_2)
					return 0;
				else if (age_1 > age_2)
					return 1;
				else
					return -1;
			}
		});
	}
	public static void spring() {
		l_index = new ArrayList<c_tree>();
		ArrayList<Integer> l_te = new ArrayList<Integer>();
		if(l_tree.size()==1) {
			eating_resource(l_tree);
			return;
		}
		for(int a=0;a<l_tree.size();a++) {
			l_temp = new ArrayList<c_tree>();
			l_temp.add(l_tree.get(a));
			if(!check(l_te,a)) {
				continue;
			}
			for(int b=a+1;b<l_tree.size();b++) {
				if((l_tree.get(a).x == l_tree.get(b).x) && (l_tree.get(a).y == l_tree.get(b).y)) {
					l_te.add(b);
					l_temp.add(l_tree.get(b));
				}
			}
			if(l_temp.size()>1) {
				sort(l_temp);
				eating_resource(l_temp);
			}
			else {
				eating_resource(l_temp);
			}
		}
	}
	public static boolean check(ArrayList<Integer> list,int x) {
		for(int a=0;a<list.size();a++) {
			if(list.get(a)==x) {
				return false;
			}
		}
		return true;
	}
}