package 삼성공채준비합격하자;

import java.util.Scanner;
import java.util.LinkedList;
import java.util.Queue;
import java.awt.Point;
public class 치킨배달_15686 {

	static int dx[]= {0,0,-1,1};
	static int dy[]= {1,-1,0,0};
	static int N,M;
	static LinkedList<Point> l_chicken = new LinkedList<Point>();
	static LinkedList<Point> l_home = new LinkedList<Point>();
	static LinkedList<Point> check_chicken = new LinkedList<Point>();
	static int chicken[][];
	static int home[][];
	static int map[][];
	static boolean visit[][];
	static int result;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		M = sc.nextInt();
		visit = new boolean[N][N];
		chicken = new int[N][N];
		home = new int[N][N];
		visit = new boolean[N][N];
		map = new int[N][N];
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				int temp = sc.nextInt();
				if(temp==0) continue;
				if(temp == 1) { //집
					home[a][b]=1;
					l_home.add(new Point(a,b));
					continue;
				}
				if(temp ==2) { //치킨집
					chicken[a][b]=2;
					l_chicken.add(new Point(a,b));
				}
			}
		}
		result = Integer.MAX_VALUE;
		for(int a=0;a<=l_chicken.size()-M;a++) {
			int rx = l_chicken.get(a).x;
			int ry = l_chicken.get(a).y;
			check_chicken.add(new Point(rx,ry));
			dfs(a,1);
			check_chicken.remove(0);
		}
		System.out.println(result);
	}
	public static void print() {
		System.out.println("-------------------------------");
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				System.out.print(map[a][b]+" ");
			}
			System.out.println();
		}
	}
	public static void check_dir() {
		int cnt =0;
		for(int a=0;a<l_home.size();a++) {
			int rx = l_home.get(a).x;
			int ry = l_home.get(a).y;
			cnt = cnt+map[rx][ry];
		}
		if(cnt < result) {
			result = cnt;
		}
		//System.out.println(cnt);
	}
	public static void bfs() {
		Queue<Point> qu = new LinkedList<Point>();
		for(int i=0;i<M;i++) {
			int qx=check_chicken.get(i).x;
			int qy=check_chicken.get(i).y;
			visit[qx][qy]=true;
			qu.add(new Point(qx,qy));
		}
		
		while(!qu.isEmpty()) {
			int rx = qu.peek().x;
			int ry = qu.peek().y;
			qu.remove();
			for(int a=0;a<4;a++) {
				int px = rx+dx[a];
				int py = ry+dy[a];
				if(px >= 0 && py >= 0 && px < N && py < N) {
					if(visit[px][py]==false) {
						map[px][py] = map[rx][ry]+1;
						qu.add(new Point(px,py));
						visit[px][py]=true;
					}
				}
			}
		}
		//print();
		check_dir();

	}
	public static void dfs(int x,int level) {
		if(level==M) {
			bfs();
			map = new int[N][N];
			visit = new boolean[N][N];
			return;
		}
		for(int a=x+1;a<=(l_chicken.size()-M)+level;a++) {
			int rx = l_chicken.get(a).x;
			int ry = l_chicken.get(a).y;
			check_chicken.add(new Point(rx,ry));
			dfs(a,level+1);
			check_chicken.remove(level);
		}
	}
}