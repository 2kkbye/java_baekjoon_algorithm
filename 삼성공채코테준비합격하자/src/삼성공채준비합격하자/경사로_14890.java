package 삼성공채준비합격하자;

import java.util.Scanner;

public class 경사로_14890 {

	static boolean visit[][];
	static int map[][];
	static int R,L;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		R = sc.nextInt();
		L = sc.nextInt();
		map = new int[R][R];
		visit = new boolean[R][R];
		for(int a=0;a<R;a++) {
			for(int b=0;b<R;b++) {
				map[a][b] = sc.nextInt();
			}
		}
		int result_cnt=0;
		int temp;
		for(int a=0;a<R;a++) {
			temp = map[a][0];
			for(int b=1;b<R;b++) {
				if((temp < (map[a][b]-1)) || (temp > (map[a][b]+1))) {
					result_cnt++;
					break;
				}
				if(temp == map[a][b]) continue;
				if(temp < map[a][b]) {
					if(back(a,b)==false) {
						result_cnt++;
						break;
					}
					else {
						temp = map[a][b];
					}
					continue;
				}
				if(temp > map[a][b]) {
					if(front(a,b)==false) {
						result_cnt++;
						break;
					}
					else {
						temp = map[a][b];
					}
					continue;
				}
			}	
		}
		visit = new boolean[R][R];
		for(int a=0;a<R;a++) {
			temp = map[0][a];
			for(int b=1;b<R;b++) {
				if((temp < (map[b][a]-1)) || (temp > (map[b][a]+1))) {
					result_cnt++;
					break;
				}
				if(temp == map[b][a]) continue;
				if(temp < map[b][a]) { //구현하기
					if(row_back(b,a)==false) {
						result_cnt++;
						break;
					}
					else {
						temp = map[b][a];
					}
					continue;
				}
				if(temp > map[b][a]) { //구현하기
					if(row_front(b,a)==false) {
						result_cnt++;
						break;
					}
					else {
						temp = map[b][a];
					}
					continue;
				}
			}
		}
		System.out.println(((R*2)-result_cnt));
	}
	public static boolean row_back(int index,int row) { //index가 행 //row가 열
		int result = index - L;
		if(result < 0) {
			return false;
		}
		int temp=0;
		for(int a=index-1;a>=result;a--) {
			if(visit[a][row]==false) {
				if(a==index-1) {
					temp = map[index-1][row];
					visit[index-1][row]=true;
					continue;
				}
				if(a!=index-1) {
					if(temp!=map[a][row]) {
						return false;
					}
				}
				visit[a][row]=true;
			}
			else {
				return false;
			}
		}
		return true;
	}
	public static boolean row_front(int index,int row) {
		int result = index +(L-1);
		if(result >= R) {
			return false;
		}
		int temp =0;
		for(int a=index;a<index+L;a++) {
			if(visit[a][row]==false) {
				if(a==index) {
					temp = map[a][row];
					visit[a][row] = true;
					continue;
				}
				if(temp!=map[a][row]){
					return false;
				}
				visit[a][row]=true;
			}
			else {
				return false;
			}
		}
		return true;
	}
	public static boolean front(int row,int index) {
		int result = index +(L-1);
		if(result >= R) {
			return false;
		}
		int temp =0;
		for(int a=index;a<index+L;a++) {
			if(visit[row][a]==false) {
				if(a==index) {
					temp = map[row][a];
					visit[row][a] = true;
					continue;
				}
				if(temp!=map[row][a]){
					return false;
				}
				visit[row][a]=true;
			}
			else {
				return false;
			}
		}
		return true;
	}
	public static boolean back(int row,int index) {
		int result = index - L;
		if(result < 0) {
			return false;
		}
		int temp=0;
		for(int a=index-1;a>=result;a--) {
			if(visit[row][a]==false) {
				if(a==index-1) {
					temp = map[row][index-1];
					visit[row][index-1]=true;
					continue;
				}
				if(a!=index-1) {
					if(temp!=map[row][a]) {
						return false;
					}
				}
				visit[row][a]=true;
			}
			else {
				return false;
			}
		}
		return true;
	}
}