package �Ｚ��ä�غ��հ�����;

import java.util.Scanner;
import java.awt.Point;
import java.util.Comparator;
import java.util.ArrayList;

public class ����_4485 {

	static int dx[]= {0,0,-1,1};
	static int dy[]= {-1,1,0,0};
	static boolean visit[][];
	static int gul[][];
	static int N;
	static ArrayList<Point> list; 
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N =1;
		int cnt=1;
		while(N!=0) {
			N = sc.nextInt();
			if(N!=0) {
				visit = new boolean[125][125];
				gul = new int[125][125];
				list = new ArrayList<Point>();
				for(int a=0;a<N;a++) {
					for(int b=0;b<N;b++) {
						gul[a][b]= sc.nextInt();
					}
				}
				list.add(new Point(0,0));
				visit[0][0]=true;
				while(!list.isEmpty()) {
					print();
					sort();
					int rx = list.get(0).x;
					int ry = list.get(0).y;
					list.remove(0);
					for(int a=0;a<4;a++) {
						int px = rx + dx[a];
						int py = ry + dy[a];
						if(px >= 0 && py >= 0 && px < N && py < N) {
							if(visit[px][py]==false) {
								visit[px][py]=true;
								list.add(new Point(px,py));
								gul[px][py]= gul[px][py]+gul[rx][ry];
							}
						}
					}
				}
				System.out.println("Problem "+cnt+":"+" "+gul[N-1][N-1]);
				cnt++;
			}
			else {
				break;
			}
		}
	}
	public static void print() {
		System.out.println("--------------------");
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				System.out.print(gul[a][b]+" ");
			}
			System.out.println();
		}
		System.out.println("--------------------");
	}
	public static void sort() {
		list.sort(new Comparator<Point>() {
			@Override
			public int compare(Point arg0, Point arg1) {
				// TODO Auto-generated method stub
				int first_x = arg0.x;
				int first_y= arg0.y;
				int second_x=arg1.x;
				int second_y=arg1.y;

				if (gul[first_x][first_y] == gul[second_x][second_y])
					return 0;
				else if (gul[first_x][first_y] > gul[second_x][second_y])
					return 1;
				else
					return -1;
			}
		});
	}
}