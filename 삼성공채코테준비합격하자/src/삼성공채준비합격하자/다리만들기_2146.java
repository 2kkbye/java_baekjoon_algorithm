package 삼성공채준비합격하자;

import java.util.LinkedList;
import java.util.Scanner;
import java.util.Queue;
import java.awt.Point;
public class 다리만들기_2146 {
	static int dx[]= {0,0,-1,1};
	static int dy[]= {1,-1,0,0};
	static int tem;

	static Queue<Point> q_spread = new LinkedList<Point>();
	static Queue<Point> q_island = new LinkedList<Point>();
	static boolean visit[][];
	static int island[][];
	static int temp[][];
	static int N;
	static int min;
	static int check_x,check_y;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();

		island = new int[N][N];
		visit = new boolean[N][N];
		temp = new int[N][N];

		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				island[a][b]= sc.nextInt();
			}
		}
		tem =-1;
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(island[a][b]==1 && temp[a][b]==0) {
					q_spread.add(new Point(a,b));
					temp[a][b]=tem;
					spread_bfs();
					tem--;
				}
			}
		}
		min = Integer.MAX_VALUE;
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(temp[a][b]!=0) continue;
				if(!check(a,b)) continue;
				visit = new boolean[N][N];
				visit[a][b]=true;
				q_island.add(new Point(a,b));
				bfs();
			}
		}
		System.out.println(min+1);
	}
	public static void bfs() {
		while(!q_island.isEmpty()) {
			//print();
			int px = q_island.peek().x;
			int py = q_island.peek().y;
			q_island.remove();
			for(int a=0;a<4;a++) {
				int rx = px + dx[a];
				int ry = py + dy[a];
				if( rx >= 0 && ry >= 0 && rx < N && ry < N) {
					if(temp[rx][ry]<0 && temp[rx][ry]!=temp[check_x][check_y]) {
						if(min > temp[px][py]) {
							min = temp[px][py];	
						}
					}
					if(visit[rx][ry]==false && temp[rx][ry]==0) {
						q_island.add(new Point(rx,ry));
						temp[rx][ry]=temp[px][py]+1;
						visit[rx][ry]=true;
						
					}
				}
			}
			temp[px][py]=0;
		}
	}
	public static boolean check(int x, int y) {
		for(int a=0;a<4;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx >=0 && ry >=0 && rx < N && ry < N) {
				if(temp[rx][ry]!=0){
					check_x=rx;
					check_y=ry;
					return true;
				}
			}
		}
		return false;
	}
	public static void print() {
		System.out.println("--------------------------");
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				System.out.print(temp[a][b]+" ");
			}
			System.out.println();
		}
		System.out.println("-------------------------");
	}
	public static void spread_bfs() {
		while(!q_spread.isEmpty()) {
			int px = q_spread.peek().x;
			int py = q_spread.peek().y;
			q_spread.remove();
			for(int a=0;a<4;a++) {
				int rx = px + dx[a];
				int ry = py + dy[a];
				if( rx >= 0 && ry >= 0 && rx < N && ry < N) {
					if(temp[rx][ry] ==0 && island[rx][ry]==1) {
						q_spread.add(new Point(rx,ry));
						temp[rx][ry]=tem;
					}
				}
			}
		}
	}
}
