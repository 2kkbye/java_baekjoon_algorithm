package �Ｚ��ä�غ��հ�����;

import java.util.Scanner;
class ball{
	int rx, ry, bx,by;
	public ball(int rx, int ry,int bx,int by) {
		this.rx = rx;
		this.ry = ry;
		this.bx = bx;
		this.by = by;
	}
}
public class ����Ż��2_13460 {
	static int dx[]= {-1,0,1,0}; //�������
	static int dy[]= {0,1,0,-1};

	static int map[][];
	static int copy[][];
	static int N,M;
	static ball chogi;
	static ball temp;
	static int visit[] = new int[10];
	static int ans;
	static boolean result;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		M = sc.nextInt();
		map = new int[N][M];
		copy = new int[N][M];
		chogi = new ball(0, 0, 0, 0);
		temp = new ball(0, 0, 0, 0);
		for(int a=0;a<N;a++) {
			String str = sc.next();
			for(int b=0;b<M;b++) {
				if(str.charAt(b)=='#') { //��
					map[a][b]=-2;
				}
				else if(str.charAt(b)=='.') {
					continue;
				}
				else if(str.charAt(b)=='R') {
					map[a][b] = 1;
					chogi.rx=a;chogi.ry = b;
				}
				else if(str.charAt(b)=='O') {
					map[a][b]=-1;
				}
				else if(str.charAt(b)=='B') {
					map[a][b] = 2;
					chogi.bx=a;chogi.by=b;
				}
			}
		}
		ans = 11;
		dfs(0,-1);
		if(ans==11) {
			System.out.println(-1);
		}
		else {
			System.out.println(ans);
		}
	}
	public static void init() {
		result = false;
		temp.rx = chogi.rx;
		temp.ry = chogi.ry;
		temp.bx = chogi.bx;
		temp.by = chogi.by;
		for(int a=0;a<N;a++) {
			for(int b=0;b<M;b++) {
				copy[a][b] = map[a][b];
			}
		}
	}
	public static boolean check(int d) {
		boolean flagR=false, flagB=false, flagRB=false, flagBR=false;
		int rx = temp.rx, ry = temp.ry,bx = temp.bx, by = temp.by;
		while(true) {
			rx = rx+dx[d];
			ry = ry+dy[d];
			if(copy[rx][ry] !=0) {
				if(copy[rx][ry]==-1) {
					flagR = true;
					break;
				}
				else if(copy[rx][ry]==2) {
					flagRB = true;
					continue;
				}
				rx = rx-dx[d];
				ry = ry-dy[d];
				break;
			}
		}
		if(flagRB) {
			rx = rx-dx[d];
			ry = ry-dy[d];
		}
		while(true) {
			bx = bx+dx[d];
			by = by+dy[d];
			if(copy[bx][by]!=0) {
				if(copy[bx][by]==-1) {
					flagB = true;
					break;
				}
				else if(copy[bx][by]==1) {
					flagBR = true;
					continue;
				}
				bx = bx-dx[d];
				by = by-dy[d];
				break;
			}
		}
		if(flagB) {
			result = true;
			return false;
		}
		if(flagR) {
			return true;
		}
		if(!flagRB && flagBR) {
			bx = bx-dx[d];
			by = by-dy[d];
		}
		copy[temp.rx][temp.ry]=0;
		copy[rx][ry] = 1;
		temp.rx = rx;temp.ry = ry;
		copy[temp.bx][temp.by]=0;
		copy[bx][by] = 2;
		temp.bx = bx;temp.by = by;
		return false;
	}
	public static void dfs(int level, int pre_index) {
		if(level ==10) {
			init();
			for(int a=0;a<10;a++) {
				if(check(visit[a])) {
					ans = Math.min(ans, a+1);
					break;
				}
				if(result) {
					break;
				}
			}
			return;
		}
		for(int a=0;a<4;a++) {
			if(a==pre_index) continue;
			visit[level]=a;
			dfs(level+1,a);
		}
	}
}