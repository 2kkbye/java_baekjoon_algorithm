package 삼성공채준비합격하자;

import java.util.Scanner;
import java.awt.Point;
import java.util.Queue;
import java.util.LinkedList;

public class 불_5427 {
	static int dx[] = {0,0,-1,1};
	static int dy[] = {1,-1,0,0};
	static boolean visit[][];
	static boolean fire_visit[][];
	static int fire[][];
	static int go[][];
	static int W,H;

	static Queue<Point> q_fire;
	static Queue<Point> q_go;
	static int ansX,ansY;
	static int firX,firY;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			W = sc.nextInt();//열
			H = sc.nextInt();//행
			fire = new int[1001][1001];
			go = new int[1001][1001];
			visit = new boolean[1001][1001];
			fire_visit = new boolean[1001][1001];
			q_go = new LinkedList<Point>();
			q_fire = new LinkedList<Point>();

			String str[][] = new String[H][1];
			for(int a=0;a<str.length;a++) {
				str[a][0] = sc.next();
			}
			for(int a=0;a<H;a++) {
				for(int b=0;b<W;b++) {
					if(str[a][0].charAt(b)=='.') { //바닥
						continue;
					}
					else if(str[a][0].charAt(b)=='#') {
						fire[a][b]=-2;
						go[a][b]=-2;
					}
					else if(str[a][0].charAt(b)=='@') {
						q_go.add(new Point(a,b));
						visit[a][b]= true;
						ansX = a;ansY=b;
					}
					else if(str[a][0].charAt(b)=='*') {
						q_fire.add(new Point(a,b));
						fire_visit[a][b] = true;
					}
				}
			}
			fire_sp();
			go();
			int min = Integer.MAX_VALUE;
			for(int a=0;a<H;a++) {
				if(a==0 || a== (H-1)) {
					for(int b=0;b<W;b++) {
						if(a==ansX && b==ansY) {
							min =0;
						}
						if(go[a][b]<=0) {
							continue;
						}
						if(min > go[a][b]) {
							min = go[a][b];
						}
					}
				}
				else {
					if((a==ansX && 0==ansY) ||(ansX ==a && ansY == W-1)) {
						min =0;
					}
					if(go[a][0]>0) {
						if(go[a][0] < min) {
							min = go[a][0];
						}
					}
					if(go[a][W-1]>0) {
						if(go[a][W-1] < min) {
							min = go[a][W-1];
						}
					}
					
				}
			}
			if(min != Integer.MAX_VALUE) {
				System.out.println(min+1);
			}
			else System.out.println("IMPOSSIBLE");
			
		}
	}
	public static void go() {
		while(!q_go.isEmpty()) {
			int rx = q_go.peek().x;
			int ry = q_go.peek().y;
			q_go.remove();
			for(int a=0;a<4;a++) {
				int px = rx+dx[a];
				int py = ry+dy[a];
				if(px >= 0 && py >= 0 && px < H && py < W) {
					if(go[px][py]==0 && visit[px][py] == false) {
						if((fire[px][py]-go[rx][ry])>=2 || fire_visit[px][py]==false) {
							q_go.add(new Point(px,py));
							go[px][py] = go[rx][ry]+1;
							visit[px][py] = true;
						}
					}
				}
			}
		}
	}
	public static void fire_sp() {
		while(!q_fire.isEmpty()) {
			int rx = q_fire.peek().x;
			int ry = q_fire.peek().y;
			q_fire.remove();
			for(int a=0;a<4;a++) {
				int px = rx+dx[a];
				int py = ry+dy[a];
				if(px >= 0 && py >= 0 && px < H && py < W) {
					if(fire[px][py]==0 && fire_visit[px][py]==false) {
						q_fire.add(new Point(px,py));
						fire[px][py] = fire[rx][ry]+1;
						fire_visit[px][py] = true;
					}
				}
			}
		}
	}

}
