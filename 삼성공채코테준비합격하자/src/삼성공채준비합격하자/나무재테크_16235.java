package 삼성공채준비합격하자;

import java.util.Scanner;
import java.util.Queue;
import java.util.Collections;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Comparator;

class tree {
	int x,y,age;
	public tree(int x, int y, int age) {
		this.x = x;
		this.y = y;
		this.age = age;
	}
}
public class 나무재테크_16235 {
	static int dx[] = {-1,-1,-1,0,1,1,1,0};
	static int dy[] = {-1,0,1,1,1,0,-1,-1};

	static Queue<tree> new_tree = new LinkedList<tree>();
	static Queue<tree> this_tree = new LinkedList<tree>();
	static Queue<tree> death_tree = new LinkedList<tree>();
	static Queue<tree> spread_tree = new LinkedList<tree>();
	static Queue<tree> next_tree = new LinkedList<tree>();
	static int add[][];
	static int map[][];

	static int N,M,K;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<tree> first_tree = new ArrayList<tree>();

		N = sc.nextInt();
		M = sc.nextInt();
		K = sc.nextInt();
		map = new int[N][N];
		add = new int[N][N];

		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				add[a][b] = sc.nextInt();
				map[a][b] = 5;
			}
		}
		for(int a=0;a<M;a++) {
			int x= sc.nextInt();
			int y = sc.nextInt();
			int age = sc.nextInt();
			first_tree.add(new tree(x-1, y-1, age));
		}
		sort(first_tree);
		for(int a=0;a<first_tree.size();a++) {
			this_tree.add(new tree(first_tree.get(a).x, first_tree.get(a).y, first_tree.get(a).age));
		}
		for(int a=0;a<K;a++) {
			spring();
			summer();
			fall();
			winter();
			copy();
		}
		System.out.println((this_tree.size()+new_tree.size()));
	}
	public static void copy() {
		while(!next_tree.isEmpty()) {
			int x= next_tree.peek().x;
			int y= next_tree.peek().y;
			int age = next_tree.peek().age;
			next_tree.remove();
			this_tree.add(new tree(x, y, age));
		}
	}
	public static void spring() {
		while(!new_tree.isEmpty()) {
			int x= new_tree.peek().x;
			int y= new_tree.peek().y;
			int age = new_tree.peek().age;
			new_tree.remove();
			if(map[x][y]>=age) {
				map[x][y] = map[x][y]-age;
				age++;
				next_tree.add(new tree(x, y, age));
			}
			else {
				death_tree.add(new tree(x, y, age));
			}
		}
		while(!this_tree.isEmpty()) {
			int x= this_tree.peek().x;
			int y= this_tree.peek().y;
			int age = this_tree.peek().age;
			this_tree.remove();
			if(map[x][y]>=age) {
				map[x][y] = map[x][y]-age;
				age++;
				next_tree.add(new tree(x, y, age));
				if((age%5)==0) {
					spread_tree.add(new tree(x, y, age));
				}
			}
			else {
				death_tree.add(new tree(x, y, age));
			}
		}
	}
	public static void summer() {
		while(!death_tree.isEmpty()) {
			int x= death_tree.peek().x;
			int y= death_tree.peek().y;
			int age = death_tree.peek().age;
			death_tree.remove();
			int temp = age/2;
			map[x][y] = map[x][y] + temp;
		}
	}
	public static void fall() {
		while(!spread_tree.isEmpty()) {
			int x= spread_tree.peek().x;
			int y= spread_tree.peek().y;
			spread_tree.remove();
			for(int b=0;b<8;b++) {
				int px = x+dx[b];
				int py = y+dy[b];
				if(px >= 0 && py >= 0 && px < N && py < N) {
					new_tree.add(new tree(px, py, 1));
				}
			}
		}
	}
	public static void winter() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				map[a][b] = map[a][b]+add[a][b];
			}
		}
	}
	public static void sort(ArrayList<tree> list) {
		list.sort(new Comparator<tree>() {
			@Override
			public int compare(tree arg0, tree arg1) {
				// TODO Auto-generated method stub
				int age_1 = arg0.age;
				int age_2= arg1.age;

				if (age_1 == age_2)
					return 0;
				else if (age_1 > age_2)
					return 1;
				else
					return -1;
			}
		});
	}
}
