package 삼성공채준비합격하자;

import java.util.Scanner;
public class 테트로미노_14500 {

	static int row_firstX[] = {0,0,0,0};
	static int row_firstY[] = {0,1,2,3};
	static int col_firstX[] = {0,1,2,3};
	static int col_firstY[] = {0,0,0,0};
	
	static int recX[] = {0,0,1,1};
	static int recY[] = {0,1,0,1};
	
	static int mountainX[] = {0,0,0,1};
	static int mountainY[] = {0,1,2,1};
	static int mountain2X[] = {0,1,2,1};
	static int mountain2Y[] = {0,0,0,1};
	static int mountain3X[] = {0,0,0,-1};
	static int mountain3Y[] = {0,1,2,1};
	static int mountain4X[] = {0,1,2,1};
	static int mountain4Y[] = {0,0,0,-1};
	
	static int arrayX[] = {0,1,2,2};
	static int arrayY[] = {0,0,0,1};
	static int array2X[] = {0,1,2,2};
	static int array2Y[] = {0,0,0,-1};
	static int array3X[] = {0,1,2,0};
	static int array3Y[] = {0,0,0,1};
	static int array4X[] = {0,0,1,2};
	static int array4Y[] = {0,1,1,1};
	static int array5X[] = {0,1,1,1};
	static int array5Y[] = {0,0,1,2};
	static int array6X[] = {0,1,1,1};
	static int array6Y[] = {0,0,-1,-2};
	static int array7X[] = {0,0,0,1};
	static int array7Y[] = {0,1,2,2};
	static int array8X[] = {0,0,0,1};
	static int array8Y[] = {0,1,2,0};
	
	static int lastX[] = {0,1,1,2};
	static int lastY[] = {0,0,1,1};
	static int last2X[] = {0,1,1,2};
	static int last2Y[] = {0,0,-1,-1};
	static int last3X[] = {0,0,1,1};
	static int last3Y[] = {0,1,0,-1};
	static int last4X[] = {0,0,1,1};
	static int last4Y[] = {0,1,1,2};
	
	static int R,C;
	static int map[][];
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		R = sc.nextInt();
		C = sc.nextInt();
		map = new int[R][C];
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				map[a][b]=sc.nextInt();
			}
		}
		int max = 0;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				max = Math.max(max, check(row_firstX,row_firstY,a,b));
				max = Math.max(max, check(col_firstX,col_firstY,a,b));

				max = Math.max(max, check(recX,recY,a,b));
				
				
				max = Math.max(max, check(mountainX,mountainY,a,b));
				max = Math.max(max, check(mountain2X,mountain2Y,a,b));
				max = Math.max(max, check(mountain3X,mountain3Y,a,b));
				max = Math.max(max, check(mountain4X,mountain4Y,a,b));
				
				max = Math.max(max, check(arrayX,arrayY,a,b));
				max = Math.max(max, check(array2X,array2Y,a,b));
				max = Math.max(max, check(array3X,array3Y,a,b));
				max = Math.max(max, check(array4X,array4Y,a,b));
				max = Math.max(max, check(array5X,array5Y,a,b));
				max = Math.max(max, check(array6X,array6Y,a,b));
				max = Math.max(max, check(array7X,array7Y,a,b));
				max = Math.max(max, check(array8X,array8Y,a,b));
				
				max = Math.max(max, check(lastX,lastY,a,b));
				max = Math.max(max, check(last2X,last2Y,a,b));
				max = Math.max(max, check(last3X,last3Y,a,b));
				max = Math.max(max, check(last4X,last4Y,a,b));
			}
		}
		System.out.println(max);
	}
	public static int check(int arrX[],int arrY[],int x,int y) {
		int max =0;
		for(int a=0;a<arrX.length;a++) {
			int rx = x+arrX[a];
			int ry = y+arrY[a];
			if(rx >=0 && ry >=0 && rx < R && ry <C) {
				max = max+map[rx][ry];
			}
			else {
				return -1;
			}
		}
		return max;
	}
}
