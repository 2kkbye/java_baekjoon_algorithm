package 삼성공채준비합격하자;

import java.util.Scanner;
public class 사다리조작_15684 {
	static int map[][];

	static int N,M,H;
	static int ans;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		M = sc.nextInt();
		H = sc.nextInt();
		map = new int[H+2][(N*2)];
		for(int a=0;a<M;a++) {
			int i = sc.nextInt();
			int j= sc.nextInt();
			map[i][j*2]=1;
		}
		if(check()) {
			ans =0;
			System.out.println(0);
			System.exit(0);
		}
		ans = 4;
		for(int a=1;a<H+2;a++) {
			for(int b=2;b<N*2;b++) {
				if((b%2)!=0) continue;
				if(map[a][b]==1) continue;
				map[a][b]=1;
				if(check()) {
					ans=1;
					System.out.println(1);
					System.exit(0);
				}
				dfs(a,2);
				map[a][b]=0;
			}
		}
		if(ans>3) {
			System.out.println(-1);
		}
		else {
			System.out.println(ans);
		}

	}
	public static void dfs(int x,int level) {
		if(level>3) {
			return;
		}
		for(int a=x;a<H+1;a++) {
			for(int b=2;b<N*2;b++) {
				if((b%2)!=0) continue;
				if(map[a][b]==1) continue;
				map[a][b]=1;
				if(check()) {
					if(ans>level) {
						ans = level;
					}
				}
				dfs(a,level+1);
				map[a][b]=0;
			}
		}
	}
	public static boolean check() {
		boolean flag=true;
		int rx = 1;
		int ry = 1;
		int x=rx;
		int y=ry;
		while(true) {
			if(y>=(N*2)) {
				break;
			}
			if(x==(H+1)) {
				if(y!=ry) {
					flag = false;
					break;
				}
				else {
					ry=ry+2;
					x=1;
					y=ry;
					continue;
				}
			}
			if((y+1)<(N*2)&&map[x][y+1]==1) {
				x=x+1;
				y=y+2;
				continue;
			}
			else if(map[x][y-1]==1) {
				x=x+1;
				y=y-2;
				continue;
			}
			x=x+1;
		}
		return flag;
	}
}
