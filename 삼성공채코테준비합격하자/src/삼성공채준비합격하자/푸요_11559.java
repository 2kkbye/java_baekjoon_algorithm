package �Ｚ��ä�غ��հ�����;

import java.util.Queue;
import java.awt.Point;
import java.util.LinkedList;
import java.util.Scanner;

public class Ǫ��_11559 {
	//�� �� �� �Ʒ�
	static int dx[] = {0,0,1,-1};
	static int dy[]	= {1,-1,0,0};
	static int map[][] = new int[12][6];
	static int R = 12;
	static int C = 6;
	static Queue<Point> q_color = new LinkedList<Point>();

	static boolean visit[][];//�ʱ�ȭ �ʿ�
	static Queue<Point> q_remove; //�ʱ�ȭ �ʿ�
	static Queue<Point> q_total = new LinkedList<Point>();

	static int cnt;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String temp[][] = new String[R][1];
		for(int a=0;a<R;a++) {
			temp[a][0]=sc.next();
		}
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(temp[a][0].charAt(b)=='.') {
					continue;
				}
				else if(temp[a][0].charAt(b)=='R') { //���� 1
					map[a][b]=1;
				}
				else if(temp[a][0].charAt(b)=='Y') { //��� 2
					map[a][b]=2;
				}
				else if(temp[a][0].charAt(b)=='G') { //�ʷ� 3
					map[a][b]=3;
				}
				else if(temp[a][0].charAt(b)=='B') { //�Ķ� 4
					map[a][b]=4;
				}
				else if(temp[a][0].charAt(b)=='P') { //���� 5
					map[a][b]=5;
				}
			}
		}
		cnt=0;
		int sw=0;
		while(sw!=3) {
			sw=0;
			visit = new boolean[R][C];
			q_remove = new LinkedList<Point>();
			for(int a=R-1;a>=0;a--) {
				for(int b=0;b<C;b++) {
					if(map[a][b]==0) continue;
					if(visit[a][b]==false) {
						q_color.add(new Point(a,b));
						q_remove.add(new Point(a,b));
						visit[a][b]=true;
					}
					if(!q_color.isEmpty()) {
						bfs();
					}
					if(q_remove.size()>=4) {
						copy();
						sw=1;
					}
					else {
						q_remove = new LinkedList<Point>();
					}
				}
			}
			if(sw==1) {
//				print();
//				System.out.println("----------------");
				remove_xy();
				down();
				cnt++;
			}
			else {
				sw=3;
			}
		}
		System.out.println(cnt);
		
	}
	public static void print() {
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				System.out.print(map[a][b]+" ");
			}
			System.out.println();
		}
	}
	public static void copy() {
		while(!q_remove.isEmpty()) {
			int x = q_remove.peek().x;
			int y = q_remove.peek().y;
			q_remove.remove();
			q_total.add(new Point(x,y));
		}
	}
	public static void down() {
		//print();
		int temp=-1;
		int sw=0;
		for(int a=0;a<C;a++) {
			for(int b=R-1;b>=0;b--) {
				if(map[b][a]==0 && sw==1) continue;
				if(map[b][a]==0) {
					temp =b; 
					sw =1;
					continue;
				}
				if(sw ==1 ) {
					map[temp][a] = map[b][a];
					map[b][a]=0;
					temp--;
				}
			}
			sw=0;
		}
	}
	public static void remove_xy() {
		while(!q_total.isEmpty()) {
			int x = q_total.peek().x;
			int y = q_total.peek().y;
			map[x][y]=0;
			q_total.remove();
		}
	}
	public static void bfs() {
		while(!q_color.isEmpty()) {
			int px=q_color.peek().x;
			int py=q_color.peek().y;
			q_color.remove();
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx >= 0 && ry >= 0 && rx < R && ry < C) {
					if((visit[rx][ry]==false) && (map[px][py])==(map[rx][ry])) {
						q_color.add(new Point(rx, ry));
						q_remove.add(new Point(rx,ry));
						visit[rx][ry]=true;
					}
				}
			}
		}
	}
}