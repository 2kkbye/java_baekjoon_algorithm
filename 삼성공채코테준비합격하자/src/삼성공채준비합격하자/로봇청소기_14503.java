package 삼성공채준비합격하자;

import java.util.Scanner;

public class 로봇청소기_14503 {
	static int dx[]= {-1,0,1,0};
	static int dy[]= {0,1,0,-1};
	static boolean visit[][];
	static int map[][];

	static int N,M;
	static int R,C;
	static int d;
	static int result;
	static int cnt;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		M = sc.nextInt();
		R = sc.nextInt();
		C = sc.nextInt();
		d = sc.nextInt();
		visit= new boolean[N][M];
		map = new int[N][M];

		for(int a=0;a<N;a++) {
			for(int b=0;b<M;b++) {
				map[a][b]=sc.nextInt();
			}
		}
		result =0;
		visit[R][C]=true;
		cnt=0;
		dfs(R,C,1);
		System.out.println(result);
	}
	public static void dfs(int x,int y,int level) {
		if(cnt==0) {
			int sw=0;
			for(int a=0;a<4;a++) {
				d=(d+3)%4;
				if(map[x+dx[d]][y+dy[d]]==0 && visit[x+dx[d]][y+dy[d]]==false) {
					sw=1;
					//System.out.println((x+dx[d])+"---------"+(y+dy[d]));
					//System.out.println(level);
					visit[x+dx[d]][y+dy[d]]=true;
					dfs(x+dx[d],y+dy[d],level+1);
				}
			}
			if(sw==0) {
				if(d>=2) {
					if(map[x+dx[(d-2)]][y+dy[(d-2)]]!=1) {
						dfs(x+dx[d-2],y+dy[d-2],level);
					}
					else {
						result=level;
						cnt=1;
					}
				}
				else if(d<2) {
					if(map[x+dx[(d+2)]][y+dy[(d+2)]]!=1) {
						dfs(x+dx[d+2],y+dy[d+2],level);
					}
					else {
						result=level;
						cnt=1;
					}
				}
			}
		}
		else return;
	}
}
