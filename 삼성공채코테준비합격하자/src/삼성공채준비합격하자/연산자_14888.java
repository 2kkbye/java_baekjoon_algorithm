package 삼성공채준비합격하자;

import java.util.Scanner;
import java.util.ArrayList;

public class 연산자_14888 {

	
	static boolean visit[]; //연산자dfs로 전체 탐색
	static int N;
	static int temp[] = new int[4];
	
	static ArrayList<Integer> ins = new ArrayList<Integer>();
	static ArrayList<Integer> number = new ArrayList<Integer>();
	static ArrayList<Integer> ins_temp = new ArrayList<Integer>();
	static int i_index;
	static int max_n;
	static int min_n;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		for(int a=0;a<N;a++) {
			number.add(sc.nextInt());
		}
		
		i_index=0;
		for(int a=0;a<4;a++) {
			int k=sc.nextInt();
			temp[a]=k;
			i_index=i_index+k;
			for(int b=0;b<k;b++) {
				ins.add(a);
			}
		}
		visit = new boolean[i_index];
		max_n = -10000000;
		min_n = Integer.MAX_VALUE;
		for(int a=0;a<ins.size();a++) {
			visit[a]=true;
			ins_temp.add(ins.get(a));
			dfs(a,1);
			visit[a]=false;
			ins_temp = new ArrayList<Integer>();
		}
		System.out.println(max_n);
		System.out.println(min_n);
	}
	public static void check() {
		int rx=0,ry=0;
		int index=0;
		for(int a=0;a<number.size();a++) {
			if(a==0) {
				rx=number.get(a);
				continue;
			}
			ry=number.get(a);
			int ch=ins_temp.get(index);
			index++;
			if(ch==0) {
				rx = rx+ry;
			}
			else if(ch==1) {
				rx=rx-ry;
			}
			else if(ch==2) {
				rx=rx*ry;
			}
			else if(ch==3) {
				if(rx<0) {
					rx=rx*-1;
					rx=rx/ry;
					rx=rx*-1;
				}
				else {
					rx=rx/ry;
				}
			}
		}
		if(rx<min_n) {
			min_n=rx;
		}
		if(rx>max_n) {
			max_n = rx;
		}
	}
	public static void dfs(int x, int level) {
		if(ins_temp.size()==i_index) { 
			//System.out.println(ins_temp);
			check();
			return;
		}
		for(int a=0;a<ins.size();a++) {
			if(visit[a]==false) {
				visit[a]=true;
				ins_temp.add(ins.get(a));
				dfs(a,level+1);
				visit[a]=false;
				ins_temp.remove(ins_temp.size()-1);
			}
		}
	}
}
