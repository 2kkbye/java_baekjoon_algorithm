package 삼성공채준비합격하자;

import java.util.Scanner;
import java.util.Queue;
import java.util.LinkedList;
import java.awt.Point;

public class 인구이동_16234 {
	static int dx[] = {0,1,0,-1}; //오른쪽, 아래, 왼, 위
	static int dy[] = {1,0,-1,0};

	static Queue<Point> q_unit = new LinkedList<Point>();
	static LinkedList<Point> l_unit = new LinkedList<Point>();
	static boolean visit[][];
	static int contury[][];

	static int N;
	static int L,R;
	static int result;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		L = sc.nextInt();
		R = sc.nextInt();
		visit = new boolean[N][N];
		contury = new int[N][N];
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				contury[a][b]=sc.nextInt();
			}
		}
		int sw=-1;
		result =0;
		while(sw!=0) {
			result++;
			sw=0;
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					if(visit[a][b]==false) {
						if(check(a,b)) {
							sw=1;
							visit[a][b]=true;
							q_unit.add(new Point(a,b));
							l_unit.add(new Point(a,b));
							bfs();
						}
					}
				}
			}
			visit = new boolean[N][N];
		}
		System.out.println(result-1);
	}
	public static void print() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				System.out.print(contury[a][b]+" ");
			}
			System.out.println();
		}
	}
	public static void change() {
		int sum =0;
		for(int a=0;a<l_unit.size();a++) {
			sum = sum+ contury[l_unit.get(a).x][l_unit.get(a).y];
		}
		sum = sum/l_unit.size();
		for(int a=0;a<l_unit.size();a++) {
			contury[l_unit.get(a).x][l_unit.get(a).y]=sum;
		}
		l_unit.removeAll(l_unit);
	}
	public static void bfs() {
		while(!q_unit.isEmpty()) {
			int rx = q_unit.peek().x;
			int ry = q_unit.peek().y;
			q_unit.remove();
			for(int a=0;a<4;a++) {
				int px = rx+dx[a];
				int py = ry+dy[a];
				if(px >= 0 && py >= 0 && px < N && py < N) {
					if(visit[px][py]==false) {
						if(Math.abs((contury[rx][ry]-contury[px][py])) >= L && Math.abs((contury[rx][ry]-contury[px][py])) <= R) {
							q_unit.add(new Point(px,py));
							visit[px][py]=true;
							l_unit.add(new Point(px,py));
						}
					}
				}
			}
			if(q_unit.size()==0) {
				change();
			}
		}
	}
	public static boolean check(int rx,int ry) {
		for(int a=0;a<2;a++) {
			int px = rx+dx[a];
			int py = ry+dy[a];
			if(px >= 0 && py >= 0 && px < N && py < N) {
				if(Math.abs((contury[rx][ry]-contury[px][py])) >= L && Math.abs((contury[rx][ry]-contury[px][py])) <= R) {
					return true;
				}
			}
		}
		return false;
	}
}
