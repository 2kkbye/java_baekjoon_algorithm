package ����;

class ListNode {
	private long data;
	public ListNode link;
	public ListNode() {
		this.data=-1;
		this.link=null;
	}
	public ListNode(long data) {
		this.data=data;
		this.link=null;
	}
	public ListNode(long data, ListNode link) {
		this.data=data;
		this.link=link;
	}
	public long getData() {
		return this.data;
	}
	public void setData(long data) {
		this.data = data;
	}
}
class LinkedList {
	private ListNode head;
	long size = 0;
	public LinkedList() {
		head = null;
	}
	public void insertFirstNode(long data) {
		ListNode newNode = new ListNode(data);
		newNode.link=head;
		head=newNode;
		size++;
	}
	public void insertMiddleNode(ListNode pre, int arr[]) {
		for(int a = 0;a<arr.length;a++) {
			ListNode newNode = new ListNode(arr[a]);
			newNode.link=pre.link;
			pre.link=newNode;
			pre = newNode;
			size++;
		}
		
	}
	public void insertLastNode(long data) {
		size++;
		ListNode newNode = new ListNode(data);
		if(head ==null) {
			this.head=newNode;
		}
		else {
			ListNode temp = head;
			while(temp.link !=null) temp = temp.link;
			temp.link=newNode;
		}
	}
	public void init() {
		head=null;
	}
	public ListNode searchNode(long data) {
		ListNode temp = this.head;
		ListNode pre = this.head;
		while(temp!=null) {
			if(data < temp.getData())
				return pre;
			else {
				pre = temp;
				temp=temp.link;
			}
		}
		pre = null;
		return pre;
	}
	public void reverseList() {
		ListNode next = head;
		ListNode current = null;
		ListNode pre = null;
		while(next !=null) {
			pre=current;
			current =next;
			next=next.link;
			current.link=pre;
		}
		head = current;
	}
	public void printList() {
		ListNode temp= this.head;
		System.out.printf("L = (");
		while(temp!=null) {
			System.out.print(temp.getData());
			temp=temp.link;
			if(temp!=null) {
				System.out.printf(", ");
			}
		}
		System.out.println(")");
	}
}
     
 
class Solution
{
    public static void main(String args[]) throws Exception
    {
        LinkedList l = new LinkedList();
        l.insertLastNode(-1);
        int a_ar[] = {2,3,4,5};
        int b_ar[] = {4,8,7,6};
        int c_ar[] = {9,10,15,16};
        int d_ar[] = {1,2,6,5};
        if(l.size == 1) {
        	for(int a = 0;a<a_ar.length;a++) {
        		l.insertLastNode(a_ar[a]);
        	}
        }
        l.printList();
        ListNode temp = l.searchNode(b_ar[0]);
        if(temp== null) {
        	for(int a = 0;a<b_ar.length;a++) {
        		l.insertLastNode(b_ar[a]);
        	}
        }
        else {
        	l.insertMiddleNode(temp, b_ar);
        }
        System.out.println();
        l.printList();
        
        
        temp = l.searchNode(c_ar[0]);
        if(temp== null) {
        	for(int a = 0;a<c_ar.length;a++) {
        		l.insertLastNode(c_ar[a]);
        	}
        }
        else {
        	l.insertMiddleNode(temp, c_ar);
        }
        System.out.println();
        l.printList();
        
        temp = l.searchNode(d_ar[0]);
        if(temp== null) {
        	for(int a = 0;a<c_ar.length;a++) {
        		l.insertLastNode(d_ar[a]);
        	}
        }
        else {
        	l.insertMiddleNode(temp, d_ar);
        }
        System.out.println();
        l.printList();
    }
}
