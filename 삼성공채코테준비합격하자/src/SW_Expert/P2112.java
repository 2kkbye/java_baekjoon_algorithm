package SW_Expert;

import java.util.Scanner;
public class P2112 {

	static int D,W,K;
	
	static int map[][];
	static int copy[][];
	static int dir[];
	static int cell_color[];
	static int insert_medicine;
	static int ans;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=1;i<=T;i++) {
			D = sc.nextInt();
			W = sc.nextInt();
			K = sc.nextInt();
			map = new int[D][W];
			copy = new int[D][W];
			for(int a=0;a<D;a++) {
				for(int b=0;b<W;b++) {
					map[a][b] = sc.nextInt();
				}
			}
			copy();
			if(solve()) {
				System.out.println("#"+i+" "+0);
				continue;
			}
			ans = Integer.MAX_VALUE;
			for(int c=1;c<50;c++) {
				for(int a=0;a<D;a++) {
					for(int color = 0;color<2;color++) {
						dir = new int[c];
						cell_color = new int[c];
						insert_medicine = c;
						//몇번째 행을 바꿀지
						dir[0]=a;
						cell_color[0]=color;
						//인덱스 참조, level
						dfs(a+1,1);
					}
				}
			}
//			if(ans==1) {
//				System.out.println("#"+i+" "+0);
//				continue;
//			}
			System.out.println("#"+i+" "+ans);
		}
	}
	public static void copy() {
		for(int a=0;a<D;a++) {
			for(int b=0;b<W;b++) {
				copy[a][b] = map[a][b];
			}
		}
	}
	public static void dfs(int index,int level) {
		if(ans < insert_medicine) {
			return;
		}
		if(level == insert_medicine) {
			copy();
			for(int b=0;b<dir.length;b++) {
				change_row(dir[b], cell_color[b]);
			}
			if(solve()) {
				ans = Math.min(ans, insert_medicine);
			}
			return;
		}
		for(int a=index;a<D;a++) {
			for(int color = 0;color<2;color++) {
				//몇번째 행을 바꿀지
				dir[level]=a;
				cell_color[level]=color;
				//인덱스 참조, level
				dfs(a+1,level+1);
			}
		}
	}
	public static boolean solve() {
		int cnt=0;
		for(int a=0;a<W;a++) {
			cnt=1;
			int temp_num = copy[0][a];
			for(int b=1;b<D;b++) {
				if(copy[b][a] == temp_num) {
					cnt++;
					if(cnt == K) {
						break;
					}
				}
				else {
					temp_num = copy[b][a];
					cnt=1;
					continue;
				}
			}
			if(cnt!=K) {
				return false;
			}
		}
		return true;
	}
	public static void change_row(int index, int num) {
		for(int a=0;a<W;a++) {
			copy[index][a] = num;
		}
	}
}