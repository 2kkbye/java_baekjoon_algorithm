package SW_Expert;

import java.util.Scanner;
import java.util.ArrayList;
class point{
	int x,y;
	public point(int x,int y) {
		this.x = x;
		this.y = y;
	}
}
public class P5656 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};
	static int map[][];
	static int copy[][];
	static int visit[];

	static int N,H,W;
	static int block_num;
	static ArrayList<point> q_ball = new ArrayList<point>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			N = sc.nextInt();
			W = sc.nextInt();
			H = sc.nextInt();
			map = new int[H][W];
			copy = new int[H][W];
			visit = new int[N];
			for(int a=0;a<H;a++) {
				for(int b=0;b<W;b++) {
					map[a][b] = sc.nextInt();
				}
			}
			block_num = Integer.MAX_VALUE;
			for(int a=0;a<W;a++) {
				visit[0] = a;
				dfs(1);
			}
			System.out.println("#"+(i+1)+" "+block_num);
		}
	}
	public static void erase_block(int x, int y) {
		int num = copy[x][y];
		copy[x][y] = 0;
		for(int a=0;a<4;a++) {
			int rx= x,ry = y;
			for(int b=1;b<=num-1;b++) {
				rx = x+(dx[a]*b);
				ry = y+(dy[a]*b);
				if(rx >= 0 && ry >=0 && rx < H && ry < W) {
					if(copy[rx][ry]!=0) {
						erase_block(rx,ry);
					}
				}
				else {
					break;
				}
			}
		}
	}
	public static void check(int d) {
		for(int a=0;a<H;a++) {
			if(copy[a][d]==0) continue;
			else {
				erase_block(a,d);
				return;
			}
		}
	}
	public static void move() {
		for(int b=0;b<W;b++) {
			for(int a=H-1;a>=0;a--) {
				if(copy[a][b]!=0) {
					int rx = a, ry = b;
					int temp = copy[rx][ry];
					copy[rx][ry]=0;
					while(true) {
						rx = rx+dx[1];
						ry = ry+dy[1];
						if(rx < 0 || ry <0 || rx >= H || ry >= W) {
							rx = rx-dx[1]; ry = ry-dy[1];
							break;
						}
						if(copy[rx][ry]!=0) {
							rx = rx-dx[1]; ry = ry-dy[1];
							break;
						}
					}
					copy[rx][ry]= temp;
				}
			}
		}
	}
	public static int check_block() {
		int cnt=0;
		for(int a=0;a<H;a++) {
			for(int b=0;b<W;b++) {
				if(copy[a][b]==0) continue;
				cnt++;
			}
		}
		return cnt;
	}
	public static void dfs(int level) {
		if(level == N) {
			init();
			for(int a=0;a<visit.length;a++) {
				check(visit[a]);
				move();
			}
			block_num = Integer.min(block_num, check_block());
			return;
		}
		for(int a=0;a<W;a++) {
			visit[level] = a;
			dfs(level+1);
		}
	}
	public static void init() {
		q_ball = new ArrayList<point>();
		for(int a=0;a<H;a++) {
			for(int b=0;b<W;b++) {
				copy[a][b] = map[a][b];
			}
		}
	}
}
