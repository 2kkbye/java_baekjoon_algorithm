package SW_Expert;

import java.util.Scanner;
public class P4014 {

	static int map[][];
	static int N,X;
	static int count;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			count=0;
			N = sc.nextInt();
			X = sc.nextInt();
			map = new int[N][N];
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					map[a][b] = sc.nextInt();
				}
			}
			//가로 시작
			int value=0;
			int temp=0;
			int sw=0;
			for(int a=0;a<N;a++) {
				sw=0;
				temp=0;
				for(int b=0;b<N;b++) {
					if(b==0) {
						value=map[a][b];
						temp++;
						continue;
					}
					if(map[a][b]==value) {
						temp++;
						continue;
					}
					if(map[a][b]>value+1 || map[a][b]<value-1) {
						sw=1;
						break;
					}
					if(map[a][b]==value+1) {
						if(temp < X) {
							sw=1;
							break;
						}
						value = map[a][b];
						temp=1;
						continue;
					}
					if(map[a][b]==value-1) {
						if(!check(0,a,b,map[a][b])) {
							sw=1;
							break;
						}
						b = b+(X-1);
						value = map[a][b];
						temp =0;
						continue;
					}
				}
				if(sw==0) count++;
			}
			for(int a=0;a<N;a++) {
				sw=0;
				temp=0;
				for(int b=0;b<N;b++) {
					if(b==0) {
						value=map[b][a];
						temp++;
						continue;
					}
					if(map[b][a]==value) {
						temp++;
						continue;
					}
					if(map[b][a]>value+1 || map[b][a]<value-1) {
						sw=1;
						break;
					}
					if(map[b][a]==value+1) {
						if(temp < X) {
							sw=1;
							break;
						}
						value = map[b][a];
						temp=1;
						continue;
					}
					if(map[b][a]==value-1) {
						if(!check(1,a,b,map[b][a])) {
							sw=1;
							break;
						}
						b = b+(X-1);
						value = map[b][a];
						temp =0;
						continue;
					}
				}
				if(sw==0) count++;
			}
			System.out.println("#"+(i+1)+" "+count);
		}
	}
	public static boolean check(int sw,int x, int y,int value) {
		if(sw==0) { //가로로 진행
			int rx = x;int ry = y;
			for(int b=1;b<X;b++) {
				ry = ry+1;
				if(ry >= N) {
					return false;
				}
				if(map[rx][ry]!=value) {
					return false;
				}
			}
		}
		else { //세로로 진행
			int rx = x;int ry = y;
			for(int b=1;b<X;b++) {
				ry = ry+1;
				if(ry >= N) {
					return false;
				}
				if(map[ry][rx]!=value) {
					return false;
				}
			}
		}
		return true;
	}
}