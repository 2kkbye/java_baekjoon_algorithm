package SW_Expert;

import java.util.Scanner;
import java.util.ArrayList;

class life {
	int x,y,become_active,become_death,life;
	public life(int x, int y, int become_active, int become_death, int life) {
		this.x = x;
		this.y = y;
		this.become_active = become_active;
		this.become_death = become_death;
		this.life = life;
	}
}

public class P5653 {
	static int N,M,K;
	static int dx[] = {-1,1,0,0};
	static int dy[] = {0,0,-1,1};
	static int map[][] = new int[901][901];
	static boolean visit[][] = new boolean[901][901];

	static ArrayList<life> active = new ArrayList<life>();
	static ArrayList<life> death = new ArrayList<life>();
	static ArrayList<life> deactive = new ArrayList<life>();
	static ArrayList<life> new_life = new ArrayList<life>();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i = 0;i<T;i++) {
			map = new int[901][901];
			visit = new boolean[901][901];
			active = new ArrayList<life>();
			death = new ArrayList<life>();
			deactive = new ArrayList<life>();
			new_life = new ArrayList<life>();
			N = sc.nextInt();
			M = sc.nextInt();
			K = sc.nextInt();
			int half = map.length/2;
			for(int a=half;a<(half+N);a++) {
				for(int b=half;b<(half+M);b++) {
					int num = sc.nextInt();
					if(num!=0) {
						map[a][b] = num;
						visit[a][b] = true;
						deactive.add(new life(a, b, num, 0, num));
					}
				}
			}
			for(int a=0;a<K;a++) {
				spread_area();
				toActive();
				new_toDeactive();
			}
			System.out.println("#"+(i+1)+" "+(deactive.size()+active.size()));
		}
	}
	public static void toActive() {
		int size = deactive.size()-1;
		for(int a=size;a>=0;a--) {
			life l = deactive.get(a);
			l.become_active = l.become_active-1;
			if(l.become_active == 0) {
				active.add(new life(l.x, l.y, 0, l.life, l.life));
				deactive.remove(a);
				continue;
			}
		}
	}
	public static void new_toDeactive() {
		int tem = new_life.size()-1;
		for(int a=tem;a>=0;a--) {
			life l = new_life.get(a);
			deactive.add(l);
			visit[l.x][l.y]=true;
			new_life.remove(a);
		}
	}
	public static void spread_area() {
		int tem = active.size()-1;
		for(int a=tem;a>=0;a--) {
			int rx = active.get(a).x;
			int ry = active.get(a).y;
			for(int b=0;b<4;b++) {
				int px = rx+dx[b];
				int py = ry+dy[b];
				if(visit[px][py]==false) {
					if(map[px][py]!=0) {
						if(map[px][py]>=active.get(a).life) {
							continue;
						}
					}
					map[px][py]=active.get(a).life;
					new_life.add(new life(px, py, active.get(a).life, 0, active.get(a).life));
				}
			}
			active.get(a).become_death= active.get(a).become_death-1;
			if(active.get(a).become_death==0) {
				death.add(active.get(a));
				active.remove(a);
			}
		}
	}
}