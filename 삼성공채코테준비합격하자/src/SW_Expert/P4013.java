package SW_Expert;

import java.util.Scanner;
public class P4013 {

	static int map[][] = new int[4][8];
	static int dir[] = new int[4];
	static int check_map [][] = new int[4][2];

	static int move[][];
	static int K;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T  = sc.nextInt();
		for(int i=1;i<=T;i++) {
			check_map = new int[4][2];
			map = new int[4][8];
			K = sc.nextInt();
			move = new int[K][2];
			for(int a=0;a<4;a++) {
				for(int b=0;b<8;b++) {
					map [a][b] = sc.nextInt();
				}
			}
			for(int a=0;a<K;a++) {
				move[a][0] = sc.nextInt()-1;
				move[a][1] = sc.nextInt();
			}
			for(int a=0;a<K;a++) {
				dir = new int[4];
				dir[move[a][0]] = move[a][1];
				check_LR();
				for(int b=move[a][0];b<3;b++) {
					if(check_map[b][1] != check_map[b+1][0]) {
						dir[b+1] = dir[b]*-1;
					}
					else {
						break;
					}
				}
				for(int b = move[a][0];b>=1;b--) {
					if(check_map[b][0] != check_map[b-1][1]) {
						dir[b-1] = dir[b]*-1;
					}
					else {
						break;
					}
				}
				turn();
				//print();
			}
			int result =0;
			int r_t = 1;
			for(int a=0;a<4;a++) {
				r_t = 1;
				if(map[a][0]==1) {
					for(int b=0;b<a;b++) {
						r_t = r_t*2;
					}
					result = result + r_t;
				}
			}
			System.out.println("#"+i+" "+result);
		}
	}
	public static void print() {
		System.out.println("----------------------");
		for(int a=0;a<4;a++) {
			for(int b=0;b<8;b++) {
				System.out.print(map[a][b]+" ");
			}
			System.out.println();
		}
	}
	public static void check_LR() {
		for(int a=0;a<check_map.length;a++) {
			check_map[a][0] = map[a][6];
			check_map[a][1] = map[a][2];
		}
	}
	public static void turn() {
		for(int a=0;a<dir.length;a++) {
			if(dir[a]==1) {
				int temp = map[a][7];
				for(int b=7;b>=1;b--) {
					map[a][b] = map[a][b-1];
				}
				map[a][0] = temp;
			}
			else if(dir[a] == -1) {
				int temp = map[a][0];
				for(int b=0;b<7;b++) {
					map[a][b] = map[a][b+1];
				}
				map[a][7] = temp;
			}
		}
	}
}
