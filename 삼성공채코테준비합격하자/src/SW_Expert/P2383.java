package SW_Expert;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Comparator;
class People{
	int x,y,dis,time,des;
	public People(int x, int y, int dis, int time,int des) {
		this.x = x;
		this.y = y;
		this.dis = dis;
		this.time = time;
		this.des = des;
	}
}
class Stair{
	int x,y,time;
	public Stair(int x, int y, int time) {
		this.x = x;
		this.y = y;
		this.time = time;
	}
}
public class P2383 {

	static ArrayList<People> people = new ArrayList<People>();
	static ArrayList<Stair> stair = new ArrayList<Stair>();

	static ArrayList<People> down_stair_a = new ArrayList<People>();
	static ArrayList<People> down_stair_b = new ArrayList<People>();
	static int [] arr = new int[7];
	static int ans;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=1;i<=T;i++) {
			int N = sc.nextInt();
			people = new ArrayList<People>();
			stair = new ArrayList<Stair>();
			down_stair_a = new ArrayList<People>();
			down_stair_b = new ArrayList<People>();
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					int temp = sc.nextInt();
					if(temp ==0) continue;
					if(temp ==1) {
						people.add(new People(a, b, 0, 0, 0));
						continue;
					}
					stair.add(new Stair(a, b, temp));
				}
			}
			ans = Integer.MAX_VALUE;
			dfs(0);
			System.out.println("#"+(i)+" "+(ans+1));
		}
	}
	public static void dfs(int level) {
		if(level == people.size()) {
			solve();
			return;
		}
		for(int a=0;a<2;a++) {
			int t_dis = Math.abs(people.get(level).x - stair.get(a).x)+ Math.abs(people.get(level).y - stair.get(a).y);
			people.get(level).des = a;
			people.get(level).dis = t_dis;
			people.get(level).time = stair.get(a).time;
			//arr[level] = a;
			dfs(level+1);
		}
	}
	public static void solve() {
		ArrayList<People> temp = new ArrayList<People>();
		copy(temp);
		sort(temp); //해당 목적지 까지의 거리가 작은것들이 먼저 온다.
		int i=0;
		while(!temp.isEmpty() || !down_stair_a.isEmpty() || !down_stair_b.isEmpty()) {
			i++;
			//System.out.println("a");
			//첫번째 계단 time 감소시킴, 삭제시키거나
			for(int a=0;a<down_stair_a.size();a++) {
				down_stair_a.get(a).time--;
				if(down_stair_a.get(a).time == 0) {
					down_stair_a.remove(a); a--;continue;
				}

			}
			for(int a=0;a<down_stair_b.size();a++) {
				down_stair_b.get(a).time--;
				if(down_stair_b.get(a).time == 0) {
					down_stair_b.remove(a); a--;continue;
				}
			}
			//시간에 맞춰서 계단 도착하는 애들 계단내려가기로 보냄
			for(int a=0;a<temp.size();a++) {
				if(temp.get(a).dis > i) break;
				if(temp.get(a).dis <= i) {
					if(temp.get(a).des==0) { //down_stair_a
						if(down_stair_a.size()>=3) continue;
						down_stair_a.add(new People(temp.get(a).x, temp.get(a).y, temp.get(a).dis, temp.get(a).time, temp.get(a).des));
						temp.remove(a);
						a--;continue;
					}
					else { //down_stair_b
						if(down_stair_b.size()>=3) continue;
						down_stair_b.add(new People(temp.get(a).x, temp.get(a).y, temp.get(a).dis, temp.get(a).time, temp.get(a).des));
						temp.remove(a);
						a--;continue;
					}
				}
			}

		}
		ans = Math.min(ans, i);
	}
	public static void sort(ArrayList<People> list) {
		list.sort(new Comparator<People>() {
			@Override
			public int compare(People arg0, People arg1) {
				// TODO Auto-generated method stub
				int a_v = arg0.dis;
				int b_v = arg1.dis;

				if (a_v == b_v)
					return 0;
				else if (a_v > b_v)
					return 1;
				else
					return -1;
			}
		});
	}
	public static void copy(ArrayList<People> list) {
		for(int a=0;a<people.size();a++) {
			list.add(people.get(a));
		}
	}
}