package SW_Expert;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Comparator;

class bat{
	int x,y,value;
	public bat(int x, int y, int value) {
		this.x = x;
		this.y = y;
		this.value = value;
	}
}
class che{
	int x, y,value, dst;
	public che(int x, int y, int value, int dst) {
		this.x = x;
		this.y = y;
		this.value = value;
		this.dst = dst;
	}
}
public class P5644 {

	static int dx [] = {0,-1,0,1,0};
	static int dy [] = {0,0,1,0,-1};

	static int map[][] = new int[10][10];
	static ArrayList<bat> a_bat = new ArrayList<bat>(); //중복될때 넣어줄 list

	static ArrayList<Integer> a_move = new ArrayList<Integer>();
	static ArrayList<Integer> b_move = new ArrayList<Integer>();
	static ArrayList<che> q_search = new ArrayList<che>();
	static int M,A;
	static int sum_a,sum_b;
	
	static ArrayList<Integer> a_xy = new ArrayList<Integer>();
	static ArrayList<Integer> b_xy = new ArrayList<Integer>();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x, y,c,p; //c는 사이즈 , p는 공급전력량
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			q_search = new ArrayList<che>();
			a_move = new ArrayList<Integer>();
			b_move = new ArrayList<Integer>();
			map = new int[10][10];
			a_bat = new ArrayList<bat>();
			M = sc.nextInt();
			A = sc.nextInt();
			sum_a = 0;sum_b = 0;
			for(int a=0;a<2;a++) {
				for(int b=0;b<M;b++) {
					int temp = sc.nextInt();
					if(a==0) {
						a_move.add(temp);
						continue;
					}
					b_move.add(temp);
				}
			}
			for(int a=0;a<A;a++) {
				y = sc.nextInt();
				x = sc.nextInt();
				c = sc.nextInt();
				p = sc.nextInt();
				q_search.add(new che(x-1, y-1, p, c));
				insert_map(x-1,y-1,c,p);
			}
			int startA_x = 0,startA_y=0;
			int startB_x = 9,startB_y=9;
			for(int a=0;a<M;a++) {
				check(startA_x, startA_y,startB_x,startB_y);
				startA_x = startA_x + dx[a_move.get(a)];
				startA_y = startA_y + dy[a_move.get(a)];
				startB_x = startB_x + dx[b_move.get(a)];
				startB_y = startB_y + dy[b_move.get(a)];
			}
			check(startA_x, startA_y,startB_x,startB_y);
			System.out.println("#"+(i+1)+" "+(sum_a+sum_b));
		}
	}
	public static void search(int x,int y,int f_x, int f_y) {
		a_xy = new ArrayList<Integer>();
		b_xy = new ArrayList<Integer>();
		for(int i=0;i<q_search.size();i++) {
			int result = Math.abs(x-q_search.get(i).x) + Math.abs(y-q_search.get(i).y);
			if(result<=q_search.get(i).dst) {
				a_xy.add(i);
			}
		}
		for(int i=0;i<q_search.size();i++) {
			int result = Math.abs(f_x-q_search.get(i).x) + Math.abs(f_y-q_search.get(i).y);
			if(result<=q_search.get(i).dst) {
				b_xy.add(i);
			}
		}
		sort(a_xy);
		sort(b_xy);
	}
	public static void sort(ArrayList<Integer> list) {
		list.sort(new Comparator<Integer>() {
			@Override
			public int compare(Integer arg0, Integer arg1) {
				// TODO Auto-generated method stub
				int a_v = q_search.get(arg0).value;
				int b_v = q_search.get(arg1).value;

				if (a_v == b_v)
					return 0;
				else if (a_v < b_v)
					return 1;
				else
					return -1;
			}
		});
	}
	public static void check(int a_x, int a_y, int b_x, int b_y) {
		if(map[a_x][a_y]!=-1 && map[b_x][b_y]!=-1) {
			if(map[a_x][a_y]!=map[b_x][b_y]) {
				sum_a = sum_a+map[a_x][a_y];
				sum_b = sum_b+map[b_x][b_y];
				return;
			}
			if(map[a_x][a_y]==map[b_x][b_y]) {
				search(a_x,a_y,b_x,b_y);
				if(!a_xy.isEmpty() && !b_xy.isEmpty() && a_xy.get(0)==b_xy.get(0)) {
					sum_a = sum_a+(map[a_x][a_y]/2);
					sum_b = sum_b+(map[b_x][b_y]/2);
				}
				else {
					sum_a = sum_a+map[a_x][a_y];
					sum_b = sum_b+map[b_x][b_y];
				}
			}
		}
		else if(map[a_x][a_y]!=-1 && map[b_x][b_y]==-1) {
			int b_first_max = -1;
			int b_second_max = -1;
			for(int a=0;a<a_bat.size();a++) {
				if((a_bat.get(a).x==b_x) && a_bat.get(a).y==b_y) {
					if(b_first_max<a_bat.get(a).value) {
						b_second_max = b_first_max;
						b_first_max = a_bat.get(a).value;
					}
					else {
						if(b_second_max<a_bat.get(a).value) {
							b_second_max = a_bat.get(a).value;
						}
					}
				}
			}
			int result=0;
			if( map[a_x][a_y] == b_first_max) {
				search(a_x,a_y,b_x,b_y);
				if(a_xy.get(0) == b_xy.get(0)) {
					result = Math.max(map[a_x][a_y],map[a_x][a_y]+b_second_max);
				}
				else {
					result = Math.max(map[a_x][a_y]*2,map[a_x][a_y]+b_second_max);
				}
			}
			else if(map[a_x][a_y]==b_second_max) {
				search(a_x,a_y,b_x,b_y);
				if(a_xy.get(0)==b_xy.get(1)) {
					result = Math.max(map[a_x][a_y]+b_first_max,map[a_x][a_y]);
				}
				else {
					result = Math.max(map[a_x][a_y]+b_first_max,map[a_x][a_y]*2);
				}
			}
			else {
				result = b_first_max;
				sum_a= sum_a + map[a_x][a_y];
			}
			sum_a = sum_a + result;
		}
		else if(map[a_x][a_y]==-1 && map[b_x][b_y]!=-1) {
			int a_first_max = -1;
			int a_second_max = -1;
			for(int a=0;a<a_bat.size();a++) { //여기에 첫번쨰가 밀렸을때의 2번쨰로 넘어가는과정이 필요
				if((a_bat.get(a).x==a_x) && a_bat.get(a).y==a_y) {
					if(a_first_max<a_bat.get(a).value) {
						a_second_max = a_first_max;
						a_first_max = a_bat.get(a).value;
					}
					else {
						if(a_second_max<a_bat.get(a).value) {
							a_second_max = a_bat.get(a).value;
						}
					}
				}
			}
			int result=0;
			if( map[b_x][b_y] == a_first_max) {
				search(a_x,a_y,b_x,b_y);
				if(b_xy.get(0)==a_xy.get(0)) {
					result = Math.max(map[b_x][b_y],map[b_x][b_y]+a_second_max);
				}
				else {
					result = Math.max(map[b_x][b_y]*2,map[b_x][b_y]+a_second_max);
				}
			}
			else if(map[b_x][b_y]==a_second_max) {
				search(a_x,a_y,b_x,b_y);
				if(b_xy.get(0)==a_xy.get(1)) {
					result = Math.max(map[b_x][b_y]+a_first_max,map[b_x][b_y]);
				}
				else {
					result = Math.max(map[b_x][b_y]+a_first_max,map[b_x][b_y]*2);
				}
			}
			else {
				result = a_first_max;
				sum_b = sum_b+ map[b_x][b_y];
			}
			sum_a = sum_a + result;
		}
		else if(map[a_x][a_y]==-1 && map[b_x][b_y]==-1) {
			int a_first_max = -1;
			int a_second_max = -1;
			for(int a=0;a<a_bat.size();a++) {
				if((a_bat.get(a).x==a_x) && a_bat.get(a).y==a_y) {
					if(a_first_max<a_bat.get(a).value) {
						a_second_max = a_first_max;
						a_first_max = a_bat.get(a).value;
					}
					else {
						if(a_second_max<a_bat.get(a).value) {
							a_second_max = a_bat.get(a).value;
						}
					}
				}
			}
			int b_first_max = -1;
			int b_second_max = -1;
			for(int a=0;a<a_bat.size();a++) {
				if((a_bat.get(a).x==b_x) && a_bat.get(a).y==b_y) {
					if(b_first_max<a_bat.get(a).value) {
						b_second_max = b_first_max;
						b_first_max = a_bat.get(a).value;
					}
					else {
						if(b_second_max<a_bat.get(a).value) {
							b_second_max = a_bat.get(a).value;
						}
					}
				}
			}
			int max, max2, result;
			if(a_first_max == b_first_max && a_second_max == b_second_max) {
				max = Math.max((a_first_max/2)+(b_first_max/2),a_first_max+b_second_max);
				max2 = Math.max(a_second_max+b_first_max,(a_second_max/2)+(b_second_max/2) );
				result = Math.max(max, max2);
			}
			else if(a_first_max == b_first_max && a_second_max !=b_second_max) {
				max = Math.max((a_first_max/2)+(b_first_max/2),a_first_max+b_second_max);
				max2 = Math.max(a_second_max+b_first_max,a_second_max+b_second_max);
				result = Math.max(max, max2);
			}
			else if(a_first_max != b_first_max && a_second_max ==b_second_max) {
				max = Math.max(a_first_max+b_first_max,a_first_max+b_second_max);
				max2 = Math.max(a_second_max+b_first_max,(a_second_max/2)+(b_second_max/2) );
				result = Math.max(max, max2);
			}
			else {
				max = Math.max(a_first_max+b_first_max,a_first_max+b_second_max);
				max2 = Math.max(a_second_max+b_first_max,a_second_max+b_second_max);
				result = Math.max(max, max2);
			}
			sum_a = sum_a + result;
		}
	}
	public static void insert_map(int x, int y,int c, int p) {
		int M = x;
		int CL = y, CR = y;
		int size = c;
		for(int a =x-size;a<=x+size;a++) {
			for(int b = CL; b<= CR;b++) {
				if(b>=0 && b<10 && a>=0 && a<10) {
					if(map[a][b]==0) {
						map[a][b] = p;
					}
					else {
						int temp = map[a][b];
						a_bat.add(new bat(a, b, temp));
						a_bat.add(new bat(a, b, p));
						map[a][b]=-1;
					}
				}
			}
			if(a<M) {
				CL--;
				CR++;
			}
			else {
				CL++;
				CR--;
			}
		}
	}
}