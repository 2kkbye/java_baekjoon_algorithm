package SW_Expert;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Comparator;
public class P5658 {

	static ArrayList<Long> result;
	static ArrayList<Character> list;
	static ArrayList<String> str_list;
	static int N,K;
	static String str;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0;i<T;i++) {
			N = sc.nextInt();
			K = sc.nextInt();
			str = sc.next();
			result = new ArrayList<Long>();
			list = new ArrayList<Character>();
			str_list = new ArrayList<String>();
			int temp = N/4;
			changeTolist();
			for(int a=0;a<temp;a++) {
				String t="";
				for(int b=0;b<str.length();b++) {
					if(((b+1)%temp ==0)) {
						t = t+list.get(b);
						insert_String(t);
						t="";
						continue;
					}
					t = t+list.get(b);
				}
				char c =list.remove(list.size()-1);
				list.add(0, c);
			}
			for(int a=0;a<str_list.size();a++) {
				long ten_number = changeToten(str_list.get(a));
				result.add(ten_number);
			}
			sort();
			System.out.println("#"+(i+1)+" "+result.get(K-1));
		}
	}
	public static void sort() {
		result.sort(new Comparator<Long>() {
			@Override
			public int compare(Long arg0, Long arg1) {
				// TODO Auto-generated method stub
				long first = arg0;
				long second=arg1;
				
				if ( first== second)
					return 0;
				else if (first < second)
					return 1;
				else
					return -1;
			}
		});
	}
	public static void insert_String(String str) {
		for(int a=0;a<str_list.size();a++) {
			if(str_list.get(a).equals(str)) {
				return;
			}
		}
		str_list.add(str);
	}
	public static long changeToten(String str) {
		long sum=0;
		for(int a=str.length()-1;a>=0;a--) {
			long temp;
			if(str.charAt(a)=='A') {
				temp = 10;
			}
			else if(str.charAt(a)=='B') {
				temp =11;
			}
			else if(str.charAt(a)=='C') {
				temp =12;
			}
			else if(str.charAt(a)=='D') {
				temp = 13;
			}
			else if(str.charAt(a)=='E') {
				temp = 14;
			}
			else if(str.charAt(a)=='F') {
				temp = 15;
			}
			else {
				temp = (long)str.charAt(a)-48;
			}
			if(a==(str.length()-1)) {
				sum = sum+temp;
			}
			else {
				long c=1;
				for(int b=str.length()-1;b>a;b--) {
					c = c*16;
				}
				sum = sum+(c*temp);
			}
		}
		return sum;
	}
	public static void changeTolist() {
		for(int a=0;a<str.length();a++) {
			list.add(str.charAt(a));
		}
	}
}
