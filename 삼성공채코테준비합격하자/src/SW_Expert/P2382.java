package SW_Expert;

import java.util.Scanner;
import java.util.ArrayList;
class Bug{
	int x, y,dir,number_bug;
	public Bug(int x, int y, int number_bug ,int dir) {
		this.x = x;
		this.y = y;
		this.number_bug = number_bug;
		this.dir = dir;
	}
}
public class P2382 {

	static int dx[] = {0,-1,1,0,0};
	static int dy[] = {0,0,0,-1,1};
	static int N,M,K;
	static ArrayList<Bug> a_bug = new ArrayList<Bug>();
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=1;i<=T;i++) {
			a_bug = new ArrayList<Bug>();
			N = sc.nextInt();
			M = sc.nextInt();
			K = sc.nextInt();
			for(int a=0;a<K;a++) {
				int x,y,number,dir;
				x = sc.nextInt();
				y = sc.nextInt();
				number = sc.nextInt();
				dir = sc.nextInt();
				a_bug.add(new Bug(x, y, number, dir));
			}
			for(int a=0;a<M;a++) {
				move();
				check();
				erase_bug();
				sum_bug();
			}
			int ans=0;
			for(int a=0;a<a_bug.size();a++) {
				ans = ans+a_bug.get(a).number_bug;
			}
			System.out.println("#"+(i)+" "+ans);
		}
	}
	public static void sum_bug() {
		ArrayList<Bug> temp = new ArrayList<Bug>();
		for(int a=0;a<a_bug.size();a++) {
			temp = new ArrayList<Bug>();
			temp.add(new Bug(a_bug.get(a).x, a_bug.get(a).y, a_bug.get(a).number_bug, a_bug.get(a).dir));
			for(int b=a+1;b<a_bug.size();b++) {
				if(a_bug.get(a).x == a_bug.get(b).x && a_bug.get(a).y == a_bug.get(b).y ) {
					temp.add(new Bug(a_bug.get(b).x, a_bug.get(b).y, a_bug.get(b).number_bug, a_bug.get(b).dir));
				}
			}
			if(temp.size()==1) continue;
			int max_num=0;
			int max_dir=0;
			int sum = 0;
			for(int b=0;b<temp.size();b++) {
				sum = sum+temp.get(b).number_bug;
				if(max_num ==0) {
					max_num = temp.get(b).number_bug;
					max_dir = temp.get(b).dir;
					continue;
				}
				if(max_num < temp.get(b).number_bug) {
					max_num = temp.get(b).number_bug;
					max_dir = temp.get(b).dir;
				}
			}
			int rx = temp.get(0).x;
			int ry = temp.get(0).y;
			a_bug.add(new Bug(temp.get(0).x, temp.get(0).y, sum, max_dir));
			for(int b=0;b<a_bug.size()-1;b++) {
				if(rx == a_bug.get(b).x && ry== a_bug.get(b).y) {
					a_bug.remove(b);b--;continue;
				}
			}
			a--;continue;
		}
	}
	public static void erase_bug() {
		for(int a=0;a<a_bug.size();a++) {
			if(a_bug.get(a).number_bug==0) {
				a_bug.remove(a);
				a--;continue;
			}
		}
	}
	public static void check() {
		for(int a=0;a<a_bug.size();a++) {
			int x = a_bug.get(a).x;
			int y = a_bug.get(a).y;
			if(x ==0 || y==0 || x == (N-1) || y==(N-1))	{
				a_bug.get(a).number_bug = a_bug.get(a).number_bug/2;
				if( a_bug.get(a).dir%2 ==0 ) {
					a_bug.get(a).dir = a_bug.get(a).dir-1;
				}
				else {
					a_bug.get(a).dir = a_bug.get(a).dir+1;
				}
			}
		}
	}
	public static void move() {
		for(int a=0;a<a_bug.size();a++) {
			a_bug.get(a).x = a_bug.get(a).x+dx[a_bug.get(a).dir];
			a_bug.get(a).y = a_bug.get(a).y+dy[a_bug.get(a).dir];
		}
	}
}
