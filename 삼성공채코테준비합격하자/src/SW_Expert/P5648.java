package SW_Expert;

import java.util.Scanner;
import java.util.Queue;
import java.util.LinkedList;
public class P5648 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		int dx[] = {0,0,-1,1};
		int dy[] = {1,-1,0,0};
		for(int i=1;i<=T;i++) {
			
			Queue<Short> a_remove = new LinkedList<Short>();
			short map[][] = new short[4002][4002];
			int N =sc.nextInt();
			short elem[][] = new short[N+1][2];
			int dir[] = new int[N+1];
			int energy[] = new int[N+1];
			int time[] = new int[N+1];
			for(int a=1;a<=N;a++) {
				int x = sc.nextInt()*2 + 2000; //��
				int y = sc.nextInt()*2 + 2000; //��
				int rd = sc.nextInt();
				int ene = sc.nextInt();
				elem[a][0]=(short)x;elem[a][1]=(short)y;
				dir[a] = rd;
				energy[a] = ene;
			}
			int score = 0;
			int deadNum=0;
			while(deadNum <N) {
				for(int a=1;a<=N;a++) {
					if(energy[a]==0) continue;
					time[a]++;
					int rx = elem[a][0];
					int ry = elem[a][1];
					map[ry][rx]=0;

					int rdir = dir[a];
					rx = rx+dx[rdir];
					ry = ry+dy[rdir];
					elem[a][0] = (short)rx;
					elem[a][1] = (short)ry;
					if(rx >=4000 || rx < 0 || ry >= 4000 || ry < 0) {
						deadNum++;
						energy[a]=0;
					}
					if(energy[a]!=0) {
						if(map[ry][rx]!=0 && time[a]==time[map[ry][rx]]) {
							a_remove.add(map[ry][rx]);
							a_remove.add((short)a);
						}
						else {
							map[ry][rx]=(short)a;
						}
					}
				}
				while(!a_remove.isEmpty()) {
					int temp = a_remove.remove();
					if(energy[temp]==0)continue;
					deadNum++;
					score = score+energy[temp];
					energy[temp]=0;
				}
			}
			System.out.println("#"+i+" "+score);
		}
		sc.close();
	}
}