package SW_Expert;

import java.util.Scanner;
import java.util.Queue;
import java.util.LinkedList;
import java.awt.Point;
public class P1953 {
	static int dx1[]= {-1,1,0,0};
	static int dy1[]= {0,0,-1,1};

	static int dx2[]= {-1,1,0,0};
	static int dy2[]= {0,0,0,0};

	static int dx3[]= {0,0,0,0};
	static int dy3[]= {0,0,-1,1};

	static int dx4[]= {-1,0,0,0};
	static int dy4[]= {0,0,0,1};

	static int dx5[]= {0,1,0,0};
	static int dy5[]= {0,0,0,1};

	static int dx6[]= {0,1,0,0};
	static int dy6[]= {0,0,-1,0};

	static int dx7[]= {-1,0,0,0};
	static int dy7[]= {0,0,-1,0};

	static boolean visit[][];
	static int map[][];
	static int new_map[][];
	static int N,M,R,C,L;
	static Queue<Point> q_person = new LinkedList<Point>();
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=1;i<=T;i++) {
			N = sc.nextInt();
			M = sc.nextInt();
			R = sc.nextInt();
			C = sc.nextInt();
			L = sc.nextInt();
			map = new int[N][M];
			visit = new boolean[N][M];
			new_map = new int[N][M];
			for(int a=0;a<N;a++) {
				for(int b=0;b<M;b++) {
					map[a][b] = sc.nextInt();
				}
			}
			q_person.add(new Point(R,C));
			visit[R][C] = true;
			new_map[R][C]=1;
			solve();
			//print();
			System.out.println("#"+i+" "+result());
		}
	}
	public static int result() {
		int ans=0;
		for(int a=0;a<N;a++) {
			for(int b=0;b<M;b++) {
				if(new_map[a][b]==0) continue;
				if(new_map[a][b]>L) continue;
				ans++;
			}
		}
		return ans;
	}
	public static boolean check(int x, int y,int index) {
		if(index==0) { //��
			if(map[x][y]==1 ||map[x][y]==2 ||map[x][y]==5 ||map[x][y]==6) {
				return true;
			}
		}
		else if(index==1) { //��
			if(map[x][y]==1 ||map[x][y]==2 ||map[x][y]==4 ||map[x][y]==7) {
				return true;
			}
		}
		else if(index==2) { //��
			if(map[x][y]==1 ||map[x][y]==3 ||map[x][y]==4 ||map[x][y]==5) {
				return true;
			}
		}
		else if(index==3) { //��
			if(map[x][y]==1 ||map[x][y]==3 ||map[x][y]==6 ||map[x][y]==7) {
				return true;
			}
		}
		return false;
	}
	public static void solve() {
		while(!q_person.isEmpty()) {
			Point p = q_person.remove();
			int px = p.x;
			int py = p.y;
			int rx,ry;
			if(map[px][py]==1) {
				for(int a=0;a<dx1.length;a++) {
					rx = px+dx1[a];
					ry = py+dy1[a];
					if(rx >=0 && ry>=0 && rx <N && ry <M) {
						if(visit[rx][ry]==false && map[rx][ry]!=0) {
							if(check(rx,ry,a)) {
								q_person.add(new Point(rx, ry));
								visit[rx][ry]=true;
								new_map[rx][ry] = new_map[px][py]+1;
							}
						}
					}
				}
			}
			else if(map[px][py]==2) {
				for(int a=0;a<dx2.length;a++) {
					rx = px+dx2[a];
					ry = py+dy2[a];
					if(rx >=0 && ry>=0 && rx <N && ry <M) {
						if(visit[rx][ry]==false && map[rx][ry]!=0) {
							if(check(rx,ry,a)) {
								q_person.add(new Point(rx, ry));
								visit[rx][ry]=true;
								new_map[rx][ry] = new_map[px][py]+1;
							}
						}
					}
				}
			}
			else if(map[px][py]==3) {
				for(int a=0;a<dx3.length;a++) {
					rx = px+dx3[a];
					ry = py+dy3[a];
					if(rx >=0 && ry>=0 && rx <N && ry <M) {
						if(visit[rx][ry]==false && map[rx][ry]!=0) {
							if(check(rx,ry,a)) {
								q_person.add(new Point(rx, ry));
								visit[rx][ry]=true;
								new_map[rx][ry] = new_map[px][py]+1;
							}
						}
					}
				}
			}
			else if(map[px][py]==4) {
				for(int a=0;a<dx4.length;a++) {
					rx = px+dx4[a];
					ry = py+dy4[a];
					if(rx >=0 && ry>=0 && rx <N && ry <M) {
						if(visit[rx][ry]==false && map[rx][ry]!=0) {
							if(check(rx,ry,a)) {
								q_person.add(new Point(rx, ry));
								visit[rx][ry]=true;
								new_map[rx][ry] = new_map[px][py]+1;
							}
						}
					}
				}
			}
			else if(map[px][py]==5) {
				for(int a=0;a<dx5.length;a++) {
					rx = px+dx5[a];
					ry = py+dy5[a];
					if(rx >=0 && ry>=0 && rx <N && ry <M) {
						if(visit[rx][ry]==false && map[rx][ry]!=0) {
							if(check(rx,ry,a)) {
								q_person.add(new Point(rx, ry));
								visit[rx][ry]=true;
								new_map[rx][ry] = new_map[px][py]+1;
							}
						}
					}
				}
			}
			else if(map[px][py]==6) {
				for(int a=0;a<dx6.length;a++) {
					rx = px+dx6[a];
					ry = py+dy6[a];
					if(rx >=0 && ry>=0 && rx <N && ry <M) {
						if(visit[rx][ry]==false && map[rx][ry]!=0) {
							if(check(rx,ry,a)) {
								q_person.add(new Point(rx, ry));
								visit[rx][ry]=true;
								new_map[rx][ry] = new_map[px][py]+1;
							}
						}
					}
				}
			}
			else if(map[px][py]==7) {
				for(int a=0;a<dx7.length;a++) {
					rx = px+dx7[a];
					ry = py+dy7[a];
					if(rx >=0 && ry>=0 && rx <N && ry <M) {
						if(visit[rx][ry]==false && map[rx][ry]!=0) {
							if(check(rx,ry,a)) {
								q_person.add(new Point(rx, ry));
								visit[rx][ry]=true;
								new_map[rx][ry] = new_map[px][py]+1;
							}
						}
					}
				}
			}
		}
	}

}
