package SW_Expert;

import java.util.Scanner;
import java.util.ArrayList;
public class P2105 {

	static int dx[]= {1,1,-1,-1};
	static int dy[]= {1,-1,-1,1};
	static int [][] map;
	static ArrayList<Integer> check = new ArrayList<Integer>();
	static int N;
	static int startX,startY;
	static int ans;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=1;i<=T;i++) {
			check = new ArrayList<Integer>();
			N = sc.nextInt();
			map = new int[N][N];
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					map[a][b] = sc.nextInt();
				}
			}
			ans = -1;
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					startX = a;startY = b;
					check.add(map[a][b]);
					dfs(a+dx[0],b+dy[0],0);
					check.remove(0);
				}
			}
			System.out.println("#"+i+" "+ans);
		}
	}
	public static boolean check() {
		for(int a=0;a<check.size();a++) {
			for(int b=a+1;b<check.size();b++) {
				if(check.get(a)== check.get(b)) {
					return false;
				}
			}
		}
		return true;
	}
	public static void dfs(int x,int y,int index) {
		if(x < 0 || y < 0 || x >= N || y >= N) {
			return;
		}
		if(x == startX && y == startY) {
			if(check.size()<4) {
				return;
			}
			if(check()) {
				ans = Math.max(ans, check.size());
			}
			return;
		}
		for(int a=index;a<4;a++) {
			check.add(map[x][y]);
			//System.out.println(x+"---"+y+"---"+a);
			dfs(x+dx[a],y+dy[a],a);
			check.remove(check.size()-1);
		}
	}

}
