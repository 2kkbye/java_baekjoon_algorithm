package SW_Expert;

import java.util.Scanner;
public class P2117 {

	static boolean visit[][];
	static int map[][];
	static int N,M;
	static int home_count;
	static int ans;
	static int current_cost;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=1;i<=T;i++) {
			home_count=0;
			N = sc.nextInt();
			M = sc.nextInt();
			map = new int[N][N];
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					int te = sc.nextInt();
					if(te==0) continue;
					home_count++;
					map[a][b] = te;
				}
			}
			ans =1;
			int total_money = home_count*M; //집 전체의 이익
			for(int a=2;a<40;a++) {
				current_cost = (a*a+(a-1)*(a-1));
				if(total_money < current_cost) break;
				dfs(a);
			}
			System.out.println("#"+i+" "+ans);
		}
	}
	public static void print() {
		System.out.println("-----------------");
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				System.out.print(visit[a][b]);
			}
			System.out.println();
		}
	}
	public static int home_count() {
		int cnt =0;
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(visit[a][b]==false) continue;
				if(map[a][b]==0) continue;
				cnt++;
			}
		}
		return cnt;
	}
	public static void dfs(int size) {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				insert_play(a,b,size-1);
				int cnt = home_count();
				if( (cnt*M) < current_cost) continue; //사용자지불과, 운영비용사이
				if(cnt<ans) continue;
				//System.out.println(size);
				//print();
				ans= cnt;
			}
		}
	}
	public static void insert_play(int x, int y, int size) {
		visit = new boolean[N][N];
		int M = x;
		int CL =y,CR = y;

		for(int a=x-size;a<=x+size;a++) {
			for(int b=CL;b<=CR;b++) {
				if(a < 0 || b < 0 || a >= N || b >= N) continue;
				visit[a][b]= true;
			}
			if(a<M) {
				CL--;CR++;
			}
			else {
				CL++;
				CR--;
			}
		}
	}
}