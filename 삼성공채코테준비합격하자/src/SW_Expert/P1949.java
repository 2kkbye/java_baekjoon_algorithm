package SW_Expert;

import java.util.Scanner;
import java.awt.Point;
import java.util.ArrayList;
public class P1949 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};
	
	static int map[][];
	static int new_map[][];
	static int N,K;
	static int ans;
	static int max;
	static boolean visit[][];
	static ArrayList<Point> a_max = new ArrayList<Point>();
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=1;i<=T;i++) {
			
			N = sc.nextInt();
			K = sc.nextInt();
			map = new int[N][N];
			new_map = new int[N][N];
			visit = new boolean[N][N];
			a_max = new ArrayList<Point>();
			max = -1;
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					int temp = sc.nextInt();
					if(temp > max) {
						max = temp;
					}
					map[a][b] = temp;
				}
			}
			max_check(max);
			ans = -1;
			for(int a=0;a<a_max.size();a++) {
				copy();
				dfs(a_max.get(a).x,a_max.get(a).y,K,1,1);
			}
			System.out.println("#"+i+" "+ans);
		}
	}
	public static void dfs(int x, int y, int k,int level,int sw) {
		if(k<0) {
			return;
		}
		visit[x][y]=true;
		int rx = x;int ry = y;
		for(int a=0;a<4;a++) {
			rx = x+dx[a];
			ry = y+dy[a];
			if(rx < 0 || ry < 0 || rx >= N || ry >= N) continue;
			if(visit[rx][ry]==true) continue;
			if(new_map[x][y] < new_map[rx][ry]) {
				if(new_map[x][y]+k>new_map[rx][ry] && sw==1) {
					int temp = new_map[rx][ry]-new_map[x][y]+1;
					new_map[rx][ry] = new_map[rx][ry]-temp;
					dfs(rx,ry,k-temp,level+1,0);
					new_map[rx][ry] = new_map[rx][ry]+temp;
				}
			}
			if(new_map[x][y] == new_map[rx][ry] && sw==1) {
				new_map[rx][ry] = new_map[rx][ry]-1; 
				dfs(rx,ry,k-1,level+1,0);
				new_map[rx][ry] = new_map[rx][ry]+1; 
			}
			else if(new_map[x][y]>new_map[rx][ry]) {
				dfs(rx,ry,k,level+1,sw);
			}
		}
		visit[x][y] = false;
		ans = Math.max(ans, level);
	}
	public static void max_check(int max) {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(map[a][b]==max) {
					a_max.add(new Point(a, b));
				}
			}
		}
	}
	public static void copy() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				new_map[a][b] = map[a][b];
			}
		}
	}
}
