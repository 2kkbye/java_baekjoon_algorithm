package SW_Expert;

import java.util.Scanner;
import java.util.ArrayList;
class ball{
	int x,y,dir;
	public ball(int x, int y, int dir) {
		this.x = x;
		this.y = y;
		this.dir = dir;
	}
}
class hall{
	int x,y,value;
	public hall(int x, int y, int value) {
		this.x = x;
		this.y = y;
		this.value = value;
	}
}
public class P5650 {
	static int N;
	static int map[][];
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};
	static ArrayList<hall> a_hall;
	static ArrayList<ball> a_ball;
	static int ans;
	static int startX,startY;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=1;i<=T;i++) {
			N = sc.nextInt();
			map = new int[N][N];
			a_hall = new ArrayList<hall>();
			a_ball = new ArrayList<ball>();
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					int temp = sc.nextInt();
					if(temp ==0) continue;
					if(temp >=6 && temp<=10) {
						a_hall.add(new hall(a, b, temp));
					}
					map[a][b] = temp;
				}
			}
			ans = -1;
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					if(map[a][b]!=0) continue;
					for(int c=0;c<4;c++) {
						startX = a;startY =b;
						ans = Math.max(ans, solve(a,b,c));
					}
				}
			}
			System.out.println("#"+i+" "+ans);
		}
	}
	public static int reverse_dir(int dir) {
		if(dir%2==1) {
			dir = dir-1;
		}
		else {
			dir = dir+1;
		}
		return dir;
	}
	public static int solve(int x, int y, int dir) {
		int score = 0;
		int rx = x,ry=y,rdir = dir;
		while(true) {
			rx = rx+dx[rdir];
			ry = ry+dy[rdir];
			if((rx <0 || ry<0 || rx>=N || ry>=N)) {
				score++;
				rdir = reverse_dir(rdir);
				continue;
			}
			if( (rx == startX && ry==startY ) || map[rx][ry]==-1) {
				break;
			}
			if(map[rx][ry]==0) continue;
			if(map[rx][ry]==5) {
				score++;
				rdir = reverse_dir(rdir);
				continue;
			}
			if(map[rx][ry]>=6 && map[rx][ry]<=10) {
				for(int a=0;a<a_hall.size();a++) {
					if(((rx != a_hall.get(a).x) || (ry != a_hall.get(a).y)) && (map[rx][ry] == a_hall.get(a).value)) {
						rx = a_hall.get(a).x;
						ry = a_hall.get(a).y;
						break;
					}
				}
				continue;
			}
			if(map[rx][ry]<5) {
				score++;
				if(map[rx][ry]==1) {
					if(rdir ==0 || rdir==3) {
						rdir = reverse_dir(rdir);
						continue;
					}
					if(rdir == 1) {
						rdir = 3;
						continue;
					}
					rdir =0;continue;
				}
				else if(map[rx][ry]==2) {
					if(rdir ==1 || rdir==3) {
						rdir = reverse_dir(rdir);
						continue;
					}
					if(rdir == 0) {
						rdir = 3;
						continue;
					}
					rdir =1;continue;
				}
				else if(map[rx][ry]==3) {
					if(rdir ==1 || rdir==2) {
						rdir = reverse_dir(rdir);
						continue;
					}
					if(rdir == 0) {
						rdir = 2;
						continue;
					}
					rdir =1;continue;
				}
				else if(map[rx][ry]==4) {
					if(rdir ==0 || rdir==2) {
						rdir = reverse_dir(rdir);continue;
					}
					if(rdir == 1) {
						rdir = 2;
						continue;
					}
					rdir =0;continue;
				}
			}	
		}
		return score;
	}
}
