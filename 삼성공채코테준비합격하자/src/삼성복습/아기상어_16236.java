package 삼성복습;

import java.io.*;
import java.util.Queue;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Comparator;
import java.awt.Point;
class fish{
	int x,y,dir;
	public fish(int x, int y, int dir) {
		this.x = x;
		this.y = y;
		this.dir = dir;
	}
}
public class 아기상어_16236 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};

	static int map[][];
	static int copy[][];
	static boolean visit[][];
	static int s_x,s_y;

	static Queue<Point> q_shark = new LinkedList<Point>();
	static ArrayList<fish> a_fish = new ArrayList<fish>();

	static int N;
	static int cur_size,eat_fish;
	static int ans;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine().trim());
		map = new int[N][N];
		copy = new int[N][N];
		for(int a=0;a<N;a++) {
			String in = br.readLine().trim();
			for(int b=0;b<N;b++) {
				int temp = Integer.parseInt(in.split(" ")[b]);
				if(temp !=9) {
					map[a][b]=temp;
					continue;
				}
				s_x = a;s_y = b;
			}
		}
		boolean temp=true;
		cur_size =2;
		eat_fish=0;
		ans=0;
		while(temp!=false) {
			visit = new boolean[N][N];
			copy = new int[N][N];
			a_fish = new ArrayList<fish>();
			q_shark.add(new Point(s_x, s_y));
			visit[s_x][s_y]=true;
			if(!check_bfs()) {
				break;
			}
			check_eating();
		}
		System.out.println(ans);
	}
	public static void check_eating() {
		if(a_fish.size()==1) {
			fish f = a_fish.remove(0);
			eat_fish++;
			if(eat_fish == cur_size) {
				cur_size++;
				eat_fish=0;
			}
			s_x = f.x;
			s_y = f.y;
			map[s_x][s_y]=0;
			ans = ans+f.dir;
			return;
		}
		sort();
		s_x = a_fish.get(0).x;
		s_y = a_fish.get(0).y;
		map[s_x][s_y]=0;
		ans = ans+a_fish.get(0).dir;
		eat_fish++;
		if(eat_fish == cur_size) {
			cur_size++;
			eat_fish=0;
		}
	}
	public static void sort() {
		a_fish.sort(new Comparator<fish>() {
			@Override
			public int compare(fish arg0, fish arg1) {
				int dir1 = arg0.dir;
				int dir2 = arg1.dir;
				if(dir1 > dir2) {
					return 1;
				}
				else if(dir1==dir2) {
					if(arg0.x>arg1.x) {
						return 1;
					}
					else if(arg0.x == arg1.x) {
						if(arg0.y > arg1.y) {
							return 1;
						}
						else {
							 return -1;
						}
					}
					else {
						return -1;
					}
				}
				else {
					return -1;
				}
			}
		});
	}
	public static boolean check_bfs() {
		while(!q_shark.isEmpty()) {
			Point p = q_shark.remove();
			int px = p.x;
			int py = p.y;
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx >= 0 && ry >= 0 && rx < N && ry < N) {
					if(visit[rx][ry]==false) {
						if(map[rx][ry]==cur_size) {
							copy[rx][ry]=copy[px][py]+1;
							visit[rx][ry]=true;
							q_shark.add(new Point(rx, ry));
						}
						else if(map[rx][ry]==0) {
							copy[rx][ry]=copy[px][py]+1;
							visit[rx][ry]=true;
							q_shark.add(new Point(rx, ry));
						}
						else if(map[rx][ry]<cur_size) {
							copy[rx][ry]=copy[px][py]+1;
							visit[rx][ry]=true;
							q_shark.add(new Point(rx, ry));
							a_fish.add(new fish(rx, ry, copy[rx][ry]));
						}
					}
				}
			}
		}
		if(a_fish.size()!=0) {
			return true;
		}
		return false;
	}
}