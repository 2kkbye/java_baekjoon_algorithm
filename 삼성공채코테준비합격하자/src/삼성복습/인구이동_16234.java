package 삼성복습;

import java.io.*;
import java.util.Queue;
import java.util.LinkedList;
import java.awt.Point;
public class 인구이동_16234 {
	static int dx[]= {-1,1,0,0};
	static int dy[] = {0,0,-1,1};

	static int map[][];
	static int copy[][];
	static boolean visit[][];
	static int N;
	static int L,R;
	static Queue<Point> q_popul = new LinkedList<Point>();
	static LinkedList<Point> l_contury = new LinkedList<Point>();

	static boolean flag;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		N = Integer.parseInt(in.split(" ")[0]);
		L = Integer.parseInt(in.split(" ")[1]);
		R = Integer.parseInt(in.split(" ")[2]);
		map = new int[N][N];
		for(int a=0;a<N;a++) {
			in = br.readLine().trim();
			for(int b=0;b<N;b++) {
				map[a][b] = Integer.parseInt(in.split(" ")[b]);
			}
		}
		flag = true;
		int ans=0;
		while(flag!=false) {
			flag = false;
			ans++;
			visit = new boolean[N][N];
			copy = new int[N][N];
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					if(visit[a][b]==true) continue;
					if(check(a,b)) {
						flag = true;
						visit[a][b]=true;
						q_popul.add(new Point(a, b));
						l_contury.add(new Point(a,b));
						bfs();
					}
				}
			}
		}
		System.out.println(ans-1);
	}
	public static boolean check(int rx,int ry) {
		for(int a=0;a<4;a++) {
			int px = rx+dx[a];
			int py = ry+dy[a];
			if(px >= 0 && py >= 0 && px < N && py < N) {
				if(copy[px][py]!=-1) {
					if(Math.abs((map[rx][ry]-map[px][py])) >= L && Math.abs((map[rx][ry]-map[px][py])) <= R) {
						return true;
					}
				}
			}
		}
		return false;
	}
	public static void bfs() {
		while(!q_popul.isEmpty()) {
			Point p = q_popul.remove();
			int px = p.x;
			int py = p.y;
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx >=0 && ry>=0 && rx<N && ry<N) {
					if(visit[rx][ry]==false) {
						int te =Math.abs(map[px][py]-map[rx][ry]); 
						if((te >= L) && (te<=R)) {
							visit[rx][ry]=true;
							l_contury.add(new Point(rx, ry));
							q_popul.add(new Point(rx, ry));
						}
					}
				}
			}
		}
		if(l_contury.size()==1) {
			return;
		}
		int sum =0;
		for(int a=0;a<l_contury.size();a++) {
			sum = map[l_contury.get(a).x][l_contury.get(a).y]+sum;
		}
		sum = sum/l_contury.size();
		for(int a=0;a<l_contury.size();a++) {
			copy[l_contury.get(a).x][l_contury.get(a).y] = -1;
			map[l_contury.get(a).x][l_contury.get(a).y] = sum;
		}
		l_contury.removeAll(l_contury);
	}
}
