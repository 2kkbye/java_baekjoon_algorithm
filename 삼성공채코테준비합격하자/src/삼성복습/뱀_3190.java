package 삼성복습;

import java.io.*;
import java.util.ArrayList;
import java.awt.Point;
public class 뱀_3190 {
	static int dx[]= {-1,0,1,0};
	static int dy[]= {0,1,0,-1};
	
	static int s_d;
	static int N;
	static int map[][];
	static boolean visit[][];
	static ArrayList<Point> q_snake = new ArrayList<Point>();
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine().trim());
		int K = Integer.parseInt(br.readLine().trim());
		map = new int[N+1][N+1];
		visit = new boolean[N+1][N+1];
		for(int a=0;a<K;a++) {
			String in = br.readLine().trim();
			int x = Integer.parseInt(in.split(" ")[0]);
			int y = Integer.parseInt(in.split(" ")[1]);
			map[x][y]=1;
		}
		int L = Integer.parseInt(br.readLine().trim());
		int time[] = new int[L];
		String dir[] = new String[L];
		for(int a=0;a<L;a++) {
			String in = br.readLine().trim();
			time[a] = Integer.parseInt(in.split(" ")[0]);
			dir[a] = String.valueOf(in.split(" ")[1]);
		}
		q_snake.add(new Point(1, 1));
		visit[1][1]=true;
		
		int index=0;
		int ans = 0;
		s_d = 1;
		for(int a=0;a<10000;a++) {
			if(index<dir.length) {
				if(time[index]==a) {
					if(dir[index].equals("D")) {
						s_d = s_d+1;
						if(s_d == 4) s_d=0;
					}
					else {
						s_d = s_d-1;
						if(s_d==-1) s_d=3;
					}
					index++;
				}
			}
			if(!check()) {
				ans = a;
				break;
			}
			continue;
		}
		System.out.println(ans+1);
	}
	public static boolean check() {
		int rx = q_snake.get(q_snake.size()-1).x;
		int ry = q_snake.get(q_snake.size()-1).y;
		rx = rx+dx[s_d];
		ry = ry+dy[s_d];
		if(rx >= (N+1) || ry >= (N+1) || rx<=0 || ry<=0) return false;
		if(visit[rx][ry]==true) return false;
		q_snake.add(new Point(rx, ry));
		visit[rx][ry]=true;
		if(map[rx][ry]==1) { //사과 있는경우
			map[rx][ry]=0;
		}
		else { //사과 없는경우
			Point p = q_snake.remove(0);
			visit[p.x][p.y]=false;
		}
		return true;
	}
}