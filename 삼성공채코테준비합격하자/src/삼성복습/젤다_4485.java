package �Ｚ����;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Comparator;
import java.awt.Point;
public class ����_4485 {

	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};
	static int map[][];
	static int N;
	static boolean visit[][];
	static ArrayList<Point> list = new ArrayList<Point>();
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int i=0;
		while(true) {
			i++;
			N=sc.nextInt();
			if(N ==0) break;
			map = new int[N][N];
			visit = new boolean[N][N];
			list = new ArrayList<Point>();
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					map[a][b] = sc.nextInt();
				}
			}
			list.add(new Point(0, 0));
			visit[0][0]=true;
			while(!list.isEmpty()) {
				//print();
				sort();
				int px = list.get(0).x;
				int py = list.get(0).y;
				list.remove(0);
				for(int a=0;a<4;a++) {
					int rx = px+dx[a];
					int ry = py+dy[a];
					if(rx < 0 || ry < 0 || rx>=N || ry>=N) continue;
					if(visit[rx][ry]==true) continue;
					visit[rx][ry]=true;
					map[rx][ry]= map[rx][ry]+map[px][py];
					list.add(new Point(rx, ry));
				}
			}
			System.out.println("Problem "+i+":"+" "+map[N-1][N-1]);
		}

	}
	public static void print() {
		System.out.println("------------------------");
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				System.out.print(map[a][b]+" ");
			}
			System.out.println();
		}
		System.out.println("------------------------");
	}
	public static void sort() {
		list.sort(new Comparator<Point>() {
			@Override
			public int compare(Point arg0, Point arg1) {
				if(map[arg0.x][arg0.y] == map[arg1.x][arg1.y]) {
					return 0;
				}
				else if(map[arg0.x][arg0.y]>map[arg1.x][arg1.y]) {
					return 1;
				}
				else {
					return -1;
				}
			}
		});
	}
}
