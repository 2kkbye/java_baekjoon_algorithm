package 삼성복습;

import java.util.Scanner;
import java.util.LinkedList;
import java.util.Queue;
import java.awt.Point;
public class 탈출_3055 {
	static int dx[]= {0,0,-1,1};
	static int dy[]= {-1,1,0,0};

	static boolean visit[][]=new boolean[51][51];
	static int water[][] = new int[51][51];
	static int go[][] = new int[51][51];

	static Queue<Point> q_water = new LinkedList<Point>();
	static Queue<Point> q_go = new LinkedList<Point>();
	static int R,C;
	static int anX,anY;
	static int wX,wY;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		R = sc.nextInt();
		C = sc.nextInt();
		String arr[][] = new String[R][1];
		for(int a=0;a<arr.length;a++) {
			arr[a][0]=sc.next();
		}
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(arr[a][0].charAt(b)=='.') { //평지
					continue;
				}
				else if(arr[a][0].charAt(b)=='X') { //벽돌-> 물, 두더지 못감
					water[a][b] = -2;
					go[a][b] = -2;
				}
				else if(arr[a][0].charAt(b)=='D') { //목적지
					water[a][b]=-1;
					anX=a;anY=b;
				}
				else if(arr[a][0].charAt(b)=='S') { //두더지 시작점
					q_go.add(new Point(a,b));
					visit[a][b] = true;
				}
				else if(arr[a][0].charAt(b)=='*') { //물 시작점
					q_water.add(new Point(a, b));
					wX=a;wY=b;
				}
			}
		}
		water();
		go();
		if(go[anX][anY]==0) {
			System.out.println("KAKTUS");
		}
		else System.out.println(go[anX][anY]);
	}
	public static void water() {
		while(!q_water.isEmpty()) {
			int px=q_water.peek().x;
			int py=q_water.peek().y;
			q_water.remove();
			for(int a=0;a<4;a++) {
				int rx=dx[a]+px;
				int ry=dy[a]+py;
				if(rx >= 0 && ry >= 0 && rx < R && ry < C) {
					if(water[rx][ry] == 0 && (rx != wX || ry !=  wY)) {
						water[rx][ry] = water[px][py]+1;
						q_water.add(new Point(rx,ry));
					}
				}
			}
		}
	}
	public static void go() {
		while(!q_go.isEmpty()) {
			int px=q_go.peek().x;
			int py=q_go.peek().y;
			q_go.remove();
			for(int a=0;a<4;a++) {
				int rx=dx[a]+px;
				int ry=dy[a]+py;
				if(rx >= 0 && ry >= 0 && rx < R && ry < C) {
					if(visit[rx][ry]==false) {
						if((water[rx][ry]-go[px][py]>=2) || (water[rx][ry]==-1||water[rx][ry]==0)) {
							go[rx][ry] = go[px][py]+1;
							visit[rx][ry]=true;
							q_go.add(new Point(rx,ry));
						}
					}
				}
			}
		}
	}
}