package �Ｚ����;

import java.io.*;

public class ����Ż��_13460 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};

	static int R,C;
	static int map[][];
	static int copy[][];

	static int dir[]=new int[10];
	static int ansX,ansY;
	static int rX,rY,bX,bY;
	static int t_rx,t_ry,t_bx,t_by;
	static boolean sw;
	static int ans;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		R = Integer.parseInt(in.split(" ")[0]);
		C = Integer.parseInt(in.split(" ")[1]);
		map = new int[R][C];
		copy = new int[R][C];
		for(int a=0;a<R;a++) {
			in = br.readLine().trim();
			for(int b=0;b<C;b++) {
				char c = in.charAt(b);
				if(c=='.')continue;
				else if(c=='#') {
					map[a][b] = 9;
				}
				else if(c=='O') {
					ansX = a;ansY=b;
					map[a][b]=-1;
				}
				else if(c=='R') {
					rX = a;
					rY = b;
					map[a][b]=1;
				}
				else {
					bX = a;
					bY = b;
					map[a][b]=2;
				}
			}
		}
		ans = 11;
		for(int a=0;a<4;a++) {
			dir[0]=a;
			dfs(1);
		}
		if(ans==11) {
			System.out.println(-1);
		}
		else {
			System.out.println(ans+1);
		}
	}
	public static void dfs(int level) {
		if(level==10) {
			copy();
			t_rx = rX;t_ry = rY;t_bx = bX;t_by = bY;
			sw=false;
			for(int a=0;a<10;a++) {
				if(!check(dir[a])) {
					if(sw) {
						break;
					}
				}
				else {
					ans = Math.min(ans, a);
				}
			}
			return;
		}
		for(int a=0;a<4;a++) {
			dir[level]=a;
			dfs(level+1);
		}
	}
	public static boolean check(int d) { //����  9������, ������ ansX,ansY�� ����
		int rx = t_rx,ry = t_ry;
		int bx = t_bx,by = t_by;
		boolean flagRB=false,flagBR=false; //��ġ����
		boolean flagR=false,flagB=false; //���ۿ� ������
		while(true) {
			rx = rx+dx[d];
			ry = ry+dy[d];
			if(copy[rx][ry]==9) {
				rx = rx-dx[d];
				ry = ry-dy[d];
				break;
			}
			if(copy[rx][ry]==-1) {
				flagR = true;
				break;
			}
			if(copy[rx][ry]==2) {
				flagRB = true;continue;
			}
		}
		while(true) {
			bx = bx+dx[d];
			by = by+dy[d];
			if(copy[bx][by]==9) {
				bx = bx-dx[d];
				by = by-dy[d];
				break;
			}
			if(copy[bx][by]==-1) {
				flagB = true;
				break;
			}
			if(copy[bx][by]==1) {
				flagBR = true;continue;
			}
		}
		if(flagB) {
			sw=true;
			return false;
		}
		if(flagR) {
			return true;
		}
		if(flagRB) {
			rx = rx-dx[d];
			ry = ry-dy[d];
		}
		if(flagBR) {
			bx = bx-dx[d];
			by = by-dy[d];
		}
		copy[t_rx][t_ry]=0;
		copy[rx][ry]=1;
		t_rx = rx;t_ry = ry;
		copy[t_bx][t_by]=0;
		copy[bx][by]=2;
		t_bx = bx;t_by = by;
		return false;
	}
	public static void print() {
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				System.out.print(map[a][b]+" ");
			}
			System.out.println();
		}
	}
	public static void copy() {
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				copy[a][b] = map[a][b];
			}
		}
	}
}