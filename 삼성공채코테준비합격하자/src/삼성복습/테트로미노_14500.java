package 삼성복습;

import java.io.*;
public class 테트로미노_14500 {

	static int R,C;
	static int map[][];
	
	static int dx1[]= {0,1,2,3};
	static int dy1[]= {0,0,0,0};
	static int dx1_2[]= {0,0,0,0};
	static int dy1_2[]= {0,1,2,3};
	
	static int dx2[]= {0,0,1,1};
	static int dy2[]= {0,1,0,1};

	static int dx3[]= {0,1,2,2};
	static int dy3[]= {0,0,0,1};
	static int dx3_2[]= {0,0,0,1};
	static int dy3_2[]= {0,1,2,0};
	static int dx3_3[]= {0,0,1,2};
	static int dy3_3[]= {0,1,1,1};
	static int dx3_4[]= {0,0,0,-1};
	static int dy3_4[]= {0,1,2,2};
	static int dx3_5[]= {0,1,2,2};
	static int dy3_5[]= {0,0,0,-1};
	static int dx3_6[]= {0,0,0,1};
	static int dy3_6[]= {0,1,2,2};
	static int dx3_7[]= {0,0,1,2};
	static int dy3_7[]= {0,1,0,0};
	static int dx3_8[]= {0,1,1,1};
	static int dy3_8[]= {0,0,1,2};
	
	static int dx4[]= {0,1,1,2};
	static int dy4[]= {0,0,1,1};
	static int dx4_2[]= {0,0,-1,-1};
	static int dy4_2[]= {0,1,1,2};
	static int dx4_3[]= {0,1,1,2};
	static int dy4_3[]= {0,0,-1,-1};
	static int dx4_4[]= {0,0,1,1};
	static int dy4_4[]= {0,1,1,2};
	
	static int dx5[]= {0,0,0,1};
	static int dy5[]= {0,1,2,1};
	static int dx5_2[]= {0,1,2,1};
	static int dy5_2[]= {0,0,0,-1};
	static int dx5_3[]= {0,1,1,1};
	static int dy5_3[]= {0,-1,0,1};
	static int dx5_4[]= {0,1,2,1};
	static int dy5_4[]= {0,0,0,1};
	
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		R = Integer.parseInt(in.split(" ")[0]);
		C = Integer.parseInt(in.split(" ")[1]);
		map = new int[R][C];
		for(int a=0;a<R;a++) {
			in = br.readLine().trim();
			for(int b=0;b<C;b++) {
				map[a][b] = Integer.parseInt(in.split(" ")[b]);
			}
		}
		int ans = -1;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				ans = Math.max(ans, check(a,b,dx1,dy1));
				ans = Math.max(ans, check(a,b,dx1_2,dy1_2));
				ans = Math.max(ans, check(a,b,dx2,dy2));
				ans = Math.max(ans, check(a,b,dx3,dy3));
				ans = Math.max(ans, check(a,b,dx3_2,dy3_2));
				ans = Math.max(ans, check(a,b,dx3_3,dy3_3));
				ans = Math.max(ans, check(a,b,dx3_4,dy3_4));
				ans = Math.max(ans, check(a,b,dx3_5,dy3_5));
				ans = Math.max(ans, check(a,b,dx3_6,dy3_6));
				ans = Math.max(ans, check(a,b,dx3_7,dy3_7));
				ans = Math.max(ans, check(a,b,dx3_8,dy3_8));
				ans = Math.max(ans, check(a,b,dx4,dy4));
				ans = Math.max(ans, check(a,b,dx4_2,dy4_2));
				ans = Math.max(ans, check(a,b,dx4_3,dy4_3));
				ans = Math.max(ans, check(a,b,dx4_4,dy4_4));
				ans = Math.max(ans, check(a,b,dx5,dy5));
				ans = Math.max(ans, check(a,b,dx5_2,dy5_2));
				ans = Math.max(ans, check(a,b,dx5_3,dy5_3));
				ans = Math.max(ans, check(a,b,dx5_4,dy5_4));	
			}
		}
		System.out.println(ans);
	}
	public static int check(int x, int y, int tx[],int ty[]) {
		int rx = x,ry=y;
		int sum = 0;
		for(int a=0;a<4;a++) {
			rx = x+tx[a];
			ry = y+ty[a];
			if(rx <0 || ry<0 || rx>=R || ry>=C) return 0;
			sum = sum+map[rx][ry];
			
		}
		
		
		return sum;
	}
}