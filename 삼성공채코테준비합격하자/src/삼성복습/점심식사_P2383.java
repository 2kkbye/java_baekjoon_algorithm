package 삼성복습;

import java.io.*;
import java.util.ArrayList;
import java.util.Queue;
import java.util.LinkedList;
class person{
	int x,y,dis,time,des;
	public person(int x, int y, int dis, int time,int des) {
		this.x = x;
		this.y = y;
		this.dis = dis;
		this.time = time;
		this.des = des;
	}
}
class stair{
	int x, y, time;
	public stair(int x, int y, int time) {
		this.x = x;
		this.y = y;
		this.time = time;
	}
}
public class 점심식사_P2383 {

	static ArrayList<person> a_person = new ArrayList<person>();
	static ArrayList<person> a_person_temp = new ArrayList<person>();
	static ArrayList<stair> a_stair = new ArrayList<stair>();

	static Queue<person> q_stair_1 = new LinkedList<person>();
	static Queue<person> q_stair_2 = new LinkedList<person>();
	static Queue<Integer> q_complete = new LinkedList<Integer>();
	static int ans;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine().trim());
		for(int i=1;i<=T;i++) {
			a_person = new ArrayList<person>();
			a_stair = new ArrayList<stair>();

			q_stair_1 = new LinkedList<person>();
			q_stair_2 = new LinkedList<person>();
			int N = Integer.parseInt(br.readLine().trim());
			for(int a=0;a<N;a++) {
				String in = br.readLine().trim();
				for(int b=0;b<N;b++) {
					int temp = Integer.parseInt(in.split(" ")[b]);
					if(temp ==0) continue;
					if(temp == 1) {
						a_person.add(new person(a, b, -1, -1,-1));
					}
					else {
						a_stair.add(new stair(a, b, temp));
					}
				}
			}
			ans = Integer.MAX_VALUE;
			for(int a=0;a<2;a++) {
				int t_dis = des_dis(0, a);
				a_person.get(0).dis = t_dis;
				a_person.get(0).time = a_stair.get(a).time;
				a_person.get(0).des = a;
				dfs(1);
			}
			System.out.println("#"+i+" "+(ans));
		}
	}
	public static void dfs(int level) {
		if(level == a_person.size()) {
			copy();
			check();
			return;
		}
		for(int a=0;a<2;a++) {
			int t_dis = des_dis(level, a);
			a_person.get(level).dis = t_dis;
			a_person.get(level).time = a_stair.get(a).time;
			a_person.get(level).des = a;
			dfs(level+1);
		}
	}
	public static void check() {
		for(int a=0;a<100000;a++) {
			down_stair();
			for(int b=0;b<a_person_temp.size();b++) {
				person p = a_person_temp.get(b);
				if(p.dis < a) {
					if(p.des==0) {
						if(q_stair_1.size()>=3) continue;
						p.time--;
						q_stair_1.add(p);
						a_person_temp.remove(b);
						b=b-1;
					}
					else {
						if(q_stair_2.size()>=3) continue;
						p.time--;
						q_stair_2.add(p);
						a_person_temp.remove(b);
						b=b-1;
					}
				}
				if(p.dis == a) {
					if(p.des==0) {
						if(q_stair_1.size()>=3) continue;
						q_stair_1.add(p);
						a_person_temp.remove(b);
						b=b-1;
					}
					else {
						if(q_stair_2.size()>=3) continue;
						q_stair_2.add(p);
						a_person_temp.remove(b);
						b=b-1;
					}
				}
			}
			if(q_complete.size() == a_person.size()) {
				ans = Math.min(ans, a);
				break;
			}
		}
	}
	public static void down_stair() {
		int size = q_stair_1.size();
		for(int a=0;a<size;a++) {
			person p = q_stair_1.remove();
			p.time--;
			if(p.time==-1) {
				q_complete.add(1);
				continue;
			}
			q_stair_1.add(new person(p.x, p.y, p.dis, p.time, p.des));
		}

		size = q_stair_2.size();
		for(int a=0;a<size;a++) {
			person p = q_stair_2.remove();
			p.time--;
			if(p.time==-1) {
				q_complete.add(1);
				continue;
			}
			q_stair_2.add(new person(p.x, p.y, p.dis, p.time, p.des));
		}
	}
	public static void copy() {
		q_complete = new LinkedList<Integer>();
		a_person_temp = new ArrayList<person>();
		for(int a=0;a<a_person.size();a++) {
			person p = a_person.get(a);
			a_person_temp.add(new person(p.x, p.y, p.dis, p.time, p.des));
		}
	}
	public static int des_dis(int p_in,int d_in) {
		int dis = Math.abs(a_person.get(p_in).x - a_stair.get(d_in).x) +Math.abs(a_person.get(p_in).y - a_stair.get(d_in).y);
		return dis;
	}
}