package 삼성복습;

import java.io.*;
import java.util.ArrayList;
class virus{
	int x, y, d, value;
	public virus(int x, int y, int d, int value) {
		this.x = x;
		this.y = y;
		this.d = d;
		this.value = value;
	}
}
public class 미생물격리_P2382 {
	static int dx[]= {0,-1,1,0,0};
	static int dy[]= {0,0,0,-1,1};
	static int N;
	static ArrayList<virus> a_virus = new ArrayList<virus>();
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine().trim());
		for(int i=1;i<=T;i++) {
			String in = br.readLine().trim();
			N = Integer.parseInt(in.split(" ")[0]);
			int M = Integer.parseInt(in.split(" ")[1]);
			int K = Integer.parseInt(in.split(" ")[2]);
			a_virus = new ArrayList<virus>();
			for(int a=0;a<K;a++) {
				in = br.readLine().trim();
				int x = Integer.parseInt(in.split(" ")[0]);
				int y = Integer.parseInt(in.split(" ")[1]);
				int num = Integer.parseInt(in.split(" ")[2]);
				int dir = Integer.parseInt(in.split(" ")[3]);
				a_virus.add(new virus(x, y, dir, num));
			}
			for(int a=0;a<M;a++) {
				virus_move();
				check();
			}
			int sum = 0;
			for(int a=0;a<a_virus.size();a++) {
				sum = sum+a_virus.get(a).value;
			}
			System.out.println("#"+(i)+" "+sum);
		}
	}
	public static void check() {
		ArrayList<Integer> temp = new ArrayList<Integer>();
		boolean flag = false;
		for(int a=0;a<a_virus.size();a++) {
			temp = new ArrayList<Integer>();
			int max_d = a_virus.get(a).d;
			int max_num = a_virus.get(a).value;
			int sum_num = a_virus.get(a).value;
			for(int b=a+1;b<a_virus.size();b++) {
				if(a_virus.get(a).x == a_virus.get(b).x && a_virus.get(a).y == a_virus.get(b).y) {
					flag = true;
					temp.add(b);
					sum_num = sum_num+a_virus.get(b).value;
					if(max_num<a_virus.get(b).value) {
						max_d = a_virus.get(b).d;
						max_num = a_virus.get(b).value;
					}
				}
			}
			if(flag) {
				for(int b=temp.size()-1;b>=0;b--) {
					int index = temp.get(b);
					a_virus.remove(index);
				}
				a_virus.get(a).d = max_d;
				a_virus.get(a).value = sum_num;
			}
		}
	}
	public static void virus_move() {
		for(int a=0;a<a_virus.size();a++) {
			virus v = a_virus.get(a);
			v.x = v.x+dx[v.d];
			v.y = v.y+dy[v.d];
			if(v.x ==0 || v.y == 0 || v.x == N-1 || v.y == N-1) {
				v.value = v.value/2;
				if(v.value==0) {
					a_virus.remove(a);
					a--;
					continue;
				}
				if(v.d%2==1) {
					v.d = v.d+1;
				}
				else {
					v.d = v.d-1;
				}
			}
		}
	}
}