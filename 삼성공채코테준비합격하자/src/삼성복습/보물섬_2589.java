package �Ｚ����;

import java.util.Queue;
import java.util.LinkedList;
import java.awt.Point;
import java.io.*;
public class ������_2589 {
	static int R,C;
	
	static int dx[] = {-1,1,0,0};
	static int dy[] = {0,0,-1,1};
	static int map[][];
	static int bomul[][];
	static boolean visit[][];
	
	static Queue<Point> q_find = new LinkedList<Point>();
	static int ans;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		R = Integer.parseInt(in.split(" ")[0]);
		C = Integer.parseInt(in.split(" ")[1]);
		
		map = new int[R][C];
		bomul = new int[R][C];
		visit = new boolean[R][C];
		
		for(int a=0;a<R;a++) {
			in = br.readLine().trim();
			for(int b=0;b<C;b++) {
				if(in.charAt(b)=='W') continue;
				map[a][b]=1;
			}
		}
		ans=-1;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(map[a][b]==0) continue;
				bomul = new int[R][C];
				visit = new boolean[R][C];
				visit[a][b]=true;
				q_find.add(new Point(a, b));
				bfs();
			}
		}
		System.out.println(ans);
	}
	public static void bfs() {
		while(!q_find.isEmpty()) {
			Point p = q_find.remove();
			int px = p.x;
			int py = p.y;
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx >= 0 &&  rx < R && ry >= 0 && ry < C) {
					if(visit[rx][ry]==false && map[rx][ry]==1) {
						visit[rx][ry]=true;
						q_find.add(new Point(rx, ry));
						bomul[rx][ry]= bomul[px][py]+1;
						ans = Math.max(ans, bomul[rx][ry]);
					}
				}
			}
		}
	}
}