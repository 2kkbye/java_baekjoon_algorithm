package �Ｚ����;

import java.io.*;
public class �ֻ���������_14499 {

	static int dx[]= {0,0,0,-1,1};
	static int dy[]= {0,1,-1,0,0};
	static int N,M,sX,sY;
	static int map[][];
	static int cube[] = new int[6];
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();

		N = Integer.parseInt(in.split(" ")[0]);
		M = Integer.parseInt(in.split(" ")[1]);
		sX = Integer.parseInt(in.split(" ")[2]);
		sY = Integer.parseInt(in.split(" ")[3]);
		int K = Integer.parseInt(in.split(" ")[4]);
		map = new int[N][M];
		for(int a=0;a<N;a++) {
			in = br.readLine().trim();
			for(int b=0;b<M;b++) {
				map[a][b] = Integer.parseInt(in.split(" ")[b]);
			}
		}
		in = br.readLine();
		for(int a=0;a<K;a++) {
			int dir = Integer.parseInt(in.split(" ")[a]);
			if(!check(dir)) continue;
			move_cube(dir);
			check_map();
			System.out.println(cube[1]);
		}
	}
	public static void check_map() {
		if(map[sX][sY]==0) {
			map[sX][sY] = cube[3];
		}
		else {
			cube[3] = map[sX][sY];
			map[sX][sY]=0;
		}
	}
	public static boolean check(int index) {
		int rx = sX+dx[index];
		int ry = sY+dy[index];
		if(rx < 0 || ry<0 || rx>=N || ry>=M) {
			return false;
		}
		sX = rx;
		sY = ry;
		return true;
	}
	public static void move_cube(int index) {
		int temp;
		if(index==1) {
			int num1 = cube[1];
			int num3 = cube[3];
			int num4 = cube[4];
			int num5 = cube[5];
			cube[1] = num4;
			cube[5] = num1;
			cube[3] = num5;
			cube[4] = num3;
		}
		else if(index==2) {
			int num1 = cube[1];
			int num3 = cube[3];
			int num4 = cube[4];
			int num5 = cube[5];
			cube[4] = num1;
			cube[1] = num5;
			cube[5] = num3;
			cube[3] = num4;
		}
		else if(index==3) {
			temp = cube[0];
			for(int a=0;a<3;a++) {
				cube[a] = cube[a+1];
			}
			cube[3]=temp;
		}
		else if(index==4) {
			temp = cube[3];
			for(int a=3;a>=1;a--) {
				cube[a]=cube[a-1];
			}
			cube[0] = temp;
		}
	}
}