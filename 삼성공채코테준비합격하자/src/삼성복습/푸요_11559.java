package �Ｚ����;

import java.io.*;
import java.util.Queue;
import java.util.LinkedList;
import java.awt.Point;
public class Ǫ��_11559 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};
	static int map[][] = new int[12][6];
	static boolean visit[][];
	static int R=12,C=6;
	static int cnt;
	static int temp;
	static Queue<Point> q_block = new LinkedList<Point>();
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		for(int a=0;a<R;a++) {
			String in = br.readLine().trim();
			for(int b=0;b<C;b++) { //R�� ����, G�� �ʷ�, B�� �Ķ�, P�� ����, Y�� ���
				if(in.charAt(b)=='.') continue;
				if(in.charAt(b)=='R') {
					map[a][b]=1;
				}
				else if(in.charAt(b)=='G') {
					map[a][b]=2;
				}
				else if(in.charAt(b)=='B') {
					map[a][b]=3;
				}
				else if(in.charAt(b)=='P') {
					map[a][b]=4;
				}
				else if(in.charAt(b)=='Y') {
					map[a][b]=5;
				}
			}
		}
		temp = 1;
		int ans =0;
		while(temp!=0) {
			temp=0;
			visit = new boolean[R][C];
			for(int a=0;a<R;a++) {
				for(int b=0;b<C;b++) {
					if(map[a][b]==0) continue;
					cnt=1;
					q_block.add(new Point(a, b));
					visit[a][b]=true;
					dfs(a,b);
					if(!q_block.isEmpty()) q_block.remove();
				}
			}
			down();
			ans++;
		}
		System.out.println(ans-1);
	}
	public static void down() {
		int tmp;
		for(int b=0;b<C;b++) {
			tmp=0;
			for(int a=R-1;a>=0;a--) {
				if(map[a][b]==0) {
					tmp++;
					continue;
				}
				if(map[a][b]!=0 && tmp ==0) continue;
				if(map[a][b]!=0 && tmp!=0) {
					map[a+tmp][b] = map[a][b];
					map[a][b]=0;
					a=a+tmp;tmp =0;continue;
				}
			}
		}
	}
	public static void dfs(int x, int y) {
		if(cnt==4) { //bfs
			temp=1;
			bfs(map[x][y]);
			return;
		}
		for(int a=0;a<4;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx>=0 && ry>=0 && rx<R && ry<C) {
				if(visit[rx][ry]==false) {
					if(map[rx][ry]==map[x][y]) {
						cnt++;
						visit[rx][ry]=true;
						dfs(rx,ry);
					}
				}
			}
		}
	}
	public static void bfs(int num) {
		while(!q_block.isEmpty()) {
			Point p = q_block.remove();
			int px = p.x;
			int py = p.y;
			map[px][py]=0;
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx>=0 && ry>=0 && rx<R && ry<C) {
					if(map[rx][ry]==num) {
						map[rx][ry]=0;
						q_block.add(new Point(rx, ry));
					}
				}
			}
		}
	}
}