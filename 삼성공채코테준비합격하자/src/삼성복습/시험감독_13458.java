package 삼성복습;

import java.util.Scanner;
public class 시험감독_13458 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		int room[] = new int[N];
		for(int i=0;i<N;i++) {
			room[i] = sc.nextInt();
		}
		int B = sc.nextInt();
		int C = sc.nextInt();
		long ans=0;
		for(int i=0;i<N;i++) {
			
			ans++;
			if(room[i]-B<=0) {
				continue;
			}
			ans = ans+((room[i]-B)/C);
			if( ((room[i]-B)%C)!=0) ans++;
		}
		System.out.println(ans);
	}
}
