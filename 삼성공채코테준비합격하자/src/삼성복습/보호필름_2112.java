package 삼성복습;

import java.io.*;

public class 보호필름_2112 {
	
	static int map[][];
	static int copy[][];
	static int visit[];
	static int R,C,K;
	static int max_level;
	static int ans;
	static boolean result;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine().trim());
		for(int i=1;i<=T;i++) {
			String in = br.readLine().trim();
			R = Integer.parseInt(in.split(" ")[0]);
			C = Integer.parseInt(in.split(" ")[1]);
			K = Integer.parseInt(in.split(" ")[2]);
			map = new int[R][C];
			copy = new int[R][C];
			
			for(int a=0;a<R;a++) {
				in = br.readLine().trim();
				for(int b=0;b<C;b++) {
					int temp = Integer.parseInt(in.split(" ")[b]);
					map[a][b] = temp;
					copy[a][b] = temp;
				}
			}
			if(check()) {
				System.out.println("#"+i+" 0");
				continue;
			}
			else {
				visit = new int[R];
				result = false;
				for(int c=1;c<=R;c++) {
					max_level = c;
					for(int a=0;a<R;a++) {
						for(int b=1;b<3;b++) {
							visit[a]=b;
							dfs(a,1);
							visit[a]=0;
						}
					}
				}
				System.out.println("#"+i+" "+ans);
			}
		}
	}
	public static void dfs(int index,int level) {
		if(result == true) {
			return;
		}
		if(level == max_level) {
			copy();
			draw_cell();
			if(check()) {
				result= true;
				ans = level;
			}
			return;
		}
		for(int a=index+1;a<R;a++) {
			for(int b=1;b<3;b++) {
				visit[a]=b;
				dfs(a,level+1);
				visit[a]=0;
			}
		}
	}
	public static void draw_cell() {
		for(int a=0;a<visit.length;a++) {
			if(visit[a]==0) continue;
			int temp;
			if(visit[a]==1) {
				temp = 0;
			}
			else {
				temp = 1;
			}
			for(int b=0;b<C;b++) {
				copy[a][b]= temp;
			}
		}
	}
	public static void copy() {
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				copy[a][b]=map[a][b];
			}
		}
	}
	public static boolean check() {
		int num;
		int count=0;
		boolean flag = false;
		for(int a=0;a<C;a++) {
			num = copy[0][a];
			flag = false;
			count=1;
			for(int b=1;b<R;b++) {
				if(num ==copy[b][a]) {
					count++;
				}
				else {
					num = copy[b][a];
					count=1;
					continue;
				}
				if(count==K) {
					flag = true;
					break;
				}
				
			}
			if(!flag) return false;
		}
		return true;
	}

}