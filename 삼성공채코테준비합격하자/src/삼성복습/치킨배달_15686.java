package 삼성복습;

import java.io.*;
import java.util.ArrayList;
import java.awt.Point;
public class 치킨배달_15686 {

	static int map[][];
	static int dir[][];
	static int N,M;
	static ArrayList<Point> a_chicken = new ArrayList<Point>();
	static ArrayList<Point> a_temp = new ArrayList<Point>();
	static int ans;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		N = Integer.parseInt(in.split(" ")[0]);
		M = Integer.parseInt(in.split(" ")[1]);
		map = new int[N][N];
		for(int a=0;a<N;a++) {
			in = br.readLine().trim();
			for(int b=0;b<N;b++) {
				int temp = Integer.parseInt(in.split(" ")[b]);
				if(temp == 2) {
					a_chicken.add(new Point(a, b));
				}
				map[a][b] = temp;
			}
		}
		ans = Integer.MAX_VALUE;
		for(int a=0;a<a_chicken.size();a++) {
			a_temp.add(a_chicken.get(a));
			dfs(a,1);
			a_temp.remove(0);
		}
		System.out.println(ans);
	}
	public static void check() {
		dir = new int[N][N];
		for(int i=0;i<a_temp.size();i++) {
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					if(map[a][b]==0) continue;
					if(map[a][b]==2) continue;
					int number = check_dir(a,b,a_temp.get(i).x,a_temp.get(i).y);
					if(dir[a][b] == 0) {
						dir[a][b]=number;
					}
					else {
						if(dir[a][b]>number) {
							dir[a][b]=number;
						}
					}
				}
			}
		}
		int sum=0;
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(dir[a][b]==0) continue;
				sum = sum+dir[a][b];
			}
		}
		ans = Math.min(ans, sum);
	}
	public static int check_dir(int x, int y, int rx, int ry) {
		int result= Math.abs(x-rx) + Math.abs(y-ry);
		return result;
	}
	public static void dfs(int index,int level) {
		if(level ==M) {
			check();
			return;
		}
		for(int a=index+1;a<a_chicken.size();a++) {
			a_temp.add(a_chicken.get(a));
			dfs(a,level+1);
			a_temp.remove(a_temp.size()-1);
		}
	}
}