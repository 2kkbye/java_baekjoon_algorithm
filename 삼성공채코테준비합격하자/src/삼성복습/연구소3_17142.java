package 삼성복습;

import java.util.ArrayList;
import java.io.*;
import java.awt.Point;
import java.util.Queue;
import java.util.LinkedList;
public class 연구소3_17142 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};
	
	static int map[][];
	static int copy[][];
	static boolean visit[][];
	static int N,M;
	static boolean vir[];
	
	static ArrayList<Point> a_virus = new ArrayList<Point>();
	static Queue<Point> q_virus = new LinkedList<Point>();
	static int ans;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		N = Integer.parseInt(in.split(" ")[0]);
		M = Integer.parseInt(in.split(" ")[1]);
		map = new int[N][N];
		for(int a=0;a<N;a++) {
			in = br.readLine().trim();
			for(int b=0;b<N;b++) {
				int temp = Integer.parseInt(in.split(" ")[b]);
				if(temp == 1) {
					map[a][b]=-1;
				}
				if(temp == 2) {
					map[a][b]=2;
					a_virus.add(new Point(a, b));
					continue;
				}
			}
		}
		ans = Integer.MAX_VALUE;
		vir = new boolean[a_virus.size()];
		dfs(0,0);
		if(ans == Integer.MAX_VALUE) {
			System.out.println(-1);
		}
		else {
			System.out.println(ans);
		}
		
	}
	public static void dfs(int index, int level) {
		if(level == M) {
			init();
			bfs();
			result();
			return;
		}
		for(int a=index;a<a_virus.size();a++) {
			vir[a]=true;
			dfs(a+1,level+1);
			vir[a]=false;
		}
	}
	public static void bfs() {
		while(!q_virus.isEmpty()) {
			Point p = q_virus.remove();
			int px = p.x;
			int py = p.y;
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx>=0 && ry>=0 && rx<N && ry<N) {
					if(visit[rx][ry]==false && copy[rx][ry]!=-1) {
						visit[rx][ry] = true;
						copy[rx][ry]= copy[px][py] + 1;
						q_virus.add(new Point(rx, ry));
					}
				}
			}
		}
	}
	public static void result() {
		int max = 0;
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(copy[a][b]==-1) continue;
				if(map[a][b]==2) continue;
				if(visit[a][b]==false) return;
				max = Math.max(max, copy[a][b]);
			}
		}
		ans = Math.min(ans, max);
	}
	public static void init() {
		visit = new boolean[N][N];
		copy = new int[N][N];
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(map[a][b]==2) {
					continue;
				}
				copy[a][b] = map[a][b];
			}
		}
		for(int a=0;a<vir.length;a++) {
			if(vir[a]==true) {
				int rx = a_virus.get(a).x;
				int ry = a_virus.get(a).y;
				visit[rx][ry]=true;
				q_virus.add(new Point(rx, ry));
			}
		}
	}
}