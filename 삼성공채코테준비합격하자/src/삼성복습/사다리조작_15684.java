package 삼성복습;

import java.io.*;
public class 사다리조작_15684 {
	static int map[][];
	static int N,M,H;
	static int start;
	static int ans;
	static int sw;
	static int arr[][];
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		N = Integer.parseInt(in.split(" ")[0]);
		M = Integer.parseInt(in.split(" ")[1]);
		H = Integer.parseInt(in.split(" ")[2]);
		map = new int[H+2][N*2];
		for(int a=0;a<M;a++) {
			in = br.readLine().trim();
			int x = Integer.parseInt(in.split(" ")[0]);
			int y = Integer.parseInt(in.split(" ")[1]);
			map[x][y*2]=1;
		}
		ans = 4;
		check(0);
		for(int a=1;a<H+1;a++) {
			for(int b=2;b<N*2;b=b+2) {
				if(map[a][b]==1) continue;
				map[a][b]=1;
				check(1);
				dfs(a,2);
				map[a][b]=0;
			}
		}
		if(ans == 4) {
			System.out.println(-1);
		}
		else {
			System.out.println(ans);
		}
	}
	public static void check(int level) {
		boolean temp = true;
		for(int b=1;b<N*2;b=b+2) {
			start = b;
			int x = 0;int y=b;
			while(true) {
				if(x==H+1) {
					break;
				}
				if(map[x][y-1]==1) {
					x = x+1;
					y = y-2;
				}
				else if((y+1<N*2)&&map[x][y+1]==1) {
					x = x+1;
					y = y+2;
				}
				else {
					x=x+1;
				}
			}
			if(y!=start){
				temp = false;
				break;
			}
		}
		if(temp==true) {
			ans = Math.min(ans, level);
		}
	}
	public static void dfs(int x, int level) {
		if(level == 4) {
			return;
		}
		if(ans<level) {
			return;
		}
		for(int a=x;a<H+1;a++) {
			for(int b=2;b<N*2;b=b+2) {
				if(map[a][b]==1) continue;
				map[a][b]=1;
				check(level);
				dfs(a,level+1);
				map[a][b]=0;
			}
		}
	}
}