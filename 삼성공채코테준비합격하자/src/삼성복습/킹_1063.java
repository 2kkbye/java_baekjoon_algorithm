package 삼성복습;

import java.io.*;
public class 킹_1063 {
	static int dx[]= {-1,1,0,0,-1,-1,1,1};
	static int dy[]= {0,0,-1,1,1,-1,1,-1};
	static int map[][] = new int[9][9];
	static int k_x,k_y,b_x,b_y;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		change_xy(in.split(" ")[0],0);
		change_xy(in.split(" ")[1],1);
		map[k_x][k_y]=1;
		map[b_x][b_y]=2;
		int T = Integer.parseInt(in.split(" ")[2]);
		for(int i=0;i<T;i++) {
			int index=-1;
			in = br.readLine().trim();
			if(in.equals("T")) { //상
				index = 0;
			}
			else if(in.equals("B")) { //하
				index = 1;
			}
			else if(in.equals("L")) {//좌
				index = 2;
			}
			else if(in.equals("R")) {//우
				index = 3;
			}
			else if(in.equals("RT")) {//오른대각선
				index = 4;
			}
			else if(in.equals("LT")) {//왼대각선
				index = 5;
			}
			else if(in.equals("RB")) {//오른아래대각선
				index = 6;
			}
			else if(in.equals("LB")) {//왼아래대각선
				index = 7;
			}
			if(check(index)) { //검은돌도 같이 움직임
				move(b_x,b_y,index,0);
				move(k_x,k_y,index,1);
			}
			else { //킹만 움직임
				move(k_x,k_y,index,1);
			}
		}
		System.out.println(((char)(k_y+64))+""+(9-k_x));
		System.out.println(((char)(b_y+64))+""+(9-b_x));
	}
	public static void move(int x, int y, int dir,int index) {
		if(index==0) { //검은돌
			x=x+dx[dir];
			y=y+dy[dir];
			if(x>=1 && y>=1 && x<9 && y<9) {
				map[x][y]=2;
				map[b_x][b_y]=0;
				b_x = x;b_y=y;
			}
		}
		if(index==1) { //킹
			x=x+dx[dir];
			y=y+dy[dir];
			if(x>=1 && y>=1 && x<9 && y<9) {
				if(map[x][y]!=2) {
					map[x][y]=1;
					map[k_x][k_y]=0;
					k_x=x;k_y=y;
				}
			}
		}
	}
	public static boolean check(int index) {
		int rx =k_x;
		int ry = k_y;
		for(int a=0;a<1;a++) {
			rx = rx+dx[index];
			ry = ry+dy[index];			
			if(rx<1 || ry<1 || rx >=9 || ry>=9) {
				return false;
			}
			if(map[rx][ry]==2) {
				return true;
			}
		}
		return false;
	}
	public static void change_xy(String s,int index) {
		int x = 8-((int)s.charAt(1)-48)+1;
		int y = (int)s.charAt(0)-64;
		if(index==0) {
			k_x = x;
			k_y= y;
			return;
		}
		b_x=x;
		b_y=y;
	}
}