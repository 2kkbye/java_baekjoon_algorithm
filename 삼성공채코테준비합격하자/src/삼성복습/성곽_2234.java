package 삼성복습;

import java.util.Scanner;
import java.util.ArrayList;
public class 성곽_2234 {
	static int dx[] = {1,0,-1,0}; //남동북서
	static int dy[] = {0,1,0,-1};
	static int map[][];
	static int new_map[][];
	static boolean visit[][];
	static int total_castel_count=0; //사용
	static int max_castel_count=0; //사용
	static int write_count=0; //사용
	static int max_ans=-1; //사용
	static int final_ans = -1;
	static int N,M;
	static ArrayList<Integer> list = new ArrayList<Integer>();
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		M = sc.nextInt(); //열
		N = sc.nextInt(); //행
		map = new int[N][M];
		new_map = new int[N][M];
		visit = new boolean[N][M];
		for(int a=0;a<N;a++) {
			for(int b=0;b<M;b++) {
				map[a][b] = sc.nextInt();
			}
		}
		for(int a=0;a<N;a++) {
			for(int b=0;b<M;b++) {
				if(visit[a][b]==true) continue;
				max_castel_count=0;
				total_castel_count++;
				write_count++;
				dfs(a,b);
				//System.out.println(max_ans);
				max_ans = Math.max(max_ans, max_castel_count);
			}
		}
		//print();
		visit = new boolean[N][M];
		final_ans = -1;
		for(int a=0;a<N;a++) {
			for(int b=0;b<M;b++) {
				if(visit[a][b]==true) continue;
				list = new ArrayList<Integer>();
				list.add(new_map[a][b]);
				check(a,b);
				double_castel();
			}
		}
		System.out.println(total_castel_count);
		System.out.println(max_ans);
		System.out.println(final_ans);
	}
	public static void double_castel() {
		int max = 0;
		int temp =0;
		for(int a=0;a<N;a++) {
			for(int b=0;b<M;b++) {
				if(list.get(0)==new_map[a][b]) {
					max++;
				}
			}
		}
		for(int c=1;c<list.size();c++) {
			temp=0;
			for(int a=0;a<N;a++) {
				for(int b=0;b<M;b++) {
					if(list.get(c)==new_map[a][b]) {
						temp++;
					}
				}
			}
			final_ans = Math.max(final_ans, max+temp);
		}
	}
	public static boolean check_list(int x) {
		for(int a=0;a<list.size();a++) {
			if(list.get(a)==x) {
				return false;
			}
		}
		return true;
	}
	public static void check(int x, int y) {
		visit[x][y]=true;
		for(int a=0;a<4;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx < 0 || ry < 0 || rx >=N || ry >=M ) continue;
			if(new_map[rx][ry]!=new_map[x][y]) {
				if(check_list(new_map[rx][ry])) {
					list.add(new_map[rx][ry]);
				}
				continue;
			}
			if(visit[rx][ry]==true) continue;
			if(new_map[rx][ry]==new_map[x][y]) {
				check(rx,ry);
			}
		}
	}
	public static void dfs(int x, int y) {
		max_castel_count++;
		new_map[x][y] = write_count;
		visit[x][y] = true;
		String s = changeToBinary(map[x][y]);
		for(int a=0;a<4;a++) {
			if(s.charAt(a)=='1') continue;
			if(((x+dx[a]) < 0 )||((x+dx[a]) >= N ) || ((y+dy[a]) < 0 ) || ((y+dy[a]) >= M )) continue;
			if(visit[x+dx[a]][y+dy[a]]==true) continue;
			dfs(x+dx[a],y+dy[a]);
		}

	}
	public static String changeToBinary(int x) {
		String b = Integer.toBinaryString(x);
		if(b.length()<4) {
			int temp = b.length();
			for(int c=0;c<4-temp;c++) {
				b="0"+b;
			}
		}
		return b;
	}
}