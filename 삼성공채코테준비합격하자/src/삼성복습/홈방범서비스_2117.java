package 삼성복습;

import java.io.*;
public class 홈방범서비스_2117 {

	static int N,M;
	static int map[][];
	static boolean visit[][];
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine().trim());
		for(int i=1;i<=T;i++) {
			String in = br.readLine().trim();
			N = Integer.parseInt(in.split(" ")[0]);
			M = Integer.parseInt(in.split(" ")[1]);
			map = new int[N][N];
			visit = new boolean[N][N];
			for(int a=0;a<N;a++) {
				in = br.readLine().trim();
				for(int b=0;b<N;b++) {
					map[a][b]=Integer.parseInt(in.split(" ")[b]);
				}
			}
			int ans = 1;
			for(int c=0;c<50;c++) { //운영 영역
				for(int a=0;a<N;a++) {
					for(int b=0;b<N;b++) {
						visit = new boolean[N][N];
						int cost = c*c+(c-1)*(c-1);
						check(a, b, c);
						int cont = count();
						if(cont*M -cost >=0) {
							ans = Math.max(ans, cont);
						}
					}
				}
			}
			System.out.println("#"+i+" "+ans);
		}
	}
	public static int count() {
		int cnt=0;
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(visit[a][b]==false) continue;
				if(map[a][b]==0) continue;
				cnt++;
			}
		}
		return cnt;
	}
	public static void check(int x, int y, int size) {
		int CL =y,CR=y;
		int center = x;
		for(int a=x-(size-1);a<=x+(size-1);a++) {
			for(int b=CL;b<=CR;b++) {
				if(a>=0 && b>=0 && a<N && b<N) {
					visit[a][b]=true;
				}
			}
			if(a<x) {
				CL--;
				CR++;
			}
			else {
				CL++;
				CR--;
			}
		}
	}
}