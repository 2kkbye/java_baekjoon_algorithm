package 삼성복습;

import java.io.*;
import java.util.Queue;
import java.util.LinkedList;
import java.awt.Point;
import java.util.ArrayList;
public class 연구소_14502 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};

	static int map[][];
	static int copy[][];
	static boolean visit[][];
	static int R,C;
	static Queue<Point> q_virus = new LinkedList<Point>();
	static ArrayList<Point> a_virus = new ArrayList<Point>();
	static int ans;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		R = Integer.parseInt(in.split(" ")[0]);
		C = Integer.parseInt(in.split(" ")[1]);
		map = new int[R][C];
		visit = new boolean[R][C];
		copy = new int[R][C];
		for(int a=0;a<R;a++) {
			in = br.readLine().trim();
			for(int b=0;b<C;b++) {
				map[a][b] = Integer.parseInt(in.split(" ")[b]);
				if(map[a][b]==2) a_virus.add(new Point(a, b));
			}
		}
		ans = -1;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(map[a][b]!=0) continue;
				map[a][b]=1;
				dfs(a,1);
				map[a][b]=0;
			}
		}
		System.out.println(ans);
	}
	public static void dfs(int x,int level) {
		if(level==3) {
			visit = new boolean[R][C];
			copy();
			copy_virus();
			bfs();
			count_safe();
			return;
		}
		for(int a=x;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(map[a][b]!=0) continue;
				map[a][b]=1;
				dfs(a,level+1);
				map[a][b]=0;
			}
		}
	}
	public static void bfs() {
		while(!q_virus.isEmpty()) {
			Point p = q_virus.remove();
			int px = p.x;
			int py = p.y;
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx >= 0 && ry >= 0 && rx < R && ry < C) {
					if(copy[rx][ry]!=1 && visit[rx][ry]==false) {
						copy[rx][ry]=2;
						visit[rx][ry]=true;
						q_virus.add(new Point(rx, ry));
					}
				}
			}
		}
	}
	public static void count_safe() {
		int count=0;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(copy[a][b]!=0) continue;
				count++;
			}
		}
		ans = Math.max(ans, count);
	}
	public static void copy_virus() {
		for(int a=0;a<a_virus.size();a++) {
			visit[a_virus.get(a).x][a_virus.get(a).y]=true;
			q_virus.add(new Point(a_virus.get(a).x, a_virus.get(a).y));
		}
	}
	public static void copy() {
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				copy[a][b]=map[a][b];
			}
		}
	}

}
