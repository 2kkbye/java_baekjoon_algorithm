package �Ｚ����;

import java.io.*;
import java.util.LinkedList;
import java.util.Queue;
import java.awt.Point;
public class ��_5427 {

	static int N,M;
	static int fire[][];
	static int person[][];
	static int startX,startY;
	static boolean visit[][];
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};
	static int map[][];
	static Queue<Point> q_fire = new LinkedList<Point>();
	static Queue<Point> q_person = new LinkedList<Point>();
	static int ans;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());
		for(int i=1;i<=T;i++) {
			String in = br.readLine();
			M = Integer.parseInt(in.split(" ")[0]);
			N = Integer.parseInt(in.split(" ")[1]);
			fire = new int[N][M];
			person = new int[N][M];
			visit = new boolean[N][M];
			map = new int [N][M];
			for(int a=0;a<N;a++) {
				in = br.readLine();
				for(int b=0;b<M;b++) {
					if(in.charAt(b)=='.') continue;
					if(in.charAt(b)=='#') {
						fire[a][b] = -1;
						person[a][b] = -1;
						continue;
					}
					if(in.charAt(b)=='@') {
						q_person.add(new Point(a, b));
						startX =a;startY=b;
						continue;
					}
					if(in.charAt(b)=='*') {
						map[a][b]=-3;
						q_fire.add(new Point(a, b));
					}
				}
			}
			ans = Integer.MAX_VALUE;
			fire();
			person();
			if(startX==0 || startX ==N-1 || startY ==0 || startY==M-1) {
				ans = Math.min(ans, 0);
			}
			if(ans == Integer.MAX_VALUE) {
				System.out.println("IMPOSSIBLE");
			}
			else {
				System.out.println(ans+1);
			}
		}
	}
	public static void person() {
		while(!q_person.isEmpty()) {
			Point p = q_person.remove();
			int px = p.x;
			int py = p.y;
			for(int i=0;i<4;i++) {
				int rx = px+dx[i];
				int ry = py+dy[i];
				if(rx>=0 && ry>=0 && rx<N && ry<M) {
					if(rx!=startX || ry!=startY) {
						if(person[rx][ry]!=-1 && person[rx][ry]==0) {
							if(map[rx][ry]!=-3) {
								if(fire[rx][ry] - person[px][py]>=2 || fire[rx][ry]==0) {
									person[rx][ry]=person[px][py]+1;
									if(rx==0 || rx ==N-1 || ry ==0 || ry==M-1) {
										ans = Math.min(ans, person[rx][ry]);
									}
									q_person.add(new Point(rx, ry));
								}
							}
						}
					}
				}
			}
		}
	}
	public static void fire() {
		while(!q_fire.isEmpty()) {
			Point p = q_fire.remove();
			int px = p.x;
			int py = p.y;
			visit[px][py]=true;
			for(int i=0;i<4;i++) {
				int rx = px+dx[i];
				int ry = py+dy[i];
				if(rx>=0 && ry>=0 && rx<N && ry<M) {
					if(visit[rx][ry]==false) {
						if(fire[rx][ry]!=-1 && fire[rx][ry]==0) {
							fire[rx][ry]= fire[px][py]+1;
							visit[rx][ry]=true;
							q_fire.add(new Point(rx, ry));
						}
					}
				}
			}
		}
	}
}
