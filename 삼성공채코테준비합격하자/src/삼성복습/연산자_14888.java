package 삼성복습;

import java.io.*;
public class 연산자_14888 {

	static int N;
	static int ins[];
	static int total[];
	static boolean visit[];
	static int max,min;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		N = Integer.parseInt(in);
		ins = new int[N-1];
		visit = new boolean[N-1];

		N = N+(N-1);
		total = new int[N];
		in = br.readLine().trim();
		int index = 0;
		for(int a=0;a<total.length;a=a+2) {
			total[a] = Integer.parseInt(in.split(" ")[index]);
			index++;
		}
		index = 0;
		in = br.readLine().trim();
		for(int a=0;a<4;a++) {
			int temp = Integer.parseInt(in.split(" ")[a]);
			for(int b=0;b<temp;b++) {
				ins[index] = a;
				index++;
			}
		}
		max = Integer.MIN_VALUE;
		min = Integer.MAX_VALUE;
		for(int a=0;a<ins.length;a++) {
			if(visit[a]==false) {
				total[1] = ins[a];
				visit[a]=true;
				dfs(3);
				visit[a]=false;
			}
		}
		System.out.println(max);
		System.out.println(min);
	}
	public static void check() {
		int num = total[0];
		for(int a=1;a<total.length;a=a+2) {
			if(total[a]==0) { //더하기
				num = num+total[a+1];
			}
			else if(total[a]==1) {
				num = num-total[a+1];
			}
			else if(total[a]==2) {
				num = num*total[a+1];
			}
			else if(total[a]==3) {
				if(num<0) {
					num=num*-1;
					num = num/total[a+1];
					num = num*-1;
				}
				else {
					num = num/total[a+1];
				}
			}
		}
		max = Integer.max(max, num);
		min = Integer.min(min, num);
	}
	public static void dfs(int level) {
		if(level >=N) {
			check();
			return;
		}
		for(int a=0;a<ins.length;a++) {
			if(visit[a]==false) {
				total[level] = ins[a];
				visit[a]=true;
				dfs(level+2);
				visit[a]=false;
			}
		}
	}
}
