package 삼성복습;

import java.util.Queue;
import java.util.LinkedList;
import java.io.*;
import java.awt.Point;
public class 다리만들기_2146 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};

	static int N;
	static int map[][];
	static int temp_map[][];
	static boolean visit[][];
	static Queue<Point> q_check = new LinkedList<Point>();
	static int ans;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine().trim());

		map = new int[N][N];
		visit = new boolean[N][N];
		for(int a=0;a<N;a++) {
			String in = br.readLine().trim();
			for(int b=0;b<N;b++) {
				map[a][b] = Integer.parseInt(in.split(" ")[b]);
			}
		}
		int temp_count=1;
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(visit[a][b]==false && map[a][b]==1) {
					visit[a][b]=true;
					q_check.add(new Point(a, b));
					map[a][b]=temp_count;
					bfs_insert(temp_count);
					temp_count++;
				}
			}
		}
		ans = Integer.MAX_VALUE;
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(map[a][b]==0) continue;
				if(!check(a,b)) continue;
				temp_map= new int[N][N];
				visit = new boolean[N][N];
				visit[a][b]=true;
				q_check.add(new Point(a, b));
				bfs_distance(map[a][b]);
			}
		}
		System.out.println(ans);
	}
	public static void bfs_distance(int num) {
		while(!q_check.isEmpty()) {
			Point p = q_check.remove();
			int px = p.x;
			int py = p.y;
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx>=0 && ry>=0 && rx<N && ry<N) {
					if(visit[rx][ry]==false) {
						if(map[rx][ry]==num) continue;
						if(map[rx][ry]==0) {
							visit[rx][ry]=true;
							q_check.add(new Point(rx, ry));
							temp_map[rx][ry] = temp_map[px][py]+1;
						}
						else if(map[rx][ry]!=num) {
							ans = Math.min(ans, temp_map[px][py]);
						}
					}
				}
			}
		}
	}
	public static boolean check(int x, int y) {
		for(int a=0;a<4;a++) {
			int rx = x+dx[a];
			int ry = y+dy[a];
			if(rx>=0 && ry>=0 && rx<N && ry<N) {
				if(map[rx][ry]==0) {
					return true;
				}
			}
		}
		return false;
	}
	public static void bfs_insert(int num) {
		while(!q_check.isEmpty()) {
			Point p = q_check.remove();
			int px = p.x;
			int py = p.y;
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx>=0 && ry>=0 && rx<N && ry<N) {
					if(visit[rx][ry]==false && map[rx][ry]==1) {
						map[rx][ry]=num;
						visit[rx][ry]=true;
						q_check.add(new Point(rx, ry));
					}
				}
			}
		}
	}
}