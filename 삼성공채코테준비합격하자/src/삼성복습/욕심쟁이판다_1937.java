package �Ｚ����;

import java.util.Scanner;
public class ��������Ǵ�_1937 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};

	static int N;
	static int panda[][];
	static int dp[][];
	public static void main(String[] args)  {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		panda = new int[N][N];
		dp = new int[N][N];

		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				panda[a][b] = sc.nextInt();
			}
		}
		int ans=-1;
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(dp[a][b]==0) {
					ans = Math.max(ans, dfs(a,b));
				}
			}
		}
		System.out.println(ans);
		sc.close();
	}
	public static int dfs(int x, int y) {
		if(dp[x][y]!=0) {
			return dp[x][y];
		}
		int day =1;
		for(int i=0;i<4;i++) {
			int rx = x+dx[i];
			int ry = y+dy[i];
			if(rx >= 0 && ry >= 0 && rx < N && ry < N) {
				if(panda[x][y]<panda[rx][ry]) {
					day = Math.max(day, dfs(rx,ry)+1);
					dp[x][y]=day;
				}
			}
		}
		return day;
	}
}