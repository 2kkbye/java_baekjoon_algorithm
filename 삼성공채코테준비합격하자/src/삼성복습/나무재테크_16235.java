package 삼성복습;

import java.io.*;
import java.util.Queue;
import java.util.LinkedList;
class tree{
	int x,y,age;
	public tree(int x, int y, int age) {
		this.x = x;
		this.y = y;
		this.age = age;
	}
}
public class 나무재테크_16235 {
	static int dx[]= {-1,1,0,0,-1,-1,1,1};
	static int dy[]= {0,0,-1,1,-1,1,-1,1};
	static int total_map[][];
	static int add_map[][];
	static int N;
	static Queue<tree> q_new = new LinkedList<tree>();
	static Queue<tree> q_temp = new LinkedList<tree>();
	static Queue<tree> q_death = new LinkedList<tree>();
	static Queue<tree> q_spring = new LinkedList<tree>();
	static Queue<tree> q_fall_temp = new LinkedList<tree>();
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		N = Integer.parseInt(in.split(" ")[0]);
		int M = Integer.parseInt(in.split(" ")[1]);
		int K = Integer.parseInt(in.split(" ")[2]);
		total_map = new int[N][N];
		add_map = new int[N][N];
		for(int a=0;a<N;a++) {
			in = br.readLine().trim();
			for(int b=0;b<N;b++) {
				add_map[a][b] = Integer.parseInt(in.split(" ")[b]);
				total_map[a][b]=5;
			}
		}
		for(int a=0;a<M;a++) {
			in = br.readLine().trim();
			int x = Integer.parseInt(in.split(" ")[0]);
			int y = Integer.parseInt(in.split(" ")[1]);
			int age = Integer.parseInt(in.split(" ")[2]);
			q_spring.add(new tree(x-1, y-1, age));
		}
		for(int a=0;a<K;a++) {
			spring();
			summer();
			fall();
			winter();
			copy();
		}
		System.out.println(q_spring.size());
	}
	public static void spring() {
		
		while(!q_spring.isEmpty()) {
			//System.out.println("a");
			tree t = q_spring.remove();
			if((total_map[t.x][t.y] - t.age) >=0) {
				total_map[t.x][t.y] = total_map[t.x][t.y]-t.age;
				t.age++;
				q_temp.add(t);
			}
			else {
				q_death.add(t);
			}
		}
	}
	public static void summer() {
		while(!q_death.isEmpty()) {
			tree t = q_death.remove();
			total_map[t.x][t.y] = total_map[t.x][t.y]+(t.age)/2;
		}
	}
	public static void fall() {
		while(!q_temp.isEmpty()) {
			tree t = q_temp.remove();
			q_fall_temp.add(t);
			if(t.age%5==0) {
				int px = t.x;
				int py = t.y;
				for(int a=0;a<8;a++) {
					int rx = px+dx[a];
					int ry = py+dy[a];
					if(rx >= 0 && ry >= 0 && rx < N && ry < N) {
						q_new.add(new tree(rx, ry, 1));
					}
				}
			}
		}
	}
	public static void winter() {
		for(int a=0;a<N;a++){
			for(int b=0;b<N;b++) {
				total_map[a][b] = total_map[a][b]+add_map[a][b];
			}
		}
	}
	public static void copy() {
		while(!q_new.isEmpty()) {
			tree t = q_new.remove();
			q_spring.add(t);
		}
		while(!q_fall_temp.isEmpty()) {
			tree t = q_fall_temp.remove();
			q_spring.add(t);
		}

	}
}
