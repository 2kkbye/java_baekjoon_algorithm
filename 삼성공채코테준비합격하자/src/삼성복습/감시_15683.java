package �Ｚ����;

import java.io.*;
import java.util.ArrayList;
class CCTV{
	int x, y,dir;
	public CCTV(int x, int y, int dir) {
		this.x = x;
		this.y = y;
		this.dir = dir;
	}
}
public class ����_15683 {

	static int dx[]= {-1,0,1,0};
	static int dy[]= {0,1,0,-1};
	static int map[][];
	static int copy[][];
	static int N,M;
	static int ans;
	static ArrayList<CCTV> a_cctv = new ArrayList<CCTV>();
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		N = Integer.parseInt(in.split(" ")[0]);
		M = Integer.parseInt(in.split(" ")[1]);
		map = new int[N][M];
		copy = new int[N][M];
		for(int a=0;a<N;a++) {
			in = br.readLine().trim();
			for(int b=0;b<M;b++) {
				int temp = Integer.parseInt(in.split(" ")[b]);
				if(temp >=1 && temp<=5) {
					a_cctv.add(new CCTV(a, b, -1));
				}
				map[a][b] = temp; 
			}
		}
		ans =Integer.MAX_VALUE;
		dfs(0);
		System.out.println(ans);
	}
	public static void count_ans() {
		int temp=0;
		for(int a=0;a<N;a++) {
			for(int b=0;b<M;b++) {
				if(copy[a][b]!=0) continue;
				temp++;
			}
		}
		ans = Math.min(ans, temp);
	}
	public static void check() {
		for(int a=0;a<a_cctv.size();a++) {
			int px = a_cctv.get(a).x;
			int py = a_cctv.get(a).y;
			int dir = a_cctv.get(a).dir;
			if(map[px][py]==1) {
				spread(px,py,dir);
			}
			else if(map[px][py]==2) {
				if(dir == 0 ) {
					spread(px,py,1);
					spread(px,py,3);
				}
				else {
					spread(px,py,0);
					spread(px,py,2);
				}
			}
			else if(map[px][py]==3) {
				for(int b=dir;b<=dir+1;b++) {
					int temp = b;
					if(b==4) temp = 0;
					spread(px,py,temp);
				}
			}
			else if(map[px][py]==4) {
				for(int b=dir-1;b<=dir+1;b++) {
					int temp = b;
					if(b<0) {
						temp = 3;
					}
					if(b>3) {
						temp = 0;
					}
					spread(px,py,temp);
				}
			}
			else if(map[px][py]==5) {
				for(int b=0;b<4;b++) {
					spread(px,py,b);
					spread(px,py,b);
					spread(px,py,b);
					spread(px,py,b);
				}
			}
		}
	}
	public static void spread(int x, int y, int dir) {
		int rx = x;
		int ry = y;
		while(true) {
			rx = rx+dx[dir];
			ry = ry+dy[dir];
			if(rx >=0 && ry>=0 && rx<N && ry<M) {
				if(copy[rx][ry]==6) {
					break;
				}
				if(copy[rx][ry]>=1 && copy[rx][ry]<=5) continue;
				copy[rx][ry]=8;
			}
			else { 
				break;
			}
		}
	}
	public static void dfs(int level) {
		if(level == a_cctv.size()) {
			copy();
			check();
			count_ans();
			return;
		}
		int px = a_cctv.get(level).x;
		int py = a_cctv.get(level).y;
		if(map[px][py]==1 || map[px][py]==3 || map[px][py]==4) {
			for(int a=0;a<4;a++) {
				a_cctv.get(level).dir=a;
				dfs(level+1);
			}
		}
		else if(map[px][py]==2) {
			for(int a=0;a<2;a++) {
				a_cctv.get(level).dir=a;
				dfs(level+1);
			}
		}
		else if(map[px][py]==5) {
			a_cctv.get(level).dir=1;
			dfs(level+1);
		}
	}
	public static void copy() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<M;b++) {
				copy[a][b]=map[a][b];
			}
		}
	}

}
