package 삼성복습;

import java.io.*;
public class EASY_2048 {
	
	static int N;
	static int map[][];
	static int copy[][];
	static int ins[] = new int[5];
	static int ans;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine().trim());
		map = new int[N][N];
		copy = new int[N][N];
		
		for(int a=0;a<N;a++) {
			String in = br.readLine();
			for(int b=0;b<N;b++) {
				map[a][b] = Integer.parseInt(in.split(" ")[b]);
			}
		}
		ans = -1;
		for(int a=0;a<4;a++) {
			ins[0] = a;
			dfs(1);
		}
		System.out.println(ans);
	}
	public static void dfs(int level) {
		if(level ==5) {
			copy();
			for(int a=0;a<5;a++) {
				move(ins[a]);
				sum(ins[a]);
				move(ins[a]);
			}
			result();
			return;
		}
		for(int a=0;a<4;a++) {
			ins[level] = a;
			dfs(level+1);
		}
	}
	public static void result() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(copy[a][b]==0) continue;
				ans = Math.max(ans, copy[a][b]);
			}
		}
	}
	public static void copy() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				copy[a][b] = map[a][b];
			}
		}
	}
	public static void sum(int index) {
		if(index==0) { //위 기준
			for(int a=0;a<N;a++) {
				for(int b=0;b<N-1;b++) {
					if(copy[b][a]==copy[b+1][a]) {
						copy[b][a] = copy[b][a]*2;
						copy[b+1][a]=0;
					}
				}
			}
		}
		else if(index==1) { //오른쪽기준
			for(int a=0;a<N;a++) {
				for(int b=N-1;b>0;b--) {
					if(copy[a][b]==copy[a][b-1]) {
						copy[a][b] = copy[a][b]*2;
						copy[a][b-1]=0;
					}
				}
			}
		}
		else if(index==2) { //아래기준
			for(int a=0;a<N;a++) {
				for(int b=N-1;b>0;b--) {
					if(copy[b][a]==copy[b-1][a]) {
						copy[b][a] = copy[b][a]*2;
						copy[b-1][a]=0;
					}
				}
			}
		}
		else if(index==3) { //왼쪽기준
			for(int a=0;a<N;a++) {
				for(int b=0;b<N-1;b++) {
					if(copy[a][b]==copy[a][b+1]) {
						copy[a][b] = copy[a][b]*2;
						copy[a][b+1]=0;
					}
				}
			}
		}
	}
	public static void move(int index) {
		int temp;
		if(index==0) { //다 위로
			for(int a=0;a<N;a++) {
				temp=0;
				for(int b=0;b<N;b++) {
					if(copy[b][a]==0) {
						temp++;continue;
					}
					if(temp==0) continue;
					copy[b-temp][a] = copy[b][a];
					copy[b][a]=0; b=b-temp;
					temp=0;
				}
			}
		}
		else if(index==1) { //다 오른쪽으로
			for(int a=0;a<N;a++) {
				temp=0;
				for(int b=N-1;b>=0;b--) {
					if(copy[a][b]==0) {
						temp++;
						continue;
					}
					if(temp==0) continue;
					copy[a][b+temp] = copy[a][b];
					copy[a][b]=0;b=b+temp;
					temp=0;
				}
			}
		}
		else if(index==2) { //다 밑으로
			for(int a=0;a<N;a++) {
				temp=0;
				for(int b=N-1;b>=0;b--) {
					if(copy[b][a]==0) {
						temp++;continue;
					}
					if(temp==0) continue;
					copy[b+temp][a] = copy[b][a];
					copy[b][a]=0; b=b+temp;
					temp=0;
				}
			}
		}
		else if(index==3) { //다 왼쪽으로
			for(int a=0;a<N;a++) {
				temp=0;
				for(int b=0;b<N;b++) {
					if(copy[a][b]==0) {
						temp++;
						continue;
					}
					if(temp==0) continue;
					copy[a][b-temp] = copy[a][b];
					copy[a][b]=0;b=b-temp;
					temp=0;
				}
			}
		}
	}
}