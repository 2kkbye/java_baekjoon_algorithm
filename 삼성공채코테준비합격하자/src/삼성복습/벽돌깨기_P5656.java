package �Ｚ����;

import java.io.*;
import java.util.Queue;
import java.util.LinkedList;
import java.awt.Point;
public class ��������_P5656 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};
	static int map[][];
	static int copy[][];
	static int index[];

	static int N;
	static int R,C;
	static int ans;
	static Queue<Point> q_num = new LinkedList<Point>();
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine().trim());
		for(int i=1;i<=T;i++) {
			String in = br.readLine().trim();
			N =Integer.parseInt(in.split(" ")[0]);
			C = Integer.parseInt(in.split(" ")[1]);
			R = Integer.parseInt(in.split(" ")[2]);
			map = new int[R][C];
			copy = new int[R][C];
			index = new int[N];
			for(int a=0;a<R;a++) {
				in = br.readLine().trim();
				for(int b=0;b<C;b++) {
					map[a][b] = Integer.parseInt(in.split(" ")[b]);
				}
			}
			ans = Integer.MAX_VALUE;
			for(int a=0;a<C;a++) {
				index[0]=a;
				dfs(1);
			}
			System.out.println("#"+i+" "+ans);
		}
	}
	public static void dfs(int level) {
		if(level == N) {
			copy();
			for(int a=0;a<index.length;a++) {
				check(index[a]);
				down();
				//print();
			}
			ans = Math.min(ans, count());
			return;
		}
		for(int a=0;a<C;a++) {
			index[level]=a;
			dfs(level+1);
		}
	}
	public static void check(int index) {
		for(int a=0;a<R;a++) {
			if(copy[a][index] ==0 ) continue;
			if(copy[a][index]==1) {
				copy[a][index]=0;
				return;
			}
			else {
				q_num.add(new Point(a, index));
				while(!q_num.isEmpty()) {
					Point p = q_num.remove();
					int px = p.x;
					int py = p.y;
					int num = copy[px][py];
					copy[px][py]=0;
					erase_num(px,py,num);
				}
				break;
			}
		}
	}
	public static void erase_num(int x, int y,int num) {
		for(int a=0;a<4;a++) {
			int rx = x,ry = y;
			for(int b=1;b<num;b++) {
				rx = rx+dx[a];
				ry = ry+dy[a];
				if(rx>=0 && ry>=0 && rx<R && ry<C) {
					if(copy[rx][ry]==0) continue;
					if(copy[rx][ry]==1) {
						copy[rx][ry]=0;
						continue;
					}
					q_num.add(new Point(rx, ry));
				}
			}
		}
	}
	public static void print() {
		System.out.println("--------------------------------");
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				System.out.print(copy[a][b]);
			}
			System.out.println();
		}
	}
	public static void down() {
		for(int a=0;a<C;a++) {
			int temp=0;
			for(int b=R-1;b>=0;b--) {
				if(copy[b][a]==0) {
					temp++;
					continue;
				}
				if(copy[b][a]!=0 && temp==0) continue;
				copy[b+temp][a] = copy[b][a];
				copy[b][a]=0;
				b=b+temp;temp=0;continue;

			}
		}
	}
	public static int count() {
		int cnt=0;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(copy[a][b]==0) continue;
				cnt++;
			}
		}
		return cnt;
	}
	public static void copy() {
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				copy[a][b]= map[a][b];
			}
		}
	}
}