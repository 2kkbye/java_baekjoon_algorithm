package �Ｚ����;

import java.io.*;
public class ���_14501 {

	static int N;
	static int time[];
	static int value[];
	static int ans;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine().trim());
		time = new int[N+1];
		value = new int[N+1];
		for(int a=1;a<=N;a++) {
			String in = br.readLine().trim();
			time[a] = Integer.parseInt(in.split(" ")[0]);
			value[a] = Integer.parseInt(in.split(" ")[1]);
		}
		ans = -1;
		for(int a=1;a<=N;a++) {
			if(a+time[a]>(N+1)) {
				dfs(a+1,0);
			}
			else {
				dfs(a+time[a],value[a]);
			}
		}
		System.out.println(ans);
	}
	public static void dfs(int x,int sum) {
		if(x>=(N+1)) {
			ans = Math.max(ans, sum);
			return;
		}
		for(int a = x;a<=N;a++) {
			if(a+time[a]>(N+1)) {
				dfs(a+1,sum);
			}
			else {
				dfs(a+time[a],sum+value[a]);
			}
		}
	}
}