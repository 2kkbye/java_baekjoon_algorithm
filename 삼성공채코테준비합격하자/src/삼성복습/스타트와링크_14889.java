package 삼성복습;

import java.io.*;
import java.util.ArrayList;
public class 스타트와링크_14889 {

	static int N;
	static int ans;
	static int map[][];
	static int team[];
	static ArrayList<Integer> team_a = new ArrayList<Integer>();
	static ArrayList<Integer> team_b = new ArrayList<Integer>();
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		map = new int[N][N];
		team = new int[N];
		for(int a=0;a<N;a++) {
			String in = br.readLine().trim();
			for(int b=0;b<N;b++) {
				map[a][b] = Integer.parseInt(in.split(" ")[b]);
			}
		}
		ans = Integer.MAX_VALUE;
		for(int a=0;a<=(N/2);a++) {
			team[a]=1;
			dfs(a,1);
			team[a]=0;
		}
		System.out.println(ans);
	}
	public static void dfs(int x,int level) {
		if(level == N/2) {
			check();
			return;
		}
		for(int a=x+1;a<=((N/2)+level);a++) {
			team[a]=1;
			dfs(a,level+1);
			team[a]=0;
		}
	}
	public static void check() {
		team_a = new ArrayList<Integer>();
		team_b = new ArrayList<Integer>();
		for(int a=0;a<team.length;a++) {
			if(team[a]==1) team_a.add(a);
			else team_b.add(a);
		}
		int team_a_sum=0;
		int team_b_sum=0;
		for(int a=0;a<team_a.size();a++) {
			for(int b=a+1;b<team_a.size();b++) {
				int rx = team_a.get(a);
				int ry = team_a.get(b);
				int px = team_b.get(a);
				int py = team_b.get(b);
				team_a_sum = team_a_sum + map[rx][ry];
				team_a_sum = team_a_sum + map[ry][rx];
				team_b_sum = team_b_sum + map[px][py];
				team_b_sum = team_b_sum + map[py][px];
			}
		}
		ans = Math.min(ans, Math.abs(team_a_sum-team_b_sum));
	}
}