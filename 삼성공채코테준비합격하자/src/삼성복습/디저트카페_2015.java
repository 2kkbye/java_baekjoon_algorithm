package 삼성복습;

import java.io.*;
import java.util.ArrayList;
public class 디저트카페_2015 {
	static int dx[]= {1,1,-1,-1};
	static int dy[]= {1,-1,-1,1};

	static int staX,staY;
	static int N;
	static int map[][];
	static boolean visit[][];
	static int ans;
	static ArrayList<Integer> a_num = new ArrayList<Integer>();
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine().trim());
		String in;
		for(int i=1;i<=T;i++) {
			N = Integer.parseInt(br.readLine().trim());
			a_num = new ArrayList<Integer>();
			map = new int[N][N];
			visit = new boolean[N][N];
			for(int a=0;a<N;a++) {
				in = br.readLine().trim();
				for(int b=0;b<N;b++) {
					map[a][b] = Integer.parseInt(in.split(" ")[b]);
				}
			}
			ans  = -1;
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					staX = a; staY=b;
					int px = a+dx[0];
					int py = b+dy[0];
					if(px<0 || py<0 || px>=N || py>=N) continue;
					visit[px][py]=true;
					a_num.add(map[a][b]);
					dfs(px,py,0);
					a_num.remove(a_num.size()-1);
					visit[px][py]=false;
				}
			}
			System.out.println("#"+i+" "+ans);
		}
	}
	public static void dfs(int x, int y,int index) {
		if(x==staX && y==staY) {
			if(check()) {
				ans = Math.max(ans, a_num.size());
			}
			return;
		}
		for(int a=index;a<4;a++) {
			if(a>index+1) {
				continue;
			}
			int px = x+dx[a];
			int py = y+dy[a];
			if(px<0 || py<0 || px>=N || py>=N) continue;
			if(visit[px][py]==true) continue;
			visit[px][py]=true;
			a_num.add(map[x][y]);
			dfs(px,py,a);
			a_num.remove(a_num.size()-1);
			visit[px][py]=false;
		}
	}
	public static boolean check() {
		for(int a=0;a<a_num.size();a++) {
			for(int b=a+1;b<a_num.size();b++) {
				if(a_num.get(a)==a_num.get(b)) {
					return false;
				}
			}
		}
		
		return true;
	}
}