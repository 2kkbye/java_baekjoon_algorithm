package 삼성복습;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
public class 보물상자_P5658 {

	static ArrayList<Integer> a_change = new ArrayList<Integer>();
	static ArrayList<String> a_num = new ArrayList<String>();
	static ArrayList<Character> a_st = new ArrayList<Character>();
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine().trim());
		for(int i=1;i<=T;i++) {
			a_change = new ArrayList<Integer>();
			a_num = new ArrayList<String>();
			a_st = new ArrayList<Character>();

			String in = br.readLine();
			int N = Integer.parseInt(in.split(" ")[0]);
			int K = Integer.parseInt(in.split(" ")[1]);
			String num = br.readLine().trim();
			for(int a=0;a<N;a++) {
				a_st.add(num.charAt(a));
			}
			for(int a=0;a<N/4;a++) {
				String temp="";
				for(int b=0;b<N;b++) {
					if(((b+1)%(N/4))==0) {
						temp=temp+a_st.get(b);
						if(check(temp)) {
							a_num.add(temp);
						}
						temp="";
						continue;
					}
					temp=temp+a_st.get(b);
				}
				char c = a_st.remove(a_st.size()-1);
				a_st.add(0,c);
			}
			//System.out.println(a_num);
			changeToTen();
			sort();
			System.out.println("#"+i+" "+a_change.get(K-1));
		}
	}
	public static int changeCh(char tt) {
		int temp=0;
		if(tt>='A' && tt<='F') {
			if(tt=='A') temp = 10;
			else if(tt=='B') temp = 11;
			else if(tt=='C') temp =12;
			else if(tt=='D') temp = 13;
			else if(tt=='E') temp = 14;
			else if(tt=='F') temp= 15;
		}
		else {
			temp = (int)tt-48;
		}
		return temp;
	}
	public static void sort() {
		a_change.sort(new Comparator<Integer>() {
			@Override
			public int compare(Integer arg0, Integer arg1) {
				if(arg0>arg1) {
					return -1;
				}
				if(arg0<arg1) {
					return 1;
				}
				else {
					return 0;
				}
			}
		});
	}
	public static void changeToTen() {
		for(int a=0;a<a_num.size();a++) {
			char tt = a_num.get(a).charAt(a_num.get(a).length()-1);
			int sum = changeCh(tt);
			int temp=0;
			for(int b=0;b<a_num.get(a).length()-1;b++) {
				temp = changeCh(a_num.get(a).charAt(b));
				int num = 1;
				for(int c=b;c<a_num.get(a).length()-1;c++) {
					num = num*16;
				}
				temp = temp*num;
				sum= sum+temp;
			}
			a_change.add(sum);
		}
	}
	public static boolean check(String c) {
		for(int a=0;a<a_num.size();a++) {
			if(a_num.get(a).equals(c)) {
				return false;
			}
		}
		return true;
	}
}