package 삼성알고리즘_2차준비;

import java.io.*;
public class 인구이동_16234 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};

	static int map[][];
	static int copy[][];
	static boolean visit[][];
	static int ans;
	static int country_count,country_sum;
	static int N,L,R;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		N = Integer.parseInt(in.split(" ")[0]);
		L = Integer.parseInt(in.split(" ")[1]);
		R = Integer.parseInt(in.split(" ")[2]);

		map = new int[N][N];
		copy = new int[N][N];

		for(int a=0;a<N;a++) {
			in = br.readLine().trim();
			for(int b=0;b<N;b++){
				map[a][b] = Integer.parseInt(in.split(" ")[b]);
			}
		}
		for(int count = 0;count<2001;count++) {
			visit = new boolean[N][N];
			init();
			boolean flag = false;
			country_count = 0;
			country_sum = 0;
			for(int a=0;a<N;a++) {
				for(int b=0;b<N;b++) {
					if(visit[a][b]==true) continue;
					if(check(a,b)) {
						flag = true;
						dfs(a,b);
						move();
					}
				}
			}
			if(!flag) {
				ans = count;
				break;
			}
			recover();
			//print();
		}
		System.out.println(ans);
	}
	public static void print() {
		System.out.println("=====================");
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				System.out.print(map[a][b]+" ");
			}
			System.out.println();
		}
		System.out.println("=====================");

	}
	public static void recover() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(copy[a][b]==-1) continue;
				map[a][b] = copy[a][b];
			}
		}
	}
	public static void move() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(visit[a][b]==false) continue;
				if(copy[a][b] !=-1) continue;
				copy[a][b] = country_sum/country_count;
			}
		}
		country_count = 0;
		country_sum = 0;
	}
	public static void dfs(int x, int y) {
		visit[x][y] = true;
		country_count++;
		country_sum = country_sum + map[x][y];
		for(int a=0;a<4;a++) {
			int rx = x + dx[a];
			int ry = y + dy[a];
			if(rx>= 0 && ry>=0 && rx<N && ry<N) {
				if(visit[rx][ry]==false) {
					if(Math.abs(map[rx][ry]-map[x][y])>=L && Math.abs(map[rx][ry]-map[x][y])<=R) {
						dfs(rx,ry);
					}
				}
			}
		}
	}
	public static boolean check(int x, int y) {
		for(int a=0;a<4;a++) {
			int rx = x + dx[a];
			int ry = y + dy[a];
			if(rx>= 0 && ry>=0 && rx<N && ry<N) {
				if(Math.abs(map[rx][ry]-map[x][y])>=L && Math.abs(map[rx][ry]-map[x][y])<=R) {
					return true;
				}
			}
		}
		return false;
	}
	public static void init() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				copy[a][b] = -1;
			}
		}
	}

}
