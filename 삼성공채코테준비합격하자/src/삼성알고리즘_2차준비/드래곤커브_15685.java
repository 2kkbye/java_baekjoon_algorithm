package 삼성알고리즘_2차준비;

import java.io.*;
import java.util.ArrayList;

public class 드래곤커브_15685 {
	static int dx[]= {0,-1,0,1};
	static int dy[]= {1,0,-1,0};

	static boolean visit[][] = new boolean[101][101];
	static int ans;
	static ArrayList<Integer> a_dir;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		int T = Integer.parseInt(in);
		for(int a=0;a<T;a++) {
			a_dir = new ArrayList<Integer>();
			in = br.readLine().trim();
			int y = Integer.parseInt(in.split(" ")[0]);
			int x = Integer.parseInt(in.split(" ")[1]);
			int d = Integer.parseInt(in.split(" ")[2]);
			int g = Integer.parseInt(in.split(" ")[3]);
			visit[x][y] = true;
			a_dir.add(d);
			add_dir(g);
			check_map(x,y);
		}
		ans = 0;
		result();
		System.out.println(ans);
	}
	public static void result() {
		for(int a=0;a<=99;a++) {
			for(int b=0;b<=99;b++) {
				if(visit[a][b] && visit[a][b+1] && visit[a+1][b] && visit[a+1][b+1]) {
					ans++;
				}
			}
		}
	}
	public static void add_dir(int g) {
		for(int b=0;b<g;b++) {
			int size = a_dir.size();
			for(int a=size-1;a>=0;a--) {
				int temp = a_dir.get(a);
				if(temp == 3) temp = 0;
				else temp++;
				a_dir.add(temp);
			}
		}
	}
	public static void check_map(int x, int y) {
		int rx = x,ry = y;
		for(int a=0;a<a_dir.size();a++) {
			if(rx>=0 && ry>=0 && rx<101 && ry<101) {
				rx = rx+dx[a_dir.get(a)];
				ry = ry+dy[a_dir.get(a)];
				visit[rx][ry]= true;
			}
		}
	}
}
