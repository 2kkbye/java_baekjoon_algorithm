package 삼성알고리즘_2차준비;

import java.io.*;
import java.util.Queue;
import java.util.LinkedList;
class virus{
	int x,y,a_s,d_s;
	public virus(int x, int y, int a_s, int d_s) {
		this.x = x;
		this.y = y;
		this.a_s = a_s;
		this.d_s = d_s;
	}
}
public class 줄기세포배양_5653 {
	static int map[][];
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};
	static Queue<virus> q_active;
	static Queue<virus> q_deactive;
	static Queue<virus> q_new;
	static Queue<virus> q_spread;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		int T = Integer.parseInt(in);
		for(int i=1;i<=T;i++) {
			q_active = new LinkedList<virus>();
			q_deactive = new LinkedList<virus>();
			q_new = new LinkedList<virus>();
			q_spread = new LinkedList<virus>();
			map = new int[4000][4000];
			in = br.readLine().trim();
			int N = Integer.parseInt(in.split(" ")[0]);
			int M = Integer.parseInt(in.split(" ")[1]);
			int K = Integer.parseInt(in.split(" ")[2]);
			for(int a=2000;a<2000+N;a++) {
				in = br.readLine().trim();
				int te = 0;
				for(int b=2000;b<2000+M;b++) {
					int temp = Integer.parseInt(in.split(" ")[te]);
					if(temp == 0 ) {
						te++;
						continue;
					}
					map[a][b] = temp;
					q_deactive.add(new virus(a, b, temp, temp));
					te++;
				}
			}
			//print();
			for(int a=0;a<K;a++) {
				spread();
				detoAc();
				actoDeath();
				spread2();
			}
			int ans = q_deactive.size()+q_active.size();
			System.out.println("#"+i+" "+ans);
		}
	}
	public static void print() {
		for(int a=1990;a<2040;a++) {
			for(int b=1990;b<2040;b++) {
				if(map[a][b] ==0) System.out.print(". ");
				else System.out.print(map[a][b]+" ");
			}
			System.out.println();
		}
	}
	public static void detoAc() {
		int size = q_deactive.size();
		for(int a=0;a<size;a++) {
			virus v = q_deactive.remove();
			v.a_s--;
			if(v.a_s == 0) {
				q_spread.add(new virus(v.x, v.y, v.d_s, v.d_s));
				q_active.add(new virus(v.x, v.y, v.d_s, v.d_s));
			}
			else {
				q_deactive.add(new virus(v.x, v.y, v.a_s, v.d_s));
			}
		}
	}
	public static void actoDeath() {
		int size = q_active.size();
		for(int a=0;a<size;a++) {
			virus v = q_active.remove();
			v.d_s--;
			if(v.d_s!=-1) {
				q_active.add(new virus(v.x, v.y, v.a_s, v.d_s));
			}
		}
	}
	public static void spread() {
		while(!q_spread.isEmpty()) {
			virus v = q_spread.remove();
			int px = v.x;
			int py = v.y;
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(map[rx][ry]==0) {
					q_new.add(new virus(rx, ry, v.d_s, v.d_s));
				}
			}
		}
	}
	public static void spread2() {
		LinkedList<virus> a_temp = new LinkedList<virus>();
		while(!q_new.isEmpty()) {
			virus v = q_new.remove();
			int rx = v.x;
			int ry = v.y;
			if(map[rx][ry]==0) {
				map[rx][ry] = v.a_s;
				a_temp.add(new virus(rx, ry, v.a_s, v.d_s));
			}
			else {
				if(map[rx][ry]<v.a_s) {
					map[rx][ry] = v.a_s;
					for(int a=0;a<a_temp.size();a++) {
						if(a_temp.get(a).x == rx && a_temp.get(a).y==ry) {
							a_temp.remove(a);
							break;
						}
					}
					a_temp.add(new virus(rx, ry, v.a_s, v.d_s));
				}
			}
		}
		for(int a=0;a<a_temp.size();a++) {
			virus v = a_temp.get(a);
			q_deactive.add(new virus(v.x, v.y, v.a_s, v.d_s));
		}
	}
}