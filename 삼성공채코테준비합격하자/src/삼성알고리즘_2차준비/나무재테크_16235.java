package 삼성알고리즘_2차준비;

import java.io.*;
import java.util.Queue;
import java.util.LinkedList;
class tree{
	int x,y,age;
	public tree(int x, int y, int age) {
		this.x = x;
		this.y = y;
		this.age = age;
	}
}
public class 나무재테크_16235 {
	static int dx[]= {-1,-1,-1,0,0,1,1,1};
	static int dy[]= {-1,0,1,-1,1,-1,0,1};

	static int energy[][];
	static int add_energy[][];

	static Queue<tree> q_spring_t = new LinkedList<tree>();
	static Queue<tree> q_death_t = new LinkedList<tree>();
	static Queue<tree> q_new_t = new LinkedList<tree>();
	static Queue<tree> q_temp_t = new LinkedList<tree>();
	static int N,M,K;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		N = Integer.parseInt(in.split(" ")[0]);
		M = Integer.parseInt(in.split(" ")[1]);
		K = Integer.parseInt(in.split(" ")[2]);
		energy = new int[N][N];
		add_energy = new int[N][N];
		for(int a=0;a<N;a++) {
			in = br.readLine().trim();
			for(int b=0;b<N;b++) {
				add_energy[a][b] = Integer.parseInt(in.split(" ")[b]);
				energy[a][b] = 5;
			}
		}
		for(int a=0;a<M;a++) {
			in = br.readLine().trim();
			int x = Integer.parseInt(in.split(" ")[0])-1;
			int y = Integer.parseInt(in.split(" ")[1])-1;
			int age = Integer.parseInt(in.split(" ")[2]);
			q_spring_t.add(new tree(x, y, age));
		}
		for(int a=0;a<K;a++) {
			spring();
			summer();
			fall();
			winter();
		}
		System.out.println(q_spring_t.size());
	}
	public static void spring() {
		while(!q_spring_t.isEmpty()) {
			tree t = q_spring_t.remove();
			if(energy[t.x][t.y]-t.age>=0) {
				energy[t.x][t.y] = energy[t.x][t.y]-t.age;
				t.age++;
				q_temp_t.add(new tree(t.x, t.y, t.age));
			}
			else {
				q_death_t.add(new tree(t.x, t.y, t.age));
			}
		}
	}
	public static void summer() {
		while(!q_death_t.isEmpty()) {
			tree t = q_death_t.remove();
			energy[t.x][t.y] = energy[t.x][t.y]+(t.age/2);
		}
	}
	public static void fall() {
		int size = q_temp_t.size();
		for(int a=0;a<size;a++) {
			tree t = q_temp_t.remove();
			if(t.age%5 == 0) {
				int px = t.x;
				int py = t.y;
				for(int b=0;b<8;b++) {
					int rx = px+dx[b];
					int ry = py+dy[b];
					if(rx>=0 && ry>=0 && rx<N && ry<N) {
						q_new_t.add(new tree(rx, ry, 1));
					}
				}
			}
			q_temp_t.add(new tree(t.x, t.y, t.age));
		}
	}
	public static void winter() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				energy[a][b] = energy[a][b]+add_energy[a][b];
			}
		}
		while(!q_new_t.isEmpty()) {
			tree t = q_new_t.remove();
			q_spring_t.add(new tree(t.x, t.y, t.age));
		}
		while(!q_temp_t.isEmpty()) {
			tree t = q_temp_t.remove();
			q_spring_t.add(new tree(t.x, t.y, t.age));
		}
	}
}
