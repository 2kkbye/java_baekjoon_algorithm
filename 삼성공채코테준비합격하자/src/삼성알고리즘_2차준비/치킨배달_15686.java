package 삼성알고리즘_2차준비;

import java.io.*;
import java.util.ArrayList;
import java.util.Queue;
import java.util.LinkedList;
import java.awt.Point;
public class 치킨배달_15686 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};
	
	static boolean visit[][];
	static int map[][];
	static boolean chicken[];
	static Queue<Point> q_chicken = new LinkedList<Point>();
	
	static ArrayList<Point> a_person = new ArrayList<Point>();
	static ArrayList<Point> a_chicken = new ArrayList<Point>();
	static int N,M,ans;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		N = Integer.parseInt(in.split(" ")[0]);
		M = Integer.parseInt(in.split(" ")[1]);
		for(int a=0;a<N;a++) {
			in = br.readLine().trim();
			for(int b=0;b<N;b++) {
				int temp = Integer.parseInt(in.split(" ")[b]);
				if(temp == 0) continue;
				if(temp == 1) {
					a_person.add(new Point(a, b));
					continue;
				}
				if(temp == 2) {
					a_chicken.add(new Point(a, b));
				}
			}
		}
		ans = Integer.MAX_VALUE;
		chicken = new boolean[a_chicken.size()];
		for(int a=0;a<chicken.length;a++) {
			chicken[a] = true;
			dfs(a+1,1);
			chicken[a]= false;
		}
		System.out.println(ans);
	}
	public static void init() {
		visit = new boolean[N][N];
		map = new int[N][N];
	}
	public static void insert_queue() {
		for(int a=0;a<chicken.length;a++) {
			if(chicken[a]==false) continue;
			int rx = a_chicken.get(a).x;
			int ry = a_chicken.get(a).y;
			q_chicken.add(new Point(rx, ry));
			visit[rx][ry] = true;
		}
	}
	public static void count_result() {
		int sum = 0;
		for(int a=0;a<a_person.size();a++) {
			int rx = a_person.get(a).x;
			int ry = a_person.get(a).y;
			sum = sum+map[rx][ry];
		}
		ans = Math.min(ans, sum);
	}
	public static void bfs() {
		while(!q_chicken.isEmpty()) {
			Point p = q_chicken.remove();
			int px = p.x;
			int py = p.y;
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx>=0 && ry>=0 && rx <N && ry<N) {
					if(visit[rx][ry]==false) {
						map[rx][ry] = map[px][py]+1;
						visit[rx][ry] = true;
						q_chicken.add(new Point(rx, ry));
					}
				}
			}
		}
	}
	public static void dfs(int x, int level) {
		if(level == M) {
			init();
			insert_queue();
			bfs();
			count_result();
			return;
		}
		for(int a=x;a<chicken.length;a++) {
			chicken[a] = true;
			dfs(a+1,level+1);
			chicken[a]= false;
		}
	}
}
