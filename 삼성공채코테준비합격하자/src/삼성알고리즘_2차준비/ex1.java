package 삼성알고리즘_2차준비;

import java.util.ArrayList;
import java.util.Comparator;
class nnuum{
	int num, cnt;
	public nnuum(int num, int cnt) {
		this.num = num;
		this.cnt = cnt;
	}
}
public class ex1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<nnuum> a_list = new ArrayList<nnuum>();
		a_list.add(new nnuum(2, 1));
		a_list.add(new nnuum(3, 1));
		a_list.add(new nnuum(1, 1));
		System.out.println(a_list.get(0).num+" "+a_list.get(1).num);
		a_list.sort(new Comparator<nnuum>() {
			@Override
			public int compare(nnuum arg0, nnuum arg1) {
				int num1 = arg0.num;
				int num2 = arg1.num;
				int cnt1 = arg0.cnt;
				int cnt2 = arg1.cnt;
				if(cnt1>cnt2) {
					return 1;
				}
				else if(cnt1==cnt2) {
					if(num1>num2) {
						return 1;
					}
					else {
						return -1;
					}
				}
				else {
					return -1;
				}
			}
		});
		System.out.println(a_list.get(0).num+" "+a_list.get(1).num);
	}
}