package 삼성알고리즘_2차준비;

import java.io.*;
import java.util.ArrayList;
class shark{
	int x,y,speed,dir,power;
	public shark(int x, int y, int speed, int dir, int power) {
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.dir = dir;
		this.power = power;
	}
}
public class 낚시왕_17143 {
	static int dx[] = {0,-1,1,0,0};
	static int dy[] = {0,0,0,1,-1};
	
	static int map[][];
	static int R,C;
	static int ans;
	static int k_x,k_y;
	static ArrayList<shark> a_shark = new ArrayList<shark>();
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		R =Integer.parseInt(in.split(" ")[0])+1;
		C =Integer.parseInt(in.split(" ")[1])+1;
		int K =Integer.parseInt(in.split(" ")[2]);
		
		map = new int[R][C];
		
		//상어 추가
		for(int a=0;a<K;a++) {
			in = br.readLine().trim();
			int x = Integer.parseInt(in.split(" ")[0]);
			int y = Integer.parseInt(in.split(" ")[1]);
			int speed = Integer.parseInt(in.split(" ")[2]);
			int dir = Integer.parseInt(in.split(" ")[3]);
			int power = Integer.parseInt(in.split(" ")[4]);
			map[x][y]=1;
			a_shark.add(new shark(x, y, speed, dir, power));
		}
		ans = 0;
		for(int a=1;a<C;a++) {
			if(check_shark(a)) { //있다는 뜻
				kill_shark();
				move_shark();
				check_map();
				//print();
			}
			else {
				move_shark();
				check_map();
				//print();
			}
		}
		System.out.println(ans);
	}
	public static void print() {
		System.out.println("=========================");
		for(int a=1;a<R;a++) {
			for(int b=1;b<C;b++) {
				System.out.print(map[a][b]+" ");
			}
			System.out.println();
		}
		System.out.println("=========================");
	}
	public static void move_shark() {
		for(int a=0;a<a_shark.size();a++) {
			int rx = a_shark.get(a).x;
			int ry = a_shark.get(a).y;
			map[rx][ry]= map[rx][ry]-1;
			int rdir = a_shark.get(a).dir;
			int rspeed = a_shark.get(a).speed;
			for(int b=0;b<rspeed;b++) {
				rx = rx+dx[rdir];
				ry = ry+dy[rdir];
				if(rx>=R || ry >=C || rx<1 || ry <1) {
					if(rdir %2==0) {
						rdir = rdir-1;
					}
					else {
						rdir = rdir+1;
					}
					rx = rx+(dx[rdir]*2);
					ry = ry+(dy[rdir]*2);
				}
			}
			a_shark.get(a).x = rx;
			a_shark.get(a).y = ry;
			a_shark.get(a).dir = rdir;
			map[rx][ry]++;
		}
	}
	public static void check_map() { //중복 체크
		for(int a=1;a<R;a++) {
			for(int b=1;b<C;b++) {
				if(map[a][b]==0) continue;
				if(map[a][b]==1) continue;
				int max_speed = -1;
				int max_dir = -1;
				int max_power = -1;
				
				for(int c=0;c<a_shark.size();c++) {
					if(a_shark.get(c).x == a && a_shark.get(c).y==b) {
						shark s = a_shark.get(c);
						a_shark.remove(c);c--;
						
						if(s.power > max_power) {
							max_power = s.power;
							max_speed = s.speed;
							max_dir = s.dir;
						}
					}
				}
				a_shark.add(new shark(a, b, max_speed, max_dir,max_power));
				map[a][b]=1;
			}
		}
	}
	public static boolean check_shark(int index) {
		for(int a=1;a<R;a++) {
			if(map[a][index]==1) {
				k_x = a;k_y = index;
				return true;
			}
		}
		return false;
	}
	public static void kill_shark() {
		map[k_x][k_y]=0;
		for(int a=0;a<a_shark.size();a++) {
			if(a_shark.get(a).x == k_x && k_y == a_shark.get(a).y) {
				ans = ans + a_shark.get(a).power;
				a_shark.remove(a);
				return;
			}
		}
	}
}