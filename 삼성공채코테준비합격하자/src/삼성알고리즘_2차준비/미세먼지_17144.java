package 삼성알고리즘_2차준비;

import java.io.*;
import java.util.ArrayList;
import java.awt.Point;

public class 미세먼지_17144 {
	static int dx_u[]= {0,-1,0,1};
	static int dy_u[]= {1,0,-1,0};

	static int dx_d[]= {0,1,0,-1};
	static int dy_d[]= {1,0,-1,0};

	static int map[][];
	static int copy[][];

	static int R,C,T;
	static ArrayList<Point> a_air = new ArrayList<Point>();
	static int s_x,s_y;
	static boolean flag;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		R = Integer.parseInt(in.split(" ")[0]);
		C = Integer.parseInt(in.split(" ")[1]);
		T = Integer.parseInt(in.split(" ")[2]);
		map = new int[R][C];
		copy = new int[R][C];
		for(int a=0;a<R;a++) {
			in = br.readLine().trim();
			for(int b=0;b<C;b++) {
				int temp = Integer.parseInt(in.split(" ")[b]);
				if(temp ==0) continue;
				if(temp == -1) {
					a_air.add(new Point(a, b));
					map[a][b] = temp;
					continue;
				}
				map[a][b] = temp;
			}
		}
		for(int c = 0;c<T;c++) {
			for(int a=0;a<R;a++) {
				for(int b=0;b<C;b++) {
					if(map[a][b]== -1) continue;
					if(map[a][b]==0) continue;
					check(a,b);
				}
			}
			sum_map();
			//print();
			for(int d=0;d<a_air.size();d++) {
				flag = false;
				int rx = a_air.get(d).x;
				int ry = a_air.get(d).y; 
				if(d==0) {
					s_x = rx;s_y = ry;
					dfs(0,rx,ry+1,0,0);
					continue;
				}
				s_x = rx;s_y = ry;
				dfs(1,rx,ry+1,0,0);
			}
			//print();
		}
		System.out.println(result());
	}
	public static void dfs(int sw,int x, int y,int index,int num) {
		if(x<0 || y<0 || x>=R || y>=C) {
			return;
		}
		if(flag) {
			return;
		}
		if(x == s_x && y==s_y) {
			flag = true;
			return;
		}
		int temp = map[x][y];
		map[x][y] = num;
		int rx, ry;
		for(int a=index;a<4;a++) {
			if(sw==0) {
				rx = x+dx_u[a];
				ry = y+dy_u[a];
			}
			else {
				rx = x+dx_d[a];
				ry = y+dy_d[a];
			}
			if(flag) {
				break;
			}
			dfs(sw,rx,ry,a,temp);
		}
	}
	public static void check(int x, int y) {
		int cnt=0;
		int spread=0,my = 0;
		for(int a=0;a<4;a++) {
			int rx = x+dx_u[a];
			int ry = y+dy_u[a];
			if(rx >=0 && ry>=0 && rx<R && ry<C){
				if(map[rx][ry]==-1) continue;
				cnt++;
			}
		}
		spread = map[x][y]/5;
		my = map[x][y]-(spread*cnt);
		map[x][y] = my;
		for(int a=0;a<4;a++) {
			int rx = x+dx_u[a];
			int ry = y+dy_u[a];
			if(rx >=0 && ry>=0 && rx<R && ry<C){
				if(map[rx][ry]==-1) continue;
				copy[rx][ry] = copy[rx][ry]+spread;
			}
		}
	}
	public static void sum_map() {
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				map[a][b] = copy[a][b]+map[a][b];
			}
		}
		copy = new int[R][C];
	}
	public static void print() {
		System.out.println("=======================");
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				System.out.print(map[a][b]+" ");
			}
			System.out.println();
		}
		System.out.println("=======================");
	}
	public static int result() {
		int num=0;
		for(int a=0;a<R;a++) {
			for(int b=0;b<C;b++) {
				if(map[a][b]==0) continue;
				if(map[a][b] == -1) continue;
				num = num +map[a][b];
			}
		}
		return num;
	}
}