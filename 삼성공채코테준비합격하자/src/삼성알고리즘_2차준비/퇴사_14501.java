package �Ｚ�˰�����_2���غ�;

import java.io.*;
public class ���_14501 {
	static int time[];
	static int value[];
	static boolean visit[];
	static int N;
	static int ans;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		N = Integer.parseInt(in)+1;
		time = new int[N];
		value = new int[N];
		visit = new boolean[N];
		for(int a=1;a<N;a++) {
			in = br.readLine().trim();
			time[a] = Integer.parseInt(in.split(" ")[0]);
			value[a] = Integer.parseInt(in.split(" ")[1]);
		}
		for(int a=1;a<N;a++) {
			visit[a] = true;
			dfs(a+time[a]);
			visit[a] = false;
		}
	}
	public static void dfs(int level) {
		for(int a=level;a<N;a++) {
			if((a+time[a]) >= N) {
				dfs(level+1);
			}
			else {
				visit[a] = true;
				dfs(a+time[a]);
				visit[a] = false;
			}
		}
	}
}