package 삼성알고리즘_2차준비;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Queue;

import java.util.LinkedList;
import java.awt.Point;
class fish{
	int x,y,dir;
	public fish(int x, int y,int dir) {
		this.x = x;
		this.y = y;
		this.dir = dir;
	}
}
public class 아기상어_16236 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};

	static int map[][];
	static int copy[][];
	static boolean visit[][];
	static Queue<Point> q_fish;
	static ArrayList<fish> a_fish;
	static int fish_size,fish_eat;
	static int N;
	static int f_x,f_y;
	static boolean flag;
	static int ans;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		N = Integer.parseInt(in);
		map = new int[N][N];
		visit = new boolean[N][N];

		for(int a=0;a<N;a++) {
			in = br.readLine().trim();
			for(int b=0;b<N;b++) {
				int temp = Integer.parseInt(in.split(" ")[b]);
				if(temp ==9) {
					f_x = a;f_y=b;
					continue;
				}
				map[a][b] = temp;
			}
		}

		fish_size = 2;
		fish_eat = 0;
		flag = false;
		ans = 0;
		while(flag!=true) {
			a_fish = new ArrayList<fish>();
			q_fish = new LinkedList<Point>();
			copy = new int[N][N];
			visit = new boolean[N][N];
			visit[f_x][f_y] = true;
			q_fish.add(new Point(f_x, f_y));
			bfs();
			if(fish_eat==fish_size) {
				fish_size++;
				fish_eat = 0;
			}
		}
		System.out.println(ans);
	}
	public static void bfs() {
		a_fish = new ArrayList<fish>();
		while(!q_fish.isEmpty()) {
			Point p = q_fish.remove();
			int px = p.x;
			int py = p.y;
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx>=0 && ry>=0 && rx<N && ry<N) {
					if(visit[rx][ry]==false) {
						if(map[rx][ry]==fish_size || map[rx][ry]==0) {
							visit[rx][ry]=true;
							copy[rx][ry]= copy[px][py]+1;
							q_fish.add(new Point(rx, ry));
						}
						else if(map[rx][ry]<fish_size) {
							visit[rx][ry]=true;
							copy[rx][ry]= copy[px][py]+1;
							q_fish.add(new Point(rx, ry));
							a_fish.add(new fish(rx, ry,copy[rx][ry]));
						}
					}
				}
			}
		}
		if(a_fish.size()==0) {
			flag = true;
			return;
		}
		else if(a_fish.size()>=1) {
			sort();
			int rx = a_fish.get(0).x;
			int ry = a_fish.get(0).y;
			ans = ans + a_fish.get(0).dir;
			map[rx][ry]=0;
			f_x = rx;
			f_y = ry;
			fish_eat++;
		}
	}
	public static void sort() {
		a_fish.sort(new Comparator<fish>() {
			@Override
			public int compare(fish arg0, fish arg1) {
				int dir1 = arg0.dir;
				int dir2 = arg1.dir;
				if(dir1 > dir2) {
					return 1;
				}
				else if(dir1==dir2) {
					if(arg0.x>arg1.x) {
						return 1;
					}
					else if(arg0.x == arg1.x) {
						if(arg0.y > arg1.y) {
							return 1;
						}
						else {
							return -1;
						}
					}
					else {
						return -1;
					}
				}
				else {
					return -1;
				}
			}
		});
	}
}