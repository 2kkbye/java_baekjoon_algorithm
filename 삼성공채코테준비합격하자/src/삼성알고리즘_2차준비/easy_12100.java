package �Ｚ�˰�����_2���غ�;

import java.io.*;
public class easy_12100 {
	static int N;
	static int map[][];
	static int copy[][];
	static int ins[];
	static int ans;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		N = Integer.parseInt(in);
		map = new int[N][N];
		copy = new int[N][N];
		for(int a=0;a<N;a++) {
			in = br.readLine().trim();
			for(int b=0;b<N;b++) {
				map[a][b] = Integer.parseInt(in.split(" ")[b]);
			}
		}
		ins = new int[5];
		ans = -1;
		for(int a=0;a<4;a++) {
			ins[0] =a;
			dfs(1);
		}
		System.out.println(ans);
	}
	public static void dfs(int level) {
		if(level == 5) {
			init();
			for(int a=0;a<ins.length;a++) {
				move(ins[a]);
				sum(ins[a]);
				move(ins[a]);
			}
			result();
			return;
		}
		for(int a=0;a<4;a++) {
			ins[level] = a;
			dfs(level+1);
		}
	}
	public static void result() {
		int max = 0;
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				if(copy[a][b]==0) continue;
				max = Math.max(max, copy[a][b]);
			}
		}
		ans = Math.max(ans, max);
	}
	public static void sum(int index) {
		if(index == 0) { //��
			for(int a=0;a<N;a++) {
				for(int b=0;b<N-1;b++) {
					if(copy[b][a]==0) continue;
					if(copy[b][a] == copy[b+1][a]) {
						copy[b][a] = copy[b][a]*2;
						copy[b+1][a] = 0;
						continue;
					}
				}
			}
		}
		else if(index==1) { //��
			for(int a=0;a<N;a++) {
				for(int b=N-1;b>0;b--) {
					if(copy[b][a]==0) continue;
					if(copy[b][a] == copy[b-1][a]) {
						copy[b][a] = copy[b][a]*2;
						copy[b-1][a] = 0;
						continue;
					}
				}
			}
		}
		else if(index==2) { //��
			for(int a=0;a<N;a++) {
				for(int b=0;b<N-1;b++) {
					if(copy[a][b]==0) continue;
					if(copy[a][b] == copy[a][b+1]) {
						copy[a][b] = copy[a][b]*2;
						copy[a][b+1] = 0;
						continue;
					}
				}
			}
		}
		else if(index==3) { //��
			for(int a=0;a<N;a++) {
				for(int b=N-1;b>0;b--) {
					if(copy[a][b]==0) continue;
					if(copy[a][b] == copy[a][b-1]) {
						copy[a][b] = copy[a][b]*2;
						copy[a][b-1] = 0;
						continue;
					}
				}
			}
		}
	}
	public static void move(int index) {
		if(index == 0) { //��
			for(int a=0;a<N;a++) {
				int temp = 0;
				for(int b=0;b<N;b++) {
					if(copy[b][a]==0) temp++;
					if(copy[b][a]!=0 && temp!=0) {
						int num = copy[b][a];
						copy[b][a]=0;
						copy[b-temp][a] = num;
						b = b-temp;
						temp=0;continue;
					}
				}
			}
		}
		else if(index==1) { //��
			for(int a=0;a<N;a++) {
				int temp = 0;
				for(int b=N-1;b>=0;b--) {
					if(copy[b][a]==0) temp++;
					if(copy[b][a]!=0 && temp!=0) {
						int num = copy[b][a];
						copy[b][a]=0;
						copy[b+temp][a] = num;
						b = b+temp;
						temp=0;continue;
					}
				}
			}
		}
		else if(index==2) { //��
			for(int a=0;a<N;a++) {
				int temp = 0;
				for(int b=0;b<N;b++) {
					if(copy[a][b]==0) temp++;
					if(copy[a][b]!=0 && temp!=0) {
						int num = copy[a][b];
						copy[a][b]=0;
						copy[a][b-temp] = num;
						b = b-temp;
						temp=0;continue;
					}
				}
			}
		}
		else if(index==3) { //��
			for(int a=0;a<N;a++) {
				int temp = 0;
				for(int b=N-1;b>=0;b--) {
					if(copy[a][b]==0) temp++;
					if(copy[a][b]!=0 && temp!=0) {
						int num = copy[a][b];
						copy[a][b]=0;
						copy[a][b+temp] = num;
						b = b+temp;
						temp=0;continue;
					}
				}
			}
		}
	}
	public static void print() {
		System.out.println("==============");
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				System.out.print(copy[a][b]+" ");
			}
			System.out.println();
		}
		System.out.println("==============");

	}
	public static void init() {
		for(int a=0;a<N;a++) {
			for(int b=0;b<N;b++) {
				copy[a][b]= map[a][b];
			}
		}
	}
}