package 삼성알고리즘_2차준비;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;

class value{
	int num, freq;
	public value(int num, int freq) {
		this.num = num;
		this.freq = freq;
	}
}
public class 이차원배열_17140 {
	static int map[][] = new int[100][100];
	static int number[];
	static ArrayList<value> a_value = new ArrayList<value>();

	static int r,c,k;
	static int ans;
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		r = Integer.parseInt(in.split(" ")[0])-1;
		c = Integer.parseInt(in.split(" ")[1])-1;
		k = Integer.parseInt(in.split(" ")[2]);
		for(int a=0;a<3;a++) {
			in = br.readLine().trim();
			for(int b=0;b<3;b++) {
				map[a][b] = Integer.parseInt(in.split(" ")[b]);
			}
		}
		ans = -1;
		for(int a=0;a<101;a++) {
			if(result_check()) {
				ans = a;
				break;
			}
			int temp = check();
			change(temp);
		}
		System.out.println(ans);
	}
	public static void change(int sw) { //1이면 R, 0이면 C
		if(sw==1) {
			for(int a=0;a<map.length;a++) {
				number = new int[101];
				 a_value = new ArrayList<value>();
				for(int b=0;b<map.length;b++) {
					if(map[a][b]==0) continue;
					number[map[a][b]]++;
				}
				for(int b=1;b<number.length;b++) {
					if(number[b]==0) continue;
					a_value.add(new value(b, number[b]));
				}
				if(a_value.size()==0) continue;
				sort();
				int t=0;
				boolean fl = false;
				for(int b=0;b<map.length;b++) {
					if(fl == true) {map[a][b]=0;continue;}
					if(b%2==0) {
						map[a][b] = a_value.get(t).num;
					}
					else {
						map[a][b] = a_value.get(t).freq;
						t++;
					}
					if(t>=a_value.size()) fl = true;
				}
			}
		}
		else { //C
			for(int a=0;a<map.length;a++) {
				number = new int[101];
				//print();
				 a_value = new ArrayList<value>();
				for(int b=0;b<map.length;b++) {
					if(map[b][a]==0) continue;
					number[map[b][a]]++;
				}
				for(int b=1;b<number.length;b++) {
					if(number[b]==0) continue;
					a_value.add(new value(b, number[b]));
				}
				if(a_value.size()==0) continue;
				sort();
				int t=0;
				boolean fl = false;
				for(int b=0;b<map.length;b++) {
					if(fl == true) {map[b][a]=0;continue;}
					if(b%2==0) {
						map[b][a] = a_value.get(t).num;
					}
					else {
						map[b][a] = a_value.get(t).freq;
						t++;
					}
					if(t>=a_value.size()) fl = true;
				}
			}
		}
	}
	public static int check() {
		int max_row = -1; //행
		int max_col = -1; //열
		for(int a=0;a<100;a++) {
			for(int b=0;b<100;b++) {
				if(map[a][b]==0) break;
				max_row = Math.max(max_row, b);
			}
		}
		for(int a=0;a<100;a++) {
			for(int b=0;b<100;b++) {
				if(map[b][a]==0) break;
				max_col = Math.max(max_col, b);
			}
		}
		if(max_row>max_col) {
			return 0;
		}
		else {
			return 1;
		}
	}
	public static void sort() {
		a_value.sort(new Comparator<value>() {
			@Override
			public int compare(value arg0, value arg1) {
				int num1 = arg0.num;
				int num2 = arg1.num;
				int cnt1 = arg0.freq;
				int cnt2 = arg1.freq;
				if(cnt1>cnt2) {
					return 1;
				}
				else if(cnt1==cnt2) {
					if(num1>num2) {
						return 1;
					}
					else {
						return -1;
					}
				}
				else {
					return -1;
				}
			}
		});
	}
	public static boolean result_check() {
		if(map[r][c]==k) {
			return true;
		}
		return false;
	}
}