package 삼성알고리즘_2차준비;

import java.io.*;
import java.util.Queue;
import java.util.LinkedList;
import java.awt.Point;
public class 연구소3_17142 {
	static int dx[]= {-1,1,0,0};
	static int dy[]= {0,0,-1,1};

	static int map[][];
	static int copy[][];
	static boolean visit[];
	static boolean b_visit[][];

	static int ans;
	static int R;
	static int M;
	static Queue<Point> q_virus = new LinkedList<Point>();
	static LinkedList<Point> a_virus = new LinkedList<Point>();
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine().trim();
		R = Integer.parseInt(in.split(" ")[0]);
		M = Integer.parseInt(in.split(" ")[1]);
		map = new int[R][R];
		copy = new int[R][R];
		b_visit = new boolean[R][R];
		q_virus = new LinkedList<Point>();
		for(int a=0;a<R;a++) {
			in = br.readLine().trim();
			for(int b=0;b<R;b++) {
				int temp = Integer.parseInt(in.split(" ")[b]);
				if(temp==0) continue;
				if(temp == 1) { //벽
					map[a][b]=-1;
					continue;
				}
				if(temp==2) {
					map[a][b]=-2;
					a_virus.add(new Point(a, b));
				}
			}
		}
		visit = new boolean[a_virus.size()];
		ans = Integer.MAX_VALUE;

		dfs(0,0);
		if(ans == 999) {
			System.out.println(-1);
		}
		else {
			System.out.println(ans);
		}
	}
	public static void bfs() {
		while(!q_virus.isEmpty()) {
			//print();
			Point p = q_virus.remove();
			int px = p.x;
			int py = p.y;
			b_visit[px][py]=true;
			for(int a=0;a<4;a++) {
				int rx = px+dx[a];
				int ry = py+dy[a];
				if(rx>=0 && ry>=0 && rx<R && ry<R) {
					if(b_visit[rx][ry]==false && copy[rx][ry]!=-1) {
						copy[rx][ry]=copy[px][py]+1;
						b_visit[rx][ry]=true;
						q_virus.add(new Point(rx, ry));
					}
				}
			}
		}
		ans = Math.min(ans, check());
	}
	public static int check() {
		int max = 0;
		for(int a=0;a<R;a++) {
			for(int b=0;b<R;b++) {
				if(copy[a][b] ==0 && b_visit[a][b]==false) {
					return 999;
				}
				if(copy[a][b]==-1) continue;
				if(map[a][b]==-2) continue;
				
				max = Math.max(max,copy[a][b]);
			}
		}
		return max;
	}
	public static void dfs(int x, int level) {
		if(level == M) {
			copy();
			bfs();
			recover();
			return;
		}
		for(int a=x;a<a_virus.size();a++) {
			visit[a]=true;
			dfs(a+1,level+1);
			visit[a] = false;
		}
	}
	public static void recover() {
		for(int a=0;a<a_virus.size();a++) {
			if(visit[a]==false) continue;
			Point p = a_virus.get(a);
			map[p.x][p.y]=-2;
		}
	}
	public static void copy() {
		b_visit = new boolean[R][R];
		for(int a=0;a<a_virus.size();a++) {
			if(visit[a]==false) continue;
			Point p = a_virus.get(a);
			b_visit[p.x][p.y]= true;
			map[p.x][p.y]=0;
			q_virus.add(new Point(p.x, p.y));
		}
		for(int a=0;a<R;a++) {
			for(int b=0;b<R;b++) {
				copy[a][b] = map[a][b];
			}
		}
	}
}