package nhn_1;
import java.io.*;
import java.util.StringTokenizer;
import java.util.LinkedList;

class Node{
	int count,num;
	public Node(int num,int count) {
		// TODO Auto-generated constructor stub
		this.num = num;
		this.count = count;
	}
}
public class Main {

	static String name[] = new String[1000];
	static int cnt[] = new int[1000];
	static int index = 0;
	static LinkedList<Node> a_list = new LinkedList<Node>();
	static int ans = -1;
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		String temp;
		st = new StringTokenizer(br.readLine());
		for(int a = 0;a<N;a++) {
			temp = st.nextToken();
			if(!check(temp)) continue;
			name[index] = temp;
			cnt[index++]++;
		}
		if(solve()) {
			System.out.println("Y");
			System.out.println(ans);
			System.out.println(index);
		}
		else {
			System.out.println("N");
			System.out.println(ans);
			System.out.println(index);

		}
	}
	public static boolean solve() {
		for(int a = 0;a<index;a++) {
			check_num(cnt[a]);
		}

		if(a_list.size() == 1) {
			ans = a_list.get(0).count * a_list.get(0).num;
			return true;
		}
		else if(a_list.size()==2) {
			int temp = Math.abs(a_list.get(0).num - a_list.get(1).num);
			if(temp == 1) {
				if(a_list.get(0).num > a_list.get(1).num) {
					if(a_list.get(0).count >= a_list.get(1).count) {
						ans = (a_list.get(0).num*a_list.get(0).count)+ ((a_list.get(1).num+1)*a_list.get(1).count);
						return true;
					}
				}
				else if(a_list.get(0).num < a_list.get(1).num) {
					if(a_list.get(0).count <= a_list.get(1).count) {
						ans =(a_list.get(0).num+1)*a_list.get(0).count+ (a_list.get(1).num*(a_list.get(1).count));
						return true;
					}
				}
			}
		}
		int sum = 0;
		for(int a = 0;a<a_list.size();a++) {
			sum += (a_list.get(a).count * a_list.get(a).num);
		}
		ans = sum;
		return false;
	}
	public static void check_num(int temp) {
		for(int a = 0;a<a_list.size();a++) {
			if(a_list.get(a).num == temp) {
				a_list.get(a).count++;
				return;
			}
		}
		a_list.add(new Node(temp, 1));
	}
	public static boolean check(String temp) {
		for(int a = 0;a<index;a++) {
			if(name[a].equals(temp)) {
				cnt[a]++;
				return false;
			}
		}
		return true;
	}

}
