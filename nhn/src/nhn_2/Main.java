package nhn_2;

import java.io.*;
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.Comparator;

class Node{
	int time, num,count;
	public Node(int num,int time, int count) {
		// TODO Auto-generated constructor stub
		this.num = num;
		this.time = time;
		this.count = count;
	}
}
public class Main {

	static ArrayList<Node> a_list = new ArrayList<Node>();
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken());
		for(int a =0 ;a<N;a++) {
			st = new StringTokenizer(br.readLine());
			String input = st.nextToken();
			if(input.equals("enqueue")) {
				int inp_num = Integer.parseInt(st.nextToken());
				int count = freq_check(inp_num);
				a_list.add(new Node(inp_num, a, count));
			}
			else {
				sort();
				Node n = a_list.remove(a_list.size()-1);
				System.out.print(n.num+" ");
				minus_freq(n.num);
			}
			
		}
	}
	public static void minus_freq(int num) {
		for(int a = 0;a<a_list.size();a++) {
			if(a_list.get(a).num == num) {
				a_list.get(a).count--;
			}
		}
	}
	public static int freq_check(int temp) {
		int freq = 1;
		for(int a = 0;a<a_list.size();a++) {
			if(a_list.get(a).num  == temp) {
				a_list.get(a).count++;
				freq = a_list.get(a).count;
			}
		}
		return freq;
	}
	public static void sort() {
		a_list.sort(new Comparator<Node>() {
			@Override
			public int compare(Node arg0, Node arg1) {
				// TODO Auto-generated method stub
				int f_freq = arg0.count;
				int f_time= arg0.time;
				int s_freq=arg1.count;
				int s_time=arg1.time;
				
				if (f_freq == s_freq) {
					if(f_time<s_time) {
						return 1;
					}
					else {
						return -1;
					}
				}
				else if(f_freq>s_freq) return 1;
				else
					return -1;
			}
		});
	}
}
