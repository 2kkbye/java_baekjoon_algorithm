package com.za.tutorial.ga.cs;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.ArrayList;
import com.za.tutorial.ga.cs.domain.Class;
public class Driver {
	public static final int POPULATION_SIZE =20; //원래 9
	public static final double MUTATION_RATE = 0.1; //원래 0.1
	public static final double CROSSOVER_RATE = 0.9; //원래 0.9
	public static final int TOURNAMENT_SELECTION_SIZE = 7; //원래3
	public static final int NUMB_OF_ELITE_SCHEDULES = 1; //원래 1
	private int scheduleNumb=0;
	private int classNumb=1;
	private Data data;
	static ArrayList<Double> fitArray= new ArrayList<Double>();
	static ArrayList<Double> AverageFit = new ArrayList<Double>();
	public static void main(String[] args) throws IOException  {
		
		Driver driver = new Driver();
		driver.data = new Data();
		long generationNumber =0;
		driver.printAvailableData(); //이 밑으로는 0 세대에 나오는 스케줄링과 적합도.
		System.out.println("> generation # "+generationNumber);
 		System.out.print(" Schedule # |                                          ");
		System.out.print("Classes [dept,class,room.instructor,meeting-time]          ");
		System.out.println("                                               | Fitness | Conflicts");
		System.out.println("-------------------------------------------------------------------------------------------------------------------------");
		System.out.println("-------------------------------------------------------------------------------------------------------------------------");
		GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm(driver.data); //유전자 알고리즘에 전체 데이터(교수, 강의실, 강좌,시간대 넣음
		Population population = new Population(Driver.POPULATION_SIZE, driver.data).sortByFitness();
		population.getSchedules().forEach(schedule -> System.out.println("        "+driver.scheduleNumb++ +
				"      | "+ schedule + " | " +
				String.format("%.5f",schedule.getFitness()) +
				" | "+schedule.getNumbOfConflicts()));
		driver.printScheduleAsTable(population.getSchedules().get(0), generationNumber);
		driver.classNumb =1;
		while (population.getSchedules().get(0).getFitness() != 1.0) { //적합도가 1.0이면 그만둔다.
			double sum=0;
			System.out.println("> Generation # "+ ++generationNumber); //여기부터는 0세대 이후 시작되는 스케줄링과 적합도.
			System.out.print(" Schedule # |                                  ");
			System.out.print("classes [class,room,instructor,meeting-time]           ");
			System.out.println("                                       | Fitness | Conflicts");
			System.out.print("-------------------------------------------------------------------------------------");
			System.out.println("---------------------------------------------------------------------------------------");
			population=geneticAlgorithm.evolve(population).sortByFitness();
			driver.scheduleNumb = 0;
			population.getSchedules().forEach(schedule -> System.out.println("       "+driver.scheduleNumb++ +
					"    | "+ schedule + " | " +
					String.format("%.5f", schedule.getFitness()) +
					" | "+schedule.getNumbOfConflicts()));


			driver.printScheduleAsTable(population.getSchedules().get(0), generationNumber);
//			if(generationNumber%30==0){
//				fitArray.add(population.getSchedules().get(0).getFitness());
//			}
//			if(population.getSchedules().get(0).getFitness()==1.0){
//				driver.writeExcel(fitArray, generationNumber);
//
//			}
			for(int a=0;a<20;a++){
				sum +=population.getSchedules().get(a).getFitness();
			}
			AverageFit.add(sum/20);
			if(population.getSchedules().get(0).getFitness()==1.0){
				driver.writeExcel2(AverageFit, generationNumber);
			}
		}
	}
	private void printScheduleAsTable(Schedule schedule, long generation) { //여기는 각 스케줄링 한 값들중 적합도가 제일 높은거를 출력하는 메서드.
		ArrayList<Class> classes = schedule.getClasses();
		System.out.print("\n                   ");
		System.out.println("      Class #    |  Course (number, max # of students)| Room (Capacity) |  Instructor (Id)  | Meeting Time(Id)");
		System.out.print("                       ");
		System.out.print("-----------------------------------------------------");
		System.out.println("-------------------------------------------------------------------");
		classes.forEach(x-> {
			//int majorIndex = data.getDepts().indexOf(x.getDept());
			int coursesIndex = data.getCourses().indexOf(x.getCourse());
			int roomsIndex = data.getRooms().indexOf(x.getRoom());
			int instructorsIndex = data.getInstructors().indexOf(x.getInstructor());
			int meetingTimeIndex = data.getMeetingTimes().indexOf(x.getMeetingTime());
			System.out.print("                        ");
			System.out.print(String.format("   %1$02d   ", classNumb) + "  |  ");
			//System.out.print(String.format("%1$4s", data.getDepts().get(majorIndex).getName()) + "  |  "); //학과 출력해주는것
			System.out.print(String.format("%1$21s", data.getCourses().get(coursesIndex).getName() + 
					" ( "+data.getCourses().get(coursesIndex).getNumber()+", "+
					x.getCourse().getTraining())+")		| ");
			System.out.print(String.format("%1$10s", data.getRooms().get(roomsIndex).getNumber() +
					" ( "+x.getRoom().getTrainingAble()) + ")     | ");
			System.out.print(String.format("%1$15s", data.getInstructors().get(instructorsIndex).getName()+
					" ("+data.getInstructors().get(instructorsIndex).getId()+")") + "  |  ");
			System.out.println(data.getMeetingTimes().get(meetingTimeIndex).getTime()+
					" ("+data.getMeetingTimes().get(meetingTimeIndex).getId()+")");
			classNumb++;

		});
		if (schedule.getFitness() == 1) {
			//			driver.printScheduleAsTable(population.getSchedules().get(0), generationNumber);
			System.out.println("> Solution Found in "+ (generation +1) +" generations");
		}
		System.out.print("-----------------------------------------------------------------------------------------");
		System.out.println("-----------------------------------------------------------------------------------------");
	}
	private void printAvailableData() { // 프로그램 시작하면 첫번째로 출력되는 값.저장되어 있는 데이터값들을 다 출력해준다.
		System.out.println("Available Departments ==>");
		data.getDepts().forEach(x->System.out.println("name : "+x.getName()+", courses : "+x.getCourses()));
		System.out.println("\nAvailable Courses ==> ");
		data.getCourses().forEach(x->System.out.println("course #: "+x.getNumber()+", name: "+x.getName()+", 강좌에 실습있는지 여부를남: "
				+ x.getTraining()+", instructors: "+x.getInstructors()));
		System.out.println("\nAvailable Rooms ==> ");
		data.getRooms().forEach(x->System.out.println("room # : "+x.getNumber()+", 강의실의 실습 여부를 나타내는냄: "+x.getTrainingAble()));
		System.out.println("\nAvailable Instructors ==> ");
		data.getInstructors().forEach(x->System.out.println("id: "+x.getId()+",name: "+x.getName()));
		System.out.println("\nAvailable Meeting Times ==> ");   // meeting time은 날짜가 정해져있다.
		data.getMeetingTimes().forEach(x->System.out.println("id: "+x.getId()+", Meeting Time: "+x.getTime()));
		System.out.print("---------------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------------");
	}
	private void writeExcel(ArrayList<Double> fitArray,long generationNumber) throws IOException {
		String fname="GENETIC.csv";

		BufferedWriter out = new BufferedWriter(new FileWriter(fname));
		String outstring;


		out.write("최고성능 적합도");

		for(int i=0;i<fitArray.size();i++){

			if (new File(fname).isFile()){
				outstring=" "+fitArray.get(i);
				out.newLine();
				out.write(outstring);
			}	
		}


		out.close();

	}
	private void writeExcel2(ArrayList<Double> fitArray,long generationNumber) throws IOException {
		String fname="GENETIC3.csv";

		BufferedWriter out = new BufferedWriter(new FileWriter(fname));
		String outstring;


		out.write("최고성능 적합도");

		for(int i=0;i<fitArray.size();i++){

			if (new File(fname).isFile()){
				outstring=" "+fitArray.get(i);
				out.newLine();
				out.write(outstring);
			}	
		}
		out.close();
	}
}
