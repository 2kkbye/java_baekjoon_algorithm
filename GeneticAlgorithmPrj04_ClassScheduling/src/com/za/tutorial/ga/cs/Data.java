package com.za.tutorial.ga.cs;

import java.util.ArrayList;
import java.util.Arrays;

import com.za.tutorial.ga.cs.domain.Course;
import com.za.tutorial.ga.cs.domain.Department;
import com.za.tutorial.ga.cs.domain.Instructor;
import com.za.tutorial.ga.cs.domain.MeetingTime;
import com.za.tutorial.ga.cs.domain.Room;

public class Data {
	private ArrayList<Room> rooms;
	private ArrayList<Instructor> instructors;
	private ArrayList<Course> courses;
	private ArrayList<Department> depts;
	private ArrayList<MeetingTime> meetingTimes;
	private int numberOfClasses = 0;
	public Data() { initialize(); }
	private Data initialize() {
		Room room1 = new Room("411",0);
		Room room2 = new Room("418",1);
		Room room3 = new Room("511",0);
		Room room4 = new Room("517",1);
		Room room5 = new Room("605",0);
		Room room6 = new Room("606",0);
		Room room7 = new Room("608",1);
		Room room8 = new Room("609",0);
		Room room9 = new Room("610",0);
		rooms = new ArrayList<Room>(Arrays.asList(room1, room2, room3, room4,room5,room6,room7,room8,room9));
		
		MeetingTime meetingTime1 = new MeetingTime(1,"월목","09:00 - 10:30",0,0,1,1,0,0,0); //이부분에 객체 번호, 점심, 금요일, 1교시인지 판단하기,월목(3시간수업), 화수금인지표시하기(2시간2시간수업), 처음 겹치는 시간, 마지막 겹치는시간
		MeetingTime meetingTime2 = new MeetingTime(2,"월목","10:30 - 12:00",0,0,0,1,0,0,0);
		MeetingTime meetingTime3 = new MeetingTime(3,"월목","12:00 - 13:30",1,0,0,1,0,0,0);
		MeetingTime meetingTime4 = new MeetingTime(4,"월목","13:30 - 15:00",0,0,0,1,0,0,0);
		MeetingTime meetingTime5 = new MeetingTime(5,"월목","15:00 - 16:30",0,0,0,1,0,0,0);
		MeetingTime meetingTime6 = new MeetingTime(6,"월목","16:30 - 18:00",0,0,0,1,0,0,0);
		
		MeetingTime meetingTime7 = new MeetingTime(7,"화","09:00 - 11:00",0,0,1,0,1,0,8);
		MeetingTime meetingTime8 = new MeetingTime(8,"화","10:00 - 12:00",0,0,0,0,1,7,9);
		MeetingTime meetingTime9 = new MeetingTime(9,"화","11:00 - 13:00",1,0,0,0,1,8,10);
		MeetingTime meetingTime10 = new MeetingTime(10,"화","12:00 - 14:00",1,0,0,0,1,9,11);
		MeetingTime meetingTime11 = new MeetingTime(11,"화","13:00 - 15:00",0,0,0,0,1,10,12);
		MeetingTime meetingTime12 = new MeetingTime(12,"화","14:00 - 16:00",0,0,0,0,1,11,13);
		MeetingTime meetingTime13 = new MeetingTime(13,"화","15:00 - 17:00",0,0,0,0,1,12,14);
		MeetingTime meetingTime14 = new MeetingTime(14,"화","16:00 - 18:00",0,0,0,0,1,13,0);
		
		MeetingTime meetingTime15 = new MeetingTime(15,"수","09:00 - 11:00",0,0,1,0,1,0,16);
		MeetingTime meetingTime16 = new MeetingTime(16,"수","10:00 - 12:00",0,0,0,0,1,15,17);
		MeetingTime meetingTime17 = new MeetingTime(17,"수","11:00 - 13:00",1,0,0,0,1,16,18);
		MeetingTime meetingTime18 = new MeetingTime(18,"수","12:00 - 14:00",1,0,0,0,1,17,19);		
		MeetingTime meetingTime19 = new MeetingTime(19,"수","13:00 - 15:00",0,0,0,0,1,18,20);
		MeetingTime meetingTime20 = new MeetingTime(20,"수","14:00 - 16:00",0,0,0,0,1,19,21);
		MeetingTime meetingTime21 = new MeetingTime(21,"수","15:00 - 17:00",0,0,0,0,1,20,22);
		MeetingTime meetingTime22 = new MeetingTime(22,"수","16:00 - 18:00",0,0,0,0,1,21,0);
		
		MeetingTime meetingTime23 = new MeetingTime(23,"금","09:00 - 11:00",0,1,1,0,1,0,24);
		MeetingTime meetingTime24 = new MeetingTime(24,"금","10:00 - 12:00",0,1,0,0,1,23,25);
		MeetingTime meetingTime25 = new MeetingTime(25,"금","11:00 - 13:00",1,1,0,0,1,24,26);
		MeetingTime meetingTime26 = new MeetingTime(26,"금","12:00 - 14:00",1,1,0,0,1,25,27);
		MeetingTime meetingTime27 = new MeetingTime(27,"금","13:00 - 15:00",0,1,0,0,1,26,28);
		MeetingTime meetingTime28 = new MeetingTime(28,"금","14:00 - 16:00",0,1,0,0,1,27,29);
		MeetingTime meetingTime29 = new MeetingTime(29,"금","15:00 - 17:00",0,1,0,0,1,28,30);
		MeetingTime meetingTime30 = new MeetingTime(30,"금","16:00 - 18:00",0,1,0,0,1,29,0);
		meetingTimes = new ArrayList<MeetingTime>(Arrays.asList(meetingTime1, meetingTime2, meetingTime3, meetingTime4,meetingTime5, meetingTime6,
				meetingTime7,meetingTime8,meetingTime9,meetingTime10,meetingTime11,meetingTime12,meetingTime13,meetingTime14,meetingTime15,meetingTime16,
				meetingTime17,meetingTime18,meetingTime19,meetingTime20,meetingTime21,meetingTime22,meetingTime23,meetingTime24,meetingTime25,
				meetingTime26,meetingTime27,meetingTime28,meetingTime29,meetingTime30));
		
		Instructor instructor1 = new Instructor("I1","권 호 열",0,1,1); //점심, 금요일, 아침시간가능한지
		Instructor instructor2 = new Instructor("I2","김 만 배",1,0,1);
		Instructor instructor3 = new Instructor("I3","김 용 석",0,0,1);
		Instructor instructor4 = new Instructor("I4","김 학 수",0,1,0);
		Instructor instructor5 = new Instructor("I5","김 화 종",0,1,1);
		Instructor instructor6 = new Instructor("I6","이 구 연",0,0,1);
		Instructor instructor7 = new Instructor("I7","이 헌 길",0,0,0);
		Instructor instructor8 = new Instructor("I8","정 인 범",0,0,1);
		Instructor instructor9 = new Instructor("I9","정 충 교",0,1,1);
		Instructor instructor10 = new Instructor("I10","최 황 규",0,1,1);
		Instructor instructor11 = new Instructor("I11","최 창 열",0,0,1);
		Instructor instructor12 = new Instructor("I12","하 진 영",0,0,1);
		Instructor instructor13 = new Instructor("I13","황 환 규",0,0,1);
		Instructor instructor14 = new Instructor("I14","석 호 식",0,1,1);
		instructors = new ArrayList<Instructor>(Arrays.asList(instructor1, instructor2, instructor3, instructor4,instructor5,instructor6,instructor7,
				instructor8,instructor9,instructor10,instructor11,instructor12,instructor13,instructor14));
		
		Course course1 = new Course("C1","이산수학", new ArrayList<Instructor>(Arrays.asList(instructor1)),0,1,0,0); //권호열, 실습 표현해 놓음, 월목(3시간수업), 화수금표현하기(4시간수업), 학년
		Course course2 = new Course("C2","이산수학", new ArrayList<Instructor>(Arrays.asList(instructor2)),0,1,0,0); //김만배		
		Course course3 = new Course("C3","디시스이론1", new ArrayList<Instructor>(Arrays.asList(instructor1)),0,0,1,2); //권호열
		Course course4 = new Course("C3","디시스실습1", new ArrayList<Instructor>(Arrays.asList(instructor1)),1,0,1,2); //권호열		
		Course course5 = new Course("C4","디시스이론2", new ArrayList<Instructor>(Arrays.asList(instructor1)),0,0,1,2); //권호열
		Course course6 = new Course("C4","디시스실습2", new ArrayList<Instructor>(Arrays.asList(instructor1)),1,0,1,2); //권호열		
		Course course7 = new Course("C5","고급소프트", new ArrayList<Instructor>(Arrays.asList(instructor1)),0,1,0,0); //권호열
		Course course8 = new Course("C6","영상통신", new ArrayList<Instructor>(Arrays.asList(instructor2)),0,1,0,4); //김만배
		Course course9 = new Course("C7","컴그응용", new ArrayList<Instructor>(Arrays.asList(instructor2)),0,1,0,0); //김만배		
		Course course10 = new Course("C8","리눅스이론", new ArrayList<Instructor>(Arrays.asList(instructor3)),0,0,1,0); //김용석
		Course course11 = new Course("C8","리눅스실습", new ArrayList<Instructor>(Arrays.asList(instructor3)),1,0,1,0); //김용석		
		Course course12 = new Course("C9","리눅스이론", new ArrayList<Instructor>(Arrays.asList(instructor7)),0,0,1,0); //이헌길
		Course course13 = new Course("C9","리눅스실습", new ArrayList<Instructor>(Arrays.asList(instructor7)),1,0,1,0); //이헌길		
		Course course14 = new Course("C10","리눅스이론", new ArrayList<Instructor>(Arrays.asList(instructor8)),0,0,1,0); //정인범
		Course course15 = new Course("C10","리눅스실습", new ArrayList<Instructor>(Arrays.asList(instructor8)),1,0,1,0); //정인범		
		Course course16 = new Course("C11","임베드이론1", new ArrayList<Instructor>(Arrays.asList(instructor8)),0,0,1,3); //정인범
		Course course17 = new Course("C11","임베드실습1", new ArrayList<Instructor>(Arrays.asList(instructor8)),1,0,1,3); //정인범
		Course course18 = new Course("C12","임베드이론2", new ArrayList<Instructor>(Arrays.asList(instructor8)),0,0,1,3); //정인범
		Course course19 = new Course("C12","임베드실습2", new ArrayList<Instructor>(Arrays.asList(instructor8)),1,0,1,3); //정인범		
		Course course20 = new Course("C13","컴시설계", new ArrayList<Instructor>(Arrays.asList(instructor8)),0,1,0,0); //정인범
		Course course21 = new Course("C14","컴시보안", new ArrayList<Instructor>(Arrays.asList(instructor7)),0,1,0,4); //이헌길
		Course course22 = new Course("C15","마이크로1", new ArrayList<Instructor>(Arrays.asList(instructor3)),0,1,0,2); //김용석
		Course course23 = new Course("C16","마이크로2", new ArrayList<Instructor>(Arrays.asList(instructor3)),0,1,0,2); //김용석
		Course course24 = new Course("C17","알고리즘", new ArrayList<Instructor>(Arrays.asList(instructor3)),0,1,0,0); //김용석	
		Course course25 = new Course("C18","자연어처리이론", new ArrayList<Instructor>(Arrays.asList(instructor4)),0,0,1,4); //김학수
		Course course26 = new Course("C18","자연어처리실습", new ArrayList<Instructor>(Arrays.asList(instructor4)),1,0,1,4); //김학수
		Course course27 = new Course("C19","자연어마닝이론", new ArrayList<Instructor>(Arrays.asList(instructor4)),0,0,1,0); //김학수
		Course course28 = new Course("C19","자연어마닝실습", new ArrayList<Instructor>(Arrays.asList(instructor4)),1,0,1,0); //김학수		
		Course course29 = new Course("C20","자연어프로", new ArrayList<Instructor>(Arrays.asList(instructor4)),0,1,0,0); //김학수
		Course course30 = new Course("C21","분산데이터이론", new ArrayList<Instructor>(Arrays.asList(instructor5)),0,0,1,0); //김화종
		Course course31 = new Course("C21","분산데이터실습", new ArrayList<Instructor>(Arrays.asList(instructor5)),1,0,1,0); //김화종		
		Course course32 = new Course("C22","컴퓨터네트워크", new ArrayList<Instructor>(Arrays.asList(instructor5)),0,1,0,3); //김화종
		Course course33 = new Course("C23","IT융합프로젝.", new ArrayList<Instructor>(Arrays.asList(instructor5)),0,1,0,0); //김화종		
		Course course34 = new Course("C24","자료구조이론1", new ArrayList<Instructor>(Arrays.asList(instructor14)),0,0,1,2); //석호식
		Course course35 = new Course("C24","자료구조실습1", new ArrayList<Instructor>(Arrays.asList(instructor14)),1,0,1,2); //석호식			
		Course course36 = new Course("C25","자료구조이론2", new ArrayList<Instructor>(Arrays.asList(instructor14)),0,0,1,2); //석호식
		Course course37 = new Course("C25","자료구조실습2", new ArrayList<Instructor>(Arrays.asList(instructor14)),1,0,1,2); //석호식		
		Course course38 = new Course("C26","자료구조", new ArrayList<Instructor>(Arrays.asList(instructor10)),0,1,0,0); //최황규	
		Course course39 = new Course("C27","자료구조이론", new ArrayList<Instructor>(Arrays.asList(instructor13)),0,0,1,2); //황환규
		Course course40 = new Course("C27","자료구조실습", new ArrayList<Instructor>(Arrays.asList(instructor13)),1,0,1,2); //황환규	
		Course course41 = new Course("C28","인공지능", new ArrayList<Instructor>(Arrays.asList(instructor14)),0,1,0,3); //석호식
		Course course42 = new Course("C29","고급기계학습", new ArrayList<Instructor>(Arrays.asList(instructor14)),0,1,0,0); //석호식
		Course course43 = new Course("C30","컴퓨터네트워크", new ArrayList<Instructor>(Arrays.asList(instructor6)),0,1,0,3); //이구연
		Course course44 = new Course("C31","네트워크보안", new ArrayList<Instructor>(Arrays.asList(instructor6)),0,1,0,4); //이구연		
		Course course45 = new Course("C32","자바프밍이론", new ArrayList<Instructor>(Arrays.asList(instructor7)),0,0,1,0); //이헌길
		Course course46 = new Course("C32","자바프밍실습", new ArrayList<Instructor>(Arrays.asList(instructor7)),1,0,1,0); //이헌길		
		Course course47 = new Course("C33","자바프밍이론1", new ArrayList<Instructor>(Arrays.asList(instructor9)),0,0,1,0); //정충교
		Course course48 = new Course("C33","자바프밍실습1", new ArrayList<Instructor>(Arrays.asList(instructor9)),1,0,1,0); //정충교		
		Course course49 = new Course("C34","자바프밍이론2", new ArrayList<Instructor>(Arrays.asList(instructor9)),0,0,1,0); //정충교
		Course course50 = new Course("C34","자바프밍실습2", new ArrayList<Instructor>(Arrays.asList(instructor9)),1,0,1,0); //정충교
		Course course51 = new Course("C35","통계학", new ArrayList<Instructor>(Arrays.asList(instructor9)),0,1,0,0); //정충교		
		Course course52 = new Course("C36","논리회로1", new ArrayList<Instructor>(Arrays.asList(instructor11)),0,1,0,0); //최창열
		Course course53 = new Course("C37","논리회로2", new ArrayList<Instructor>(Arrays.asList(instructor11)),0,1,0,0); //최창열
		Course course54 = new Course("C38","소프트웨어융합", new ArrayList<Instructor>(Arrays.asList(instructor11)),0,1,0,0); //최창열		
		Course course55 = new Course("C39","문해프이론1", new ArrayList<Instructor>(Arrays.asList(instructor12)),0,0,1,2); //하진영
		Course course56 = new Course("C39","문해프실습1", new ArrayList<Instructor>(Arrays.asList(instructor12)),1,0,1,2); //하진영		
		Course course57 = new Course("C40","문해프이론2", new ArrayList<Instructor>(Arrays.asList(instructor12)),0,0,1,2); //하진영
		Course course58 = new Course("C40","문해프실습2", new ArrayList<Instructor>(Arrays.asList(instructor12)),1,0,1,2); //하진영		
		Course course59 = new Course("C41","HCI", new ArrayList<Instructor>(Arrays.asList(instructor12)),0,1,0,3); //하진영
		
		courses = new ArrayList<Course>(Arrays.asList(course1, course2, course3, course4, course5, course6, course7,course8,course9,course10,course11,
				course12,course13,course14,course15,course16,course17,course18,course19,course20,course21,course22,course23,course24,course25,course26,
				course27,course28,course29,course30,course31,course32,course33,course34,course35,course36,course37,course38,course39,course40,course41, course42, course43, course44, course45,course46,
				course47, course48, course49, course50, course51, course52, course53, course54, course55, course56, course57, course58, course59));
		
		Department dept1 = new Department("컴정",new ArrayList<Course>(Arrays.asList(course1, course2, course3, course4, course5, course6, course7,course8,course9,course10,course11,
				course12,course13,course14,course15,course16,course17,course18,course19,course20,course21,course22,course23,course24,course25,course26,
				course27,course28,course29,course30,course31,course32,course33,course34,course35,course36,course37,course38,course39,course40,course41, course42, course43, course44, course45,course46,
				course47, course48, course49, course50, course51, course52, course53, course54, course55, course56, course57, course58, course59)));
		depts = new ArrayList<Department>(Arrays.asList(dept1));
		depts.forEach(x -> numberOfClasses += x.getCourses().size());
		
		return this;
	}
	public ArrayList<Room> getRooms() { return rooms; }
	public ArrayList<Instructor> getInstructors() { return instructors; }
	public ArrayList<Course> getCourses() { return courses; }
	public ArrayList<Department> getDepts() { return depts; }
	public ArrayList<MeetingTime> getMeetingTimes() { return meetingTimes; }
	public int getNumberOfClasses () { return this.numberOfClasses; }
}
