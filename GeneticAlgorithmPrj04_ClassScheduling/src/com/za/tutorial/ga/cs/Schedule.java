package com.za.tutorial.ga.cs;

import java.util.ArrayList;
import com.za.tutorial.ga.cs.domain.Class;
import com.za.tutorial.ga.cs.domain.Department;
public class Schedule {
	private ArrayList<Class> classes;
	private boolean isFitnessChanged = true;
	private double fitness = -1;
	private int classNumb = 0;
	private int numbOfConflicts = 0;
	private Data data;
	public Schedule(Data data) {
		this.data = data;
		classes = new ArrayList<Class>(data.getNumberOfClasses());
		//System.out.println("b");
	}
	public Schedule initialize() {
		new ArrayList<Department>(data.getDepts()).forEach(dept -> {
			dept.getCourses().forEach(course -> {
				Class newClass = new Class(classNumb++, dept, course);
				newClass.setMeetingTime(data.getMeetingTimes().get((int) (data.getMeetingTimes().size() * Math.random())));
				newClass.setRoom(data.getRooms().get((int) (data.getRooms().size() * Math.random())));
				newClass.setInstructor(course.getInstructors().get((int)(course.getInstructors().size() * Math.random())));
				classes.add(newClass);
			});
		});
		return this;
	}
	public int getNumbOfConflicts() { return numbOfConflicts; }
	public ArrayList<Class> getClasses() {
		isFitnessChanged = true;
		return classes;
	}
	public double getFitness() {
		if (isFitnessChanged == true) {
			fitness = calculateFitness();
			isFitnessChanged = false;
		}
		return fitness;
	}
	private double calculateFitness() { //적합도 계산 만약 규칙에 어긋나는것이 있으면 conflict 1씩 증가.
		numbOfConflicts = 0;
		classes.forEach(x -> { //점심, 금요일, 1교시 가능한지.
			if (x.getMeetingTime().getOneweekThree()==1 && x.getCourse().getTuesdayClass()==1) {
				numbOfConflicts++;
				
			}
			else {
				if (x.getRoom().getTrainingAble()==0 && x.getCourse().getTraining()==1) numbOfConflicts++; //실습 수업인데 실습실이 아닌데 배치될 경우
				if (x.getInstructor().getLanuchAble()==0 && x.getMeetingTime().getLaunchTime()==1) numbOfConflicts++;
				if (x.getInstructor().getFridayAble()==0 && x.getMeetingTime().getFriDay()==1) numbOfConflicts++;
				if (x.getInstructor().getNineAble()==0 && x.getMeetingTime().getFirstClass()==1) numbOfConflicts++;
			}
			if (x.getMeetingTime().getOneweekFour()==1 && x.getCourse().getMondayClass()==1) {
				numbOfConflicts++;
				
			}
			else {
				if (x.getRoom().getTrainingAble()==0 && x.getCourse().getTraining()==1) numbOfConflicts++; //실습 수업인데 실습실이 아닌데 배치될 경우
				if (x.getInstructor().getLanuchAble()==0 && x.getMeetingTime().getLaunchTime()==1) numbOfConflicts++;
				if (x.getInstructor().getFridayAble()==0 && x.getMeetingTime().getFriDay()==1) numbOfConflicts++;
				if (x.getInstructor().getNineAble()==0 && x.getMeetingTime().getFirstClass()==1) numbOfConflicts++;
			}
			
			classes.forEach(y->{
				if(y.getCourse().getNumber().equals(x.getCourse().getNumber()) && x.getCourse().getTraining()!=y.getCourse().getTraining()) //과목코드는 같은데 이론이냐 실습이냐에 따라
					if(y.getMeetingTime().getNum()==y.getMeetingTime().getFirstTime() &&y.getMeetingTime().getNum()==y.getMeetingTime().getFinishTime()) // 요일이 같으면
						numbOfConflicts++;
			});
		
			classes.stream().filter(y-> classes.indexOf(y) >= classes.indexOf(x)).forEach(y -> { //다른강좌가 같은시간대인데 같은 강의실, 같은교수이면 각각 conflict증가.
				if (((x.getMeetingTime().getNum() == y.getMeetingTime().getNum()) && (x.getId() != y.getId()))     ||       //2시간 모두 겹치는 경우
						((x.getMeetingTime().getNum()==y.getMeetingTime().getFirstTime()) && (x.getId() != y.getId())) || // 시작하는ㅅ ㅣ간이 겹치는 경우
						((x.getMeetingTime().getNum() == y.getMeetingTime().getFinishTime()) && (x.getId() != y.getId()))) { //끝나는 시간이 겹치는 경우
					
					if(x.getRoom() == y.getRoom()) numbOfConflicts++;
					if(x.getInstructor() == y.getInstructor()) numbOfConflicts++;
				}
				
			});
			
		});
		
		
		return 1/(double)(numbOfConflicts +1);
	}
	public String toString() {
		String returnValue = new String();
		for (int x=0; x< classes.size()-1; x++) returnValue += classes.get(x) + ",";
		returnValue += classes.get(classes.size()-1);
		return returnValue;
	}
}
