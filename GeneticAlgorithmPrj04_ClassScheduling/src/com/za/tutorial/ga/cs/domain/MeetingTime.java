package com.za.tutorial.ga.cs.domain;

public class MeetingTime {
	private int num;
	private String id;
	private String time;
	private int launchTime;
	private int friDay;
	private int firstClass;
	private int oneweekThree;
	private int oneweekFour;
	private int firstTime;
	private int finishTime;
	
	public MeetingTime(int num,String id, String time,int launchTime,int friDay, int firstClass,int oneweekThree,int oneweekFour,int firstTime, int finishTime) { //이부분에 점심, 금요일, 통학러들 판단을 하기, 월목, 화수금표시하기
		this.num=num;
		this.id = id;
		this.time = time;
		this.launchTime=launchTime; // 점심시간이 포함되어 있으면
		this.friDay=friDay; // 금요일은 다
		this.firstClass=firstClass; //1교시는 못하는 교수
		this.oneweekThree=oneweekThree;
		this.oneweekFour=oneweekFour;
		this.firstTime=firstTime;
		this.finishTime=finishTime;
	}
	public int getNum() {return num;}
	public String getId() { return id; }
	public String getTime() { return time; }
	public int getLaunchTime() { return launchTime; }
	public int getFriDay() { return friDay; }
	public int getFirstClass() { return firstClass; }
	public int getOneweekThree() { return oneweekThree; }
	public int getOneweekFour() { return oneweekFour; }
	public int getFirstTime() { return firstTime;}
	public int getFinishTime() {return finishTime;}

}
