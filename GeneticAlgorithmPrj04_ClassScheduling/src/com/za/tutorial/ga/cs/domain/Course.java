package com.za.tutorial.ga.cs.domain;

import java.util.ArrayList;
public class Course {
	private String number = null;
	private String name = null;
	private int training;
	private int mondayClass;
	private int tuesdayClass;
	private ArrayList<Instructor> instructors;
	private int grade;
	public Course(String number, String name, ArrayList<Instructor> instructors, int training,int mondayClass,int tuesdayClass, int grade){ 
		//그 강좌에 대한 최대 인원, 월목인지(3시간수업), 화수금인지 표시(4시간수업)
		this.number = number;
		this.name = name;
		this.instructors = instructors;
		this.training = training; //이 변수를 실습 필요한지를 나타낼 수 있는 지를 나타 낼 수 있다.
		this.mondayClass=mondayClass;
		this.tuesdayClass=tuesdayClass;
		this.grade=grade;
	}
	public String getNumber() { return number; }
	public String getName() { return name; }
	public ArrayList<Instructor> getInstructors() { return instructors; }
	public int getTraining() { return training; }
	public String toString() { return name; }
	public int getMondayClass() { return mondayClass; }
	public int getTuesdayClass() { return tuesdayClass; }
	public int getGrade() { return grade; }
}
