package com.za.tutorial.ga.cs.domain;

public class Instructor {
		private String id;
		private String name;
		private int lanuchAble;
		private int fridayAble;
		private int nineAble;
		public Instructor(String id, String name,int lanuchAble, int fridayAble, int nineAble){ // 여기다가 점심시간, 금요일, 아침시간이 가능한지 표시를 해주는거?
			this.id = id;
			this.name = name;
			this.lanuchAble=lanuchAble;
			this.fridayAble=fridayAble;
			this.nineAble=nineAble;
		}
		public String getId() { return id; }
		public String getName() { return name; }
		public int getLanuchAble() { return lanuchAble; }
		public int getFridayAble() { return fridayAble; }
		public int getNineAble() { return nineAble; }
		public String toString() { return name; }
}
