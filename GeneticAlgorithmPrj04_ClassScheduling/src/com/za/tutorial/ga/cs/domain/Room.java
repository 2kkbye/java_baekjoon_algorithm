package com.za.tutorial.ga.cs.domain;

public class Room { 
	private String number;
	private int trainingAble;
	public Room(String number, int trainingAble) {
		this.number = number;
		this.trainingAble = trainingAble;
	}
	public String getNumber() { return number; }
	public int getTrainingAble() { return trainingAble; }
}
