package MultiThread;

public class ThreadTest implements Runnable { //Runnable이라는 인터페이스를 구현.
 //객체에다가 스레드 만드는법 객체에다가 runnable를 구현하고 run메소드에다가 구현한다. 
	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName()); //스레드가 static 선언되어있으며, 현재 구동되고있는 스레드의 이름을 얻기. 반환형 string
		System.out.println("ThreadTest");//클래스이름.

		for(int i=0;i<10;i++) {
			System.out.println("i = "+i);
			try {
				Thread.sleep(500); //시간간격으로 실행을 한다. ex1000이면 1초동안 쉬면서 진행. 
			}
			catch (Exception e) {}
		}
	}

}
