package MultiThread;

public class main {

	public static void main(String[] args) { //runnable 인터페이스 구현을 통한 thread
		ThreadTest threadtest=new ThreadTest(); //객체생성
		
		Thread thread = new Thread(threadtest,"A");//A는 스레드의 이름
		thread.start(); //start가 run메소드를 실행.
		//먼저 thread를 스타트를 시켰는데 먼저 현재 스레드를 찍는데 현재 스레드가 먼저 실행ㅇ이되고. run메소드 작업을 실행한다.
		//스레드는 별개의 작업이니깐 스타트를 실행하고나니깐 메인스레드는 자기 할일 한다. 
		System.out.println(Thread.currentThread().getName()); //main 스레드의 이름. 
	}

}
