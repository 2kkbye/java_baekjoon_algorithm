package inflearn실습;

public class ManClass {
	private int age; //데이터들 private는 ManClass내부에서만 쓰려고 할 때 쓴다.
	private int height;
	private int weight;
	private String phoneNum; 
	
	public ManClass() { //생성자-> 클래스명과 똑같다. 이 클래스가 처음에 생성될때 호출된다. 다른사람이 이거에서 객체 생성하려고하면 먼저 저 ManClass로 생성해준다.
		//생략 가능하다.
	} //메소드에 파라미터가 있는거, 없는거를 각각 호출 할 수 있따. 
	public ManClass(int age, int height, int weight, String phoneNum) {
		this.age=age; //this는 객체의 age를 가리킨다. 이 클래스의 private 변수
		this.height=height;
		this.weight=weight;
		this.phoneNum=phoneNum;
	}
	public double calculateBMI() { //메소드 접근 제한자를 이용해서 바뀔 수 있다.
		double result= weight/(height*height);
		return result;
	}
	public int getAge() { //오른쪽 클릭해서 source들어가서 getter, setter을 클릭하면 만들 수 있다. 
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	
}
