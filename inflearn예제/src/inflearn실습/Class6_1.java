package inflearn실습;

import java.util.*;
public class Class6_1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();
		
		Class6_1 Gugudan = new Class6_1(); //클래스를 이용한 객체생성
		Gugudan.Gugudan(number); //메소드 호출방법
	}
	public void Gugudan(int num) {
		for(int i=2;i<=num;i++) {
			System.out.println(i+"단");
			for(int j=1;j<10;j++) {
				System.out.println(i+"*"+j+"="+i*j);
			}
		}
	}
}
