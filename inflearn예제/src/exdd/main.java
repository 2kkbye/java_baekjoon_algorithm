package exdd;
abstract class A{
    public abstract int b();
    public void d(){
        System.out.println("world");
    }
}
class B extends A{
    public int b(){return 1;}
    //public void d(){System.out.println("ww");}

}
public class main {
    public static void main(String[] args) {
        A obj = new B();
        obj.b();
    }
}