package MultiThread3;

public class main { //객체 1개에 스레드 n개
	public static void main(String[] args) {
		ThreadTest threadTest = new ThreadTest();
		
		Thread thread0 = new Thread(threadTest, "A");
		Thread thread1 = new Thread(threadTest, "B");
		
		thread0.start();
		thread1.start();
		
		System.out.println(thread0.currentThread().getName());
		
	}
}
