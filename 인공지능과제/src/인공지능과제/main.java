package 인공지능과제;

import mineSweeper.*;

import java.io.*;
import java.util.*;

public class main {
   static Oracle oracle = new Oracle(0); // 객체의 예시는 인스턴스 Oracle이라는 객체 만들고 그 인스터스는 oracle 이거는 인스턴스를 선언한 객체선언문
   public static ArrayList<Coordinate> actionPerformAL = new ArrayList<Coordinate>(); // 한 좌표를 눌렀을 때 정보가 공개되는 모든 좌표
   public static ArrayList<Coordinate> valueOver1 = new ArrayList<Coordinate>(); // actionPerformAL 중에서 value가 1이상인 것 저장
   public static ArrayList<ArrayList<Coordinate>> surroundCoordinate = new ArrayList<ArrayList<Coordinate>>(); // valueOver1의 주변  8개의 좌표 저장 (조건을만족하는 주변좌표만 저장)
   public static ArrayList<Coordinate> probabilityOfMine = new ArrayList<Coordinate>(); // 지뢰일것같은 좌표와 value저장

   public static void main(String[] args) {
      int i = 0;
      int count = 0;
      int sum_actionPerformAL = 0;
      // oracle.setMap("data/Board 5.txt");
      int arr[][] = new int[oracle.getBoardSize()][oracle.getBoardSize()];
      for (int z = 0; z < arr.length; z++) {
         for (int j = 0; j < arr[z].length; j++) {
            arr[z][j] = 999;
         }
      }
      int rand_x = (int) (Math.random() * oracle.getBoardSize());
      int rand_y = (int) (Math.random() * oracle.getBoardSize());
      ArrayList<Coordinate> temp = oracle.actionPerform(rand_x, rand_y, 0);
      actionPerformAL = temp;
      int size = temp.size();
      System.out.println("start : " + rand_x + "," + rand_y + "," + size);
      while (size < 2 && !oracle.isGameOver()) { //처음 이후 2이상의 칸이 나올때까지 계속 클릭해준다. 지도가 넓혀질때까지
         arr[rand_y][rand_x] = temp.get(0).getValue();
         rand_x = (int) (Math.random() * oracle.getBoardSize());
         rand_y = (int) (Math.random() * oracle.getBoardSize());
         // 중복
         if (arr[rand_y][rand_x] == 999) {
            temp = oracle.actionPerform(rand_x, rand_y, 0);
            size = temp.size();
            actionPerformAlAdd(temp);
         }
      }
      sum_actionPerformAL = actionPerformAL.size();
      int status;
      while (oracle.isGameOver() == false) {
         status = 0;
         restoreCoordinate(actionPerformAL);
         guessMine1(surroundCoordinate, valueOver1);
         guessMine234(surroundCoordinate, valueOver1);
         pressAllCoordinate();
         if(oracle.getRemainedMines()==0){
        	 actionPerformAL.clear();
        	 valueOver1.clear();
        	 surroundCoordinate.clear();
        	 probabilityOfMine.clear();
        	 break;
         }
         System.out.println("남은 지뢰 개수" + oracle.getRemainedMines());
         if (sum_actionPerformAL == actionPerformAL.size()) {
            for (Coordinate x : actionPerformAL) {
               arr[x.getY()][x.getX()] = x.getValue();
            }
            ArrayList<Coordinate> temp1 = new ArrayList<Coordinate>();
            for (int q = 0; q < arr.length; q++) {
               for (int p = 0; p < arr[q].length; p++) {
                  if (arr[q][p] == 999) {
                     temp1.add(new Coordinate(p, q, arr[q][p]));
                  }
               }
            }
            Coordinate y = temp1.get((int) (Math.random() * (temp1.size() - 1)));
            actionPerformAlAdd(oracle.actionPerform(y.getX(), y.getY(), 0));
            System.out.println("랜덤 좌표 찍음");
            System.out.println(y.getX() + "," + y.getY());
            //oracle.currentStatus();

         }
         sum_actionPerformAL = actionPerformAL.size();
      }
      oracle.printScore();
   }

   public static void restoreCoordinate(ArrayList<Coordinate> actionPerformAL) {

      for (int i = 0; i < actionPerformAL.size(); i++) {
         // System.out.println("처음 클릭에 열리는 모든좌표 : "+actionPerformAL.get(i));
         // System.out.println("이번 좌표의 사이즈 : "+actionPerformAL.size());
         if (actionPerformAL.get(i).getValue() >= 1 && actionPerformAL.get(i).getValue() <= 8) {
            valueOver1.add(actionPerformAL.get(i));
         }
      }
      valueOver1SE(valueOver1); // valueOver1을 중복검사해서 제거해주는 코드
      surroundCoordinate.clear();

      for (int i = 0; i < valueOver1.size(); i++) { // value가 1이상인 좌표를 주변좌표 검색하게 보내기
         // System.out.println("초기 열리는 좌표중 value가 1이상 : "+valueOver1.get(i));
         int x = valueOver1.get(i).getX();
         int y = valueOver1.get(i).getY();
         surroundCoordinate.add(AllCoordinate(x, y)); // 한개 좌표씩 건너가서 return 되서 저장된다.
      }

      // for (int i = 0; i < surroundCoordinate.size(); i++) { //surround 좌표 출력
      // System.out.println("surroundCoordinate valueOver1의 주변 : " +
      // surroundCoordinate.get(i));
      // }

      // oracle.currentStatus();

   }

   public static ArrayList AllCoordinate(int x, int y) { // 지뢰 좌표 8개 검사해주는 코드
      ArrayList<Coordinate> AllCo = new ArrayList<Coordinate>(); // 주변좌표를 들어갈 임시 좌표

      if (isValidCoordinate(x - 1, y - 1)) { //좌표값이 맵을 벗어나는지
         if ((getValueInActionPerform(x - 1, y - 1) != 0)) { 
            if (isSameCoordinate(x - 1, y - 1)) { // 1 이상인 좌표들 중에서 똑같은 값이 있는지 검사
               AllCo.add(new Coordinate(x - 1, y - 1, getValueInActionPerform(x - 1, y - 1)));
            }
         }
      }

      if (isValidCoordinate(x - 1, y)) {
         if ((getValueInActionPerform(x - 1, y) != 0)) {
            if (isSameCoordinate(x - 1, y)) {
               AllCo.add(new Coordinate(x - 1, y, getValueInActionPerform(x - 1, y)));
            }
         }
      }
      if (isValidCoordinate(x - 1, y + 1)) {

         if ((getValueInActionPerform(x - 1, y + 1) != 0)) {
            if (isSameCoordinate(x - 1, y + 1)) {
               AllCo.add(new Coordinate(x - 1, y + 1, getValueInActionPerform(x - 1, y + 1)));
            }
         }
      }
      if (isValidCoordinate(x, y - 1)) {

         if ((getValueInActionPerform(x, y - 1) != 0)) { //모르는 좌표면 진행
            if (isSameCoordinate(x, y - 1)) {
               AllCo.add(new Coordinate(x, y - 1, getValueInActionPerform(x, y - 1)));
            }
         }
      }
      if (isValidCoordinate(x, y + 1)) {

         if ((getValueInActionPerform(x, y + 1) != 0)) {
            if (isSameCoordinate(x, y + 1)) {
               AllCo.add(new Coordinate(x, y + 1, getValueInActionPerform(x, y + 1)));
            }
         }
      }
      if (isValidCoordinate(x + 1, y - 1)) {

         if ((getValueInActionPerform(x + 1, y - 1) != 0)) {
            if (isSameCoordinate(x + 1, y - 1)) {
               AllCo.add(new Coordinate(x + 1, y - 1, getValueInActionPerform(x + 1, y - 1)));
            }
         }
      }
      if (isValidCoordinate(x + 1, y)) {

         if ((getValueInActionPerform(x + 1, y) != 0)) {
            if (isSameCoordinate(x + 1, y)) {
               AllCo.add(new Coordinate(x + 1, y, getValueInActionPerform(x + 1, y)));
            }
         }
      }
      if (isValidCoordinate(x + 1, y + 1)) {

         if ((getValueInActionPerform(x + 1, y + 1) != 0)) {
            if (isSameCoordinate(x + 1, y + 1)) {
               AllCo.add(new Coordinate(x + 1, y + 1, getValueInActionPerform(x + 1, y + 1)));
            }
         }
      }

      return AllCo;

   }

   private static boolean isValidCoordinate(int x, int y) { // 보드사이즈 벗어나면 false
      if (x < 0 || x >= oracle.getBoardSize() || y < 0 || y >= oracle.getBoardSize())
         return false;
      return true;
   }

   public static int getValueInActionPerform(int x, int y) { // 모르는 좌표를 value값을 999주기
      int value = 0;
      for (int i = 0; i < actionPerformAL.size(); i++) {
         if ((x == actionPerformAL.get(i).getX()) && (y == actionPerformAL.get(i).getY())) {
            value = actionPerformAL.get(i).getValue();
            break;
         }
         if (!(x == actionPerformAL.get(i).getX() && y == actionPerformAL.get(i).getY())) {
            value = 999;
         }
      }
      return value;

   }

   public static boolean isSameCoordinate(int x, int y) { //1넘는 좌표들 중에서 겹치는 거 있는 지 확인
      for (int i = 0; i < valueOver1.size(); i++) {
         if ((x == valueOver1.get(i).getX()) && (y == valueOver1.get(i).getY())) {
            return false;
         }
      }
      return true;
   }

   public static void guessMine1(ArrayList<ArrayList<Coordinate>> surroundCoordinate2,
         ArrayList<Coordinate> valueOver1) { // 지뢰 1개에 surroundCoordinate에 1개저장되면 100%지뢰
      int count = 0;
      int x, y, value, temp;
      int valueNum;
      // System.out.println("surrondCoordinate size : " + surroundCoordinate2.size());

      for (int i = 0; i < surroundCoordinate2.size(); i++) {
         valueNum = valueOver1.get(i).getValue();
         if (valueNum == 1) {
            for (int j = 0; j < surroundCoordinate2.get(i).size(); j++) {
               if (surroundCoordinate2.get(i).size() == 1) {
                  x = surroundCoordinate2.get(i).get(j).getX();
                  y = surroundCoordinate2.get(i).get(j).getY();
                  value = surroundCoordinate2.get(i).get(j).getValue();
                  temp = value;
                  value = 99;
                  probabilityOfMine.add(new Coordinate(x, y, value));
               }
            }
         }
      }
   }
   
   public static void guessMine234(ArrayList<ArrayList<Coordinate>> surroundCoordinate2,
         ArrayList<Coordinate> valueOver1) { // 지뢰 2,3,4일 경우 확실할때 지뢰 추가 해주는 메소드
      int count = 0;
      int valueNum;
      int x, y, value, temp;
      for (int i = 0; i < valueOver1.size(); i++) { // 지뢰 2개있을 때 surroundCoordinate에 2개 저장되면 100%지뢰 확률은 낮다.
         valueNum = valueOver1.get(i).getValue();
         if (valueNum >= 2) {
            for (int j = 0; j < surroundCoordinate2.get(i).size(); j++) {
               if (valueOver1.get(i).getValue() == valueNum) {
                  if (surroundCoordinate2.get(i).size() <= valueNum) {
                     x = surroundCoordinate2.get(i).get(j).getX();
                     y = surroundCoordinate2.get(i).get(j).getY();
                     value = surroundCoordinate2.get(i).get(j).getValue();
                     temp = value;
                     value = 99;
                     probabilityOfMine.add(new Coordinate(x, y, value));
                  }

               }
            }
         }
      }
      Deduplication(probabilityOfMine);
   }

   public static void valueOver1SE(ArrayList<Coordinate> valueOver1) { //중복검사 value값이 1이상인것들 중복검사
      for (int i = 0; i < valueOver1.size() - 1; i++) {
         for (int j = i + 1; j < valueOver1.size(); j++) {
            if ((valueOver1.get(i).getX() == valueOver1.get(j).getX())
                  && (valueOver1.get(i).getY() == valueOver1.get(j).getY())) {
               i = 0;
               valueOver1.remove(j--);

            }
         }
      }
   }

   public static void Deduplication(ArrayList<Coordinate> probabilityOfMine) { // 지뢰 중복된 좌표 없애주고 지뢰 확실한 것들
                                                            // actionperform하는 메소드
      ArrayList<Coordinate> getList;
      for (int i = 0; i < probabilityOfMine.size() - 1; i++) { //중복된 지뢰 없애기
         for (int j = i + 1; j < probabilityOfMine.size(); j++) {
            if ((probabilityOfMine.get(i).getX() == probabilityOfMine.get(j).getX())
                  && (probabilityOfMine.get(i).getY() == probabilityOfMine.get(j).getY())) {
               i = 0;
               probabilityOfMine.remove(j--);

            }
         }
      }
      oracle.currentStatus();
      System.out.println();
      //지뢰 눌러주기
      for (int i = 0; i < probabilityOfMine.size(); i++) {
         //System.out.println("지뢰 : " + probabilityOfMine.get(i));
         if (!actionPerformAL.contains(probabilityOfMine.get(i))) {
            getList = oracle.actionPerform(probabilityOfMine.get(i).getX(), probabilityOfMine.get(i).getY(), 1);
            actionPerformAlAdd(getList);
         }
      }
      if(oracle.getRemainedMines()==0){
     	 actionPerformAL.clear();
     	 valueOver1.clear();
     	 surroundCoordinate.clear();
     	 probabilityOfMine.clear();
      }
      probabilityOfMine.clear(); // 지뢰 좌표를 초기화해줘야지 안그러면 또 누르게된다.
   }

   public static void actionPerformAlAdd(ArrayList<Coordinate> newList) { // actionPerform 어레이리스트에 추가하기 각각의 원소마다
      if (newList != null)
         for (int i = 0; i < newList.size(); i++) {
            actionPSearch(newList.get(i));
         }

   }

   public static void actionPSearch(Coordinate search) { // actionPerformAL을 중복검사해주는 함수
      if (!actionPerformAL.contains(search)) {
         actionPerformAL.add(search);

      }

   }

   public static void pressAllCoordinate() {
      int coNum = 0;
      double random_x = Math.random();
      double random_y = Math.random();
      int rand_x, rand_y;
      ArrayList<Coordinate> getList = new ArrayList<Coordinate>();
      ArrayList<Coordinate> autoActionPerform = new ArrayList<Coordinate>();
      ArrayList<Coordinate> randomCoordinate = new ArrayList<Coordinate>();

      for (int j = 0; j < surroundCoordinate.size(); j++) {
         for (int k = 0; k < surroundCoordinate.get(j).size(); k++) {
            if (surroundCoordinate.get(j).get(k).getValue() == 99) {
               coNum++;
            }
         }
         if (valueOver1.get(j).getValue() == coNum) {
            for (int k = 0; k < surroundCoordinate.get(j).size(); k++) {
               if (surroundCoordinate.get(j).get(k).getValue() == 999
                     && surroundCoordinate.get(j).size() > valueOver1.get(j).getValue()) {
                  System.out.println(surroundCoordinate.get(j).get(k).getX() + " "
                        + surroundCoordinate.get(j).get(k).getY() + " coNum : " + coNum);
                  getList.add(new Coordinate(surroundCoordinate.get(j).get(k).getX(),
                        surroundCoordinate.get(j).get(k).getY(), surroundCoordinate.get(j).get(k).getValue()));
               }
            }
         }
         coNum = 0;

      }

      valueOver1SE(getList);
      int size = getList.size();
      int status = 0;
      for (int i = 0; i < size; i++) {
         for (int j = 0; j < actionPerformAL.size(); j++) {
            if ((getList.get(i).getX() == actionPerformAL.get(j).getX())
                  && (getList.get(i).getY() == actionPerformAL.get(j).getY())) {
               // getList.remove(i--);
               status = 1;
               break;
            }
         }
         if (status == 1) {
            status = 0;
            break;
         }
         autoActionPerform = oracle.actionPerform(getList.get(i).getX(), getList.get(i).getY(), 0); // 여기서 이미 열린 getList 좌표를 actionPerform으로 연다.

         if (oracle.isGameOver() == false) {
            actionPerformAlAdd(autoActionPerform);
            guessMine1(surroundCoordinate, valueOver1);
            guessMine234(surroundCoordinate, valueOver1);

         }
      }
   }

}